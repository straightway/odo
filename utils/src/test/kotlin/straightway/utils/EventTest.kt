/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.utils

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.calls
import com.nhaarman.mockitokotlin2.inOrder
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.never
import com.nhaarman.mockitokotlin2.verify
import org.junit.jupiter.api.Test
import straightway.error.Panic
import straightway.testing.bdd.*
import straightway.testing.flow.assertThat
import straightway.testing.flow.is_

class EventTest {

    private interface Listener {
        fun onEvent(s: String)
    }

    private val test get() = Given {
        object {
            val sut = Event<String>()
            val listener = mock<Listener>()
        }
    }

    @Test
    fun `firing an event without listeners has no effect`() =
        test when_ { sut("Hello") } then {
            verify(listener, never()).onEvent(any())
        }

    @Test
    fun `firing an event with one listener notifies it on firing`() =
        test while_ {
            sut attach listener::onEvent
        } when_ {
            sut("Hello")
        } then {
            verify(listener).onEvent("Hello")
        }

    @Test
    fun `firing an event with the same listener twice notifies it twice on firing`() =
        test while_ {
            sut attach listener::onEvent
            sut attach listener::onEvent
        } when_ {
            sut("Hello")
        } then {
            inOrder(listener) {
                verify(listener, calls(2)).onEvent("Hello")
            }
        }

    @Test
    fun `firing an event with a detached listener has no effect`() =
        test while_ {
            sut detach (sut attach listener::onEvent)
        } when_ {
            sut("Hello")
        } then {
            verify(listener, never()).onEvent(any())
        }

    @Test
    fun `detaching an unconnected handler has no effect`() =
        test when_ {
            sut attach listener::onEvent
        } then throws.nothing

    @Test
    fun `detaching a lambda as handler via token`() =
        test while_ {
            val token = sut.attach { listener.onEvent(it) }
            sut detach token
        } when_ {
            sut("Hello")
        } then {
            verify(listener, never()).onEvent(any())
        }

    @Test
    fun `detach on not connected handler returns false`() =
        test when_ { sut detach EventHandlerToken() } then { assertThat(it is_ false) }

    @Test
    fun `detach on connected event returns true`() =
        test andGiven {
            object {
                val sut = it.sut
                val token = sut attach it.listener::onEvent
            }
        } when_ {
            sut detach token
        } then {
            assertThat(it is_ true)
        }

    @Test
    fun `attaching while event is executed is not considered in this execution`() =
        test while_ {
            sut attach { sut attach listener::onEvent }
        } when_ {
            sut("Hello")
        } then {
            verify(listener, never()).onEvent(any())
        }

    @Test
    fun `attaching while event is executed is considered in the next execution`() =
        test while_ {
            sut attach { sut attach listener::onEvent }
        } when_ {
            sut("Hello")
            sut("Hello")
        } then {
            verify(listener).onEvent("Hello")
        }

    @Test
    fun `recursive invocation throws`() {
        test while_ {
            var token = EventHandlerToken()
            token = sut attach { sut detach token; sut("Hello") }
        } when_ {
            sut("Hello")
        } then panics
    }

    @Test
    fun `another invocation after first invocation threw works`() =
        test while_ {
            val token = sut attach { throw Panic("Aaaah!") }
            try { sut("Hello") } catch (e: Panic) {}
            sut detach token
        } when_ {
            sut("Hello")
        } then throws.nothing

    @Test
    fun `handleOnce handles the event`() =
        test while_ {
            sut.handleOnce { listener.onEvent(it) }
        } when_ {
            sut("Hello")
        } then {
            verify(listener).onEvent("Hello")
        }

    @Test
    fun `handleOnce detaches itself after handling the event`() =
        test while_ {
            sut.handleOnce { listener.onEvent(it) }
        } when_ {
            sut("Hello")
            sut("Hello")
        } then {
            verify(listener).onEvent("Hello")
        }

    @Test
    fun `handleOnce returns token allowing to detach manually`() {
        lateinit var token: EventHandlerToken
        test while_ {
            token = sut.handleOnce { listener.onEvent(it) }
        } when_ {
            sut.detach(token)
            sut("Hello")
        } then {
            verify(listener, never()).onEvent("Hello")
        }
    }
}
