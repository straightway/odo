/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.utils

import com.nhaarman.mockitokotlin2.*
import org.junit.jupiter.api.Test
import straightway.error.Panic
import straightway.testing.assertPanics
import straightway.testing.flow.*

class UsingTest {

    @Test
    fun `using calls action`() {
        var isActionCalled = false
        using(null) {
            isActionCalled = true
        }

        assertThat(isActionCalled is_ true)
    }

    @Test
    fun `using returns action result`() =
        assertThat(using(null) { 83 } is_ equal to_ 83)

    @Test
    fun `using does nothing with non-AutoClosable`() {
        var isActionCalled = false
        using("hello") {
            isActionCalled = true
        }

        assertThat(isActionCalled is_ true)
    }

    @Test
    fun `using closes AutoCloseable on regular scope leave`() {
        val autoCloseable = mock<AutoCloseable>()
        using(autoCloseable) {
            verify(autoCloseable, never()).close()
        }

        verify(autoCloseable, times(1)).close()
    }

    @Test
    fun `using closes AutoCloseable when directly returning from action`() {
        val autoCloseable = mock<AutoCloseable>()
        assertThat(directReturnFromAction(autoCloseable) is_ equal to_ 83)
        verify(autoCloseable, times(1)).close()
    }

    @Test
    fun `using closes AutoCloseable on scope leave via exception`() {
        val autoCloseable = mock<AutoCloseable>()
        assertPanics {
            using(autoCloseable) {
                throw Panic("Aaaah!")
            }
        }

        verify(autoCloseable, times(1)).close()
    }

    // region Private

    private fun directReturnFromAction(autoClosable: AutoCloseable): Int {
        using(autoClosable) {
            return 83
        }
    }

    // endregion
}
