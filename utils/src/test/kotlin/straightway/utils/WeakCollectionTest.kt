/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.utils

import org.junit.jupiter.api.Test
import straightway.testing.bdd.Given
import straightway.testing.flow.*
import java.lang.ref.WeakReference

class WeakCollectionTest {

    @Test
    fun `size yields proper value`() =
        Given {
            object {
                val obj = "Hello"
                val sut = WeakCollection.of(obj)
            }
        } when_ {
            sut.size
        } then {
            assertThat(it is_ equal to_ 1)
        }

    @Test
    fun `size is reduced when weak ref is deleted`() =
        Given {
            WeakCollection.of(Any())
        } when_ {
            afterGarbageCollection { size }
        } then {
            assertThat(it is_ equal to_ 0)
        }

    @Test
    fun `contains yields proper value`() =
        Given {
            object {
                val obj = "Hello"
                val sut = WeakCollection.of(obj)
            }
        } when_ {
            sut.contains("Hello")
        } then {
            assertThat(it is_ true)
        }

    @Test
    fun `contains yields false when weak ref is deleted`() =
        Given {
            WeakCollection.of(TestItem("Hello"))
        } when_ {
            afterGarbageCollection { contains(TestItem("Hello")) }
        } then {
            assertThat(it is_ false)
        }

    @Test
    fun `containsAll yields proper value`() =
        Given {
            object {
                val objs = listOf("Hello", "World")
                val sut = WeakCollection(objs)
            }
        } when_ {
            sut.containsAll(objs)
        } then {
            assertThat(it is_ true)
        }

    @Test
    fun `containsAll yields false when weak ref is deleted`() =
        Given {
            WeakCollection.of(TestItem("Hello"))
        } when_ {
            afterGarbageCollection { containsAll(listOf(TestItem("Hello"))) }
        } then {
            assertThat(it is_ false)
        }

    @Test
    fun `isEmpty yields proper value`() =
        Given {
            object {
                val obj = "Hello"
                val sut = WeakCollection.of(obj)
            }
        } when_ {
            sut.isEmpty()
        } then {
            assertThat(it is_ equal to_ false)
        }

    @Test
    fun `isEmpty is reduced when weak ref is deleted`() =
        Given {
            WeakCollection.of(Any())
        } when_ {
            afterGarbageCollection { isEmpty() }
        } then {
            assertThat(it is_ equal to_ true)
        }

    @Test
    fun `iterator yields proper values`() =
        Given {
            object {
                val obj = "Hello"
                val sut = WeakCollection.of(obj)
            }
        } when_ {
            sut.iterator()
        } then {
            assertThat(it.next() is_ equal to_ "Hello")
            assertThat(it.hasNext() is_ false)
        }

    @Test
    fun `iterator omits values freed by GC`() =
        Given {
            WeakCollection.of(Any())
        } when_ {
            afterGarbageCollection { iterator() }
        } then {
            assertThat(it.hasNext() is_ false)
        }

    @Test
    fun `plus adds two collections`() =
        Given {
            object {
                val obj1 = "Hello"
                val obj2 = "World"
                val sut = WeakCollection.of(obj1)
            }
        } when_ {
            sut + listOf(obj2)
        } then {
            assertThat(it is_ equal to_ listOf(obj1, obj2))
        }

    @Test
    fun `plus considers garbage collected items`() =
        Given {
            object {
                val obj = TestItem("Hello")
                val sut = WeakCollection.of(TestItem("World"))
            }
        } when_ {
            afterGarbageCollection { sut + obj }
        } then {
            assertThat(it is_ equal to_ listOf(obj))
        }

    // region Private

    private data class TestItem(val item: String)

    private fun <T> afterGarbageCollection(action: () -> T): T {
        val checker = WeakReference(Any())
        do { System.gc() } while (checker.get() != null)
        return action()
    }

    // endregion
}
