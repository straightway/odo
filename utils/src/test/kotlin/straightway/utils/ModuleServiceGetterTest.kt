/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.utils

import org.junit.jupiter.api.Test
import straightway.expr.minus
import straightway.testing.bdd.Given
import straightway.testing.flow.*
import java.lang.module.ModuleFinder
import kotlin.reflect.KClass

class ModuleServiceGetterTest {

    private val test get() =
        Given {
            object {
                var loads: List<Pair<KClass<*>, ModuleLayer?>> = listOf()
                var services: List<Any?> = listOf("Service")
                var serviceToModuleLayer: Map<KClass<*>, ModuleLayer> = mapOf()
                val sut by lazy {
                    ModuleServiceGetter(serviceToModuleLayer) {
                        loads += (it to this)
                        services
                    }
                }
            }
        }

    @Test
    fun `service not found in module layer is loaded without module layer`() =
        test when_ {
            sut.getServicesUntyped(String::class)
        } then {
            assertThat(loads.single() is_ equal to (String::class to null))
        }

    @Test
    fun `service found in module layer is loaded from there`() =
        test while_ {
            serviceToModuleLayer += (String::class to ModuleLayer.boot())
        } when_ {
            sut.getServicesUntyped(String::class)
        } then {
            assertThat(
                loads.single() is_ equal
                    to (String::class to ModuleLayer.boot())
            )
        }

    @Test
    fun `services from service load function are returned`() =
        test while_ {
            services = listOf("Hello", "World")
        } when_ {
            sut.getServicesUntyped(String::class)
        } then {
            assertThat(it is_ equal to listOf("Hello", "World"))
        }

    @Test
    fun `null services are filtered out`() =
        test while_ {
            services = listOf("Hello", null, "World")
        } when_ {
            sut.getServicesUntyped(String::class)
        } then {
            assertThat(it is_ equal to listOf("Hello", "World"))
        }

    @Test
    fun `load with null module layer loads without module layer`() {
        assertThat(
            (null as ModuleLayer?).load(java.security.Provider::class)
                is_ not - empty
        )
    }

    @Test
    fun `load with module layer loads service from module layer`() {
        val moduleLayer = ModuleLayer.boot().defineModulesWithOneLoader(
            ModuleLayer.boot().configuration().resolveAndBind(
                ModuleFinder.of(),
                ModuleFinder.ofSystem(),
                listOf()
            ),
            ClassLoader.getSystemClassLoader()
        )
        assertThat(
            moduleLayer.load(java.security.Provider::class)
                is_ not - empty
        )
    }
}
