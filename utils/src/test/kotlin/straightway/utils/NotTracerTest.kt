/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.utils

import org.junit.jupiter.api.Test
import straightway.testing.bdd.Given
import straightway.testing.bdd.throws
import straightway.testing.flow.assertThat
import straightway.testing.flow.empty
import straightway.testing.flow.equal
import straightway.testing.flow.is_
import straightway.testing.flow.to_

class NotTracerTest {

    private val test get() = Given { NotTracer() }

    @Test
    fun `traces is empty`() =
        test when_ {
            traces
        } then {
            assertThat(it is_ empty)
        }

    @Test
    fun `clear has no effect`() =
        test when_ {
            clear()
        } then throws.nothing

    @Test
    fun `onTrace has no effect`() =
        test when_ {
            onTrace { }
        } then throws.nothing

    @Test
    fun `trace has no effect`() =
        test when_ {
            traceMessage(TraceLevel.Info) { "Message" }
        } then throws.nothing

    @Test
    fun `invoke invokes action`() =
        test when_ {
            this { 83 }
        } then {
            assertThat(it is_ equal to_ 83)
        }
}
