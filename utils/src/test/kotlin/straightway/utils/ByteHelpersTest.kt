/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.utils

import org.junit.jupiter.api.Test
import straightway.testing.flow.assertThat
import straightway.testing.flow.equal
import straightway.testing.flow.is_
import straightway.testing.flow.to_

class ByteHelpersTest {

    @Test
    fun `toIntUnsigned of positive value is equal`() =
        assertThat(byteOf(1).toIntUnsigned() is_ equal to_ 1)

    @Test
    fun `toIntUnsigned of negative value is equal`() =
        assertThat(byteOf(-128).toIntUnsigned() is_ equal to_ 0x80)

    @Test
    fun `toIntUnsigned of -1 is 0xff`() =
        assertThat(byteOf(-1).toIntUnsigned() is_ equal to_ 0xff)

    @Test
    fun `toIntUnsigned of -2 is 0xfe`() =
        assertThat(byteOf(-2).toIntUnsigned() is_ equal to_ 0xfe)

    @Test
    fun `toIntUnsigned of -127 is 0x81`() =
        assertThat(byteOf(-127).toIntUnsigned() is_ equal to_ 0x81)

    @Test
    fun `toHex for 0 yields 00`() =
        assertThat(byteOf(0).toHex() is_ equal to_ "00")

    @Test
    fun `toHex for 1 yields 01`() =
        assertThat(byteOf(1).toHex() is_ equal to_ "01")

    @Test
    fun `toHex for 12 yields 0c`() =
        assertThat(byteOf(12).toHex() is_ equal to_ "0c")

    @Test
    fun `toHex for 16 yields 10`() =
        assertThat(byteOf(16).toHex() is_ equal to_ "10")
    @Test
    fun `toHex for 255 yields ff`() =
        assertThat(byteOf(255).toHex() is_ equal to_ "ff")

    @Test
    fun `toHexBlocks of empty array is empty`() =
        assertThat(byteArrayOf().toHexBlocks(2) is_ equal to_ "")

    @Test
    fun `toHexBlocks of two element array`() =
        assertThat(byteArrayOf(0xff.toByte(), 0).toHexBlocks(2) is_ equal to_ "ff 00")

    @Test
    fun `toHexBlocks with multiple lines`() =
        assertThat(byteArrayOf(0xff.toByte(), 0, 1).toHexBlocks(2) is_ equal to_ "ff 00\n01")

    private fun byteOf(v: Int) = v.toByte()
}
