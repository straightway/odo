/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.utils

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import org.junit.jupiter.api.Test
import straightway.error.Panic
import straightway.testing.bdd.Given
import straightway.testing.flow.assertThat
import straightway.testing.flow.empty
import straightway.testing.flow.equal
import straightway.testing.flow.is_
import straightway.testing.flow.to_
import kotlin.reflect.full.memberFunctions

class TaggedHandlerExecutorTest {

    @Target(AnnotationTarget.FUNCTION)
    annotation class Tag

    interface IHandler {
        @Tag
        fun handlerFun(request: Int)

        @Tag
        fun handlerFun(request: Int, otherParameter: Int) {}

        @Tag
        fun handlerFun(request: Any) {}

        @Tag
        fun inheritedTagFun(request: Double)
    }

    interface IOtherHandler {
        @Tag
        fun otherHandlerFun()
    }

    open class BaseHandler {
        @Tag
        fun baseHandlerFun() {}
    }

    class OtherHandler : IOtherHandler {
        override fun otherHandlerFun() {
            throw Panic("Do not call!")
        }
    }

    class Handler : BaseHandler(), IHandler, IOtherHandler by OtherHandler() {
        var receivedRequests = listOf<Int>()

        fun funWithoutTags(i: Int) {
            throw Panic("Do not call (parameter: $i)!")
        }

        fun funWithReturnValue(i: Int): Int {
            throw Panic("Do not call (parameter: $i)!")
        }

        override fun handlerFun(request: Int) {
            receivedRequests += request
        }

        override fun inheritedTagFun(request: Double) {}

        @Tag
        fun handlerFunWithOtherType(request: Double) {
            throw Panic("Do not call (parameter: $request)!")
        }
    }

    @Test
    fun `getHandlers yields executable handler`() =
        Given {
            Handler()
        } when_ {
            val handlerFunction = getHandlers<Tag>(isClass(Int::class)).single()
            handlerFunction(83)
        } then {
            assertThat(receivedRequests is_ equal to_ listOf(83))
        }

    @Test
    fun `getHandlers works with a mock`() =
        Given {
            mock<IHandler>()
        } when_ {
            getHandlers<Tag>(isClass(Int::class)).single()(83)
        } then {
            verify(this).handlerFun(83)
        }

    @Test
    fun `isHandlerOf yields true for function with matching parameters`() =
        Given {
            getHandlerFunction("funWithoutTags")
        } when_ {
            isHandlerOf(isClass(Int::class))
        } then {
            assertThat(it is_ true)
        }

    @Test
    fun `isHandlerOf yields false for function with not matching parameters`() =
        Given {
            getHandlerFunction("funWithoutTags")
        } when_ {
            isHandlerOf(isClass(Double::class))
        } then {
            assertThat(it is_ false)
        }

    @Test
    fun `isHandlerOf yields false for function with return value`() =
        Given {
            getHandlerFunction("funWithReturnValue")
        } when_ {
            isHandlerOf(isClass(Int::class))
        } then {
            assertThat(it is_ false)
        }

    @Test
    fun `recursiveAnnotations yields empty list if noting is annotated`() =
        Given {
            getHandlerFunction("funWithoutTags")
        } when_ {
            recursiveAnnotations
        } then {
            assertThat(it is_ empty)
        }

    @Test
    fun `recursiveAnnotations returns directly annotated annotations`() =
        Given {
            getHandlerFunction("handlerFunWithOtherType")
        } when_ {
            recursiveAnnotations
        } then {
            assert(it.single() is Tag)
        }

    @Test
    fun `recursiveAnnotations returns annotations from base interfaces`() =
        Given {
            getHandlerFunction("inheritedTagFun")
        } when_ {
            recursiveAnnotations
        } then {
            assert(it.single() is Tag)
        }

    @Test
    fun `recursiveAnnotations returns annotations from mixin interfaces`() =
        Given {
            getHandlerFunction("otherHandlerFun")
        } when_ {
            recursiveAnnotations
        } then {
            assert(it.single() is Tag)
        }

    @Test
    fun `recursiveAnnotations returns annotations from base class`() =
        Given {
            getHandlerFunction("baseHandlerFun")
        } when_ {
            recursiveAnnotations
        } then {
            assert(it.single() is Tag)
        }

    private fun getHandlerFunction(name: String) =
        Handler::class.memberFunctions.single { it.name == name }
}
