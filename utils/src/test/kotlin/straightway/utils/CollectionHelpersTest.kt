/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.utils

import org.junit.jupiter.api.Test
import straightway.testing.bdd.Given
import straightway.testing.flow.assertThat
import straightway.testing.flow.does
import straightway.testing.flow.empty
import straightway.testing.flow.equal
import straightway.testing.flow.is_
import straightway.testing.flow.throw_
import straightway.testing.flow.to_

class CollectionHelpersTest {

    @Test
    fun `repeatAsPattern starts with given sequence`() =
        Given {
            listOf(1, 2, 3, 4)
        } when_ {
            repeatAsPattern()
        } then {
            assertThat(it.take(size) is_ equal to_ this)
        }

    @Test
    fun `repeatAsPattern repeats the given sequence`() =
        Given {
            listOf(1, 2, 3, 4)
        } when_ {
            repeatAsPattern()
        } then {
            assertThat(it.take(2 * size).drop(size).take(size) is_ equal to_ this)
        }

    @Test
    fun `repeatAsPattern for an empty sequence is itself empty`() =
        Given {
            listOf<Int>()
        } when_ {
            repeatAsPattern()
        } then {
            assertThat(it is_ empty)
        }

    @Test
    fun `repeatAsPattern iterator next() throws if pattern is empty`() =
        Given {
            listOf<Int>()
        } when_ {
            repeatAsPattern()
        } then {
            assertThat({ it.iterator().next() } does throw_.type<NoSuchElementException>())
        }
}
