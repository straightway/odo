/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.utils

import com.nhaarman.mockitokotlin2.mock
import org.junit.jupiter.api.Test

class TracerTest {

    @Test
    fun `companion invocation with TimeProvider and true returning action returns BufferedTracer`() =
        assert(Tracer(mock()) { true } is BufferTracer)

    @Test
    fun `companion invocation with TimeProvider and false returning action returns NotTracer`() =
        assert(Tracer(mock()) { false } is NotTracer)

    @Test
    fun `companion invocation without TimeProvider and true returning action returns BufferedTracer`() =
        assert(Tracer { true } is BufferTracer)

    @Test
    fun `companion invocation without TimeProvider and false returning action returns NotTracer`() =
        assert(Tracer { false } is NotTracer)
}
