/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.utils

import com.nhaarman.mockitokotlin2.*
import org.junit.jupiter.api.Test
import straightway.testing.bdd.Given
import straightway.testing.bdd.panics
import straightway.testing.flow.*

class ServiceGetterTest {

    private val test get() =
        Given {
            object {
                var services = listOf("Hello", "world")
                val sut = mock<ServiceGetter> {
                    on { getServicesUntyped(any()) }.thenAnswer { services }
                }
            }
        }

    @Test
    fun `getServices with class argument calls interface`() =
        test when_ {
            sut.getServices(String::class)
        } then {
            verify(sut, times(1)).getServicesUntyped(String::class)
            assertThat(it is_ equal to services)
        }

    @Test
    fun `getServices with invalid types returned from interface panics`() =
        test when_ {
            sut.getServices(Int::class)
        } then panics

    @Test
    fun `getServices with type argument calls interface`() =
        test when_ {
            sut.getServices<String>()
        } then {
            verify(sut, times(1)).getServicesUntyped(String::class)
            assertThat(it is_ equal to services)
        }

    @Test
    fun `getService with type argument calls interface`() =
        test while_ {
            services = listOf("Hi")
        } when_ {
            sut.getService<String>()
        } then {
            verify(sut, times(1)).getServicesUntyped(String::class)
            assertThat(it is_ equal to services.single())
        }

    @Test
    fun `getService with type argument panics if service is not present`() =
        test while_ {
            services = listOf()
        } when_ {
            sut.getService<String>()
        } then panics

    @Test
    fun `getService with type argument panics with more than one service`() =
        test while_ {
            services = listOf("Hello", "World")
        } when_ {
            sut.getService<String>()
        } then panics
}
