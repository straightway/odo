/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.utils

import org.junit.jupiter.api.Test
import straightway.testing.bdd.Given
import straightway.testing.flow.assertThat
import straightway.testing.flow.equal
import straightway.testing.flow.is_
import straightway.testing.flow.to_
import java.time.LocalDateTime

class TraceEntryTest {

    private companion object {
        val stackTrace = StackTraceElement("class", "method", "file", 2)
        val timestamp = LocalDateTime.of(0, 1, 1, 0, 0)!!
    }

    @Test
    fun `toString with value and known level has defined format`() =
        Given {
            TraceEntry(
                timestamp,
                0,
                stackTrace,
                0,
                TraceEvent.Enter,
                TraceLevel.Fatal,
                83
            )
        } when_ {
            toString()
        } then {
            assertThat(
                it is_ equal to_
                    "0001-01-01 00:00:00.000 [00] Fatal: $stackTrace enters: 83"
            )
        }

    @Test
    fun `toString contains positive thread ID in hex`() =
        Given {
            TraceEntry(
                timestamp,
                0x12345678,
                stackTrace,
                0,
                TraceEvent.Enter,
                TraceLevel.Fatal,
                83
            )
        } when_ {
            toString()
        } then {
            assertThat(
                it is_ equal to_
                    "0001-01-01 00:00:00.000 [12345678] Fatal: $stackTrace enters: 83"
            )
        }

    @Test
    fun `toString contains thread ID ignoring negative values`() =
        Given {
            TraceEntry(
                timestamp,
                -1,
                stackTrace,
                0,
                TraceEvent.Enter,
                TraceLevel.Fatal,
                83
            )
        } when_ {
            toString()
        } then {
            assertThat(
                it is_ equal to_
                    "0001-01-01 00:00:00.000 [ffffffffffffffff] Fatal: $stackTrace enters: 83"
            )
        }

    @Test
    fun `toString with value and unknown level has defined format`() =
        Given {
            TraceEntry(
                timestamp,
                0,
                stackTrace,
                0,
                TraceEvent.Enter,
                TraceLevel.Unknown,
                83
            )
        } when_ {
            toString()
        } then {
            assertThat(
                it is_ equal to_
                    "0001-01-01 00:00:00.000 [00] $stackTrace enters: 83"
            )
        }

    @Test
    fun `toString without value and known level has defined format`() =
        Given {
            TraceEntry(
                timestamp,
                0,
                stackTrace,
                0,
                TraceEvent.Enter,
                TraceLevel.Fatal,
                null
            )
        } when_ {
            toString()
        } then {
            assertThat(
                it is_ equal to_
                    "0001-01-01 00:00:00.000 [00] Fatal: $stackTrace enters"
            )
        }

    @Test
    fun `toString without value and unknown level has defined format`() =
        Given {
            TraceEntry(
                timestamp,
                0,
                stackTrace,
                0,
                TraceEvent.Enter,
                TraceLevel.Unknown,
                null
            )
        } when_ {
            toString()
        } then {
            assertThat(
                it is_ equal to_
                    "0001-01-01 00:00:00.000 [00] $stackTrace enters"
            )
        }

    @Test
    fun `toString with array value has defined format`() =
        Given {
            TraceEntry(
                timestamp,
                0,
                stackTrace,
                0,
                TraceEvent.Enter,
                TraceLevel.Unknown,
                arrayOf(1, null, "Hello")
            )
        } when_ {
            toString()
        } then {
            assertThat(
                it is_ equal to_
                    "0001-01-01 00:00:00.000 [00] $stackTrace enters: [1, <null>, \"Hello\"]"
            )
        }

    @Test
    fun `toString with list value has defined format`() =
        Given {
            TraceEntry(
                timestamp,
                0,
                stackTrace,
                0,
                TraceEvent.Enter,
                TraceLevel.Unknown,
                listOf(1, null, "Hello")
            )
        } when_ {
            toString()
        } then {
            assertThat(
                it is_ equal to_
                    "0001-01-01 00:00:00.000 [00] $stackTrace enters: [1, <null>, \"Hello\"]"
            )
        }

    @Test
    fun `toString with map value has defined format`() =
        Given {
            TraceEntry(
                timestamp,
                0,
                stackTrace,
                0,
                TraceEvent.Enter,
                TraceLevel.Unknown,
                mapOf(1 to null, 2 to "Hello")
            )
        } when_ {
            toString()
        } then {
            assertThat(
                it is_ equal to_
                    "0001-01-01 00:00:00.000 [00] $stackTrace enters: {1=<null>, 2=\"Hello\"}"
            )
        }

    @Test
    fun `toString with nested collection`() =
        Given {
            TraceEntry(
                timestamp,
                0,
                stackTrace,
                0,
                TraceEvent.Enter,
                TraceLevel.Unknown,
                mapOf(1 to arrayOf("Hello"))
            )
        } when_ {
            toString()
        } then {
            assertThat(
                it is_ equal to_
                    "0001-01-01 00:00:00.000 [00] $stackTrace enters: {1=[\"Hello\"]}"
            )
        }

    @Test
    fun `toString with nestingLevel 1`() =
        Given {
            TraceEntry(
                timestamp,
                0,
                stackTrace,
                1,
                TraceEvent.Enter,
                TraceLevel.Unknown,
                mapOf(1 to arrayOf("Hello"))
            )
        } when_ {
            toString()
        } then {
            assertThat(
                it is_ equal to_
                    "0001-01-01 00:00:00.000 [00]   $stackTrace enters: {1=[\"Hello\"]}"
            )
        }

    @Test
    fun `toString with multi line value is properly indented`() =
        Given {
            TraceEntry(
                timestamp,
                0,
                stackTrace,
                1,
                TraceEvent.Enter,
                TraceLevel.Unknown,
                "Hello\nWorld"
            )
        } when_ {
            toString()
        } then {
            assertThat(
                it is_ equal to_
                    "0001-01-01 00:00:00.000 [00]   $stackTrace enters: \"Hello\n" +
                    "                               World\""
            )
        }

    @Test
    fun `toString with timestamp other than min contains timestamp`() =
        Given {
            TraceEntry(
                LocalDateTime.of(2000, 1, 2, 3, 4, 5, 6000000),
                0,
                stackTrace,
                1,
                TraceEvent.Enter,
                TraceLevel.Unknown,
                "Hello\nWorld"
            )
        } when_ {
            toString()
        } then {
            assertThat(
                it is_ equal to_
                    "2000-01-02 03:04:05.006 [00]   $stackTrace enters: \"Hello\n" +
                    "                               World\""
            )
        }
}
