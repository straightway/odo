/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.utils

import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Test
import straightway.error.Panic
import straightway.testing.bdd.Given
import straightway.testing.bdd.panics
import straightway.testing.flow.*

class StateMachineTest {

    @Test
    fun `initial state is accessible`() =
        Given {
            StateMachine<String, String>("Initial") {}
        } when_ {
            initialState
        } then {
            assertThat(it is_ equal to_ "Initial")
        }

    @Test
    fun `initial state is initially applied`() =
        Given {
            StateMachine<String, String>("Initial") {}
        } when_ {
            state
        } then {
            assertThat(it is_ equal to_ "Initial")
        }

    @Test
    fun `reset goes to initial state`() =
        Given {
            StateMachine<String, String>("Initial") {
                inState("Initial") {
                    on("go") then { transitionTo("Other") }
                }
            }
        } whileBlocking {
            handleEvent("go")
        } whenBlocking {
            reset()
        } then {
            assertThat(state is_ equal to_ "Initial")
        }

    @Test
    fun `state transition with verbatim state value`() =
        Given {
            StateMachine<String, String>("Initial") {
                inState("Initial") {
                    on("GoToNextState") then {
                        transitionTo("NextState")
                    }
                }
            }
        } whenBlocking {
            handleEvent("GoToNextState")
        } then {
            assertThat(state is_ equal to_ "NextState")
        }

    @Test
    fun `state transition with state predicate`() =
        Given {
            StateMachine<String, String>("Initial") {
                inState("Initial") {
                    on { it == "GoToNextState" } then {
                        transitionTo("NextState")
                    }
                }
            }
        } whenBlocking {
            handleEvent("GoToNextState")
        } then {
            assertThat(state is_ equal to_ "NextState")
        }

    @Test
    fun `unknown event panics`() =
        Given {
            StateMachine<String, String>("Initial") {}
        } whenBlocking {
            handleEvent("Unknown Event")
        } then panics

    @Test
    fun `first matching handler is used`() =
        Given {
            StateMachine<String, String>("Initial") {
                inState("Initial") {
                    on("Event") then { transitionTo("OK") }
                    on("Event") then { transitionTo("Not OK") }
                }
            }
        } whenBlocking {
            handleEvent("Event")
        } then {
            assertThat(state is_ equal to_ "OK")
        }

    fun `other handler code is executed`() {
        var calls = 0
        Given {
            StateMachine<String, String>("Initial") {
                inState("Initial") {
                    on("Event") then { calls++ }
                }
            }
        } whenBlocking {
            handleEvent("Event")
        } then {
            assertThat(state is_ equal to_ "Initial")
            assertThat(calls is_ equal to_ 1)
        }
    }

    @Test
    fun `otherwise catches all other events`() =
        Given {
            StateMachine<String, String>("Initial") {
                inState("Initial") {
                    otherwise { transitionTo("OK") }
                }
            }
        } whenBlocking {
            handleEvent("Event")
        } then {
            assertThat(state is_ equal to_ "OK")
        }

    fun `other handler code in otherwise is executed`() {
        var calls = 0
        Given {
            StateMachine<String, String>("Initial") {
                inState("Initial") {
                    otherwise { calls++ }
                }
            }
        } whenBlocking {
            handleEvent("Event")
        } then {
            assertThat(state is_ equal to_ "Initial")
            assertThat(calls is_ equal to_ 1)
        }
    }

    @Test
    fun `complex example`() = runBlocking {
        val sut = StateMachine<String, String>("Initial") {
            inState("Initial") {
                on("GotoB") then { transitionTo("B") }
                on("GotoC") then { transitionTo("C") }
            }
            inState("B") {
                on("GoOn") then { transitionTo("Finished") }
            }
            inState("C") {
                on("Finish") then { transitionTo("Finished") }
                on("GoBack") then { transitionTo("Initial") }
            }
        }

        assertThat(sut.state is_ equal to_ "Initial")

        sut.handleEvent("GotoC")
        assertThat(sut.state is_ equal to_ "C")

        sut.handleEvent("GoBack")
        assertThat(sut.state is_ equal to_ "Initial")

        sut.handleEvent("GotoB")
        assertThat(sut.state is_ equal to_ "B")

        sut.handleEvent("GoOn")
        assertThat(sut.state is_ equal to_ "Finished")

        sut.reset()
        assertThat(sut.state is_ equal to_ "Initial")

        sut.handleEvent("GotoC")
        assertThat(sut.state is_ equal to_ "C")

        sut.handleEvent("Finish")
        assertThat(sut.state is_ equal to_ "Finished")
    }

    @Test
    fun `onEntry block for initial state is not executed initially`() =
        Given {
            StateMachine<String, String>("Initial") {
                inState("Initial") {
                    onEntry { throw Panic("Shall not be entered") }
                }
            }
        } when_ {
            state
        } then {
            assertThat(state is_ equal to_ "Initial")
        }

    @Test
    fun `onEntry block is executed when state is entered`() {
        var calls = 0
        var oldState = ""
        Given {
            StateMachine<String, String>("Initial") {
                inState("Initial") {
                    on("go") then { transitionTo("Other") }
                }
                inState("Other") {
                    onEntry { old ->
                        oldState = old
                        calls++
                        assertThat(state is_ equal to_ "Other")
                    }
                }
            }
        } whenBlocking {
            handleEvent("go")
        } then {
            assertThat(calls is_ equal to_ 1)
            assertThat(oldState is_ equal to_ "Initial")
        }
    }

    @Test
    fun `exception in onEntry block does not prevent state change`() {
        Given {
            StateMachine<String, String>("Initial") {
                inState("Initial") {
                    on("go") then { transitionTo("Other") }
                }
                inState("Other") {
                    onEntry { throw Panic("Aaaah1") }
                }
            }
        } whenBlocking {
            handleEvent("go")
        } then panics {
            assertThat(state is_ equal to_ "Other")
        }
    }

    @Test
    fun `onEntry block is not executed when the state is unchanged`() {
        Given {
            StateMachine<String, String>("Initial") {
                inState("Initial") {
                    onEntry { throw Panic("Shall not be entered") }
                    on("go") then { transitionTo("Initial") }
                }
            }
        } whenBlocking {
            handleEvent("go")
        } then {
            assertThat(state is_ equal to_ "Initial")
        }
    }

    @Test
    fun `onExit block is executed when state is entered`() {
        var calls = 0
        var newState = ""
        Given {
            StateMachine<String, String>("Initial") {
                inState("Initial") {
                    on("go") then { transitionTo("Other") }
                    onExit { new ->
                        newState = new
                        calls++
                        assertThat(state is_ equal to_ "Initial")
                    }
                }
            }
        } whenBlocking {
            handleEvent("go")
        } then {
            assertThat(calls is_ equal to_ 1)
            assertThat(newState is_ equal to_ "Other")
        }
    }

    @Test
    fun `onExit block is not executed when the state is unchanged`() {
        Given {
            StateMachine<String, String>("Initial") {
                inState("Initial") {
                    onExit { throw Panic("Shall not be entered") }
                    on("go") then { transitionTo("Initial") }
                }
            }
        } whenBlocking {
            handleEvent("go")
        } then {
            assertThat(state is_ equal to_ "Initial")
        }
    }

    @Test
    fun `exception in onExit block does not prevent state change`() {
        Given {
            StateMachine<String, String>("Initial") {
                inState("Initial") {
                    on("go") then { transitionTo("Other") }
                    onExit { throw Panic("Aaaah1") }
                }
            }
        } whenBlocking {
            handleEvent("go")
        } then panics {
            assertThat(state is_ equal to_ "Other")
        }
    }

    @Test
    fun `exception in onEntry and onExit block calls both nevertheless`() {
        Given {
            StateMachine<String, String>("Initial") {
                inState("Initial") {
                    on("go") then { transitionTo("Other") }
                    onExit { throw Panic("Panic in onExit") }
                }
                inState("Other") {
                    onEntry { throw Panic("Panic oin onEntry") }
                }
            }
        } whenBlocking {
            handleEvent("go")
        } then panics {
            assertThat(state is_ equal to_ "Other")
            val subException = (it.state as Iterable<*>).toList()
            assertThat((subException[0] as Panic).state is_ equal to_ "Panic in onExit")
            assertThat((subException[0] as Panic).state is_ equal to_ "Panic in onExit")
        }
    }

    @Test
    fun `nested handleEvent in onExit`() =
        Given {
            StateMachine<String, String>("Initial") {
                inState("Initial") {
                    on("go") then { transitionTo("Other") }
                    onExit { stateMachine.handleEvent("back") }
                }
                inState("Other") {
                    on("back") then { transitionTo("Initial") }
                }
            }
        } whenBlocking {
            handleEvent("go")
        } then {
            assertThat(state is_ equal to_ "Initial")
        }

    @Test
    fun `nested handleEvent in onEntry`() =
        Given {
            StateMachine<String, String>("Initial") {
                inState("Initial") {
                    on("go") then { transitionTo("Other") }
                }
                inState("Other") {
                    onEntry { stateMachine.handleEvent("back") }
                    on("back") then { transitionTo("Initial") }
                }
            }
        } whenBlocking {
            handleEvent("go")
        } then {
            assertThat(state is_ equal to_ "Initial")
        }

    @Test
    fun `nested handleEvent in eventHandler`() =
        Given {
            StateMachine<String, String>("Initial") {
                inState("Initial") {
                    on("go") then { stateMachine.handleEvent("go!") }
                    on("go!") then { transitionTo("Other") }
                }
                inState("Other") {
                    on("back") then { transitionTo("Initial") }
                }
            }
        } whenBlocking {
            handleEvent("go")
        } then {
            assertThat(state is_ equal to_ "Other")
        }

    @Test
    fun `StayInThisState stays in the state`() =
        Given {
            StateMachine<String, String>("Initial") {
                inState("Initial") {
                    on("go") then stayInThisState
                }
            }
        } whenBlocking {
            handleEvent("go")
        } then {
            assertThat(state is_ equal to_ "Initial")
        }

    @Test
    fun `transitionTo outside handler block`() =
        Given {
            StateMachine<String, String>("Initial") {
                inState("Initial") {
                    on("GoToNextState") then transitionTo("NextState")
                }
            }
        } whenBlocking {
            handleEvent("GoToNextState")
        } then {
            assertThat(state is_ equal to_ "NextState")
        }
}
