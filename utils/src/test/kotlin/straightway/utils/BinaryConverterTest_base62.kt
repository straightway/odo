/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.utils

import io.seruco.encoding.base62.Base62
import org.junit.jupiter.api.Test
import straightway.error.Panic
import straightway.testing.flow.*

class BinaryConverterTest_base62 {
    private companion object {
        val TEST_DATA = byteArrayOf(2, 3, 5, 7, 11)
        val TEST_DATA_BASE62 =
            Base62.createInstance()
                .encode(TEST_DATA)
                .toString(Charsets.UTF_8)
    }

    @Test
    fun `toBase62String works`() =
        assertThat(TEST_DATA.toBase62String() is_ equal to TEST_DATA_BASE62)

    @Test
    fun `fromBase62String works`() =
        assertThat(TEST_DATA_BASE62.base62ToByteArray() is_ equal to TEST_DATA)

    @Test
    fun `fromBase62String with invalid input panics`() =
        assertThat({ "/".base62ToByteArray() } does throw_.type<Panic>())
}
