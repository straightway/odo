/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.utils

import kotlinx.coroutines.sync.Mutex
import straightway.error.Panic

/**
 * A state machine with definable state and event type, which can be configured
 * with a special DSL. For examples, see the unit tests.
 * Note: The execution of a state machine is thread safe.
 */
class StateMachine<TState, TEvent>(
    val initialState: TState,
    configure: Configurator<TState, TEvent>.() -> Unit
) {

    val state get() = currentState

    suspend fun reset() = withStateLock { currentState = initialState }

    suspend fun handleEvent(event: TEvent) = withStateLock {
        val handler = stateHandlers[currentState]?.firstOrNull { it.eventChecker(event) }
            ?: throw Panic("Unexpected event $event in state $currentState")
        handler.handler()
    }

    class Configurator<TState, TEvent>(
        val stateMachine: StateMachine<TState, TEvent>
    ) {
        fun inState(state: TState, block: StateConfigurator<TState, TEvent>.() -> Unit) =
            StateConfigurator(this, state).block()

        // region Non-public

        internal val stateHandlers = mutableMapOf<TState, MutableList<StateHandler<TEvent>>>()
        internal val stateEntryCallbacks = mutableMapOf<TState, suspend (TState) -> Unit>()
        internal val stateExitCallbacks = mutableMapOf<TState, suspend (TState) -> Unit>()

        internal fun addEventHandlerForState(
            state: TState,
            handler: StateHandler<TEvent>
        ) {
            var handlersForEvent = stateHandlers[state]
            if (handlersForEvent == null) {
                handlersForEvent = mutableListOf()
                stateHandlers[state] = handlersForEvent
            }
            handlersForEvent.add(handler)
        }

        // endregion
    }

    class StateConfigurator<TState, TEvent>(
        private val configurator: Configurator<TState, TEvent>,
        val state: TState
    ) {
        fun onEntry(block: suspend (TState) -> Unit) {
            configurator.stateEntryCallbacks[state] = block
        }

        fun onExit(block: suspend (TState) -> Unit) {
            configurator.stateExitCallbacks[state] = block
        }

        fun on(event: TEvent) =
            StateEventChecker(configurator, state) { it == event }

        fun on(eventChecker: (TEvent) -> Boolean) =
            StateEventChecker(configurator, state, eventChecker)

        fun otherwise(block: suspend EventConfigurator<TState, TEvent>.() -> Unit) {
            configurator.addEventHandlerForState(
                state,
                StateHandler({ true }) {
                    EventConfigurator(configurator.stateMachine).block()
                }
            )
        }

        fun transitionTo(
            newState: TState
        ): suspend EventConfigurator<TState, TEvent>.() -> Unit = {
            transitionTo(newState)
        }

        val stayInThisState = object : StaticStateAction<TState, TEvent> {
            override val doIt: suspend EventConfigurator<TState, TEvent>.() -> Unit = {}
        }
    }

    class StateEventChecker<TState, TEvent>(
        private val configurator: Configurator<TState, TEvent>,
        private val state: TState,
        private val eventChecker: (TEvent) -> Boolean
    ) {
        infix fun then(block: suspend EventConfigurator<TState, TEvent>.() -> Unit) {
            configurator.addEventHandlerForState(
                state,
                StateHandler(eventChecker) {
                    EventConfigurator(configurator.stateMachine).block()
                }
            )
        }

        infix fun then(action: StaticStateAction<TState, TEvent>) {
            configurator.addEventHandlerForState(
                state,
                StateHandler(eventChecker) {
                    EventConfigurator(configurator.stateMachine).(action.doIt)()
                }
            )
        }
    }

    class EventConfigurator<TState, TEvent>(
        private val stateMachine: StateMachine<TState, TEvent>
    ) {
        suspend fun transitionTo(newState: TState) {
            val oldState = stateMachine.currentState
            stateMachine.currentState = newState
            if (oldState != newState) {
                val exceptions =
                    callCollectingExceptions {
                        stateMachine.stateExitCallbacks[oldState]?.invoke(newState)
                    } + callCollectingExceptions {
                        stateMachine.stateEntryCallbacks[newState]?.invoke(oldState)
                    }

                if (exceptions.any()) throw Panic(exceptions)
            }
        }
    }

    interface StaticStateAction<TState, TEvent> {
        val doIt: suspend EventConfigurator<TState, TEvent>.() -> Unit
    }

    // region Private

    internal data class StateHandler<TEvent>(
        val eventChecker: (TEvent) -> Boolean,
        val handler: suspend () -> Unit
    )

    private suspend fun <T> withStateLock(block: suspend () -> T): T {
        val isLocked = stateMutex.tryLock()
        return try {
            block()
        } finally {
            if (isLocked) stateMutex.unlock()
        }
    }

    private val stateHandlers: Map<TState, List<StateHandler<TEvent>>>
    private val stateEntryCallbacks: Map<TState, suspend (TState) -> Unit>
    private val stateExitCallbacks: Map<TState, suspend (TState) -> Unit>
    private val stateMutex = Mutex()
    private var currentState = initialState

    init {
        val configurator = Configurator(this)
        configurator.configure()
        stateHandlers = configurator.stateHandlers.toMap()
        stateEntryCallbacks = configurator.stateEntryCallbacks.toMap()
        stateExitCallbacks = configurator.stateExitCallbacks.toMap()
    }

    // endregion
}

private suspend fun callCollectingExceptions(block: suspend () -> Unit) =
    try {
        block()
        listOf()
    } catch (ex: Throwable) {
        listOf(ex)
    }
