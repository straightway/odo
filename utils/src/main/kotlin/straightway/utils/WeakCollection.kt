/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.utils

import straightway.compiler.Generated
import java.lang.ref.WeakReference

/**
 * A collection of items which are held by weak references. If items are
 * cleaned up by the garbage collector, these items are automatically removed
 * from the collection.
 */
class WeakCollection<T>(initial: Iterable<T>) : Collection<T> {

    private var members: List<WeakReference<T>>

    companion object {
        fun <T> of(vararg members: T) = WeakCollection(members.toList())
    }

    override val size: Int
        get() = cleanedUpReferences.size

    override fun contains(element: T) =
        cleanedUpReferences.contains(element)

    override fun containsAll(elements: Collection<T>) =
        cleanedUpReferences.containsAll(elements)

    override fun isEmpty() =
        cleanedUpReferences.isEmpty()

    override fun iterator() =
        cleanedUpReferences.iterator()

    operator fun plus(newMember: T) =
        WeakCollection<T>(cleanedUpReferences + newMember)

    // region Private

    private val cleanedUpReferences
        get() =
            members.filter { it.get() != null }.let {
                members = it
                decorate @Generated("Some inline cases cannot be generated") {
                    it.mapNotNull { it.get() }
                }
            }

    init {
        members = initial.map { WeakReference(it) }
    }

    // endregion
}
