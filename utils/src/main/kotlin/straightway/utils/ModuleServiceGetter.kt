/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.utils

import straightway.compiler.Generated
import java.util.*
import kotlin.reflect.KClass

/**
 * Implementation for getting service implementations from a jvm module layer.
 * Please notice that the serviceLoadFunction by default should be a call to the
 * inline load function defined below. As this is an inline function, the "requires"
 * statement has to be made in the calling module, and not here.
 */
class ModuleServiceGetter(
    private val serviceToModuleLayer: Map<KClass<*>, ModuleLayer>,
    private val serviceLoadFunction: ModuleLayer?.(KClass<*>) -> Collection<*>
) : ServiceGetter {

    override fun getServicesUntyped(clazz: KClass<*>): Collection<Any> =
        serviceLoadFunction(serviceToModuleLayer[clazz], clazz)
            .filterNotNull()
}

// This has to be inline to make sure that the service loader is called
// from the calling module, not from this one (because this one may not have
// access to the module to load).
@Suppress("NOTHING_TO_INLINE")
@Generated("Too complicated to test simple function with module layer")
inline fun <T : Any> ModuleLayer?.load(clazz: KClass<T>): Collection<T> =
    if (this === null)
        ServiceLoader.load(clazz.java).toList()
    else
        ServiceLoader.load(this, clazz.java).toList()
