/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.utils

import straightway.error.Panic
import kotlin.reflect.KClass

/**
 * Get a collection of service implementations by class.
 */
interface ServiceGetter {
    fun getServicesUntyped(clazz: KClass<*>): Collection<Any>
}
inline fun <reified T : Any> ServiceGetter.getServices(clazz: KClass<T>): Collection<T> =
    getServicesUntyped(clazz).map {
        if (it is T) it
        else throw Panic(
            "Invalid service type. " +
                "Expected: $clazz, " +
                "got: ${it::class.qualifiedName}"
        )
    }
inline fun <reified T : Any> ServiceGetter.getServices(): Collection<T> =
    getServices(T::class)
inline fun <reified T : Any> ServiceGetter.getService(): T =
    getServices<T>().singleOrNull()
        ?: throw Panic("Could not get service ${T::class}")
