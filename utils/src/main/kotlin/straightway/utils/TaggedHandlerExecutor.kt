/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.utils

import straightway.compiler.Generated
import kotlin.reflect.KClass
import kotlin.reflect.KFunction
import kotlin.reflect.full.*
import kotlin.reflect.jvm.javaMethod

/**
 * Determine if the given function is a handler function matching the given RequestTypeSelector.
 */
fun KFunction<*>.isHandlerOf(selector: RequestTypeSelector) =
    returnType.classifier == Unit::class &&
        valueParameters.singleOrNull { it.type.selector() } != null

/**
 * Get all annotations for the given function, also recursively for all base classes.
 */
val KFunction<*>.recursiveAnnotations: Set<Annotation> get() =
    annotations.toSet() + fromSupertypes.flatMap { it.recursiveAnnotations }

/**
 * Get all handler functions as callable lambdas of the target object being annotated
 * with the TTag annotation and matching the given request type selector.
 */
inline fun <reified TTag : Annotation> Any.getHandlers(noinline selector: RequestTypeSelector) =
    getHandlerFunctionsFor<TTag>(selector)
        .map {
            @Generated("jacoco does not detect execution") {
                request: Any ->
                it.call(this, request)
            }
        }

/**
 * Get all handler functions of the target object being annotated with the TTag annotation
 * and having a parameter matching the given request type selector.
 */
inline fun <reified TTag : Annotation> Any.getHandlerFunctionsFor(
    noinline selector: RequestTypeSelector
) =
    this::class.functions
        .filter { fn -> fn.recursiveAnnotations.any { it is TTag } }
        .filter { it.isHandlerOf(selector) }

private val KFunction<*>.fromSupertypes: List<KFunction<*>> get() =
    javaMethod!!.declaringClass.kotlin.supertypes.mapNotNull {
        val classifier = it.classifier
        if (classifier is KClass<*>) classifier.getFunction(this) else null
    }

private fun KClass<*>.getFunction(function: KFunction<*>) =
    declaredFunctions.singleOrNull { declaredFunction ->
        declaredFunction.name == function.name &&
            declaredFunction.returnType == function.returnType &&
            declaredFunction.parameterTypes == function.parameterTypes
    }

private val KFunction<*>.parameterTypes get() = valueParameters.map { it.type }
