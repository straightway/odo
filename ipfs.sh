#!/usr/bin/env bash

# Copyright 2016 straightway
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

trap "exit" INT TERM ERR
trap "kill 0" EXIT

if [ "$ODO_ROOT" == "" ]
then
  ODO_ROOT=~/.odo
fi

if [ ! -e build/install/go-ipfs/ipfs ]
then
  ./gradlew odo.cloud.ipfs:installIPFS
fi

IPFS_PATH=$ODO_ROOT/ipfs/repo build/install/go-ipfs/ipfs "$@"
trap "" EXIT
