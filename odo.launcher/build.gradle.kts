/*
 * Copyright 2016 straightway
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
plugins {
    id("org.openjfx.javafxplugin") version "0.0.9"
    application
}

javafx {
    version = "13"
    modules = listOf(
        "javafx.base",
        "javafx.controls",
        "javafx.graphics",
        "javafx.fxml",
        "javafx.media",
        "javafx.swing",
        "javafx.web")
}

configure<org.javamodularity.moduleplugin.extensions.ModularityExtension> {
    disableEffectiveArgumentsAdjustment()
}

application {
    mainClass.set("straightway.odo.launcher.MainKt")
    applicationDefaultJvmArgs +=
        "--add-opens=javafx.controls/javafx.scene.control.skin=tornadofx"
    applicationDefaultJvmArgs +=
        "--add-opens=javafx.graphics/javafx.scene=tornadofx"
}

dependencies {
    implementation(project(":odo.ui.desktop"))
}