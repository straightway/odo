/*
 * Copyright 2016 straightway
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
rootProject.name = "odo"
include(
    "compiler",
    "crypto",
    "error",
    "expr",
    "http",
    "numbers",
    "odo.api",
    "odo.api.desktop",
    "odo.boot",
    "odo.cloud",
    "odo.cloud.ipfs",
    "odo.content",
    "odo.launcher",
    "odo.hello",
    "odo.kernel",
    "odo.kernel_it",
    "odo.loader",
    "odo.loader_it",
    "odo.ui",
    "odo.ui.desktop",
    "odo.ui.desktop.model",
    "random",
    "testing",
    "units",
    "utils")

pluginManagement.resolutionStrategy.eachPlugin {
    if (requested.id.id.startsWith("org.jetbrains.kotlin.")) {
        useVersion("1.4.30")
    }
}
