/*
 * Copyright 2016 straightway
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
val runtimeLib by configurations.creating
val runtimeLibTest by configurations.creating

dependencies {
    implementation(project(":compiler"))
    implementation(project(":crypto"))
    implementation(project(":error"))
    implementation(project(":odo.boot"))
    implementation(project(":odo.cloud"))
    implementation(project(":odo.content"))
    implementation(project(":utils"))

    testImplementation(project(":odo.boot"))
    testImplementation(project(":odo.loader_it"))
    testImplementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.3.9")
    testImplementation("org.jetbrains.kotlinx:kotlinx-coroutines-jdk8:1.3.9")
}
/*
tasks.named<Test>("test") {
    val compileKotlin = tasks.named<org.jetbrains.kotlin.gradle.tasks.KotlinCompile>("compileKotlin")
    println("${compileKotlin.get().destinationDir}")
    modularity {
        patchModule(
            "straightway.odo.loader",
            compileKotlin.get().destinationDir.path)
    }
    if (extensions.findByType(org.javamodularity.moduleplugin.extensions.TestModuleOptions::class) != null)
        extensions.configure(org.javamodularity.moduleplugin.extensions.TestModuleOptions::class) {
            runOnClasspath = false
            println("Run ${this@named.path} as module")
        }
}
*/