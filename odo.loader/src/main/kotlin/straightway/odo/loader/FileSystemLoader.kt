/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.loader

import straightway.compiler.Generated
import straightway.error.Panic
import straightway.odo.boot.BootLoader
import java.lang.module.*
import java.net.URI
import java.nio.file.Path
import java.util.*

/**
 * Load an application from a jar file in the file system.
 */
@Generated("Not fully tested due to issue #4")
class FileSystemLoader : ApplicationLoader {

    override val supportedUriSchemes = setOf("file")

    override suspend fun load(
        appJarUri: URI,
        applicationModule: String,
        settings: Map<String, Any>
    ): BootLoader {
        if (appJarUri.scheme !in supportedUriSchemes)
            throw Panic("Cannot load $appJarUri: Invalid URI scheme")
        val moduleLayer = creadeModuleLayerFor(appJarUri, applicationModule)
        return moduleLayer.bootLoader
    }

    // region Private

    private val ModuleLayer.bootLoader: BootLoader
        get() = bootLoaders.single().let {
            object : BootLoader by it {
                override fun initialize(args: Map<String, Any>) =
                    it.initialize(args + ("__appModuleLayer" to this@bootLoader))
            }
        }

    private val ModuleLayer.bootLoaders
        get() = ServiceLoader.load(this, BootLoader::class.java)

    private fun creadeModuleLayerFor(
        appJarUri: URI,
        applicationModule: String
    ): ModuleLayer {
        val moduleConfiguration = getModuleConfiguration(appJarUri, applicationModule)
        return createModuleLayerFor(moduleConfiguration)
    }

    private fun createModuleLayerFor(moduleConfiguration: Configuration) =
        ModuleLayer.defineModulesWithManyLoaders(
            moduleConfiguration,
            listOf(ModuleLayer.boot()),
            this::class.java.classLoader
        ).layer()

    private fun getModuleConfiguration(appJarUri: URI, applicationModule: String) =
        ModuleLayer.boot().configuration().resolveAndBind(
            ModuleFinder.of(
                Path.of(appJarUri)
                    .apply {
                        if (!toFile().exists())
                            throw Panic("$this does not exist")
                    }
            ),
            ModuleFinder.of(),
            setOf(applicationModule)
        )

    // endregion
}
