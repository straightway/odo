/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.loader.test

import org.junit.jupiter.api.Test
import straightway.expr.minus
import straightway.odo.loader.FileSystemLoader
import straightway.testing.bdd.Given
import straightway.testing.bdd.panics
import straightway.testing.flow.*
import java.io.File
import java.net.URI
import java.nio.file.Path

class FileSystemLoaderTest {

    private val test get() = Given {
        object {
            val executionList = mutableListOf<Any>()
            val sut = FileSystemLoader()
        }
    }

    @Test
    fun `construction with existing module succeeds`() =
        assertThat(
            { FileSystemLoader() } does
                not - throw_.exception
        )

    @Test
    fun `only file URIs are supported`() =
        test when_ {
            sut.supportedUriSchemes
        } then {
            assertThat(it is_ equal to_ setOf("file"))
        }

    /*@Test
    fun `load does not run application`() =
        test whenBlocking {
            sut.load(
                appJarUri,
                "straightway.odo.loaderTestEnvironment",
                mapOf()
            )
        } then {
            assertThat(executionList is_ empty)
        }*/

    /*@Test
    fun `load returns boot loader that runs application`() =
        test whenBlocking {
            sut.load(
                appJarUri,
                "straightway.odo.loaderTestEnvironment",
                mapOf()
            ).apply {
                initialize(mapOf("ExecutionList" to executionList))
                boot()
            }
        } then {
            assertThat(executionList.single() is_ equal to_ "BootLoaderMock Executed")
        }*/

    @Test
    fun `load with invalid path to app jar panics`() =
        test whenBlocking {
            sut.load(
                Path.of("invalid/path").toUri(),
                "straightway.odo.loaderTestEnvironment",
                mapOf("ExecutionList" to executionList)
            )
        } then panics

    @Test
    fun `load with invalid URI scheme panics`() =
        test whenBlocking {
            sut.load(
                URI("http", null, appJarUri.path, null),
                "straightway.odo.loaderTestEnvironment",
                mapOf("ExecutionList" to executionList)
            )
        } then panics

    // region private

    private val appJarUri: URI get() {
        val baseDir = File(System.getProperty("user.dir")).parent
        val nameRegex = Regex("odo\\.loader_it-.*\\.jar")
        return Path.of(
            File("$baseDir/odo.loader_it/build/libs")
                .walk()
                .single { nameRegex.matches(it.name) }
                .absolutePath
        ).toUri()
    }

    // endregion
}
