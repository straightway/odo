/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.expr

inline fun <reified TArg, TResult> untyped(
    noinline typed: (TArg) -> TResult
): (Any?) -> Any? = { typed(it as TArg) }

inline fun <reified T> untypedOp(
    noinline typed: (T) -> T
): (Any?) -> Any? = { typed(it as T) }

inline fun <reified TArg1, reified TArg2, TResult> untyped(
    noinline typed: (TArg1, TArg2) -> TResult
): (Any?, Any?) -> Any? = { a, b -> typed(a as TArg1, b as TArg2) }

inline fun <reified T> untypedOp(
    noinline typed: (T, T) -> T
): (Any?, Any?) -> Any? = { a, b -> typed(a as T, b as T) }
