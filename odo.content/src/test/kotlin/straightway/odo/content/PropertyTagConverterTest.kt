/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.content

import org.junit.jupiter.api.Test
import straightway.testing.bdd.*
import straightway.testing.flow.*

class PropertyTagConverterTest {

    private val test get() =
        Given {
            PropertySet("Name" to "Hans", "Age" to "83")
        }

    @Test
    fun `mandatory string property exists`() =
        test when_ {
            convert {
                mandatory("Name")
            }
        } then {
            assertThat(it is_ equal to "Hans")
        }

    @Test
    fun `mandatory converted property exists`() =
        test when_ {
            convert {
                mandatory("Age") { it.toInt() }
            }
        } then {
            assertThat(it is_ equal to 83)
        }

    @Test
    fun `mandatory property does not exists`() =
        test when_ {
            convert {
                mandatory("NotExisting")
            }
        } then panics {
            assertThat(it.state is_ equal to "Tag NotExisting not found")
        }

    @Test
    fun `optional string property exists`() =
        test when_ {
            convert {
                optional("Name")
            }
        } then {
            assertThat(it is_ equal to "Hans")
        }

    @Test
    fun `optional converted property exists`() =
        test when_ {
            convert {
                optional("Age") { it.toInt() }
            }
        } then {
            assertThat(it is_ equal to 83)
        }

    @Test
    fun `optional property does not exists`() =
        test when_ {
            convert {
                optional("NotExisting")
            }
        } then isNull
}
