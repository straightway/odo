/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.content

import org.junit.jupiter.api.Test
import straightway.testing.bdd.*
import straightway.testing.flow.*
import straightway.utils.toBase62String
import java.net.URI

class UriUtilityTest {

    private companion object {
        private val binaryData = byteArrayOf(2, 3, 5, 7)
        private val binaryBase62 = binaryData.toBase62String()
    }

    @Test
    fun `adding directory appends new folder to path`() =
        Given {
            URI("http://host/path")
        } when_ {
            this / "subPath"
        } then {
            assertThat(it is_ equal to_ URI("http://host/path/subPath"))
        }

    @Test
    fun `adding directory keeps unaffected URI properties`() =
        with({ it: URI -> it / "subPath" }) {
            assertSchemeIsKept()
            assertHostIsKept()
            assertAuthorityIsKept()
            assertFragmentIsKept()
            assertPortIsKept()
            assertQueryIsKept()
            assertUserInfoIsKept()
        }

    @Test
    fun `adding directory to uri with ssp panics`() =
        Given {
            URI("http", "ssp", "fragment")
        } when_ {
            this / "subPath"
        } then panics

    @Test
    fun `adding directory to uri without path sets path`() =
        Given {
            URI("http:ssp#fragment")
        } when_ {
            this / "subPath"
        } then panics

    @Test
    fun `adding to directory ending with slash omits double slash`() =
        Given {
            URI("http://host/path/")
        } when_ {
            this / "subPath"
        } then {
            assertThat(it is_ equal to_ URI("http://host/path/subPath"))
        }

    @Test
    fun `adding to root ending with slash omits double slash`() =
        Given {
            URI("http://host/")
        } when_ {
            this / "subPath"
        } then {
            assertThat(it is_ equal to_ URI("http://host/subPath"))
        }

    @Test
    fun `adding to uri without path`() =
        Given {
            URI("http://host")
        } when_ {
            this / "subPath"
        } then {
            assertThat(it is_ equal to_ URI("http://host/subPath"))
        }

    @Test
    fun `adding double dot gets parent`() =
        Given {
            URI("http://host/path/sub")
        } when_ {
            this / ".."
        } then {
            assertThat(it is_ equal to_ URI("http://host/path"))
        }

    @Test
    fun `adding double dot gets parent when last char is a slash`() =
        Given {
            URI("http://host/path/sub/")
        } when_ {
            this / ".."
        } then {
            assertThat(it is_ equal to_ URI("http://host/path"))
        }

    @Test
    fun `adding double dot gets to root removes path`() =
        Given {
            URI("http://host/")
        } when_ {
            this / ".."
        } then {
            assertThat(it is_ equal to_ URI("http://host"))
        }

    @Test
    fun `queryArgs for URI without query part, returns empty map`() =
        Given {
            URI("http://host/path")
        } when_ {
            queryArgs
        } then {
            assertThat(it is_ empty)
        }

    @Test
    fun `queryArgs for URI with single query arg`() =
        Given {
            URI("http://host/path?key=value")
        } when_ {
            queryArgs
        } then {
            assertThat(it is_ equal to_ mapOf("key" to "value"))
        }

    @Test
    fun `queryArgs for URI with multiple query arg`() =
        Given {
            URI("http://host/path?key=value&otherKey=otherValue")
        } when_ {
            queryArgs
        } then {
            assertThat(
                it is_ equal
                    to_ mapOf("key" to "value", "otherKey" to "otherValue")
            )
        }

    @Test
    fun `queryArgs without key value delimiter, panics`() =
        Given {
            URI("http://host/path?key")
        } when_ {
            queryArgs
        } then panics

    @Test
    fun `withQueryArg returns URI with new query arg, if no args exist in original`() =
        Given {
            URI("http://host/path")
        } when_ {
            withQueryArg("key", "value")
        } then {
            assertThat(it is_ equal to_ URI("http://host/path?key=value"))
        }

    @Test
    fun `withQueryArg returns URI with appended query arg, if args exist in original`() =
        Given {
            URI("http://host/path?oldKey=oldValue")
        } when_ {
            withQueryArg("key", "value")
        } then {
            assertThat(
                it is_ equal
                    to_ URI("http://host/path?oldKey=oldValue&key=value")
            )
        }

    @Test
    fun `withQueryArg with already existing key overrides value`() =
        Given {
            URI("http://host/path?key=oldValue")
        } when_ {
            withQueryArg("key", "newValue")
        } then {
            assertThat(it is_ equal to_ URI("http://host/path?key=newValue"))
        }

    @Test
    fun `withQueryArg for URI with only scheme specific part panics`() =
        Given {
            URI("scheme:ssp")
        } when_ {
            withQueryArg("key", "newValue")
        } then panics

    @Test
    fun `withQueryArg keeps unaffected URI properties`() =
        with({ it: URI -> it.withQueryArg("key", "newValue") }) {
            assertSchemeIsKept()
            assertHostIsKept()
            assertAuthorityIsKept()
            assertPathIsKept()
            assertFragmentIsKept()
            assertPortIsKept()
            assertUserInfoIsKept()
        }

    @Test
    fun `binary withQueryArg returns URI with new query arg, if no args exist in original`() =
        Given {
            URI("http://host/path")
        } when_ {
            withQueryArg("key", binaryData)
        } then {
            assertThat(it is_ equal to_ URI("http://host/path?key=$binaryBase62"))
        }

    @Test
    fun `tryGetBinaryQueryArg gets value from query args`() {
        Given {
            URI("http://a.b/c?key=$binaryBase62")
        } when_ {
            tryGetBinaryQueryArg("key")
        } then {
            assertThat(it is_ equal to_ binaryData)
        }
    }

    @Test
    fun `tryGetBinaryQueryArg returns null if key is not found`() {
        Given {
            URI("http://a.b/c")
        } when_ {
            tryGetBinaryQueryArg("key")
        } then isNull
    }

    @Test
    fun `tryGetBinaryQueryArg panics if value is not base62 encoded`() {
        Given {
            URI("http://a.b/c?key=+")
        } when_ {
            tryGetBinaryQueryArg("key")
        } then panics
    }

    @Test
    fun `decryptionKey is retrieved from query args`() =
        Given {
            URI("http://a.b/c?decryptionKey=$binaryBase62")
        } when_ {
            decryptionKey
        } then {
            assertThat(it is_ equal to_ binaryData)
        }

    @Test
    fun `withDecryptonKey returns new URI with key as query argument`() =
        Given {
            URI("http://a.b/c")
        } when_ {
            withDecryptonKey(binaryData)
        } then {
            assertThat(it is_ equal to_ URI("http://a.b/c?decryptionKey=$binaryBase62"))
        }

    @Test
    fun `hash is retrieved from query args`() =
        Given {
            URI("http://a.b/c?hash=$binaryBase62")
        } when_ {
            hash
        } then {
            assertThat(it is_ equal to_ binaryData)
        }

    @Test
    fun `withHash returns new URI with hash as query argument`() =
        Given {
            URI("http://a.b/c")
        } when_ {
            withHash(binaryData)
        } then {
            assertThat(it is_ equal to_ URI("http://a.b/c?hash=$binaryBase62"))
        }

    @Test
    fun `isContentBundle is false if absent`() =
        Given {
            URI("http://a.b/c")
        } when_ {
            isContentBundle
        } then {
            assertThat(it is_ false)
        }

    @Test
    fun `isContentBundle is false if not 'true'`() =
        Given {
            URI("http://a.b/c").withQueryArg("isContentBundle", "Anything")
        } when_ {
            isContentBundle
        } then {
            assertThat(it is_ false)
        }

    @Test
    fun `setting isContentBundle true`() =
        Given {
            URI("http://a.b/c")
        } when_ {
            withContentBundleFlag
        } then {
            assertThat(it.isContentBundle is_ true)
        }

    @Test
    fun `fileName yields last component of path`() =
        Given {
            URI("http://a.b/c/d.txt")
        } when_ {
            fileName
        } then {
            assertThat(it is_ equal to "d.txt")
        }

    @Test
    fun `fileName yields last component of path without directory`() =
        Given {
            URI("http://a.b/d.txt")
        } when_ {
            fileName
        } then {
            assertThat(it is_ equal to "d.txt")
        }

    @Test
    fun `fileName is retrieved from query args if specified`() =
        Given {
            URI("http://a.b/c/d.txt?fileName=file.txt")
        } when_ {
            fileName
        } then {
            assertThat(it is_ equal to "file.txt")
        }

    @Test
    fun `fileName is retrieved from scheme specific part if no path is specified`() =
        Given {
            URI("scheme:ssp")
        } when_ {
            fileName
        } then {
            assertThat(it is_ equal to "ssp")
        }

    @Test
    fun `withFileName sets file name in query args`() =
        Given {
            URI("http://a.b/c/d.txt")
        } when_ {
            withFileName("file.txt")
        } then {
            assertThat(it is_ equal to URI("http://a.b/c/d.txt?fileName=file.txt"))
        }

    // region Private

    private fun ((URI) -> URI).assertHostIsKept() =
        Given {
            URI("http://host/path")
        } when_ {
            (this@assertHostIsKept)(this)
        } then {
            assertThat(it.host is_ equal to_ "host")
        }

    private fun ((URI) -> URI).assertSchemeIsKept() =
        Given {
            URI("http://host/path")
        } when_ {
            (this@assertSchemeIsKept)(this)
        } then {
            assertThat(it.scheme is_ equal to_ "http")
        }

    private fun ((URI) -> URI).assertFragmentIsKept() =
        Given {
            URI("http://host/path#fragment")
        } when_ {
            (this@assertFragmentIsKept)(this)
        } then {
            assertThat(it.fragment is_ equal to_ "fragment")
        }

    private fun ((URI) -> URI).assertPortIsKept() =
        Given {
            URI("http://host:4711/path")
        } when_ {
            (this@assertPortIsKept)(this)
        } then {
            assertThat(it.port is_ equal to_ 4711)
        }

    private fun ((URI) -> URI).assertUserInfoIsKept() =
        Given {
            URI("http://user@host/path")
        } when_ {
            (this@assertUserInfoIsKept)(this)
        } then {
            assertThat(it.userInfo is_ equal to_ "user")
        }

    private fun ((URI) -> URI).assertQueryIsKept() =
        Given {
            URI("http://host/path?q=lala")
        } when_ {
            (this@assertQueryIsKept)(this)
        } then {
            assertThat(it.query is_ equal to_ "q=lala")
        }

    private fun ((URI) -> URI).assertAuthorityIsKept() =
        Given {
            URI("http", "auth", "/path", null, null)
        } when_ {
            (this@assertAuthorityIsKept)(this)
        } then {
            assertThat(it.authority is_ equal to_ "auth")
        }

    private fun ((URI) -> URI).assertPathIsKept() =
        Given {
            URI("http://host/path")
        } when_ {
            (this@assertPathIsKept)(this)
        } then {
            assertThat(it.path is_ equal to_ "/path")
        }

    // endregion
}
