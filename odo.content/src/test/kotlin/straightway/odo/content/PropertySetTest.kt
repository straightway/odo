/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.content

import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import org.junit.jupiter.api.Test
import straightway.testing.assertPanics
import straightway.testing.bdd.Given
import straightway.testing.flow.*
import kotlin.reflect.full.functions

class PropertySetTest {

    @Test
    fun `properties are accessible`() =
        Given {
            PropertySet(mapOf("key" to "value"))
        } when_ {
            properties
        } then {
            assertThat(it is_ equal to mapOf("key" to "value"))
        }

    @Test
    fun `vararg constructor`() =
        Given {
            PropertySet("key" to "value")
        } when_ {
            properties
        } then {
            assertThat(it is_ equal to mapOf("key" to "value"))
        }

    @Test
    fun `copyWithProperties creates new instance with new poperties`() =
        Given {
            PropertySet()
        } when_ {
            copyWithProperties(mapOf("key" to "value"))
        } then {
            assertThat(it.properties is_ equal to mapOf("key" to "value"))
        }

    @Test
    fun `toJson yields proper result`() =
        Given {
            PropertySet(mapOf("key" to "value"))
        } when_ {
            toJson()
        } then {
            assertThat(it is_ equal to "{\"key\":\"value\"}")
        }

    @Test
    fun `fromJson yields proper result`() =
        assertThat(
            PropertySet.fromJson("{\"key\":\"value\"}")
                is_ equal to PropertySet(mapOf("key" to "value"))
        )

    @Test
    fun `fromJson with invalid input panics`() =
        assertPanics { PropertySet.fromJson("lalala") }

    @Test
    fun `is json serializable`() =
        Given {
            PropertySet(mapOf("key" to "value"))
        } when_ {
            Json.encodeToString(this)
        } then {
            val reDeSerialized =
                Json.decodeFromString(PropertySet.serializer(), it)
            assertThat(reDeSerialized is_ equal to this)
        }

    @Test
    fun `serializer typeParameterSerializers is called to satisfy jacoco`() {
        val sut = PropertySet.serializer()
        val fn = sut::class.functions.single { it.name == "typeParametersSerializers" }
        fn.call(sut)
    }
}
