/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.content

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import org.junit.jupiter.api.Test
import straightway.testing.bdd.Given
import straightway.testing.bdd.isNull
import straightway.testing.flow.*
import straightway.utils.toBase62String

class TagsTest {

    private fun sut(props: Map<String, String>): PropertyHolder<PropertyHolder<*>> =
        mock { _ ->
            on { properties }.thenReturn(props)
            on { copyWithProperties(any()) }.thenAnswer { args ->
                sut(args.getArgument(0))
            }
        }

    @Test
    fun `tags are retrieved from properties`() =
        Given {
            sut(mapOf(TAGS to Json.encodeToString(setOf("A", "B", "C"))))
        } when_ {
            tags
        } then {
            assertThat(it is_ equal to setOf("A", "B", "C"))
        }

    @Test
    fun `tags are empty if absent`() =
        Given {
            sut(mapOf())
        } when_ {
            tags
        } then {
            assertThat(it is_ empty)
        }

    @Test
    fun `signature is retrieved from properties`() =
        Given {
            sut(
                mapOf(
                    SIGNATURE to byteArrayOf(2, 3, 5, 7).toBase62String()
                )
            )
        } when_ {
            signature
        } then {
            assertThat(it is_ equal to byteArrayOf(2, 3, 5, 7))
        }

    @Test
    fun `signature is null if absent`() =
        Given {
            sut(mapOf())
        } when_ {
            signature
        } then isNull
}
