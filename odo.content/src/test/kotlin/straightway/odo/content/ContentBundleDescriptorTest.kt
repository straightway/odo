/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.content

import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import org.junit.jupiter.api.Test
import straightway.testing.assertPanics
import straightway.testing.bdd.Given
import straightway.testing.bdd.isNull
import straightway.testing.flow.*
import straightway.utils.deserializeTo
import straightway.utils.serializeToByteArray
import java.net.URI
import kotlin.reflect.full.functions

class ContentBundleDescriptorTest {

    private companion object {
        val URI = URI("ipns://bundle/home")
        val ITEMS = listOf(
            ContentItemDescriptor(URI("http://a.b").withFileName("Item1")),
            ContentItemDescriptor(URI("http://c.d").withFileName("Item2"))
        )
        val PROPS = mapOf("tag" to "value")
    }

    private val test get() =
        Given {
            ContentBundleDescriptor(ITEMS, PROPS)
        }

    @Test
    fun `construction with items with ambiguous names panics`() = assertPanics {
        ContentBundleDescriptor(
            listOf(
                ContentItemDescriptor(URI.withFileName("item")),
                ContentItemDescriptor(URI.withFileName("item"))
            )
        )
    }

    @Test
    fun `properties are empty by default`() =
        Given {
            ContentBundleDescriptor(ITEMS)
        } when_ {
            properties
        } then {
            assertThat(it is_ empty)
        }

    @Test
    fun `items are accessible`() =
        test when_ { items } then { assertThat(it is_ equal to_ ITEMS) }

    @Test
    fun `properties are accessible`() =
        test when_ { properties } then { assertThat(it is_ equal to_ PROPS) }

    @Test
    fun `copyWithProperties copies all other items`() =
        test when_ {
            copyWithProperties(properties)
        } then {
            assertThat(it is_ equal to this)
        }

    @Test
    fun `copyWithProperties sets specified properties`() =
        test when_ {
            copyWithProperties(mapOf("otherTag" to "otherValue"))
        } then {
            assertThat(it.properties is_ equal to mapOf("otherTag" to "otherValue"))
        }

    @Test
    fun `description without property is null`() =
        test when_ {
            description
        } then isNull

    @Test
    fun `description is accessible`() =
        test when_ {
            val updated = with(DESCRIPTION, "description")
            updated.description
        } then {
            assertThat(it is_ equal to "description")
        }

    @Test
    fun `is binary serializable`() =
        test when_ {
            serializeToByteArray()
        } then {
            val reDeSerialized = it.deserializeTo<ContentBundleDescriptor>()
            assertThat(reDeSerialized is_ equal to this)
        }

    @Test
    fun `is json serializable`() =
        test when_ {
            Json.encodeToString(this)
        } then {
            val reDeSerialized =
                Json.decodeFromString(ContentBundleDescriptor.serializer(), it)
            assertThat(reDeSerialized is_ equal to this)
        }

    @Test
    fun `serializer typeParameterSerializers is called to satisfy jacoco`() {
        val sut = ContentBundleDescriptor.serializer()
        val fn = sut::class.functions.single { it.name == "typeParametersSerializers" }
        fn.call(sut)
    }
}
