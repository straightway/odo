/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.content

import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import org.junit.jupiter.api.Test
import straightway.expr.minus
import straightway.testing.bdd.Given
import straightway.testing.bdd.isNull
import straightway.testing.flow.*
import straightway.utils.deserializeTo
import straightway.utils.serializeToByteArray
import java.net.URI
import kotlin.reflect.full.functions

class ContentItemDescriptorTest {

    private companion object {
        val defaultUri = URI("https://codeberg.org/straightway")
        val defaultSignature = byteArrayOf(7, 11, 13, 137.toByte())
        val defaultRoles = setOf("Role")
    }

    @Test
    fun `uri is accessible`() =
        Given {
            ContentItemDescriptor(defaultUri)
        } when_ {
            uri
        } then {
            assertThat(it is_ equal to_ defaultUri)
        }

    @Test
    fun `uriString is accessible`() =
        Given {
            ContentItemDescriptor("uriString", mapOf())
        } when_ {
            uriString
        } then {
            assertThat(it is_ equal to_ "uriString")
        }

    @Test
    fun `properties is empty by default when constructing with URI`() =
        Given {
            ContentItemDescriptor(defaultUri)
        } when_ {
            properties
        } then {
            assertThat(it is_ empty)
        }

    @Test
    fun `properties is as specified when constructing with URI`() =
        Given {
            ContentItemDescriptor(defaultUri, mapOf("tag" to "value"))
        } when_ {
            properties
        } then {
            assertThat(it is_ equal to mapOf("tag" to "value"))
        }

    @Test
    fun `properties is as specified when constructing with URI string`() =
        Given {
            ContentItemDescriptor("uri", mapOf("tag" to "value"))
        } when_ {
            properties
        } then {
            assertThat(it is_ equal to mapOf("tag" to "value"))
        }

    @Test
    fun `copyWithProperties copies all other items`() =
        Given {
            ContentItemDescriptor(defaultUri)
        } when_ {
            copyWithProperties(properties)
        } then {
            assertThat(it is_ equal to this)
        }

    @Test
    fun `copyWithProperties sets specified properties`() =
        Given {
            ContentItemDescriptor(defaultUri)
        } when_ {
            copyWithProperties(mapOf("tag" to "value"))
        } then {
            assertThat(it.properties is_ equal to mapOf("tag" to "value"))
        }

    @Test
    fun `mimeType is null by default`() =
        Given {
            ContentItemDescriptor(defaultUri)
        } when_ {
            mimeType
        } then isNull

    @Test
    fun `mimeType is accessible if specified`() =
        Given {
            ContentItemDescriptor(defaultUri).with(MIME_TYPE, "text/plain")
        } when_ {
            mimeType
        } then {
            assertThat(it is_ equal to_ "text/plain")
        }

    @Test
    fun `encoding is null by default`() =
        Given {
            ContentItemDescriptor(defaultUri)
        } when_ {
            encoding
        } then isNull

    @Test
    fun `encoding is accessible if specified`() =
        Given {
            ContentItemDescriptor(defaultUri).with(ENCODING, "utf8")
        } when_ {
            encoding
        } then {
            assertThat(it is_ equal to_ "utf8")
        }

    @Test
    fun `is binary serializable`() =
        Given {
            ContentItemDescriptor(defaultUri)
        } when_ {
            serializeToByteArray()
        } then {
            assertThat(it.deserializeTo<ContentItemDescriptor>() is_ equal to this)
        }

    @Test
    fun `is json serializable`() =
        Given {
            ContentItemDescriptor(defaultUri, mapOf("tag" to "value"))
        } when_ {
            Json.encodeToString(this)
        } then {
            val reDeSerialized =
                Json.decodeFromString(ContentItemDescriptor.serializer(), it)
            assertThat(reDeSerialized is_ equal to this)
        }

    @Test
    fun `serializer typeParameterSerializers is called to satisfy jacoco`() {
        val sut = ContentItemDescriptor.serializer()
        val fn = sut::class.functions.single { it.name == "typeParametersSerializers" }
        fn.call(sut)
    }

    // region Private

    private data class HashCodeTestCase(val a: Any, val b: Any) {
        fun shallBeEqual() = assertThat(a.hashCode() is_ equal to_ b.hashCode())
        fun shallDiffer() = assertThat(a.hashCode() is_ not - equal to_ b.hashCode())
    }

    private fun Any.compareHashCodeWith(other: Any) =
        HashCodeTestCase(this, other)

    private fun altered(array: ByteArray?) =
        array!!.clone().also { it[0] = (it[0] - 127).toByte() }

    // endregion
}
