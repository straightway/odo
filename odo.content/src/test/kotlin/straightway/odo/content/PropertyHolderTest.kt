/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.content

import org.junit.jupiter.api.Test
import straightway.testing.bdd.*
import straightway.testing.flow.*

class PropertyHolderTest {

    private data class PropertyHolderMock(
        override val properties: Map<String, String>
    ) : PropertyHolder<PropertyHolderMock> {
        override fun copyWithProperties(newProperties: Map<String, String>) =
            copy(properties = newProperties)
    }

    private val test get() =
        Given {
            object {
                var mockedProperties = mapOf<String, String>()
                val sut by lazy { PropertyHolderMock(mockedProperties) }
            }
        }

    @Test
    fun `with new non-null string tag`() =
        test when_ {
            sut.with("tag", "value")
        } then {
            assertThat(it.properties is_ equal to mapOf("tag" to "value"))
        }

    @Test
    fun `with new null string tag`() =
        test when_ {
            sut.with("tag", null as String?)
        } then {
            assertThat(it.properties is_ empty)
        }

    @Test
    fun `with non-null string for existing tag`() =
        test while_ {
            mockedProperties = mapOf("tag" to "oldValue")
        } when_ {
            sut.with("tag", "value")
        } then {
            assertThat(it.properties is_ equal to mapOf("tag" to "value"))
        }

    @Test
    fun `with null string for existing tag`() =
        test while_ {
            mockedProperties = mapOf("tag" to "oldValue")
        } when_ {
            sut.with("tag", null as String?)
        } then {
            assertThat(it.properties is_ empty)
        }

    @Test
    fun `with non-null string set`() =
        test when_ {
            sut.with("tag", setOf("c", "b", "a"))
        } then {
            assertThat(it.properties["tag"].stringSet is_ equal to setOf("a", "b", "c"))
        }

    @Test
    fun `with non-null string set containing special characters`() =
        test when_ {
            sut.with("tag", setOf(",", "\"", "'", "[", "]"))
        } then {
            assertThat(
                it.properties["tag"].stringSet is_
                    equal to setOf(",", "\"", "'", "[", "]")
            )
        }

    @Test
    fun `with null string set`() =
        test while_ {
            mockedProperties = mapOf("tag" to "value")
        } when_ {
            sut.with("tag", null as Set<String>?)
        } then {
            assertThat(it.properties is_ empty)
        }

    @Test
    fun `stringSet with null string`() =
        test when_ {
            (null as String?).stringSet
        } then {
            assertThat(it is_ empty)
        }

    @Test
    fun `stringSet with non-null string`() =
        test when_ {
            "[\"a\", \"b\"]".stringSet
        } then {
            assertThat(it is_ equal to setOf("a", "b"))
        }

    @Test
    fun `stringSet with malformed string panics`() =
        test when_ {
            "],[".stringSet
        } then panics

    @Test
    fun `with null ByteArray`() =
        test while_ {
            mockedProperties = mapOf("tag" to "value")
        } when_ {
            sut.with("tag", null as ByteArray?)
        } then {
            assertThat(it.properties is_ empty)
        }

    @Test
    fun `with non-null ByteArray`() =
        test while_ {
            mockedProperties = mapOf("tag" to "value")
        } when_ {
            sut.with("tag", byteArrayOf(2, 3, 5, 7))
        } then {
            assertThat(
                it.properties["tag"].byteArray is_
                    equal to byteArrayOf(2, 3, 5, 7)
            )
        }

    @Test
    fun `byteArray from malformed string panics`() =
        test when_ {
            "\\//".byteArray
        } then panics

    @Test
    fun `byteArray from null string yields null`() =
        test when_ {
            (null as String?).byteArray
        } then isNull
}
