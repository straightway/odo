/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.content

import straightway.error.Panic
import straightway.utils.base62ToByteArray
import straightway.utils.toBase62String
import java.net.URI

operator fun URI.div(sub: String) = URI(
    scheme,
    userInfo,
    host,
    port,
    combinePath(path ?: throw Panic("URI without path: $this"), sub),
    query,
    fragment
)

val URI.queryArgs: Map<String, String>
    get() = try {
        query
            ?.split('&')
            ?.map {
                val (key, value) = it.split('=')
                key to value
            }?.toMap() ?: mapOf()
    } catch (e: Throwable) {
        throw Panic("Could not parse query args of $this ($e)")
    }

fun URI.tryGetBinaryQueryArg(key: String): ByteArray? =
    queryArgs[key].let { stringArg ->
        if (stringArg !== null)
            stringArg.base62ToByteArray()
        else null
    }

fun URI.withQueryArg(key: String, value: String) =
    if (path == null)
        throw Panic("Cannot set query args for $this (no path)")
    else
        URI(
            scheme,
            userInfo,
            host,
            port,
            path,
            updatedQueryArgs(key, value),
            fragment
        )

fun URI.withQueryArg(key: String, value: ByteArray): URI =
    withQueryArg(key, value.toBase62String())

val URI.decryptionKey: ByteArray? get() = tryGetBinaryQueryArg(DECRYPTON_KEY_QUERY_ARG)
fun URI.withDecryptonKey(key: ByteArray): URI = withQueryArg(DECRYPTON_KEY_QUERY_ARG, key)

val URI.hash: ByteArray? get() = tryGetBinaryQueryArg(HASH_QUERY_ARG)
fun URI.withHash(key: ByteArray): URI = withQueryArg(HASH_QUERY_ARG, key)

val URI.isContentBundle: Boolean get() = queryArgs[IS_CONTENT_BUNDLE_ARG] == TRUE
val URI.withContentBundleFlag: URI get() = withQueryArg(IS_CONTENT_BUNDLE_ARG, TRUE)

val URI.fileName: String
    get() = queryArgs[FILE_NAME]
        ?: path?.substringAfterLast('/')
        ?: schemeSpecificPart
fun URI.withFileName(fileName: String) = withQueryArg(FILE_NAME, fileName)

// region Private

private const val DECRYPTON_KEY_QUERY_ARG = "decryptionKey"
private const val HASH_QUERY_ARG = "hash"
private const val IS_CONTENT_BUNDLE_ARG = "isContentBundle"
private const val FILE_NAME = "fileName"
private const val TRUE = true.toString()

private fun URI.updatedQueryArgs(key: String, value: String): String {
    val argsMap = queryArgs.toMutableMap()
    argsMap[key] = value
    return argsMap.map { i -> "${i.key}=${i.value}" }.joinToString("&")
}

private fun combinePath(head: String, tail: String) =
    when (tail) {
        ".." -> head.removeSuffix("/").substringBeforeLast("/")
        else -> "${head.removeSuffix("/")}/${tail.removePrefix("/")}"
    }

// endregion
