/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.content

private const val NAMESPACE = "straightway.odo.content"
const val TAGS = "$NAMESPACE.tags"
const val SIGNATURE = "$NAMESPACE.signature"
const val MIME_TYPE = "$NAMESPACE.mimetype"
const val ENCODING = "$NAMESPACE.encoding"
const val DESCRIPTION = "$NAMESPACE.description"
const val APP_NAME = "$NAMESPACE.appName"
const val APP_ICON = "$NAMESPACE.appIcon"
const val APP_MODULE = "$NAMESPACE.appModule"
const val APP_URI = "$NAMESPACE.appUri"
const val IDENTITY_NAME = "$NAMESPACE.identityName"
const val IDENTITY_IMAGE = "$NAMESPACE.identityImage"
const val IDENTITY_CRYPTO = "$NAMESPACE.identityCrypto"

val <T> PropertyHolder<T>.tags get() = properties[TAGS].stringSet
val <T> PropertyHolder<T>.signature get() = properties[SIGNATURE].byteArray
