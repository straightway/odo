/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.content

import kotlinx.serialization.SerializationException
import kotlinx.serialization.json.*
import kotlinx.serialization.json.Json.Default.decodeFromString
import straightway.error.Panic
import java.io.Serializable

/**
 * A set of generic string properties, identified by string IDs.
 */
@kotlinx.serialization.Serializable
data class PropertySet(
    override val properties: Map<String, String>
) : PropertyHolder<PropertySet>, Serializable {

    constructor(vararg props: Pair<String, String>) : this(props.toMap())

    override fun copyWithProperties(newProperties: Map<String, String>) =
        copy(properties = newProperties)

    fun toJson() =
        JsonObject(properties.map { it.key to JsonPrimitive(it.value) }.toMap())
            .toString()

    companion object {
        fun fromJson(json: String) =
            try {
                val properties = decodeFromString(JsonObject.serializer(), json)
                PropertySet(
                    properties.map { it.key to it.value.jsonPrimitive.content }.toMap()
                )
            } catch (x: SerializationException) {
                throw Panic("Could not get property set from $json: $x")
            }

        const val serialVersionUID = 1L
    }
}
