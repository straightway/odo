/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.content

import straightway.error.Panic

/**
 * Helper interface for converting tags of property sets.
 */
interface PropertyTagConverter {
    fun <T> mandatory(tag: String, block: (String) -> T): T
    fun <T> optional(tag: String, block: (String) -> T): T?
}

fun PropertyTagConverter.mandatory(tag: String): String = mandatory(tag) { it }
fun PropertyTagConverter.optional(tag: String) = optional(tag) { it }

fun <T> PropertyHolder<*>.convert(block: PropertyTagConverter.() -> T) =
    object : PropertyTagConverter {
        override fun <T> mandatory(tag: String, block: (String) -> T) =
            block(properties[tag] ?: throw Panic("Tag $tag not found"))
        override fun <T> optional(tag: String, block: (String) -> T) =
            properties[tag].let {
                if (it === null)
                    null
                else block(it)
            }
    }.block()
