/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.content

import kotlinx.serialization.*
import kotlinx.serialization.json.*
import straightway.error.Panic
import straightway.utils.base62ToByteArray
import straightway.utils.toBase62String

/**
 * Interface for generically holding and manipulation string properties,
 * addressed by string tags.
 */
interface PropertyHolder<T> {
    val properties: Map<String, String>
    fun copyWithProperties(newProperties: Map<String, String>): T
}

fun <T> PropertyHolder<T>.with(tag: String, value: String?) =
    if (value === null)
        copyWithProperties(newProperties = properties - tag)
    else copyWithProperties(newProperties = properties - tag + (tag to value))

fun <T> PropertyHolder<T>.with(tag: String, tags: Set<String>?) =
    with(tag, if (tags === null) null else Json.encodeToString(tags))

fun <T> PropertyHolder<T>.with(tag: String, data: ByteArray?) =
    with(tag, data?.toBase62String())

val String?.byteArray
    get() = this?.base62ToByteArray()

val String?.stringSet: Set<String>
    get() = this?.stringSetInternal ?: setOf()

private val String.stringSetInternal: Set<String>
    get() = try {
        Json.decodeFromString(this)
    } catch (x: SerializationException) {
        throw Panic("Could not get set from string $this: $x")
    }
