/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.content

import straightway.error.Panic
import java.io.Serializable

/**
 * Descriptor for a set of content items.
 */
@kotlinx.serialization.Serializable
data class ContentBundleDescriptor(
    val items: List<ContentItemDescriptor>,
    override val properties: Map<String, String>
) : PropertyHolder<ContentBundleDescriptor>, Serializable {

    constructor(items: List<ContentItemDescriptor>) : this(items, mapOf())

    override fun copyWithProperties(newProperties: Map<String, String>) =
        copy(properties = newProperties)

    companion object {
        const val serialVersionUID = 1L
    }

    init {
        if (items.map { it.uri.fileName }.distinct().size != items.size)
            throw Panic("${items.map { it.uri.fileName }} contains duplicate file names")
    }
}

val ContentBundleDescriptor.description get() = properties[DESCRIPTION]
