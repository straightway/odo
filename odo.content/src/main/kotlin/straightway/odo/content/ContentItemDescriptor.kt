/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.content

import java.io.Serializable
import java.net.URI

/**
 * Descriptor for one content item of a cloud storage.
 */
@kotlinx.serialization.Serializable
data class ContentItemDescriptor(
    val uriString: String,
    override val properties: Map<String, String>
) : PropertyHolder<ContentItemDescriptor>, Serializable {

    constructor(
        uri: URI,
        properties: Map<String, String> = mapOf()
    ) : this(uri.toString(), properties)

    val uri get() = URI(uriString)

    override fun copyWithProperties(newProperties: Map<String, String>) =
        copy(properties = newProperties)

    companion object {
        const val serialVersionUID = 1L
    }
}

val ContentItemDescriptor.mimeType get() = properties[MIME_TYPE]
val ContentItemDescriptor.encoding get() = properties[ENCODING]
