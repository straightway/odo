/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.ui.model

import straightway.compiler.Generated
import straightway.crypto.Hasher
import straightway.utils.min
import straightway.utils.toBase62String
import java.net.URI

private const val MAX_APP_FOLDER_LEN = 31

fun URI.getFolderName(hasher: Hasher) =
    getHash(hasher).relevantPart.toByteArray().toBase62String()

private fun URI.getHash(hasher: Hasher) =
    hasher.getHash(hashablePart)

private val URI.hashablePart
    @Generated("Jacoco cannot know that host and ssp cannot be both null")
    get() = (host ?: schemeSpecificPart).toByteArray()

private val ByteArray.relevantPart get() =
    slice(0..min(MAX_APP_FOLDER_LEN, lastIndex))
