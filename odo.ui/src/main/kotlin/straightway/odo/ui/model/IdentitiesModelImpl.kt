/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.ui.model

import javafx.collections.FXCollections
import straightway.odo.content.*
import straightway.utils.base62ToByteArray
import straightway.utils.toBase62String
import java.nio.file.Path

/**
 * Productive implementation of the identities model.
 */
class IdentitiesModelImpl(
    private val services: Services,
    val repoPath: Path
) : IdentitiesModel {

    override val instances = FXCollections.observableList(mutableListOf<Identity>())

    override fun write(new: Identity) {
        instances.add(new)
        val props = mutableMapOf(
            IDENTITY_NAME to new.name,
            IDENTITY_CRYPTO to new.cryptoIdentity.rawKeyPair.toBase62String()
        )
        if (new.image !== null)
            props[IDENTITY_IMAGE] = new.image.toBase62String()
        val json = PropertySet(props).toJson()
        val rawKeyHash =
            services.hasher.getHash(new.cryptoIdentity.rawKeyPair).toBase62String()
        val file = Path.of(repoPath.toString(), "$rawKeyHash.json").toFile()
        file.writeText(json)
    }

    override fun create(name: String, image: ByteArray?) {
        write(
            Identity(
                name,
                image,
                services.cryptoFactory.cryptoIdentity.createCryptoIdentity()
            )
        )
    }

    override fun refresh() {
        instances.setAll(identitiesInRepo ?: listOf())
    }

    private val identitiesInRepo get() = repoPath.toFile().listFiles()?.map {
        PropertySet.fromJson(it.readText()).convert {
            Identity(
                mandatory(IDENTITY_NAME),
                optional(IDENTITY_IMAGE) { it.base62ToByteArray() },
                mandatory(IDENTITY_CRYPTO) {
                    val keyPair = it.base62ToByteArray()
                    services.cryptoFactory.cryptoIdentity.getCryptoIdentity(keyPair)
                }
            )
        }
    }

    init {
        repoPath.toFile().mkdirs()
        refresh()
    }
}
