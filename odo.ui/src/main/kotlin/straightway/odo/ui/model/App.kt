/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.ui.model

import java.io.Serializable
import java.net.URI

/**
 * Model for a single app.
 */
data class App(
    val name: String,
    val icon: ByteArray?,
    val localPath: String,
    val module: String,
    val uri: URI? = null
) : Serializable {

    override fun equals(other: Any?): Boolean =
        other is App && equalsSpecific(other)

    override fun hashCode() =
        name.hashCode() xor
            icon.contentHashCode() xor
            localPath.hashCode() xor
            module.hashCode() xor
            (uri?.hashCode() ?: 0)

    companion object {
        const val serialVersionUID = 1L
    }

    // region Private

    private fun equalsSpecific(other: App): Boolean =
        other.name == name &&
            other.icon.contentEquals(icon) &&
            equalsModuleProperties(other)

    private fun equalsModuleProperties(other: App) =
        other.localPath == localPath && other.module == module && other.uri == uri

    // endregion
}
