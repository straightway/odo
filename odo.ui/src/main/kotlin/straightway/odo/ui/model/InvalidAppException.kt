/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.ui.model

/**
 * Thrown if an app could not be loaded or displayed.
 */
class InvalidAppException(
    val reason: Reason,
    val detail: String = ""
) : RuntimeException() {

    enum class Reason {
        INVALID_APP_DIR,
        NO_MANIFEST,
        MISSING_FILES,
        MISSING_CODE,
        INVALID_MANIFEST,
        OTHER_SEE_DETAIL
    }

    override val message: String get() =
        reason.toString() + if (detail == "") "" else ": $detail"
}
