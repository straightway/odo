/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.ui.model

import javafx.collections.FXCollections
import straightway.error.Panic
import straightway.odo.cloud.CloudRead
import straightway.odo.content.*
import straightway.odo.loader.ApplicationLoader
import java.io.File
import java.net.URI
import java.nio.file.Path

/**
 * Default implementation of the AppsModel interface.
 */
class AppsModelImpl(
    val repoPath: Path,
    val services: Services,
    private val kernelLoaderFactory: () -> ApplicationLoader
) : AppsModel {

    override val instances = FXCollections.observableList(mutableListOf<App>())!!

    override fun refresh() {
        val allApps = appsInRepo
        services.uiThreadDispatcher { instances.setAll(allApps) }
    }

    override fun addCopyOfApp(appDir: File?) {
        if (appDir === null)
            return
        if (!appDir.isDirectory)
            throw InvalidAppException(
                InvalidAppException.Reason.INVALID_APP_DIR,
                appDir.toString()
            )
        if (!appDir.isStartable)
            throw InvalidAppException(InvalidAppException.Reason.MISSING_CODE)
        val target = Path.of(repoPath.toString(), "local", appDir.name).toFile()
        appDir.copyRecursively(target)
        refresh()
    }

    override suspend fun launch(app: App) {
        val loader = kernelLoaderFactory()
        val kernel = loader.load(
            URI("file://$repoPath/${app.localPath}"),
            app.module,
            mapOf()
        )
        kernel.initialize(mapOf())
        kernel.boot()
    }

    override suspend fun upload(app: App) {
        services.appUploader.upload(app)
        refresh()
    }

    override suspend fun download(uri: URI) {
        val targetFolder = createAppTargetFolderForAppUri(uri)
        val cloud = getCloudForDownloading(uri)
        cloud.download(uri, targetFolder)
        refresh()
    }

    override fun delete(app: App) {
        Path.of(repoPath.toString(), app.localPath).toFile().deleteRecursively()
        val hostDir = Path.of(repoPath.toString(), app.localPath).toFile().parentFile
        if (hostDir.listFiles()!!.isEmpty())
            hostDir.deleteRecursively()
        refresh()
    }

    // region Private

    private suspend fun CloudRead.download(
        uri: URI,
        targetFolder: File
    ) {
        val contentDownloader = services.createContentDownloader(this)
        contentDownloader.download(targetFolder, uri)
    }

    private fun getCloudForDownloading(uri: URI): CloudRead {
        val decryptionKey = uri.decryptionKey
            ?: throw Panic("URI $uri has no decryption key")
        val decryptor =
            services.cryptoFactory.symmetricCryptor.getCryptor(decryptionKey)
        return services.cloud.decryptingWith(decryptor).readVia(uri.scheme)
    }

    private fun createAppTargetFolderForAppUri(uri: URI): File {
        val targetFolderName = uri.getFolderName(services.hasher)
        val targetFolder = Path.of(repoPath.toString(), targetFolderName).toFile()
        targetFolder.mkdirs()
        return targetFolder
    }

    private val appsInRepo: List<App>
        get() = repoPath.toFile().listFiles()?.flatMap { it.appsForHost } ?: listOf()

    private val File.appsForHost: List<App>
        get() = listFiles()?.mapNotNull {
            it.getAppFromDir(Path.of(name, it.name).toString())
        } ?: listOf()

    private fun File.getAppFromDir(localPath: String): App? =
        appManifest?.convert {
            App(
                optional(APP_NAME) ?: name,
                optional(APP_ICON) { getChild(it)?.readBytes() },
                localPath,
                optional(APP_MODULE) ?: name,
                optional(APP_URI) { URI(it) }
            )
        }

    private val File.isStartable get() =
        listFiles()?.any(::isJar) ?: false

    init {
        // We cannot use refresh here, because this would dispatch loading the apps
        // to the ui thread. In this case, this call has no effect, probably because
        // it happens at a "bad" time, when the UI is already initialized, but does
        // not yet properly handle model updates.
        instances.setAll(appsInRepo)
    }

    private companion object {

        private fun isJar(file: File) =
            file.name.endsWith(".jar", ignoreCase = true)
    }

    // endregion
}

internal const val APP_MANIFEST_FILE_NAME = "appManifest.json"

@Suppress("SwallowedException")
internal val File.appManifest: PropertySet?
    get() {
        val appManifestFile = getChild(APP_MANIFEST_FILE_NAME)
        if (appManifestFile === null)
            return null
        val appManifestContent = appManifestFile.readText()
        return try {
            PropertySet.fromJson(appManifestContent)
        } catch (x: Panic) {
            null
        }
    }

internal fun File.getChild(wantedName: String): File? =
    listFiles()?.singleOrNull { it.name == wantedName }
