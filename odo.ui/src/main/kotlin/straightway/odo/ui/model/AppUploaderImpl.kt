/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.ui.model

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import straightway.crypto.Cryptor
import straightway.error.Panic
import straightway.odo.cloud.CloudWrite
import straightway.odo.content.*
import java.net.URI
import java.nio.file.Files.createDirectory
import java.nio.file.Files.move
import java.nio.file.Path

/**
 * Default implementation of the AppUploader class. Uploads apps
 * to the IPFS cloud.
 */
class AppUploaderImpl(
    private val repoPath: Path,
    private val services: Services
) : AppUploader {

    override suspend fun upload(app: App) = with(app) {
        val cryptor = getOrCreateCryptor()
        val cloudWrite = cryptor.createIpfsCloud()
        updateUriInManifestIfNeeded(cloudWrite, cryptor)
        uploadTo(cloudWrite)
        moveToDefaultFolderIfNeeded()
    }

    // region Private

    private suspend fun App.moveToDefaultFolderIfNeeded() {
        if (!isStoredInProperFolder)
            folder.moveTo(defaultFolderName!!)
    }

    private val App.isStoredInProperFolder get() = actualFolderName == defaultFolderName
    private val App.actualFolderName get() = folder.parent.toFile().name
    private val App.defaultFolderName get() = manifestProperties.uri?.folderName

    private suspend fun App.updateUriInManifestIfNeeded(
        cloudWrite: CloudWrite,
        cryptor: Cryptor
    ) {
        if (manifestProperties.uri === null)
            updateManifestWithUri(cloudWrite.createNewCloudRoot(this, cryptor))
    }

    private suspend fun App.uploadTo(cloudWrite: CloudWrite) {
        val uploader = createUploader(cloudWrite)
        val uploadURI = uploader.upload()
        cloudWrite.publish(manifestProperties.uri!!, uploadURI)
    }

    private fun App.createUploader(cloudWrite: CloudWrite) =
        services.createContentUploader(cloudWrite, folderFile) { tree() }

    private fun App.updateManifestWithUri(root: URI) {
        val manifestProperties = manifestProperties.toMutableMap()
        manifestProperties[APP_URI] = root.toString()
        writeManifest(manifestProperties.toJson())
    }

    private fun App.writeManifest(manifest: String) =
        folderFile.getChild(APP_MANIFEST_FILE_NAME)?.writeText(manifest)

    private fun Map<String, String>.toJson() = PropertySet(this).toJson()

    private suspend fun CloudWrite.createNewCloudRoot(app: App, cryptor: Cryptor) =
        createRoot()
            .withContentBundleFlag
            .withFileName(app.folderFile.name)
            .withDecryptonKey(cryptor.decryptionKey)

    private fun Cryptor.createIpfsCloud() =
        services.cloud.encryptingWith(this).writeVia("ipns")

    private fun App.getOrCreateCryptor(): Cryptor = manifestProperties.uri.let {
        if (it === null)
            services.cryptoFactory.symmetricCryptor.createCryptor()
        else services.cryptoFactory.symmetricCryptor.getCryptor(
            it.decryptionKey
                ?: throw Panic("Cannot upload appName: invalid URI $it")
        )
    }

    private val Map<String, String>.uri
        get() = this[APP_URI].let {
            if (it === null) null else URI(it)
        }

    private val App.manifestProperties
        get() = folderFile.appManifest?.properties
            ?: throw Panic(
                "Cannot upload $name: " +
                    "App folder $localPath not found"
            )

    private val App.folderFile get() = folder.toFile()
    private val App.folder get() = Path.of(repoPath.toString(), localPath)
    private val URI.folderName get() = getFolderName(services.hasher)

    // I see no way to call createDirectory and move in a suspend function
    // in an appropriate way. So I suppress this warning.
    @Suppress("BlockingMethodInNonBlockingContext")
    private suspend fun Path.moveTo(newRootFolder: String) {
        withContext(Dispatchers.IO) {
            createDirectory(Path.of(repoPath.toString(), newRootFolder))
            move(
                this@moveTo,
                Path.of(repoPath.toString(), newRootFolder, toFile().name)
            )
        }
    }

    // endregion
}
