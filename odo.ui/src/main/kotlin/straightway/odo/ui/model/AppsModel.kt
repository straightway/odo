/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.ui.model

import javafx.collections.ObservableList
import java.io.File
import java.net.URI

/**
 * The model for a UI showing a collection of apps.
 */
interface AppsModel : AppUploader {
    val instances: ObservableList<App>
    fun refresh()
    fun addCopyOfApp(appDir: File?)
    suspend fun launch(app: App)
    suspend fun download(uri: URI)
    fun delete(app: App)
}
