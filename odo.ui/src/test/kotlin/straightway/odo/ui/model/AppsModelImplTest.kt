/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.ui.model

import com.nhaarman.mockitokotlin2.*
import org.junit.jupiter.api.Test
import straightway.crypto.CryptoFactory
import straightway.crypto.Cryptor
import straightway.crypto.Hasher
import straightway.expr.minus
import straightway.odo.boot.BootLoader
import straightway.odo.cloud.Cloud
import straightway.odo.cloud.ContentDownloader
import straightway.odo.cloud.CryptoCloud
import straightway.odo.cloud.SuperCloud
import straightway.odo.content.*
import straightway.odo.loader.ApplicationLoader
import straightway.testing.bdd.Given
import straightway.testing.bdd.panics
import straightway.testing.bdd.throws
import straightway.testing.flow.*
import java.io.File
import java.net.URI
import java.nio.file.*

class AppsModelImplTest {

    private companion object {
        val DECRYPTION_KEY = byteArrayOf(3, 13, 29)
        val ICON = byteArrayOf(2, 3, 5, 7)
        val DEFAULT_ICON = byteArrayOf(11, 13, 17, 19)
        val JAR = byteArrayOf(23, 29, 31, 37)
        val HASH = byteArrayOf(2, 11, 23)
        val URI = URI("ipns://uri").withDecryptonKey(DECRYPTION_KEY)
    }

    private fun testAppsDir(setUpInitialFiles: Path.() -> Any?) =
        test(setUpInitialFiles, {})
    private fun testLocalApp(setUpInitialFiles: Path.() -> Unit) =
        test({}, setUpInitialFiles)

    private fun test(
        setUpInitialAppDirFiles: Path.() -> Any?,
        setUpInitialLocalAppFiles: Path.() -> Unit
    ) =
        Given {
            object : AutoCloseable {
                var expectedResult: Any? = null
                val baseDir = Files
                    .createTempDirectory("AppsModelTest")
                val appsDir = Path.of(baseDir.toString(), "appsDir").apply {
                    toFile().mkdir()
                    expectedResult = setUpInitialAppDirFiles()
                }
                val localAppDir = Path.of(baseDir.toString(), "localAppsDir").apply {
                    toFile().mkdir()
                    setUpInitialLocalAppFiles()
                }
                val kernel: BootLoader = mock()
                val appLoader: ApplicationLoader = mock {
                    onBlocking { load(any(), any(), any()) }.thenAnswer { kernel }
                }
                val symmetricCryptor: Cryptor = mock()
                val symmetricCryptorFactory: Cryptor.Factory = mock {
                    on { createCryptor() }.thenReturn(symmetricCryptor)
                    on { getCryptor(DECRYPTION_KEY) }.thenReturn(symmetricCryptor)
                }
                val cloudMock: Cloud = mock()
                val encryptingCloud: SuperCloud = mock {
                    on { writeVia("ipns") }.thenReturn(cloudMock)
                    on { readVia("ipns") }.thenReturn(cloudMock)
                }
                val cryptoCloud: CryptoCloud = mock {
                    on { encryptingWith(symmetricCryptor) }.thenReturn(encryptingCloud)
                    on { decryptingWith(symmetricCryptor) }.thenReturn(encryptingCloud)
                }
                val cryptoFactory: CryptoFactory = mock {
                    on { symmetricCryptor }.thenReturn(symmetricCryptorFactory)
                }
                val uiThreadDispatcher: Dispatcher = mock {
                    on { dispatch(any()) }.thenAnswer {
                        it.getArgument<() -> Unit>(0)()
                    }
                }
                val hasher: Hasher = mock {
                    on { getHash(any()) }.thenReturn(HASH)
                }
                val contentDownloader: ContentDownloader = mock()
                val services: Services = mock {
                    on { cloud }.thenReturn(cryptoCloud)
                    on { cryptoFactory }.thenReturn(cryptoFactory)
                    on { hasher }.thenReturn(hasher)
                    on { appUploader }.thenReturn(mock())
                    on { uiThreadDispatcher }.thenReturn(uiThreadDispatcher)
                    on { createContentDownloader(any()) }.thenReturn(contentDownloader)
                }
                val sut = AppsModelImpl(appsDir, services) { appLoader }
                override fun close() {
                    baseDir.toFile().deleteRecursively()
                }
            }
        }

    @Test
    fun `repoPath is accessible`() =
        testAppsDir {
            // Nothing to set up
        } when_ {
            sut.repoPath
        } then {
            assertThat(it is_ equal to appsDir)
        }

    @Test
    fun `missing app repo dir yields empty apps collection`() =
        testAppsDir {
            // Nothing to set up
        } when_ {
            sut.instances
        } then {
            assertThat(it is_ empty)
        }

    @Test
    fun `empty repo dir is ignored`() =
        testAppsDir {
            toFile().mkdirs()
        } when_ {
            sut.instances
        } then {
            assertThat(it is_ empty)
        }

    @Test
    fun `construction does not dispatch update of instances to ui thread`() =
        testAppsDir {
        } when_ {
            sut.instances
        } then {
            verify(uiThreadDispatcher, never()).dispatch(any())
        }

    @Test
    fun `single app dir yields single element app collection`() =
        testAppsDir {
            appBundle("host", "app", ICON) {
                file("icon.png", APP_ICON, content = ICON)
            }
        } when_ {
            sut.instances
        } then {
            assertThat(it.single() is_ equal to expectedResult)
        }

    @Test
    fun `app module specified in manifest is respected`() =
        testAppsDir {
            appBundle("host", "app", ICON, appModule = "theModule") {
                file("icon.png", APP_ICON, content = ICON)
            }
        } when_ {
            sut.instances
        } then {
            assertThat(it.single().module is_ equal to "theModule")
        }

    @Test
    fun `two app dirs on same host yields two element app collection`() =
        testAppsDir {
            listOf(
                appBundle("host", "app1", ICON) {
                    file("icon.png", APP_ICON, content = ICON)
                },
                appBundle("host", "app2", ICON + byteArrayOf(2)) {
                    file("icon.png", APP_ICON, content = ICON + byteArrayOf(2))
                }
            )
        } when_ {
            sut.instances
        } then {
            assertThat(it.sortedBy { app -> app.name } is_ equal to expectedResult)
        }

    @Test
    fun `two app dirs on different hosts yields two element app collection`() =
        testAppsDir {
            listOf(
                appBundle("host1", "app", ICON) {
                    file("icon.png", APP_ICON, content = ICON)
                },
                appBundle("host2", "app", ICON + byteArrayOf(2)) {
                    file("icon.png", APP_ICON, content = ICON + byteArrayOf(2))
                }
            )
        } when_ {
            sut.instances
        } then {
            assertThat(it.sortedBy { app -> app.localPath } is_ equal to expectedResult)
        }

    @Test
    fun `app without icon uses default icon`() =
        testAppsDir {
            appBundle("host", "app", ICON) {}
        } when_ {
            sut.instances
        } then {
            assertThat(it.single().icon is_ null)
        }

    @Test
    fun `app with missing icon file uses default icon`() =
        testAppsDir {
            appBundle("host", "app", ICON)
        } when_ {
            sut.instances
        } then {
            assertThat(it.single().icon is_ null)
        }

    @Test
    fun `app with missing icon file uses default icon 2`() =
        testAppsDir {
            appBundle("host", "appName", DEFAULT_ICON, iconFileName = "notExisting.png")
        } when_ {
            sut.instances
        } then {
            assertThat(it.single().icon is_ null)
        }

    @Test
    fun `app with missing name uses directory name as app name`() =
        testAppsDir {
            appBundle("host", "appName", ICON, nameInManifest = null)
        } when_ {
            sut.instances
        } then {
            assertThat(it.single().name is_ equal to "appName")
        }

    @Test
    fun `app without manifest file is ignored`() =
        testAppsDir {
            createFile("host/app/icon.png", ICON)
        } when_ {
            sut.instances
        } then {
            assertThat(it is_ empty)
        }

    @Test
    fun `app with corrupt manifest file is ignored`() =
        testAppsDir {
            createFile("host/app/appManifest.json", "Lalala")
        } when_ {
            sut.instances
        } then {
            assertThat(it is_ empty)
        }

    @Test
    fun `other files in the host dir are ignored`() =
        testAppsDir {
            createFile("host/otherFile", byteArrayOf())
            appBundle("host", "app", ICON) {
                file("icon.png", APP_ICON, content = ICON)
            }
        } when_ {
            sut.instances
        } then {
            assertThat(it.single() is_ equal to expectedResult)
        }

    @Test
    fun `other files in the apps dir are ignored`() =
        testAppsDir {
            createFile("otherFile", byteArrayOf())
            appBundle("host", "app", ICON) {
                file("icon.png", APP_ICON, content = ICON)
            }
        } when_ {
            sut.instances
        } then {
            assertThat(it.single() is_ equal to expectedResult)
        }

    @Test
    fun `host dir without app dir is ignored`() =
        testAppsDir {
            Path.of(toString(), "otherHost").toFile().mkdirs()
            appBundle("host", "app", ICON) {
                file("icon.png", APP_ICON, content = ICON)
            }
        } when_ {
            sut.instances
        } then {
            assertThat(it.single() is_ equal to expectedResult)
        }

    @Test
    fun `refresh loads apps in app dir`() =
        testAppsDir {
            appBundle("host", "app", ICON) {
                file("icon.png", APP_ICON, content = ICON)
            }
        } while_ {
            appsDir.toFile().getChild("host").deleteRecursively()
        } when_ {
            sut.refresh()
            sut.instances
        } then {
            assertThat(it is_ empty)
        }

    @Test
    fun `refresh updates instances in ui thread`() =
        testAppsDir {
            appBundle("host", "app", ICON) {
                file("icon.png", APP_ICON, content = ICON)
            }
        } when_ {
            sut.refresh()
        } then {
            verify(uiThreadDispatcher, times(1)).dispatch(any())
        }

    @Test
    fun `appDir being a plain file, then AppsModelImpl has no instances`() {
        lateinit var otherSut: AppsModelImpl
        lateinit var alternateRoot: Path
        testAppsDir {
            alternateRoot = Paths.get(toString(), "alternateRoot")
            alternateRoot.toFile().writeText("content")
        } while_ {
            otherSut = AppsModelImpl(alternateRoot, services) { appLoader }
        } when_ {
            otherSut.instances
        } then {
            assertThat(it is_ empty)
        }
    }

    @Test
    fun `launch boots kernel with app`() =
        testAppsDir {
            appBundle("host", "app", ICON) {
                file("icon.png", APP_ICON, content = ICON)
                file("MAIN.JAR", content = JAR)
            }
        } whenBlocking {
            sut.launch(sut.instances.single())
        } then {
            verify(kernel, times(1)).initialize(mapOf())
            verifyBlocking(kernel, times(1)) { boot() }
        }

    @Test
    fun `addCopyOfApp with null dir does nothing`() =
        testLocalApp {
            // Nothing to set up
        } when_ {
            sut.addCopyOfApp(null)
        } then {
            assertThat(sut.instances is_ empty)
        }

    @Test
    fun `addCopyOfApp copies app to repository`() {
        val manifest = PropertySet(
            APP_MODULE to "module",
            APP_NAME to "appName",
            APP_ICON to "icon.png"
        ).toJson()

        testLocalApp {
            createFile("appManifest.json", manifest)
            createFile("code.jar", "jar content")
            createFile("icon.png", byteArrayOf(2, 3, 5, 7))
        } when_ {
            sut.addCopyOfApp(localAppDir.toFile())
        } then {
            assertThat(
                sut.instances is_ equal to listOf(
                    App(
                        "appName",
                        byteArrayOf(2, 3, 5, 7),
                        Path.of("local", localAppDir.toFile().name).toString(),
                        "module"
                    )
                )
            )
            val importedAppDir =
                Path.of(appsDir.toString(), "local", "localAppsDir").toFile()
            assertThat(importedAppDir.exists() is_ true)
            assertThat(importedAppDir.listFiles()!! has size of 3)
            assertThat(
                importedAppDir.getChild("appManifest.json").readText()
                    is_ equal to manifest
            )
            assertThat(
                importedAppDir.getChild("icon.png").readBytes()
                    is_ equal to byteArrayOf(2, 3, 5, 7)
            )
            assertThat(
                importedAppDir.getChild("code.jar").readText()
                    is_ equal to "jar content"
            )
        }
    }

    @Test
    fun `addCopyOfApp with unstartable app throws`() =
        testLocalApp {
            createFile("appManifest.json", PropertySet().toJson())
        } when_ {
            sut.addCopyOfApp(localAppDir.toFile())
        } then throws<InvalidAppException> {
            assertThat(it.reason is_ equal to InvalidAppException.Reason.MISSING_CODE)
        }

    @Test
    fun `addCopyOfApp with non-dir throws`() {
        lateinit var dirFile: File
        testLocalApp {
            createFile("wrongAppDir", "file content")
        } when_ {
            dirFile = localAppDir.toFile().getChild("wrongAppDir")
            sut.addCopyOfApp(dirFile)
        } then throws<InvalidAppException> {
            assertThat(it.reason is_ equal to InvalidAppException.Reason.INVALID_APP_DIR)
            assertThat(it.detail is_ equal to dirFile.toString())
        }
    }

    @Test
    fun `upload calls appUploader`() {
        val app = App("appName", null, "localPath", "module")
        testAppsDir {} whenBlocking {
            sut.upload(app)
        } then {
            verifyBlocking(services.appUploader, times(1)) { upload(app) }
        }
    }

    @Test
    fun `upload refreshes app list after upload`() {
        val app = App("appName", null, "localPath", "module")
        testAppsDir {
            appBundle("host", "app", ICON)
        } while_ {
            appsDir.toFile().listFiles()?.forEach { it.deleteRecursively() }
        } whenBlocking {
            assertThat(sut.instances is_ not - empty)
            sut.upload(app)
        } then {
            assertThat(sut.instances is_ empty)
        }
    }

    @Test
    fun `download creates app folder`() =
        testAppsDir {} whenBlocking {
            sut.download(URI)
        } then {
            assertThat(
                Path.of(appsDir.toString(), URI.getFolderName(hasher)).toFile().exists()
                    is_ true
            )
        }

    @Test
    fun `download uses decryption key encoded in the URI`() =
        testAppsDir {} whenBlocking {
            sut.download(URI)
        } then {
            verify(cryptoFactory.symmetricCryptor, times(1))
                .getCryptor(DECRYPTION_KEY)
        }

    @Test
    fun `download panics if decryption key is not encoded in the URI`() =
        testAppsDir {} whenBlocking {
            sut.download(URI("ipns://withoutDecryptionKey"))
        } then panics {
            assertThat(
                it.state is_ equal to
                    "URI ipns://withoutDecryptionKey has no decryption key"
            )
        }

    @Test
    fun `download uses encrypted cloud`() =
        testAppsDir {} whenBlocking {
            sut.download(URI)
        } then {
            verify(cryptoCloud, times(1)).decryptingWith(symmetricCryptor)
            verify(encryptingCloud, times(1)).readVia("ipns")
        }

    @Test
    fun `download uses content downloader with cloud`() =
        testAppsDir {} whenBlocking {
            sut.download(URI)
        } then {
            verify(services, times(1)).createContentDownloader(cloudMock)
            verifyBlocking(contentDownloader, times(1)) {
                download(
                    Path.of(appsDir.toString(), URI.getFolderName(hasher)).toFile(),
                    URI
                )
            }
        }

    @Test
    fun `download refreshes instance list`() =
        testAppsDir {} while_ {
            // We add this app folder AFTER the sut has been created, so it doesn't
            // yet know it and the instances collection is empty.
            appsDir.appBundle("host", "app", ICON) {
                file("icon.png", APP_ICON, content = ICON)
            }
        } whenBlocking {
            sut.download(URI)
        } then {
            // Although the content downloader is mocked without functionality,
            // the refresh call triggered by download now reads the app added in the
            // while_ block. Thus, instances become non-empty when download refreshes.
            assertThat(sut.instances is_ not - empty)
        }

    @Test
    fun `delete deletes app folder`() {
        val app = App("appName", null, "host/app", "module")
        testAppsDir {
            appBundle("host", "app", ICON)
        } whenBlocking {
            sut.delete(app)
        } then {
            assertThat(
                Path.of(appsDir.toString(), "host", "app").toFile().exists()
                    is_ false
            )
        }
    }

    @Test
    fun `delete removes app from instances`() {
        val app = App("appName", null, "host/app", "module")
        testAppsDir {
            appBundle("host", "app", ICON)
        } whenBlocking {
            sut.delete(app)
        } then {
            assertThat(sut.instances is_ empty)
        }
    }

    @Test
    fun `delete deletes host folder if empty`() {
        val app = App("appName", null, "host/app", "module")
        testAppsDir {
            appBundle("host", "app", ICON)
        } whenBlocking {
            sut.delete(app)
        } then {
            assertThat(
                Path.of(appsDir.toString(), "host").toFile().exists()
                    is_ false
            )
        }
    }

    @Test
    fun `delete keeps host folder if not empty`() {
        val app = App("appName", null, "host/app", "module")
        testAppsDir {
            appBundle("host", "app", ICON)
            createFile("host/otherFile", "otherFileContent")
        } whenBlocking {
            sut.delete(app)
        } then {
            assertThat(
                Path.of(appsDir.toString(), "host").toFile().exists()
                    is_ true
            )
            assertThat(
                Path.of(appsDir.toString(), "host", "app").toFile().exists()
                    is_ false
            )
        }
    }

    // region private

    private fun File.getChild(wantedName: String): File =
        listFiles()!!.single { it.name == wantedName }

    private fun Path.appBundle(
        host: String,
        name: String,
        icon: ByteArray,
        appModule: String? = null,
        nameInManifest: String? = name,
        iconFileName: String? = null,
        appManifestCode: String? = null,
        block: InSubDir.() -> Unit = {}
    ): App {
        val appManifest = filesInSubDir("$host/$name", block).propertySet.run {
            if (nameInManifest === null) this
            else with(APP_NAME, nameInManifest)
        }.run {
            if (appModule === null) this
            else with(APP_MODULE, appModule)
        }.run {
            if (iconFileName === null) this
            else with(APP_ICON, iconFileName)
        }

        createFile(
            "$host/$name/appManifest.json",
            appManifestCode ?: appManifest.toJson()
        )

        return App(
            name,
            icon,
            "$host/$name",
            appModule ?: name
        )
    }

    private fun Path.filesInSubDir(rootPath: String, block: InSubDir.() -> Unit) =
        InSubDir(this, rootPath, block)

    private inner class InSubDir(
        val rootPath: Path,
        val subDirPath: String,
        block: InSubDir.() -> Unit
    ) {
        var propertySet = PropertySet()

        fun file(
            name: String,
            vararg tags: String,
            content: ByteArray = byteArrayOf()
        ) {
            if (tags.contains(APP_ICON)) {
                propertySet = propertySet.with(APP_ICON, name)
            }

            rootPath.createFile("$subDirPath/$name", content)
        }

        init {
            block()
        }
    }

    private fun Path.createFile(subPath: String, content: ByteArray) {
        val file = Path.of(toString(), subPath).toFile()
        file.parentFile.mkdirs()
        file.writeBytes(content)
    }

    private fun Path.createFile(subPath: String, content: String) =
        createFile(subPath, content.toByteArray())

    // endregion
}
