/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.ui.model

import com.nhaarman.mockitokotlin2.mock
import org.junit.jupiter.api.Test
import straightway.crypto.CryptoIdentity
import straightway.expr.minus
import straightway.testing.bdd.Given
import straightway.testing.flow.*
import straightway.utils.deserializeTo
import straightway.utils.serializeToByteArray

class IdentityTest {

    private companion object {
        const val NAME = "Name"
        val IMAGE = byteArrayOf(2, 3, 5, 7)
        val identity: CryptoIdentity = mock(serializable = true)
    }

    private val test get() =
        Given {
            Identity(NAME, IMAGE, identity)
        }

    @Test
    fun `name is accessible`() =
        test when_ {
            name
        } then {
            assertThat(it is_ equal to NAME)
        }

    @Test
    fun `image is accessible`() =
        test when_ {
            image
        } then {
            assertThat(it is_ equal to IMAGE)
        }

    @Test
    fun `cryptoIdentity is accessible`() =
        test when_ {
            cryptoIdentity
        } then {
            assertThat(it is_ equal to identity)
        }

    @Test
    fun `equals with equal values yields true`() =
        test when_ {
            equals(copy())
        } then {
            assertThat(it is_ true)
        }

    @Test
    fun `equals with other object type yields false`() =
        test when_ {
            equals("Hello")
        } then {
            assertThat(it is_ false)
        }

    @Test
    fun `equals with different name yields true anyway`() =
        test when_ {
            equals(copy(name = "other $name"))
        } then {
            assertThat(it is_ true)
        }

    @Test
    fun `equals with different image yields true anyway`() =
        test when_ {
            equals(copy(image = IMAGE + 13))
        } then {
            assertThat(it is_ true)
        }

    @Test
    fun `equals with different crypto identity false`() =
        test when_ {
            equals(copy(cryptoIdentity = mock()))
        } then {
            assertThat(it is_ false)
        }

    @Test
    fun `hashCode for equal objects are equal`() =
        test when_ {
            copy().hashCode()
        } then {
            assertThat(it is_ equal to hashCode())
        }

    @Test
    fun `hashCode with different name equals anyway`() =
        test when_ {
            copy(name = "other $name").hashCode()
        } then {
            assertThat(it is_ equal to hashCode())
        }

    @Test
    fun `hashCode with different image equals anyway`() =
        test when_ {
            copy(image = IMAGE + 13).hashCode()
        } then {
            assertThat(it is_ equal to hashCode())
        }

    @Test
    fun `hashCode with different crypto identity differs`() =
        test when_ {
            copy(cryptoIdentity = mock()).hashCode()
        } then {
            assertThat(it is_ not - equal to hashCode())
        }

    @Test
    fun `is serializable`() =
        test when_ {
            serializeToByteArray()
        } then {
            val reDeSerialized = it.deserializeTo<Identity>()
            assertThat(reDeSerialized.name is_ equal to name)
            assertThat(reDeSerialized.cryptoIdentity is_ not - null_)
        }
}
