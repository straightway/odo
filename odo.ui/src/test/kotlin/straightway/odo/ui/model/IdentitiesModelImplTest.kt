/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.ui.model

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import org.junit.jupiter.api.Test
import straightway.crypto.CryptoFactory
import straightway.crypto.CryptoIdentity
import straightway.crypto.Hasher
import straightway.odo.content.*
import straightway.testing.bdd.Given
import straightway.testing.flow.*
import straightway.utils.toBase62String
import java.nio.file.Files
import java.nio.file.Path
import straightway.testing.flow.assertThat as assertThat

class IdentitiesModelImplTest {

    private companion object {
        val IMAGE = byteArrayOf(2, 3, 5, 7, 11)
        val CRYPTO_IDENTITY = CryptoFactory().cryptoIdentity.createCryptoIdentity()
        val HASH = byteArrayOf(13, 17, 19)
    }

    private fun test(block: Path.() -> Unit) =
        Given {
            object : AutoCloseable {
                val baseDir = Files
                    .createTempDirectory("IdentitiesModelImplTest")
                val identitiesDir = Path.of(baseDir.toString(), "identities").apply {
                    toFile().mkdir()
                    block()
                }
                val hasher: Hasher = mock {
                    on { getHash(any()) }.thenReturn(HASH)
                }
                val cryptoIdentityFactory: CryptoIdentity.Factory = mock {
                    on { getCryptoIdentity(CRYPTO_IDENTITY.rawKeyPair) }.thenReturn(CRYPTO_IDENTITY)
                    on { createCryptoIdentity() }.thenReturn(CRYPTO_IDENTITY)
                }
                val cryptoFactory: CryptoFactory = mock {
                    on { cryptoIdentity }.thenReturn(cryptoIdentityFactory)
                }
                val services: Services = mock {
                    on { hasher }.thenReturn(hasher)
                    on { cryptoFactory }.thenReturn(cryptoFactory)
                }
                val sut = IdentitiesModelImpl(services, identitiesDir)

                override fun close() {
                    baseDir.toFile().deleteRecursively()
                }
            }
        }

    @Test
    fun `repoPath is accessible`() =
        test {} when_ {
            sut.repoPath
        } then {
            assertThat(it is_ equal to identitiesDir)
        }

    @Test
    fun `not existing identities folder yields empty identity instances`() =
        Given {
            IdentitiesModelImpl(mock(), Path.of("......."))
        } when_ {
            instances
        } then {
            assertThat(it is_ empty)
        }

    @Test
    fun `entities folder is created if it does not exist`() =
        test {
            toFile().deleteRecursively()
        } whenBlocking {
            sut.create("Jane Doe", null)
        } then {
            assertThat(identitiesDir.toFile().exists() is_ true)
        }

    @Test
    fun `empty identities folder yields empty identity instances`() =
        test {} when_ {
            sut.instances
        } then {
            assertThat(it is_ empty)
        }

    @Test
    fun `identity in identities folder is initially loaded`() {
        val identity = PropertySet(
            IDENTITY_NAME to "Name",
            IDENTITY_IMAGE to IMAGE.toBase62String(),
            IDENTITY_CRYPTO to CRYPTO_IDENTITY.rawKeyPair.toBase62String()
        )
        test {
            Path.of(toString(), "1.json").toFile().writeText(identity.toJson())
        } when_ {
            sut.instances
        } then {
            assertThat(it.single().name is_ equal to "Name")
            assertThat(it.single().image is_ equal to IMAGE)
            assertThat(
                it.single().cryptoIdentity.rawKeyPair
                    is_ equal to CRYPTO_IDENTITY.rawKeyPair
            )
        }
    }

    @Test
    fun `write addes new identity and stores it persistently`() {
        val identity = Identity(
            "Name",
            IMAGE,
            CRYPTO_IDENTITY
        )
        test {} whenBlocking {
            sut.write(identity)
        } then {
            assertThat(sut.instances.single().isEqualTo(identity) is_ true)
            assertThat(
                identitiesDir.toFile().listFiles()!!.map { it.name }
                    has values("${HASH.toBase62String()}.json")
            )
        }
    }

    private fun Identity.isEqualTo(other: Identity) =
        name == other.name &&
            image.contentEquals(other.image) &&
            cryptoIdentity.rawKeyPair.contentEquals(
                other.cryptoIdentity.rawKeyPair
            )
}
