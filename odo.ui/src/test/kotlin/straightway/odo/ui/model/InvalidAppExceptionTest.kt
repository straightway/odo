/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.ui.model

import org.junit.jupiter.api.Test
import straightway.testing.bdd.Given
import straightway.testing.flow.*

class InvalidAppExceptionTest {
    @Test
    fun `reason is accessible`() =
        Given {
            InvalidAppException(InvalidAppException.Reason.INVALID_APP_DIR)
        } when_ {
            reason
        } then {
            assertThat(it is_ equal to InvalidAppException.Reason.INVALID_APP_DIR)
        }

    @Test
    fun `detail is empty by default`() =
        Given {
            InvalidAppException(InvalidAppException.Reason.INVALID_APP_DIR)
        } when_ {
            detail
        } then {
            assertThat(it is_ empty)
        }

    @Test
    fun `detail is accessible`() =
        Given {
            InvalidAppException(InvalidAppException.Reason.INVALID_APP_DIR, detail = "d")
        } when_ {
            detail
        } then {
            assertThat(it is_ equal to "d")
        }

    @Test
    fun `message is properly composed only with reason`() =
        Given {
            InvalidAppException(InvalidAppException.Reason.INVALID_APP_DIR)
        } when_ {
            message
        } then {
            assertThat(it is_ equal to "INVALID_APP_DIR")
        }

    @Test
    fun `message is properly composed with reason and detail`() =
        Given {
            InvalidAppException(InvalidAppException.Reason.INVALID_APP_DIR, detail = "d")
        } when_ {
            message
        } then {
            assertThat(it is_ equal to "INVALID_APP_DIR: d")
        }

    @Test
    fun `toString yields type and message`() =
        Given {
            InvalidAppException(InvalidAppException.Reason.INVALID_APP_DIR, detail = "d")
        } when_ {
            toString()
        } then {
            assertThat(
                it is_ equal to
                    "straightway.odo.ui.model.InvalidAppException: INVALID_APP_DIR: d"
            )
        }
}
