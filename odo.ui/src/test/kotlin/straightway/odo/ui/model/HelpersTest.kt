/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.ui.model

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import org.junit.jupiter.api.Test
import straightway.crypto.Hasher
import straightway.testing.bdd.Given
import straightway.testing.flow.assertThat
import straightway.testing.flow.equal
import straightway.testing.flow.is_
import straightway.utils.toBase62String
import java.net.URI

class HelpersTest {

    private companion object {
        val HASH = ByteArray(33) { (it * 2 + 1).toByte() }
    }

    private val test get() =
        Given {
            object {
                val hasher: Hasher = mock {
                    on { getHash(any()) }.thenReturn(HASH)
                }
                val uri = URI("ipns://host/path?query")
            }
        }

    @Test
    fun `getFolderName yields base62 string of first 32 bytes of uri host hash`() =
        test when_ {
            uri.getFolderName(hasher)
        } then {
            assertThat(
                it is_ equal to
                    HASH.slice(0..31).toByteArray().toBase62String()
            )
        }

    @Test
    fun `getFolderName yields base62 string of uri host hash for less than 32 bytes`() =
        test when_ {
            val hasher: Hasher = mock {
                on { getHash(any()) }.thenReturn(HASH.slice(0..30).toByteArray())
            }
            uri.getFolderName(hasher)
        } then {
            assertThat(
                it is_ equal to
                    HASH.slice(0..30).toByteArray().toBase62String()
            )
        }

    @Test
    fun `getFolderName calls hasher with URI host`() =
        test when_ {
            uri.getFolderName(hasher)
        } then {
            verify(hasher, times(1)).getHash(uri.host.toByteArray())
        }

    @Test
    fun `URI with null host uses ssp`() =
        test when_ {
            URI("ipns", "ssp", "fragment").getFolderName(hasher)
        } then {
            verify(hasher, times(1)).getHash("ssp".toByteArray())
        }
}
