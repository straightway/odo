/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.ui.model

import org.junit.jupiter.api.Test
import straightway.expr.minus
import straightway.testing.bdd.Given
import straightway.testing.bdd.isNull
import straightway.testing.flow.*
import java.net.URI

class AppTest {

    private companion object {
        const val NAME = "Name"
        val ICON = byteArrayOf(2, 3, 5, 7)
        const val LOCAL_PATH = "c/d/e"
        const val MODULE = "appModule"
        val URI: URI = URI("ipns://lalala")
    }

    private val test get() =
        Given {
            App(
                NAME,
                ICON,
                LOCAL_PATH,
                MODULE,
                URI
            )
        }

    @Test
    fun `name is accessible`() =
        test when_ {
            name
        } then {
            assertThat(it is_ equal to NAME)
        }

    @Test
    fun `icon is accessible`() =
        test when_ {
            icon
        } then {
            assertThat(it is_ equal to ICON)
        }

    @Test
    fun `module is accessible`() =
        test when_ {
            module
        } then {
            assertThat(it is_ equal to MODULE)
        }

    @Test
    fun `localPath is accessible`() =
        test when_ {
            localPath
        } then {
            assertThat(it is_ equal to LOCAL_PATH)
        }

    @Test
    fun `uri is accessible`() =
        test when_ {
            uri
        } then {
            assertThat(it is_ equal to URI)
        }

    @Test
    fun `uri is null by defaul`() =
        Given {
            App(NAME, ICON, LOCAL_PATH, MODULE)
        } when_ {
            uri
        } then isNull

    @Test
    fun `equals if all properties are equal`() =
        test when_ {
            equals(copy(icon = ICON.clone()))
        } then {
            assertThat(it is_ true)
        }

    @Test
    fun `differs with instance of other type`() =
        test when_ {
            equals("Hello")
        } then {
            assertThat(it is_ false)
        }

    @Test
    fun `differs with null`() =
        test when_ {
            equals(null)
        } then {
            assertThat(it is_ false)
        }

    @Test
    fun `differs if name is different`() =
        test when_ {
            equals(copy(name = "OtherName"))
        } then {
            assertThat(it is_ false)
        }

    @Test
    fun `differs if icon is different`() =
        test when_ {
            equals(copy(icon = byteArrayOf()))
        } then {
            assertThat(it is_ false)
        }

    @Test
    fun `differs if localPath is different`() =
        test when_ {
            equals(copy(localPath = "otherPath"))
        } then {
            assertThat(it is_ false)
        }

    @Test
    fun `differs if uri is different`() =
        test when_ {
            equals(copy(uri = URI("ipns://otherUri")))
        } then {
            assertThat(it is_ false)
        }

    @Test
    fun `differs if uri is different by null`() =
        test when_ {
            equals(copy(uri = null))
        } then {
            assertThat(it is_ false)
        }

    @Test
    fun `hashCode equals if all properties are equal`() =
        test when_ {
            copy(icon = ICON.clone()).hashCode()
        } then {
            assertThat(it is_ equal to hashCode())
        }

    @Test
    fun `hashCode differs if name is different`() =
        test when_ {
            copy(name = "OtherName").hashCode()
        } then {
            assertThat(it is_ not - equal to hashCode())
        }

    @Test
    fun `hashCode differs if icon is different`() =
        test when_ {
            copy(icon = byteArrayOf()).hashCode()
        } then {
            assertThat(it is_ not - equal to hashCode())
        }

    @Test
    fun `hashCode differs if localPath is different`() =
        test when_ {
            copy(localPath = "otherPath").hashCode()
        } then {
            assertThat(it is_ not - equal to hashCode())
        }

    @Test
    fun `hashCode differs if uri is different`() =
        test when_ {
            copy(uri = URI("ipns://otherUri")).hashCode()
        } then {
            assertThat(it is_ not - equal to hashCode())
        }

    @Test
    fun `hashCode differs if uri is different by null`() =
        test when_ {
            copy(uri = null).hashCode()
        } then {
            assertThat(it is_ not - equal to hashCode())
        }
}
