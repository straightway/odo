/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.ui.model

import com.nhaarman.mockitokotlin2.*
import org.junit.jupiter.api.Test
import straightway.crypto.CryptoFactory
import straightway.crypto.Cryptor
import straightway.crypto.Hasher
import straightway.odo.cloud.Cloud
import straightway.odo.cloud.ContentUploader
import straightway.odo.cloud.CryptoCloud
import straightway.odo.cloud.SuperCloud
import straightway.odo.content.*
import straightway.testing.bdd.Given
import straightway.testing.bdd.panics
import straightway.testing.flow.*
import straightway.utils.toBase62String
import java.net.URI
import java.nio.file.*

class AppUploaderImplTest {

    private companion object {
        const val HOST = "host"
        const val NEW_ROOT_HOST = "newRoot"
        val ICON = byteArrayOf(2, 3, 5, 7)
        val DEFAULT_APP = App("appName", null, "$HOST/appName", "module")
        val UPLOAD_CONTENT_URI = URI("ipfs://uploadContent")
        val NEW_ROOT_URI = URI("ipns://$NEW_ROOT_HOST")
        val HASH = ByteArray(34) { (it * 2 + 1).toByte() }
        val NEW_ROOT_HASH = ByteArray(34) { (it * 3 + 1).toByte() }
        val NEW_ROOT_HOST_BIN = NEW_ROOT_HOST.toByteArray()
        val NEW_HOST = NEW_ROOT_HASH.slice(0..31).toByteArray().toBase62String()
        val DECRYPTION_KEY = byteArrayOf(41, 43, 47)
        val NEW_ROOT_URI_WITH_DECRYPTION_KEY = NEW_ROOT_URI
            .withContentBundleFlag
            .withFileName(DEFAULT_APP.name)
            .withDecryptonKey(DECRYPTION_KEY)
    }

    private fun test(
        setUpInitialAppDirFiles: Path.() -> Unit
    ) = Given {
        object : AutoCloseable {
            var expectedResult: Any? = null
            val baseDir = Files
                .createTempDirectory("AppsModelTest")
            val appsDir = Path.of(baseDir.toString(), "appsDir").apply {
                toFile().mkdir()
                expectedResult = setUpInitialAppDirFiles()
            }
            val symmetricCryptor: Cryptor = mock {
                on { decryptionKey }.thenReturn(DECRYPTION_KEY)
            }
            val symmetricCryptorFactory: Cryptor.Factory = mock {
                on { createCryptor() }.thenReturn(symmetricCryptor)
                on { getCryptor(DECRYPTION_KEY) }.thenReturn(symmetricCryptor)
            }
            val ipfsCloud: Cloud = mock {
                onBlocking { createRoot() }.thenReturn(NEW_ROOT_URI)
            }
            val encryptingCloud: SuperCloud = mock {
                on { writeVia("ipns") }.thenReturn(ipfsCloud)
            }
            val cryptoCloud: CryptoCloud = mock {
                on { encryptingWith(symmetricCryptor) }.thenReturn(encryptingCloud)
            }
            val cryptoFactory: CryptoFactory = mock {
                on { symmetricCryptor }.thenReturn(symmetricCryptorFactory)
            }
            val contentUploader: ContentUploader = mock {
                onBlocking { upload() }.thenReturn(UPLOAD_CONTENT_URI)
            }
            val dirBuilder: ContentUploader.DirBuilder = mock()
            val cryptoHasher: Hasher = mock {
                on { getHash(any()) }.thenAnswer {
                    if (it.getArgument<ByteArray>(0).contentEquals(NEW_ROOT_HOST_BIN))
                        NEW_ROOT_HASH
                    else HASH
                }
            }
            val services: Services = mock {
                on { cloud }.thenReturn(cryptoCloud)
                on { cryptoFactory }.thenReturn(cryptoFactory)
                on { hasher }.thenReturn(cryptoHasher)
                on { createContentUploader(any(), any(), any()) }
                    .thenAnswer { args ->
                        val block = args.getArgument<ContentUploader.DirBuilder.() -> Unit>(2)
                        dirBuilder.block()
                        contentUploader
                    }
            }
            val sut = AppUploaderImpl(appsDir, services)
            override fun close() {
                baseDir.toFile().deleteRecursively()
            }
        }
    }

    @Test
    fun `upload for directory without appManifest panics`() =
        test {} whenBlocking {
            sut.upload(DEFAULT_APP)
        } then panics {
            assertThat(
                it.state is_ equal to
                    "Cannot upload ${DEFAULT_APP.name}: App folder ${DEFAULT_APP.localPath} not found"
            )
        }

    @Test
    fun `upload creates new symmetric cryptor for app without URI`() =
        test { appBundle() } whenBlocking {
            sut.upload(DEFAULT_APP)
        } then {
            verify(symmetricCryptorFactory, times(1)).createCryptor()
        }

    @Test
    fun `upload of app with URI, but without decryption key, throws`() =
        test { appBundle(URI("ipns://xyz")) } whenBlocking {
            sut.upload(DEFAULT_APP)
        } then panics {
            assertThat(
                it.state is_ equal to
                    "Cannot upload appName: invalid URI ipns://xyz"
            )
        }

    @Test
    fun `upload of app with URI and decryption key gets the cryptor for that key`() =
        test {
            appBundle(URI("ipns://xyz").withDecryptonKey(DECRYPTION_KEY))
        } whenBlocking {
            sut.upload(DEFAULT_APP)
        } then {
            verify(symmetricCryptorFactory, times(1))
                .getCryptor(DECRYPTION_KEY)
        }

    @Test
    fun `upload encrypts using the symmetric cryptor`() =
        test { appBundle() } whenBlocking {
            sut.upload(DEFAULT_APP)
        } then {
            verify(cryptoCloud, times(1)).encryptingWith(symmetricCryptor)
        }

    @Test
    fun `upload uses ipns as cloud`() =
        test { appBundle() } whenBlocking {
            sut.upload(DEFAULT_APP)
        } then {
            verify(encryptingCloud, times(1)).writeVia("ipns")
        }

    @Test
    fun `upload creates a new root if no url is yet defined`() =
        test { appBundle() } whenBlocking {
            sut.upload(DEFAULT_APP)
        } then {
            verifyBlocking(ipfsCloud, times(1)) { createRoot() }
        }

    @Test
    fun `upload recursively uploads all files in the app folder`() =
        test { appBundle() } whenBlocking {
            sut.upload(DEFAULT_APP)
        } then {
            verify(services, times(1)).createContentUploader(
                eq(ipfsCloud),
                eq(Path.of(appsDir.toString(), HOST, DEFAULT_APP.name).toFile()),
                any()
            )
            verify(dirBuilder, times(1)).tree()
        }

    @Test
    fun `upload publishes root content bundle for the new uploaded app`() =
        test { appBundle() } whenBlocking {
            sut.upload(DEFAULT_APP)
        } then {
            verifyBlocking(ipfsCloud, times(1)) {
                publish(
                    NEW_ROOT_URI_WITH_DECRYPTION_KEY,
                    UPLOAD_CONTENT_URI
                )
            }
        }

    @Test
    fun `upload publishes root content bundle for the re-uploaded app`() {
        val appUri = URI("ipns://uploadUri").withDecryptonKey(DECRYPTION_KEY)
        test { appBundle(appUri = appUri) } whenBlocking {
            sut.upload(DEFAULT_APP)
        } then {
            verifyBlocking(ipfsCloud, times(1)) {
                publish(appUri, UPLOAD_CONTENT_URI)
            }
        }
    }

    @Test
    fun `upload of new app moves app to unique app folder`() =
        test {
            appBundle {
                file("fileOnSameLevelAsManifest")
                file(Path.of("subDir", "fileInSubDir").toString())
            }
        } whenBlocking {
            sut.upload(DEFAULT_APP)
        } then {
            assertThat(
                Path.of(appsDir.toString(), HOST, DEFAULT_APP.name).toFile().exists()
                    is_ false
            )
            val newPath = Path.of(appsDir.toString(), NEW_HOST, DEFAULT_APP.name)
            assertThat(
                newPath.toFile()
                    .listFiles()?.map { it.name } is_ equal to setOf(
                    "appManifest.json",
                    "fileOnSameLevelAsManifest",
                    "subDir"
                )
            )
            assertThat(
                Path.of(newPath.toString(), "subDir").toFile()
                    .listFiles()?.map { it.name } is_ equal to setOf(
                    "fileInSubDir"
                )
            )
        }

    @Test
    fun `upload adds publish URI to the app manifest`() =
        test { appBundle() } whenBlocking {
            sut.upload(DEFAULT_APP)
        } then {
            val appDir = Path.of(
                appsDir.toString(),
                NEW_HOST,
                DEFAULT_APP.name
            ).toFile()
            val appManifest = appDir.appManifest
            assertThat(
                appManifest?.properties is_ equal to
                    mapOf(
                        APP_NAME to DEFAULT_APP.name,
                        APP_MODULE to DEFAULT_APP.module,
                        APP_URI to NEW_ROOT_URI_WITH_DECRYPTION_KEY.toString()
                    )
            )
        }

    @Test
    fun `upload leaves app manifest as it is for existing URI`() {
        val appUri = NEW_ROOT_URI.withDecryptonKey(DECRYPTION_KEY)
        test { appBundle(appUri = appUri) } whenBlocking {
            sut.upload(DEFAULT_APP)
        } then {
            val appDir = Path.of(
                appsDir.toString(),
                NEW_HOST,
                DEFAULT_APP.name
            ).toFile()
            val appManifest = appDir.appManifest
            assertThat(
                appManifest?.properties is_ equal to
                    mapOf(
                        APP_NAME to DEFAULT_APP.name,
                        APP_MODULE to DEFAULT_APP.module,
                        APP_URI to appUri.toString()
                    )
            )
        }
    }

    @Test
    fun `upload does not move app already in proper app folder`() {
        val appUri = NEW_ROOT_URI.withDecryptonKey(DECRYPTION_KEY)
        test { appBundle(appUri = appUri, host = NEW_HOST) } whenBlocking {
            sut.upload(
                App(
                    DEFAULT_APP.name,
                    null,
                    "$NEW_HOST/${DEFAULT_APP.name}",
                    "module"
                )
            )
        } then {
            assertThat(
                Path.of(appsDir.toString(), NEW_HOST, DEFAULT_APP.name).toFile().exists()
                    is_ true
            )
        }
    }

    // region private

    private fun Path.appBundle(
        appUri: URI? = null,
        host: String = HOST,
        block: InSubDir.() -> Unit = {}
    ) =
        appBundle(
            host,
            DEFAULT_APP.name,
            ICON,
            DEFAULT_APP.module,
            appUri = appUri,
            block = block
        )

    private fun Path.appBundle(
        host: String,
        name: String,
        icon: ByteArray,
        appModule: String? = null,
        nameInManifest: String? = name,
        iconFileName: String? = null,
        appManifestCode: String? = null,
        appUri: URI? = null,
        block: InSubDir.() -> Unit = {}
    ): App {
        val appManifest = filesInSubDir("$host/$name", block).propertySet.run {
            if (nameInManifest === null) this
            else with(APP_NAME, nameInManifest)
        }.run {
            if (appModule === null) this
            else with(APP_MODULE, appModule)
        }.run {
            if (iconFileName === null) this
            else with(APP_ICON, iconFileName)
        }.run {
            if (appUri === null) this
            else with(APP_URI, appUri.toString())
        }

        createFile(
            "$host/$name/appManifest.json",
            appManifestCode ?: appManifest.toJson()
        )

        return App(
            name,
            icon,
            "$host/$name",
            appModule ?: name
        )
    }

    private fun Path.filesInSubDir(rootPath: String, block: InSubDir.() -> Unit) =
        InSubDir(this, rootPath, block)

    private inner class InSubDir(
        val rootPath: Path,
        val subDirPath: String,
        block: InSubDir.() -> Unit
    ) {
        var propertySet = PropertySet()

        fun file(
            name: String,
            vararg tags: String,
            content: ByteArray = byteArrayOf()
        ) {
            if (tags.contains(APP_ICON)) {
                propertySet = propertySet.with(APP_ICON, name)
            }

            rootPath.createFile("$subDirPath/$name", content)
        }

        init {
            block()
        }
    }

    private fun Path.createFile(subPath: String, content: ByteArray) {
        val file = Path.of(toString(), subPath).toFile()
        file.parentFile.mkdirs()
        file.writeBytes(content)
    }

    private fun Path.createFile(subPath: String, content: String) =
        createFile(subPath, content.toByteArray())

    // endregion
}
