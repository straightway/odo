/*
 * Copyright 2016 straightway
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
plugins {
    id("org.openjfx.javafxplugin") version "0.0.9"
}

javafx {
    version = "13"
    modules = listOf("javafx.controls", "javafx.graphics")
}

dependencies {
    implementation(project(":compiler"))
    implementation(project(":crypto"))
    implementation(project(":error"))
    implementation(project(":odo.boot"))
    implementation(project(":odo.cloud"))
    implementation(project(":odo.content"))
    implementation(project(":odo.loader"))
    implementation(project(":utils"))
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.0.1")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.3.9")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-jdk8:1.3.9")
}