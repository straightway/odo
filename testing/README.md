# testing

This kotlin library makes it more convenient to write unit tests using JUnit 5 in kotlin.
It provides a flow syntax interface for expectations (similar to NUnits Expect syntax), which
is far better readable than the JUnit asserts. These asserts generate human-readable output in
case of a failure. And they "normalize" the comparison of containers and arrays. Another feature
is support for behaviour driven development style tests (BDD, Given-When-Then schema).

## Status

This software is in pre-release state. Every aspect of it may change without announcement or
notification or downward compatibility. As soon as version 1.0 is reached, all subsequent
changes for sub versions will be downward compatible. Breaking changes will then only occur
with a new major version with according deprecation marking.

## Include in gradle builds

To include this library in a gradle build, add

    repositories {
        ...
        maven { url "https://pages.codeberg.org/straightway/repo" }
    }

Then you can simply configure in your dependencies:

    dependencies {
        compile "straightway:testing:<version>"
    }
