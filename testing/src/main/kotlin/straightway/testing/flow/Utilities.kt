/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.testing.flow

import straightway.numbers.compareTo

internal val Any?.asIterable
    get() =
        when (this) {
            is Iterable<*> -> this
            is Array<*> -> this.asIterable()
            is ByteArray -> this.asIterable()
            is CharArray -> this.asIterable()
            is ShortArray -> this.asIterable()
            is IntArray -> this.asIterable()
            is LongArray -> this.asIterable()
            is FloatArray -> this.asIterable()
            is DoubleArray -> this.asIterable()
            is BooleanArray -> this.asIterable()
            is CharSequence -> this.asIterable()
            is Map<*, *> -> this.asIterable()
            else -> throw AssertionError("Cannot convert $this to_ iterable")
        }

internal inline fun <reified T : Any?, reified TComparable : Comparable<T>>
Any?.untypedCompareTo(
    other: T
): Int? =
    when {
        other === null -> null
        this is Number && other is Number -> this.compareTo(other)
        this is TComparable && this is T -> this.compareTo(other)
        else -> null
    }
