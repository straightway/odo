/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.testing.flow

import straightway.expr.DistributedExpr
import straightway.expr.Expr

/*
 * Operator which distributes its arguments to the left and right expressions and yields
 * the logical conjunction of their boolean results.
 */
infix fun Expr.and(other: Expr): Expr = logicalOp("and", other) { a, b -> a && b }

/**
 * Operator which distributes its arguments to the left and right expressions and yields
 * the logical disjunction of their boolean results.
 */
infix fun Expr.or(other: Expr): Expr = logicalOp("or", other) { a, b -> a || b }

private fun Expr.logicalOp(op: String, other: Expr, logic: (Boolean, Boolean) -> Boolean) =
    DistributedExpr(op, this, other) {
        val leftResult = left(*it)
        val rightResult = right(*it)
        when {
            leftResult !is AssertionResult ->
                AssertionResult("Logical $op: Invalid left operand: $leftResult", false)
            rightResult !is AssertionResult ->
                AssertionResult("Logical $op: Invalid right operand: $rightResult", false)
            else ->
                AssertionResult(
                    "[$leftResult $op $rightResult]",
                    logic(leftResult.isSuccessful, rightResult.isSuccessful)
                )
        }
    }
