/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.testing.flow

import straightway.numbers.compareTo
import straightway.utils.formatted
import java.nio.file.Path

/**
 * Relation checking if two objects are equal.
 */
class Equal internal constructor() : EqualBase({ a, b ->
    AssertionResult("${a.formatted()} == ${b.formatted()}", areEqual(a, b))
})

val equal = Equal()

internal fun areEqual(a: Any?, b: Any?): Boolean {
    val aa = a.toArray()
    val ba = b.toArray()
    return if (aa === null || ba === null) areSingleElementsEqual(a, b)
    else areArraysEqual(aa, ba)
}

private fun areSingleElementsEqual(a: Any?, b: Any?): Boolean {
    return when {
        a is Pair<*, *> && b is Pair<*, *> ->
            areEqual(a.first, b.first) && areEqual(a.second, b.second)
        a is Pair<*, *> && b is Map.Entry<*, *> ->
            areEqual(a.first, b.key) && areEqual(a.second, b.value)
        a is Map.Entry<*, *> && b is Pair<*, *> ->
            areEqual(a.key, b.first) && areEqual(a.value, b.second)
        a is Set<*> ->
            if (b is Set<*>) areSetsEqual(a, b) else isSetEqualToCollection(a, b)
        b is Set<*> ->
            isSetEqualToCollection(b, a)
        a is Map<*, *> ->
            if (b is Map<*, *>) areSetsEqual(a.entries, b.entries)
            else isSetEqualToCollection(a.entries, b)
        b is Map<*, *> ->
            isSetEqualToCollection(b.entries, a)
        a is Number && b is Number ->
            a.compareTo(b) == 0
        a is CharSequence && b is Regex ->
            b.matches(a)
        else -> a == b
    }
}

private fun isSetEqualToCollection(set: Set<*>, other: Any?): Boolean {
    val otherArray = other.toArray()
    return otherArray != null && areSetsEqual(set, otherArray.toList())
}

private fun areSetsEqual(a: Set<*>, b: Collection<*>) =
    a.size == b.size && a.all { aItem -> b.any { bItem -> areEqual(aItem, bItem) } }

private fun areArraysEqual(a: Array<*>, b: Array<*>) =
    a.size == b.size && a.indices.all { areEqual(a[it], b[it]) }

private fun Any?.toArray() =
    when (this) {
        is Path -> null
        is Values -> this.elements
        is Array<*> -> this
        is ByteArray -> this.toList().toTypedArray()
        is CharArray -> this.toList().toTypedArray()
        is ShortArray -> this.toList().toTypedArray()
        is IntArray -> this.toList().toTypedArray()
        is LongArray -> this.toList().toTypedArray()
        is FloatArray -> this.toList().toTypedArray()
        is DoubleArray -> this.toList().toTypedArray()
        is BooleanArray -> this.toList().toTypedArray()
        is Map<*, *> -> null
        is Set<*> -> null
        is ClosedRange<*> -> null
        is Iterable<*> -> this.toList().toTypedArray()
        else -> null
    }
