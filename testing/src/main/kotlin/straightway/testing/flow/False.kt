/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.testing.flow

import straightway.expr.FunExpr
import straightway.expr.StateExpr
import straightway.utils.formatted

/**
 * Unary relation checking if an object is boolean false.
 */
class False internal constructor() : Relation, StateExpr<Unary>, FunExpr(
    "False",
    {
        a ->
        AssertionResult("${a.formatted()} is false", (a as? Boolean).let { it != null && !it })
    }
)
