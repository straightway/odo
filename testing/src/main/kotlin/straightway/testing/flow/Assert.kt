/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.testing.flow

import org.junit.jupiter.api.Assertions
import straightway.compiler.Generated
import straightway.error.Panic
import straightway.expr.Expr
import straightway.expr.StateExpr
import straightway.testing.checkActionForException

/**
 * Expect the condition evaluated by the given expression to be true.
 */
fun assertThat(condition: Expr): Unit =
    checkActionForException(
        onFailure = condition::handleFailure
    ) @Generated("jacoco vs. exception") {
        try {
            check(condition)
        } catch (e: Panic) {
            condition.failedExpectationWith(e.state)
        }
    }

fun assertThat(expr: Pair<StateExpr<WithTo>, Any?>): Unit =
    assertThat(expr.first to_ expr.second)

// region Private

@Generated("jacoco vs. exception")
private fun Expr.failedExpectationWith(info: Any) {
    val message =
        "Expectation <${ExpressionVisualizer(this).string}> failed ($info)"
    fail(message)
}

@Generated("jacoco vs. exception")
private fun check(condition: Expr) =
    with(condition() as AssertionResult) {
        if (!isSuccessful) fail("Expectation $explanation failed.")
    }

@Generated("jacoco vs. exception")
private fun Expr.handleFailure(exception: Throwable) {
    if (exception is AssertionError) throw exception
    failedExpectationWith(exception)
}

@Generated("jacoco vs. exception")
private fun fail(message: String) = Assertions.fail<Unit>(message)

// endregion
