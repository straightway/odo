/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.testing

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.fail
import straightway.compiler.Generated
import straightway.error.Panic
import straightway.utils.stackTraceString

fun assertPanics(action: () -> Unit) {
    assertThrows<Panic>(action)
}

@Generated("jacoco vs. exception")
fun assertPanics(expectedState: Any, action: () -> Unit) {
    try {
        action()
        Assertions.fail<Unit>("Action $action did not cause panic")
    } catch (panic: Panic) {
        Assertions.assertEquals(expectedState, panic.state)
    }
}

fun assertFails(action: () -> Unit) {
    assertThrows<AssertionError>(action)
}

fun assertFails(expectedMessage: String, action: () -> Unit) {
    assertThrows<AssertionError>(expectedMessage, action)
}

fun assertFails(expectedMessage: Regex, action: () -> Unit) {
    assertThrows<AssertionError>(expectedMessage, action)
}

fun assertDoesNotThrow(action: () -> Unit) {
    checkActionForException(
        action = action,
        onSuccess = {},
        onFailure = @Generated("jacoco vs. exception") {
            Assertions.fail<Unit>(
                "Action $action threw unexpected exception $it\n${it.stackTraceString}"
            )
        }
    )
}

inline fun <reified TException : Throwable> assertThrows(noinline action: () -> Unit) {
    Assertions.assertThrows(TException::class.java, action)
}

inline fun <reified TException : Throwable> assertThrows(
    expectedMessageRegex: Regex,
    noinline action: () -> Unit
) {
    checkActionForException(
        action = action,
        onSuccess = @Generated("jacoco vs. exception") {
            faileDueToMissingException(action)
        },
        onFailure = @Generated("jacoco vs. exception") {
            it.isExpectedException<TException>(action, expectedMessageRegex)
        }
    )
}

inline fun <reified TException : Throwable> assertThrows(
    expectedMessage: String,
    noinline action: () -> Unit
) {
    checkActionForException(
        action = action,
        onSuccess = @Generated("jacoco vs. exception") {
            faileDueToMissingException(action)
        },
        onFailure = @Generated("jacoco vs. exception") {
            it.isExpectedException<TException>(action, expectedMessage)
        }
    )
}

inline fun <reified T> Any.assertType(typeAction: T.() -> Unit) {
    if (this !is T)
        fail("Expected: ${T::class}, got: $this")
    typeAction(this)
}
