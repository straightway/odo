/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.testing.bdd

import org.junit.jupiter.api.Assertions
import straightway.compiler.Generated
import straightway.error.Panic

/**
 * Reslt checker which does not use the result types.
 */
fun interface ThenUntyped {
    fun check(result: WhenResult<*>)
}

val isNull get() = ThenUntyped {
    it.throwExceptionIfAny()
    Assertions.assertNull(it.result) {
        "result expected to be null, but was: ${it.result}"
    }
}

val fails get() = throws<AssertionError>()

val panics get() = throws<Panic>()

inline fun <reified TException : Throwable> throws() =
    ThenUntyped @Generated("jacoco vs. exception") {
        it.assertThrows<TException> {}
    }

inline fun <reified TException : Throwable> throws(noinline block: (TException) -> Unit) =
    ThenUntyped @Generated("jacoco vs. exception") {
        it.assertThrows(block)
    }

inline fun <reified TException : Throwable> WhenResult<*>.assertThrows(
    block: (TException) -> Unit
) {
    assertIsExceptionResult(this)
    throwExceptionIfOtherThan(block)
    assertException(this)
}

inline fun <reified TException : Throwable> WhenResult<*>.throwExceptionIfOtherThan(
    block: (TException) -> Unit
) {
    if (exception is TException)
        block(exception)
    else
        throwExceptionIfAny()
}

fun assertException(it: WhenResult<*>) {
    Assertions.assertTrue(it.exception != null) {
        "Exception expected, but not thrown"
    }
}
