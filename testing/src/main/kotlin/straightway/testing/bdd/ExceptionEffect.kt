/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.testing.bdd

import org.junit.jupiter.api.fail
import straightway.compiler.Generated
import straightway.utils.decorate
import kotlin.reflect.KClass

val throws = ExceptionEffect()

/**
 * Result checker functions for exceptions.
 */
class ExceptionEffect internal constructor() {

    val nothing = ThenUntyped { it.throwExceptionIfAny() }
    val exception = ThenUntyped {
        assertIsExceptionResult(it)
        decorate @Generated("jacoco vs. exception") {
            if (it.exception == null)
                fail("exception expected, but no exception throws")
        }
    }

    fun exception(exception: KClass<*>) = ThenUntyped {
        throws.exception.check(it)
        if (!exception.isInstance(it.exception))
            throw it.exception!!
    }

    fun <TGiven, TResult> exception(op: TGiven.(Throwable) -> Any) =
        Then<TGiven, TResult> { given, result ->
            throws.exception.check(result)
            val exceptionCheckResult = given.op(result.exception!!)
            checkExceptionCheckResult(exceptionCheckResult, result)
        }

    // region private

    @Generated("jacoco vs. exceptions")
    private fun checkExceptionCheckResult(
        exceptionCheckResult: Any,
        result: WhenResult<*>
    ) {
        when (exceptionCheckResult) {
            true -> Unit
            Unit -> Unit
            false -> fail("unexpected exception: ${result.exception}")
            else -> fail(exceptionCheckResult.toString())
        }
    }

    // endregion
}
