/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.testing.bdd

import org.junit.jupiter.api.Assertions
import straightway.error.Panic

/**
 * Result checker.
 */
fun interface Then<TGiven, TResult> {
    fun check(given: TGiven, result: WhenResult<TResult>)
}

fun <TGiven, TResult> hasEffect(op: TGiven.() -> Unit) =
    Then<TGiven, TResult> { given, result ->
        result.throwExceptionIfAny()
        given.op()
    }

fun <TGiven, TResult> isNull(op: TGiven.() -> Unit) =
    Then<TGiven, TResult> { given, result ->
        isNull.check(result)
        given.op()
    }

fun <TGiven, TResult> fails(op: TGiven.(AssertionError) -> Unit) =
    throws.exception<TGiven, TResult> {
        if (it is AssertionError) {
            op(it)
            true
        } else
            false
    }

fun <TGiven, TResult> panics(op: TGiven.(Panic) -> Unit) =
    throws.exception<TGiven, TResult> {
        if (it is Panic) {
            op(it)
            true
        } else
            false
    }

// region Private

fun assertIsExceptionResult(result: WhenResult<*>) {
    Assertions.assertNull(result.result) {
        "no result expected, but was: ${result.result}"
    }
}

fun WhenResult<*>.throwExceptionIfAny() {
    if (exception != null)
        throw exception
}

// endregion
