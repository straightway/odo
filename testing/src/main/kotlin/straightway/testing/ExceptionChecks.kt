/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.testing

import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions
import straightway.compiler.Generated

@Generated("jacoco vs. exception")
fun <TResult> checkActionForException(
    onSuccess: () -> Unit = {},
    onFailure: (Throwable) -> TResult? = { null },
    action: () -> TResult
): TResult =
    try {
        action().apply { onSuccess() }
    } catch (e: Throwable) {
        val result = onFailure(e)
        if (result != null) result
        else Assertions.fail("Unexpected exception: $e")
    }

@Generated("jacoco vs. exception")
fun <TResult> checkActionForExceptionAsync(
    onSuccess: () -> Unit = {},
    onFailure: (Throwable) -> TResult? = { null },
    action: suspend () -> TResult
): TResult =
    checkActionForException(onSuccess, onFailure) { runBlocking { action() } }

inline fun <reified TException : Throwable> Throwable.isExpectedException(
    noinline action: () -> Unit,
    expectedMessage: String
) {
    if (this !is TException)
        faileTueToWrongExceptionType(action)
    Assertions.assertEquals(expectedMessage, message)
}

inline fun <reified TException : Throwable> Throwable.isExpectedException(
    noinline action: () -> Unit,
    expectedMessageRegex: Regex
) {
    if (this !is TException)
        faileTueToWrongExceptionType(action)
    if (!(expectedMessageRegex.matches(message!!)))
        org.junit.jupiter.api.fail(
            "Unexpected failure message: $message " +
                "(expected pattern: $expectedMessageRegex)"
        )
}

@Generated("jacoco vs. exception")
fun Throwable.faileTueToWrongExceptionType(action: () -> Unit) {
    org.junit.jupiter.api.fail("Action $action threw unexpected exception $this")
}

@Generated("jacoco vs. exception")
fun faileDueToMissingException(action: () -> Unit) {
    org.junit.jupiter.api.fail("Action $action did not throw an exception")
}
