/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
module straightway.testing {
    requires kotlin.stdlib;
    requires kotlin.reflect;
    requires kotlinx.coroutines.core.jvm;
    requires java.base;
    requires straightway.compiler;
    requires straightway.error;
    requires straightway.expr;
    requires straightway.numbers;
    requires straightway.utils;
    requires org.junit.jupiter.api;
    requires org.opentest4j;
    exports straightway.testing;
    exports straightway.testing.bdd;
    exports straightway.testing.flow;
}