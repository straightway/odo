/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.testing.bdd

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.never
import com.nhaarman.mockitokotlin2.verify
import org.junit.jupiter.api.Test
import straightway.error.Panic
import straightway.expr.minus
import straightway.testing.assertDoesNotThrow
import straightway.testing.assertFails
import straightway.testing.flow.assertThat
import straightway.testing.flow.does
import straightway.testing.flow.equal
import straightway.testing.flow.is_
import straightway.testing.flow.not
import straightway.testing.flow.null_
import straightway.testing.flow.throw_
import straightway.testing.flow.to_

class GivenWhenThenTest {

    @Test
    fun exampleFromGivenDocumentation() =
        Given {
            object {
                val value = 3
            }
        } when_ {
            value + 2
        } then {
            assertThat(it is_ not - equal to_ value)
        }

    @Test
    fun `exception in when_ can be evaluated in then using result`() {
        Given {
            "Aaaaah!"
        } when_ {
            typedPanic(this)
        } then panics
    }

    @Test
    fun `exception in when_ can be evaluated in then using exception`() =
        Given {
            "Aaaaah!"
        } when_ {
            throw Panic(this)
        } then panics {
            assertThat(it.state is_ equal to_ "Aaaaah!")
        }

    @Test
    fun `exception in when leads to failing test if not checked`() =
        assertThat(
            {
                Given {} when_ { throw Panic("Aah!") } then {}
            } does throw_.type<Panic>()
        )

    @Test
    fun `exception in when is not hidden by another exception in then`() =
        assertThat(
            {
                Given {} when_ { throw Panic("Aah!") } then { throw CloneNotSupportedException() }
            } does throw_.type<Panic>()
        )

    @Test
    fun `exception in then is not swallowed`() =
        assertThat(
            {
                Given {} when_ {} then { throw Panic("Aaaaah!") }
            } does throw_.type<Panic>()
        )

    @Test
    fun `null result is null`() =
        Given {} when_ { null } then isNull

    @Test
    fun `Unit result is not null`() =
        Given {} when_ {} then { assertThat(it is_ not - null_) }

    @Test
    fun `null result fails with then`() =
        assertFails { Given {} when_ { null } then {} }

    @Test
    fun `null result fails with then throws exception`() =
        assertFails {
            Given {} when_ { null } then throws.exception
        }

    @Test
    fun `null result succeeds with then isNull`() =
        assertDoesNotThrow { Given {} when_ { null } then isNull }

    @Test
    fun `if given object implements AutoCloseable, it is closed when then block is finished`() {
        val autoCloseable = mock<AutoCloseable>()
        Given {
            autoCloseable
        } when_ {} then {
            verify(autoCloseable, never()).close()
        }

        verify(autoCloseable).close()
    }

    @Test
    fun `if given object implements AutoCloseable, it is closed when then block throws`() {
        val autoCloseable = mock<AutoCloseable>()
        try {
            Given {
                autoCloseable
            } when_ {} then {
                verify(autoCloseable, never()).close()
                throw Panic("Aah!")
            }
        } catch (e: Panic) {
            assert(e.state == "Aah!") { "Invalid state" }
        }

        verify(autoCloseable).close()
    }

    private fun typedPanic(state: Any): Int = throw Panic(state)
}
