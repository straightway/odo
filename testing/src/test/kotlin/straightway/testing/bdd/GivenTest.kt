/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.testing.bdd

import org.junit.jupiter.api.Test
import straightway.testing.flow.as_
import straightway.testing.flow.assertThat
import straightway.testing.flow.equal
import straightway.testing.flow.is_
import straightway.testing.flow.same
import straightway.testing.flow.to_

class GivenTest {

    @Test
    fun `construction using a GivenContext object`() {
        val context = Any()
        val sut = Given { context }
        assertThat(sut.context is_ same as_ context)
    }

    @Test
    fun fieldsOfContextAreAccessible() {
        val sut = Given {
            object {
                val field = "Hello"
            }
        }
        assertThat(sut.context.field is_ equal to_ "Hello")
    }

    @Test
    fun `when_ yields GivenWhen instance with given context`() {
        val context = Any()
        val result = Given { context }.when_ { "Hello" }
        assertThat(result.given is_ same as_ context)
    }

    @Test
    fun `when_ yields GivenWhen instance with when result`() {
        val context = Any()
        val givenWhen = Given { context }.when_ { "Hello" }
        assertThat(givenWhen.result.result is_ equal to_ "Hello")
    }

    @Test
    fun `whenAsync yields GivenWhen instance with when result`() {
        val context = Any()
        val givenWhen = Given { context }.whenBlocking { "Hello" }
        assertThat(givenWhen.result.result is_ equal to_ "Hello")
    }

    @Test
    fun `given creates a new given object`() {
        val given = Given { 123 }
        val newGiven = given andGiven { it.toString() }
        assertThat(newGiven.context is_ equal to_ "123")
    }

    @Test
    fun `while_ updates the given object`() {
        val given = Given { object { var num = 82 } }
        val updated = given while_ { ++num }
        assertThat(updated.context.num is_ equal to_ 83)
    }

    @Test
    fun `whileAsync updates the given object`() {
        val given = Given { object { var num = 82 } }
        val updated = given whileBlocking { ++num }
        assertThat(updated.context.num is_ equal to_ 83)
    }
}
