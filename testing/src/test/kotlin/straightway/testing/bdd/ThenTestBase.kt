/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.testing.bdd

import straightway.error.Panic

open class ThenTestBase {
    companion object {
        const val GIVEN_VALUE = 83
        const val RESULT = "83"
        val NULL = WhenResult.null_<String>()
        val SUCCESS = WhenResult(RESULT)
        val EXCEPTION = WhenResult.threw<String>(NullPointerException())
        val PANIC = WhenResult.threw<String>(Panic("Aaaah!"))
        val FAILURE = WhenResult.threw<String>(AssertionError())
    }
}
