/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.testing.bdd

import org.junit.jupiter.api.Test
import straightway.error.Panic
import straightway.testing.flow.*

class WhenResultTest {
    @Test
    fun `constructor sets result`() =
        assertThat(WhenResult(3).result is_ equal to_ 3)

    @Test
    fun `constructor creates null exception`() =
        assertThat(WhenResult(3).exception is_ null)

    @Test
    fun `threw sets exception`() =
        Panic("""Aaaah!""").let {
            assertThat(WhenResult.threw<Int>(it).exception is_ same as_ it)
        }

    @Test
    fun `threw sets result null`() =
        Panic("""Aaaah!""").let {
            val sut = WhenResult.threw<Int>(it)
            assertThat(sut.result is_ null)
        }
}
