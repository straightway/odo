/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.testing.bdd

import org.junit.jupiter.api.Test
import straightway.error.Panic
import straightway.testing.*
import java.lang.AssertionError

class ExceptionEffectTest : ThenTestBase() {

    @Test
    fun construction() =
        assertDoesNotThrow { ExceptionEffect() }

    @Test
    fun `nothing succeeds if no exception is thrown`() =
        assertDoesNotThrow { throws.nothing.check(SUCCESS) }

    @Test
    fun `nothing fails if exception is thrown`() =
        assertPanics { throws.nothing.check(PANIC) }

    @Test
    fun `exception property succeeds if exception is thrown`() =
        assertDoesNotThrow { throws.exception.check(PANIC) }

    @Test
    fun `exception property fails if exception is not thrown with non-null result`() =
        assertFails { throws.exception.check(SUCCESS) }

    @Test
    fun `exception property fails if exception is not thrown with null result`() =
        assertFails { throws.exception.check(NULL) }

    @Test
    fun `exception function with only type succeeds if this type is thrown`() =
        assertDoesNotThrow { throws.exception(Panic::class).check(PANIC) }

    @Test
    fun `exception function with only type succeeds if derived type is thrown`() =
        assertDoesNotThrow { throws.exception(Throwable::class).check(PANIC) }

    @Test
    fun `exception function with only type forwards other thrown exception`() =
        assertThrows<Panic> { throws.exception(AssertionError::class).check(PANIC) }

    @Test
    fun `exception function with only type fails if no exception is thrown`() =
        assertFails { throws.exception(Panic::class).check(SUCCESS) }

    @Test
    fun `exception function with block succeeds if exception and block yields true`() =
        assertDoesNotThrow {
            throws.exception<Int, String> { true }.check(GIVEN_VALUE, PANIC)
        }

    @Test
    fun `exception function with block succeeds if exception and block yields nothing`() =
        assertDoesNotThrow {
            throws.exception<Int, String> {}.check(GIVEN_VALUE, PANIC)
        }

    @Test
    fun `exception function with block fails if exception and block yields false`() =
        assertFails("unexpected exception: ${PANIC.exception}") {
            throws.exception<Int, String> { false }.check(GIVEN_VALUE, PANIC)
        }

    @Test
    fun `exception function with block fails if exception and block yields anything`() =
        assertFails("Lalala") {
            throws.exception<Int, String> { "Lalala" }.check(GIVEN_VALUE, PANIC)
        }

    @Test
    fun `exception function with block fails if no exception thrown`() =
        assertFails {
            throws.exception<Int, String> { "Lalala" }.check(GIVEN_VALUE, SUCCESS)
        }

    @Test
    fun `exception function with block lets exception from check block fly`() =
        assertPanics {
            throws.exception<Int, String> {
                throw Panic("Aaah!")
            }.check(GIVEN_VALUE, FAILURE)
        }
}
