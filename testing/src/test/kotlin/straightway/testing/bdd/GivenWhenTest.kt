/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.testing.bdd

import com.nhaarman.mockitokotlin2.*
import org.junit.jupiter.api.Test
import straightway.testing.assertFails
import straightway.testing.flow.as_
import straightway.testing.flow.assertThat
import straightway.testing.flow.equal
import straightway.testing.flow.is_
import straightway.testing.flow.same
import straightway.testing.flow.to_

class GivenWhenTest {

    @Test
    fun `then calls specified code block`() {
        val context = Any()
        val sut = GivenWhen(context, WhenResult("Hello"))
        var isCalled = false
        sut.then { isCalled = true }
        assert(isCalled)
    }

    @Test
    fun `then is called on given context`() {
        val context = Any()
        val sut = GivenWhen(context, WhenResult("Hello"))
        sut.then { assertThat(this is_ same as_ context) }
    }

    @Test
    fun `then passes the when result`() {
        val context = Any()
        val sut = GivenWhen(context, WhenResult("Hello"))
        sut.then { assertThat(it is_ equal to_ "Hello") }
    }

    @Test
    fun `then fails if result is null`() {
        val context = Any()
        val sut = GivenWhen(context, WhenResult.null_<String>())
        assertFails("result is null") { sut.then { assertThat(it is_ equal to_ "Hello") } }
    }

    @Test
    fun `then auto-closes given value`() {
        val context = mock<AutoCloseable>()
        val sut = GivenWhen(context, WhenResult("Hello"))
        sut.then { verify(context, never()).close() }
        verify(context, times(1)).close()
    }

    @Test
    fun `then auto-closes result value`() {
        val context = Any()
        val result = mock<AutoCloseable>()
        val sut = GivenWhen(context, WhenResult(result))
        sut.then { verify(result, never()).close() }
        verify(result, times(1)).close()
    }

    @Test
    fun `then with typed continuation calls check method on continuation`() {
        val context = Any()
        val result = WhenResult("Lalala")
        val sut = GivenWhen(context, result)
        val cont = mock<Then<Any, String>>()
        sut then cont
        verify(cont, times(1)).check(context, result)
    }

    @Test
    fun `then with typed continuation auto-closes given value`() {
        val context = mock<AutoCloseable>()
        val result = WhenResult("Lalala")
        val sut = GivenWhen(context, result)
        sut then mock<Then<AutoCloseable, String>>()
        verify(context, times(1)).close()
    }

    @Test
    fun `then with typed continuation auto-closes result value`() {
        val context = Any()
        val result = mock<AutoCloseable>()
        val sut = GivenWhen(context, WhenResult(result))
        sut then mock<Then<Any, AutoCloseable>>()
        verify(result, times(1)).close()
    }

    @Test
    fun `then with untyped continuation calls check method on continuation`() {
        val context = Any()
        val result = WhenResult("Lalala")
        val sut = GivenWhen(context, result)
        val cont = mock<ThenUntyped>()
        sut then cont
        verify(cont, times(1)).check(result)
    }

    @Test
    fun `then with untyped continuation auto-closes given value`() {
        val context = mock<AutoCloseable>()
        val result = WhenResult("Lalala")
        val sut = GivenWhen(context, result)
        sut then mock<ThenUntyped>()
        verify(context, times(1)).close()
    }

    @Test
    fun `then with untyped continuation auto-closes result value`() {
        val context = Any()
        val result = mock<AutoCloseable>()
        val sut = GivenWhen(context, WhenResult(result))
        sut then mock<ThenUntyped>()
        verify(result, times(1)).close()
    }
}
