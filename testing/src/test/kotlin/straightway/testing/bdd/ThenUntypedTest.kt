/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.testing.bdd

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.fail
import straightway.error.Panic
import straightway.testing.*

class ThenUntypedTest : ThenTestBase() {

    @Test
    fun `isNull succeeds if result is null`() =
        assertDoesNotThrow { isNull.check(NULL) }

    @Test
    fun `isNull fails if result is not null`() =
        assertFails { isNull.check(SUCCESS) }

    @Test
    fun `isNull throws result exception`() =
        assertPanics { isNull.check(PANIC) }

    @Test
    fun `fails succeeds if result contains AssertionException`() =
        assertDoesNotThrow { fails.check(FAILURE) }

    @Test
    fun `fails fails if non-null result does not contain exception`() =
        assertFails { fails.check(SUCCESS) }

    @Test
    fun `fails fails if null result does not contain exception`() =
        assertFails { fails.check(NULL) }

    @Test
    fun `fails throws result exception other than AssertionException`() =
        assertPanics { fails.check(PANIC) }

    @Test
    fun `panics succeeds if result contains Panic`() =
        assertDoesNotThrow { panics.check(PANIC) }

    @Test
    fun `panics fails if non-null result does not contain exception`() =
        assertFails { panics.check(SUCCESS) }

    @Test
    fun `panics fails if null result does not contain exception`() =
        assertFails { panics.check(NULL) }

    @Test
    fun `panics throws result exception other than Panic`() =
        assertFails { panics.check(FAILURE) }

    @Test
    fun `throws succeeds if result contains specified exception`() =
        assertDoesNotThrow { throws<Panic>().check(PANIC) }

    @Test
    fun `throws fails if non-null result does not contain specified exception`() =
        assertFails { throws<Panic>().check(SUCCESS) }

    @Test
    fun `throws fails if null result does not contain specified exception`() =
        assertFails { throws<Panic>().check(NULL) }

    @Test
    fun `throws throws result exception other than specified exception`() =
        assertFails { throws<Panic>().check(FAILURE) }

    @Test
    fun `throws with block succeed if block does nothing`() =
        assertDoesNotThrow { throws<Panic> { /* do nothing */ }.check(PANIC) }

    @Test
    fun `throws fails if block fails`() =
        assertFails { throws<Panic> { fail("test failure") }.check(PANIC) }
}
