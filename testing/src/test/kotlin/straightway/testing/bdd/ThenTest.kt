/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.testing.bdd

import org.junit.jupiter.api.Test
import straightway.error.Panic
import straightway.testing.*
import straightway.testing.flow.*

class ThenTest : ThenTestBase() {

    @Test
    fun `hasEffect throws result exception and does on call op`() =
        expectFailure<Panic>(PANIC) { hasEffect { it() } }

    @Test
    fun `hasEffect executes op without excpetion and non-null result`() =
        expectSuccess(SUCCESS) { hasEffect { it() } }

    @Test
    fun `hasEffect executes op without excpetion and null result`() =
        expectSuccess(NULL) { hasEffect { it() } }

    @Test
    fun `hasEffect passes given value to op call`() =
        expectSuccess(NULL) {
            hasEffect {
                assertThat(this is_ equal to_ GIVEN_VALUE)
                it()
            }
        }

    @Test
    fun `isNull succeeds and calls op if result is null`() =
        expectSuccess(NULL) { isNull { it() } }

    @Test
    fun `isNull fails and does not call op if result is not null`() =
        expectFailure<AssertionError>(SUCCESS) { isNull { it() } }

    @Test
    fun `isNull fails and does not call op if result contains exception`() =
        expectFailure<Panic>(PANIC) { isNull { it() } }

    @Test
    fun `fails succeeds if assertion failed`() =
        expectSuccess(FAILURE) { fails { it() } }

    @Test
    fun `fails fails without exception`() =
        expectFailure<AssertionError>(SUCCESS) { fails { it() } }

    @Test
    fun `fails fails with other exception`() =
        expectFailure<AssertionError>(PANIC) { fails { it() } }

    @Test
    fun `panics succeeds if assertion failed`() =
        expectSuccess(PANIC) { panics { it() } }

    @Test
    fun `panics fails without exception`() =
        expectFailure<AssertionError>(SUCCESS) { panics { it() } }

    @Test
    fun `panics fails with other exception`() =
        expectFailure<AssertionError>(FAILURE) { panics { it() } }

    // region private

    private fun expectSuccess(
        whenResult: WhenResult<String>,
        sutFactory: (() -> Unit) -> Then<Int, String>
    ) = test(whenResult, sutFactory) { it() }

    private inline fun <reified TException : Throwable> expectFailure(
        whenResult: WhenResult<String>,
        noinline sutFactory: (() -> Unit) -> Then<Int, String>
    ) = test(whenResult, sutFactory) { assertThrows<TException> { it() } }

    private fun test(
        whenResult: WhenResult<String>,
        sutFactory: (() -> Unit) -> Then<Int, String>,
        callChecker: (() -> Unit) -> Unit
    ) {
        var wasCalled = false
        val sut = sutFactory { wasCalled = true }
        var expectSuccess = false
        callChecker {
            sut.check(GIVEN_VALUE, whenResult)
            expectSuccess = true
        }
        assertThat(wasCalled is_ expectSuccess)
    }

    // endregion
}
