/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.testing

import org.junit.jupiter.api.Test
import straightway.testing.flow.assertThat
import straightway.testing.flow.does
import straightway.testing.flow.equal
import straightway.testing.flow.is_
import straightway.testing.flow.throw_
import straightway.testing.flow.to_

class TestBaseTest {

    class TestSUT : TestBase<Int>() {
        fun getSut() = sut
        fun setSut(sut: Int) {
            this.sut = sut
        }
    }

    @Test
    fun callingUnsetSutThrows() =
        assertThat({ TestSUT().getSut() } does throw_.type<NullPointerException>())

    @Test
    fun settingSut() {
        val testSUT = TestSUT()
        testSUT.setSut(83)
        assertThat(testSUT.getSut() is_ equal to_ 83)
    }

    @Test
    fun tearDownResetsSut() {
        val testSUT = TestSUT()
        testSUT.setSut(83)
        testSUT.tearDown()
        assertThat({ testSUT.getSut() } does throw_.type<NullPointerException>())
    }
}
