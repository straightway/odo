/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.testing.flow

import org.junit.jupiter.api.*
import straightway.testing.assertFails

class UtilitiesTest {

    @Test
    fun `list as iterable`() =
        listOf("a", "b", "c").testAsIterableWithExpectedValues("a", "b", "c")

    @Test
    fun `array as iterable`() =
        arrayOf("a", "b", "c").testAsIterableWithExpectedValues("a", "b", "c")

    @Test
    fun `ByteArray as iterable`() =
        byteArrayOf(2, 3, 5).testAsIterableWithExpectedValues<Byte>(2, 3, 5)

    @Test
    fun `CharArray as iterable`() =
        charArrayOf('A', 'B', 'C').testAsIterableWithExpectedValues('A', 'B', 'C')

    @Test
    fun `ShortArray as iterable`() =
        shortArrayOf(2, 3, 5).testAsIterableWithExpectedValues<Short>(2, 3, 5)

    @Test
    fun `IntArray as iterable`() =
        intArrayOf(2, 3, 5).testAsIterableWithExpectedValues(2, 3, 5)

    @Test
    fun `empty IntArray as iterable`() =
        intArrayOf().testAsIterableWithExpectedValues<Int>()

    @Test
    fun `LongArray as iterable`() =
        longArrayOf(2, 3, 5).testAsIterableWithExpectedValues<Long>(2, 3, 5)

    @Test
    fun `FloatArray as iterable`() =
        floatArrayOf(2.0F, 3.0F, 5.0F).testAsIterableWithExpectedValues(2.0F, 3.0F, 5.0F)

    @Test
    fun `DoubleArray as iterable`() =
        doubleArrayOf(2.0, 3.0, 5.0).testAsIterableWithExpectedValues(2.0, 3.0, 5.0)

    @Test
    fun `BooleanArray as iterable`() =
        booleanArrayOf(true, false).testAsIterableWithExpectedValues(true, false)

    @Test
    fun `charSequence as iterable`() =
        "ABC".testAsIterableWithExpectedValues('A', 'B', 'C')

    @Test
    fun `Map as iterable`() =
        mapOf("a" to 1, "b" to 2).testAsIterableWithExpectedValues(
            mapOf("a" to 1).entries.single(),
            mapOf("b" to 2).entries.single()
        )

    @Test
    fun `unsupported type as iterable throws`() =
        assertFails { 3.testAsIterableWithExpectedValues<Any?>() }

    @Test
    fun `null untypedCompareTo non-null is null`() =
        Assertions.assertNull(null.untypedCompareTo(3))

    @Test
    fun `non-null untypedCompareTo null is null`() =
        Assertions.assertNull(3.untypedCompareTo(null as Any?))

    @Test
    fun `untypedCompareTo with incompatible types is null`() =
        Assertions.assertNull(3.untypedCompareTo("lalala"))

    @Test
    fun `untypedCompareTo with compatible type yields comparison result`() =
        Assertions.assertEquals(-1, 0.untypedCompareTo(1))

    @Test
    fun `untypedCompareTo with different number types yields comparison result`() =
        Assertions.assertEquals(-1, 0.untypedCompareTo(1.0))

    // region Private

    private fun <T> Any?.testAsIterableWithExpectedValues(vararg values: T) {
        var numberOfItems = 0
        asIterable.forEachIndexed { index, item ->
            ++numberOfItems
            Assertions.assertEquals(values[index], item)
        }

        Assertions.assertEquals(values.size, numberOfItems)
    }

    // endregion
}
