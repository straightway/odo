/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.testing.flow

import org.junit.jupiter.api.Test
import straightway.expr.minus
import straightway.testing.assertDoesNotThrow
import straightway.testing.assertFails

class RelationTestNull {

    @Test
    fun `null is null`() = assertDoesNotThrow { assertThat(null is_ null_) }

    @Test
    fun `null is not null fails`() = assertFails { assertThat(null is_ not - null_) }

    @Test
    fun `notNull is not null`() = assertDoesNotThrow { assertThat(1 is_ not - null_) }

    @Test
    fun `notNull item is null fails`() = assertFails { assertThat(1 is_ null_) }

    @Test
    fun `tested item is formatted`() =
        assertThat(
            ((arrayOf(1) is_ null_)() as AssertionResult).explanation
                is_ equal to_ "[1] is null"
        )
}
