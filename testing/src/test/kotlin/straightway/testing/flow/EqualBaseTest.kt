/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.testing.flow

import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import straightway.expr.Expr
import straightway.expr.minus

class EqualBaseTest {
    private var lastA: Any? = null
    private var lastB: Any? = null

    @AfterEach
    fun tearDown() {
        lastA = null
        lastB = null
    }

    @Test
    fun construction() =
        assertThat(
            { Sut(AssertionResult("", true)) }
                does not - throw_.exception
        )

    @Test
    fun accept() {
        var visited: Expr? = null
        val sut = sut()
        sut.accept { visited = it }
        assertThat(visited is_ equal to_ sut)
    }

    @Test
    fun invoke() {
        val sut = sut(false)
        assertThat(sut(1, 2) is_ equal to_ result(false))
        assertThat(lastA is_ equal to_ 1)
        assertThat(lastB is_ equal to_ 2)
    }

    // region Private

    private fun sut(result: Boolean = true) = Sut(result(result))

    private fun result(result: Boolean) = AssertionResult("explanation", result)

    private inner class Sut(val result: AssertionResult) :
        EqualBase({ a, b -> lastA = a; lastB = b; result })

    // endregion
}
