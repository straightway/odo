/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.testing.flow

import org.junit.jupiter.api.Test
import straightway.expr.Expr
import straightway.expr.minus
import straightway.testing.assertDoesNotThrow
import straightway.testing.assertFails

class NotTest {
    @Test
    fun construction() =
        assertDoesNotThrow { Not() }

    @Test
    fun accept() {
        var visited: Expr? = null
        not.accept { visited = it }
        assertThat(visited is_ same as_ not)
    }

    @Test
    fun `succeeds on failed condition`() =
        assertThat(1 is_ not - equal to_ 2)

    @Test
    fun `fails on true condition`() =
        assertFails { assertThat(1 is_ not - equal to_ 1) }
}
