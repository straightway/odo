/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.testing.flow

import org.junit.jupiter.api.Test
import straightway.testing.assertFails

class IsTest {
    @Test
    fun `value with relation`() =
        assertThat(1 is_ equal to_ 1)

    @Test
    fun `value with boolean true`() =
        assertThat(true is_ true)

    @Test
    fun `value with boolean false`() =
        assertThat(false is_ false)

    @Test
    fun `non-boolean value with boolean true`() =
        assertFails { assertThat("lala" is_ true) }

    @Test
    fun `non-boolean value with boolean false`() =
        assertFails { assertThat("lala" is_ false) }

    @Test
    fun `object comparison with null`() =
        assertThat(null is_ null)

    @Test
    fun `successful object comparison with not-null`() =
        assertThat("A" is_ "A")

    @Test
    fun `failed object comparison with not-null`() =
        assertFails { assertThat("A" is_ "B") }
}
