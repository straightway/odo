/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.testing.flow

import org.junit.jupiter.api.Test
import straightway.expr.Expr
import straightway.expr.minus
import straightway.testing.assertDoesNotThrow
import straightway.testing.assertFails

class SizeTest {

    @Test
    fun accept() {
        var visited: Expr? = null
        size.accept { visited = it }
        assertThat(visited is_ same as_ size)
    }

    @Test
    fun `success for List`() =
        assertDoesNotThrow { assertThat(listOf(1, 2, 3) has size of 3) }

    @Test
    fun `failure for List`() =
        assertFails { assertThat(listOf(1, 2, 3) has size of 1) }

    @Test
    fun `negated success for List`() =
        assertDoesNotThrow { assertThat(listOf(1, 2, 3) has size of 3) }

    @Test
    fun `negated failure for List`() =
        assertFails { assertThat(listOf(1, 2, 3) has size of 1) }

    @Test
    fun `success for Array`() =
        assertDoesNotThrow { assertThat(arrayOf(1, 2, 3) has size of 3) }

    @Test
    fun `failure for Array`() =
        assertFails { assertThat(arrayOf(1, 2, 3) has size of 1) }

    @Test
    fun `negated success for Array`() =
        assertDoesNotThrow { assertThat(arrayOf(1, 2, 3) has size of 3) }

    @Test
    fun `negated failure for Array`() =
        assertFails { assertThat(arrayOf(1, 2, 3) has size of 1) }

    @Test
    fun `success for String`() =
        assertDoesNotThrow { assertThat("123" has size of 3) }

    @Test
    fun `failure for String`() =
        assertFails { assertThat("123" has size of 1) }

    @Test
    fun `negated success for String`() =
        assertDoesNotThrow { assertThat("123" has size of 3) }

    @Test
    fun `negated failure for String`() =
        assertFails { assertThat("123" has size of 1) }

    @Test
    fun `explanation items are formatted`() =
        assertThat(
            ((arrayOf(1) has size of 1)() as AssertionResult).explanation
                is_ equal to_ "size of (size: 1) [1] == 1"
        )

    @Test
    fun `size of ByteArray`() =
        assertThat(byteArrayOf(1) has size of 1)

    @Test
    fun `size of CharArray`() =
        assertThat(charArrayOf('a') has size of 1)

    @Test
    fun `size of ShortArray`() =
        assertThat(shortArrayOf(1) has size of 1)

    @Test
    fun `size of IntArray`() =
        assertThat(intArrayOf(1) has size of 1)

    @Test
    fun `size of LongArray`() =
        assertThat(longArrayOf(1) has size of 1)

    @Test
    fun `size of FloatArray`() =
        assertThat(floatArrayOf(1.0F) has size of 1)

    @Test
    fun `size of DoubleArray`() =
        assertThat(doubleArrayOf(1.0) has size of 1)

    @Test
    fun `size of BooleanArray`() =
        assertThat(booleanArrayOf(true) has size of 1)

    @Test
    fun `size of ByteArray, passed directly`() =
        assertThat((size(byteArrayOf(1), 1) as AssertionResult).isSuccessful is_ true)

    @Test
    fun `size of CharArray, passed directly`() =
        assertThat((size(charArrayOf('a'), 1) as AssertionResult).isSuccessful is_ true)

    @Test
    fun `size of ShortArray, passed directly`() =
        assertThat((size(shortArrayOf(1), 1) as AssertionResult).isSuccessful is_ true)

    @Test
    fun `size of IntArray, passed directly`() =
        assertThat((size(intArrayOf(1), 1) as AssertionResult).isSuccessful is_ true)

    @Test
    fun `size of LongArray, passed directly`() =
        assertThat((size(longArrayOf(1), 1) as AssertionResult).isSuccessful is_ true)

    @Test
    fun `size of FloatArray, passed directly`() =
        assertThat((size(floatArrayOf(1.0F), 1) as AssertionResult).isSuccessful is_ true)

    @Test
    fun `size of DoubleArray, passed directly`() =
        assertThat((size(doubleArrayOf(1.0), 1) as AssertionResult).isSuccessful is_ true)

    @Test
    fun `size of BooleanArray, passed directly`() =
        assertThat((size(booleanArrayOf(true), 1) as AssertionResult).isSuccessful is_ true)

    @Test
    fun `non-numerical size fails`() =
        assertThat(arrayOf(1, 2, 3) has not - size of "a")
}
