/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.testing.flow

import org.junit.jupiter.api.Test
import straightway.expr.Expr
import straightway.testing.assertDoesNotThrow
import straightway.testing.assertFails
import straightway.testing.bdd.Given

class ValuesTest {

    @Test
    fun accept() {
        var visited: Expr? = null
        val sut = values(1, 2, 3)
        sut.accept { visited = it }
        assertThat(visited is_ same as_ sut)
    }

    @Test
    fun `empty collection has not elements`() =
        assertFails { assertThat(arrayOf<Int>() has values(2)) }

    @Test
    fun `single element collection with searched element succeeds`() =
        assertDoesNotThrow { assertThat(arrayOf(2) has values(2)) }

    @Test
    fun `single element collection without searched element fails`() =
        assertFails { assertThat(arrayOf(2) has values(3)) }

    @Test
    fun `single element collection with one of two searched elements fails`() =
        assertFails { assertThat(arrayOf(2) has values(2, 3)) }

    @Test
    fun `two element collection with both searched elements succeeds`() =
        assertDoesNotThrow { assertThat(arrayOf(3, 2) has values(2, 3)) }

    @Test
    fun `three element collection with both searched elements succeeds`() =
        assertDoesNotThrow { assertThat(arrayOf(5, 3, 2) has values(2, 3)) }

    @Test
    fun `works with List`() =
        assertDoesNotThrow { assertThat(listOf(2) has values(2)) }

    @Test
    fun `works with String`() =
        assertDoesNotThrow { assertThat("Hello World" has values('l', 'W')) }

    @Test
    fun `convert from collection`() =
        assertDoesNotThrow { assertThat("Hello World" has listOf('l', 'W').values()) }

    @Test
    fun `toString contains checked references`() =
        Given { values(1, 2, 3) } when_ { toString() } then {
            assertThat(it is_ equal to_ "Values[1, 2, 3]")
        }

    @Test
    fun `explanation items are formatted`() =
        assertThat(
            (values(arrayOf(1))(listOf(arrayOf(2))) as AssertionResult).explanation
                is_ equal to_ "[[2]] contains all [[1]]"
        )

    @Test
    fun `compare items of sub collections`() =
        assertThat(listOf(byteArrayOf(1)) has values(byteArrayOf(1)))
}
