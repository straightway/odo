/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.testing.flow

import org.junit.jupiter.api.Test
import straightway.expr.Expr
import straightway.expr.minus
import straightway.testing.bdd.Given

class ReferencesTest {

    private data class Item(val x: Int)

    @Test
    fun accept() {
        var visited: Expr? = null
        val sut = references(2)
        sut.accept { visited = it }
        assertThat(visited is_ same as_ sut)
    }

    @Test
    fun `empty collection has not references`() =
        Given { listOf<Item>() } when_ { Item(2) } then {
            assertThat(this has not - references(it))
        }

    @Test
    fun `single element collection with searched reference`() =
        Given { listOf(Item(2)) } when_ { first() } then {
            assertThat(this has references(it))
        }

    @Test
    fun `single element collection with searched equal but not same reference`() =
        Given { listOf(Item(2)) } when_ { Item(2) } then {
            assertThat(this has not - references(it))
        }

    @Test
    fun `multi element collection with single searched reference`() =
        Given { listOf(Item(3), Item(2)) } when_ { last() } then {
            assertThat(this has references(it))
        }

    @Test
    fun `multi element collection with multiple searched references`() =
        Given { listOf(Item(3), Item(2)) } when_ { toTypedArray() } then {
            assertThat(this has references(*it))
        }

    @Test
    fun `multi element collection with multiple searched references, but one missing`() =
        Given {
            listOf(Item(3), Item(2))
        } when_ {
            (this + Item(5) + last()).toTypedArray()
        } then {
            assertThat(this has not - references(*it))
        }

    @Test
    fun `toString contains checked references`() =
        Given { references(1, 2, 3) } when_ { toString() } then {
            assertThat(it is_ equal to_ "References[1, 2, 3]")
        }

    @Test
    fun `name items are formatted`() =
        assertThat(references(byteArrayOf(1)).name is_ equal to_ "References[[01]]")

    @Test
    fun `explanation items are formatted`() =
        assertThat(
            (
                (listOf(arrayOf(1)) has references(byteArrayOf(1)))()
                    as AssertionResult
                ).explanation
                is_ equal to_ "[[1]] contains all [[01]] by reference"
        )
}
