/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.testing.flow

import org.junit.jupiter.api.Test
import straightway.expr.Expr
import straightway.expr.minus
import straightway.testing.assertDoesNotThrow
import straightway.testing.assertFails

class EmptyTest {

    @Test
    fun accept() {
        var visited: Expr? = null
        empty.accept { visited = it }
        assertThat(visited is_ same as_ empty)
    }

    @Test
    fun `succeeds on empty collection`() =
        assertDoesNotThrow { assertThat(listOf<Int>() is_ empty) }

    @Test
    fun `fails on non empty collection`() =
        assertFails(Regex("Expectation \\[1] is empty failed.*")) {
            assertThat(listOf(1) is_ empty)
        }

    @Test
    fun `negation succeeds on non empty collection`() =
        assertDoesNotThrow { assertThat(listOf(1) is_ not - empty) }

    @Test
    fun `negation fails on empty collection`() =
        assertFails(Regex("Expectation not \\[] is empty failed.*")) {
            assertThat(listOf<Int>() is_ not - empty)
        }

    @Test
    fun `succeeds on empty array`() =
        assertDoesNotThrow { assertThat(arrayOf<Int>() is_ empty) }

    @Test
    fun `fails on non empty array`() =
        assertFails(Regex("Expectation \\[1] is empty failed.*")) {
            assertThat(arrayOf(1) is_ empty)
        }

    @Test
    fun `negation succeeds on non empty array`() =
        assertDoesNotThrow { assertThat(arrayOf(1) is_ not - empty) }

    @Test
    fun `negation fails on empty array`() =
        assertFails(Regex("Expectation not \\[] is empty failed.*")) {
            assertThat(arrayOf<Int>() is_ not - empty)
        }

    @Test
    fun `succeeds on empty string`() =
        assertDoesNotThrow { assertThat("" is_ empty) }

    @Test
    fun `fails on non empty string`() =
        assertFails(Regex("Expectation \"Hello\" is empty failed.*")) {
            assertThat("Hello" is_ empty)
        }

    @Test
    fun `negation succeeds on non empty string`() =
        assertDoesNotThrow { assertThat("Hello" is_ not - empty) }

    @Test
    fun `negation fails on empty string`() =
        assertFails(Regex("Expectation not \"\" is empty failed.*")) {
            assertThat("" is_ not - empty)
        }

    @Test
    fun `succeeds on empty map`() =
        assertDoesNotThrow { assertThat(mapOf<String, Any>() is_ empty) }

    @Test
    fun `fails on non-empty map`() =
        assertFails(Regex("Expectation \\{1=\"A\"} is empty failed.*")) {
            assertThat(mapOf(1 to "A") is_ empty)
        }

    @Test
    fun `fails for non collections`() =
        assertFails { assertThat(2 is_ empty) }

    @Test
    fun `negation fails for non collections`() =
        assertFails { assertThat(2 is_ not - empty) }

    @Test
    fun `fails for null`() =
        assertFails { assertThat(null is_ empty) }

    @Test
    fun `negation fails for null`() =
        assertFails { assertThat(null is_ not - empty) }
}
