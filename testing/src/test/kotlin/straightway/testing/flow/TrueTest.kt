/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.testing.flow

import org.junit.jupiter.api.Test
import straightway.expr.Expr
import straightway.testing.assertFails

class TrueTest {

    @Test
    fun accept() {
        var visited: Expr? = null
        val sut = True()
        sut.accept { visited = it }
        assertThat(visited is_ same as_ sut)
    }

    @Test
    fun `succeeds with true`() =
        assertThat(true is_ True())

    @Test
    fun `fails with false`() =
        assertFails { assertThat(false is_ True()) }

    @Test
    fun `fails with null`() =
        assertFails { assertThat(null is_ True()) }

    @Test
    fun `fails with anything else`() =
        assertFails { assertThat(1 is_ True()) }
}
