/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.testing.flow

import org.junit.jupiter.api.Test
import straightway.error.Panic
import straightway.expr.minus
import straightway.testing.assertDoesNotThrow
import straightway.testing.assertFails
import java.io.InvalidClassException

class EffectTestThrow {
    @Test
    fun passes() =
        assertDoesNotThrow { assertThat(::panic does throw_.exception) }

    @Test
    fun passes_withSpecificException() =
        assertDoesNotThrow { assertThat(::panic does throw_.type<Panic>()) }

    @Test
    fun negation_passes() =
        assertDoesNotThrow { assertThat(::throwNothing does not - throw_.exception) }

    @Test
    fun negation_passes_withSpecificException() =
        assertDoesNotThrow { assertThat(::panic does not - throw_.type<NullPointerException>()) }

    @Test
    fun fails() =
        assertFails { assertThat(::throwNothing does throw_.exception) }

    @Test
    fun fails_withSpecificException() =
        assertFails { assertThat(::panic does throw_.type<NullPointerException>()) }

    @Test
    fun negation_fails() =
        assertFails { assertThat(::panic does not - throw_.exception) }

    @Test
    fun fails_withMeaningfulMessage() =
        assertFails(
            Regex("Expectation class kotlin.Throwable thrown, but nothing thrown.*")
        ) {
            assertThat(::throwNothing does throw_.exception)
        }

    @Test
    fun `fails with different exception and stack trace`() =
        assertFails(
            Regex(
                ".*stack trace:.*straightway.testing.flow.EffectTestThrowKt.panic.*",
                RegexOption.DOT_MATCHES_ALL
            )
        ) {
            assertThat(::panic does throw_.type<InvalidClassException>())
        }

    @Test
    fun negated_fails_withMeaningfulMessage() =
        assertFails(
            Regex("Expectation not exception class kotlin.Throwable thrown failed.*")
        ) {
            assertThat(::panic does not - throw_.exception)
        }
}

private fun throwNothing() {}
private fun panic() {
    throw Panic("panic!")
}
