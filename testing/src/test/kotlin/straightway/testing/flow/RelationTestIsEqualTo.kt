/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.testing.flow

import org.junit.jupiter.api.Assertions.assertTimeoutPreemptively
import org.junit.jupiter.api.Test
import straightway.expr.minus
import straightway.testing.assertDoesNotThrow
import straightway.testing.assertFails
import straightway.utils.formatted
import java.nio.file.Path
import java.time.Duration
import java.time.LocalDateTime

class RelationTestIsEqualTo {

    @Test
    fun passes() = assertDoesNotThrow { assertThat(1 is_ equal to_ 1) }

    @Test
    fun fails() = assertFails { assertThat(1 is_ equal to_ 2) }

    @Test
    fun negation_passes() = assertDoesNotThrow { assertThat(1 is_ not - equal to_ 2) }

    @Test
    fun number_range_smallerFirst_passes() =
        assertDoesNotThrow { assertThat(0.9 is_ equalWithin(0.2) to_ 1.0) }

    @Test
    fun number_range_biggerFirst_passes() =
        assertDoesNotThrow { assertThat(1.1 is_ equalWithin(0.2) to_ 1.0) }

    @Test
    fun number_range_fails() =
        assertFails { assertThat(1.3 is_ equalWithin(0.2) to_ 1.0) }

    @Test
    fun number_range_negation_passes() =
        assertDoesNotThrow { assertThat(1.3 is_ not - equalWithin(0.2) to_ 1.0) }

    @Test
    fun duration_range_smallerFirst_passes() = assertDoesNotThrow {
        assertThat(
            LocalDateTime.of(0, 1, 1, 0, 0, 0)
                is_ equalWithin(Duration.ofDays(2)) to_ LocalDateTime.of(0, 1, 2, 0, 0, 0)
        )
    }

    @Test
    fun duration_range_biggerFirst_passes() = assertDoesNotThrow {
        assertThat(
            LocalDateTime.of(0, 1, 2, 0, 0, 0)
                is_ equalWithin(Duration.ofDays(2)) to_ LocalDateTime.of(0, 1, 1, 0, 0, 0)
        )
    }

    @Test
    fun duration_range_fails() = assertFails {
        assertThat(
            LocalDateTime.of(0, 1, 1, 0, 0, 0)
                is_ equalWithin(Duration.ofDays(2)) to_ LocalDateTime.of(0, 1, 3, 0, 0, 0)
        )
    }

    @Test
    fun duration_range_negation_passes() = assertDoesNotThrow {
        assertThat(
            LocalDateTime.of(0, 1, 1, 0, 0, 0)
                is_ not - equalWithin(Duration.ofDays(2))
                to_ LocalDateTime.of(0, 1, 3, 0, 0, 0)
        )
    }

    @Test
    fun `equal arrays`() = assertDoesNotThrow {
        assertThat(arrayOf<Int>() is_ equal to_ arrayOf<Int>())
    }

    @Test
    fun `array and Values item`() = assertDoesNotThrow {
        assertThat(arrayOf(1, 2, 3) is_ equal to_ values(1, 2, 3))
    }

    @Test
    fun `iterable and Values item`() = assertDoesNotThrow {
        assertThat(listOf(1, 2, 3) is_ equal to_ values(1, 2, 3))
    }

    @Test
    fun `map and Values item`() = assertDoesNotThrow {
        assertThat(
            mapOf(Pair("A", 1), Pair("B", 2)) is_ equal to_
                values(Pair("A", 1), Pair("B", 2))
        )
    }

    @Test
    fun `array in array`() =
        assertThat(arrayOf(arrayOf(1)) is_ equal to_ arrayOf(arrayOf(1)))

    @Test
    fun `array in list with values`() =
        assertThat(listOf(arrayOf(1)) is_ equal to_ values(arrayOf(1)))

    @Test
    fun `array in map with values`() =
        assertThat(mapOf("a" to arrayOf(1)) is_ equal to_ values("a" to arrayOf(1)))

    @Test
    fun `compare map with values not containing pairs`() =
        assertThat(mapOf("a" to 1) is_ not - equal to_ values("a"))

    @Test
    fun `compare array to list`() =
        assertThat(arrayOf(1, 2, 3) is_ equal to_ listOf(1, 2, 3))

    @Test
    fun `compare list to array`() =
        assertThat(listOf(1, 2, 3) is_ equal to_ arrayOf(1, 2, 3))

    @Test
    fun `compare ByteArray to unspecific array`() =
        assertThat(byteArrayOf(1) is_ equal to_ arrayOf<Byte>(1))

    @Test
    fun `compare CharArray to unspecific array`() =
        assertThat(charArrayOf('a') is_ equal to_ arrayOf('a'))

    @Test
    fun `compare ShortArray to unspecific array`() =
        assertThat(shortArrayOf(1) is_ equal to_ arrayOf<Short>(1))

    @Test
    fun `compare IntArray to unspecific array`() =
        assertThat(intArrayOf(1) is_ equal to_ arrayOf(1))

    @Test
    fun `compare LongArray to unspecific array`() =
        assertThat(longArrayOf(1) is_ equal to_ arrayOf<Long>(1))

    @Test
    fun `compare FloatArray to unspecific array`() =
        assertThat(floatArrayOf(1.0F) is_ equal to_ arrayOf(1.0F))

    @Test
    fun `compare DoubleArray to unspecific array`() =
        assertThat(doubleArrayOf(1.0) is_ equal to_ arrayOf(1.0))

    @Test
    fun `compare BooleanArray to unspecific array`() =
        assertThat(booleanArrayOf(true) is_ equal to_ arrayOf(true))

    @Test
    fun `compare set to set`() =
        assertThat(setOf(1, 2, 3) is_ equal to_ setOf(3, 2, 1))

    @Test
    fun `compare set to collection`() =
        assertThat(setOf(1, 2, 3) is_ equal to_ listOf(3, 2, 1))

    @Test
    fun `compare set to array`() =
        assertThat(setOf(1, 2, 3) is_ equal to_ arrayOf(3, 2, 1))

    @Test
    fun `compare set to number`() =
        assertThat(setOf(1, 2, 3) is_ not - equal to_ 1)

    @Test
    fun `compare collection to set`() =
        assertThat(listOf(1, 2, 3) is_ equal to_ setOf(3, 2, 1))

    @Test
    fun `compare sets of different size`() =
        assertThat(setOf(1, 2, 3) is_ not - equal to_ setOf(1, 2, 3, 4))

    @Test
    fun `compare sets deeply`() =
        assertThat(setOf(arrayOf(1)) is_ equal to_ setOf(arrayOf(1)))

    @Test
    fun `compare map to map`() =
        assertThat(mapOf("a" to 1, "b" to 2) is_ equal to_ mapOf("b" to 2, "a" to 1))

    @Test
    fun `compare array to map`() =
        assertThat(arrayOf("a" to 1, "b" to 2) is_ equal to_ mapOf("b" to 2, "a" to 1))

    @Test
    fun `compare a Pair to a equal Map Entry`() =
        assertThat(("a" to 1) is_ equal to_ mapOf("a" to 1).entries.single())

    @Test
    fun `compare a Pair to a Map Entry differing in first component`() =
        assertThat(("b" to 1) is_ not - equal to_ mapOf("a" to 1).entries.single())

    @Test
    fun `compare a Pair to a Map Entry differing in second component`() =
        assertThat(("a" to 2) is_ not - equal to_ mapOf("a" to 1).entries.single())

    @Test
    fun `compare a Map Entry to an equal Pair`() =
        assertThat(mapOf("a" to 1).entries.single() is_ equal to_ ("a" to 1))

    @Test
    fun `compare a Map Entry to a Pair differing in first component`() =
        assertThat(mapOf("b" to 1).entries.single() is_ not - equal to_ ("a" to 1))

    @Test
    fun `compare a Map Entry to a Pair differing in second component`() =
        assertThat(mapOf("a" to 2).entries.single() is_ not - equal to_ ("a" to 1))

    @Test
    fun `compare two equal pairs`() =
        assertThat(("a" to 1) is_ equal to_ ("a" to 1))

    @Test
    fun `compare two pairs differing in first component`() =
        assertThat(("b" to 1) is_ not - equal to_ ("a" to 1))

    @Test
    fun `compare two pairs differing in second component`() =
        assertThat(("a" to 2) is_ not - equal to_ ("a" to 1))

    @Test
    fun `compare two incompatible value`() =
        assertThat(("a" to 1) is_ not - equal to_ 83)

    @Test
    fun `string representation of first Equal item is formatted`() =
        assertThat((arrayOf(1) is_ equal to_ 2)().formatted() is_ equal to_ "Failure: [1] == 2")

    @Test
    fun `string representation of second Equal item is formatted`() =
        assertThat((1 is_ equal to_ arrayOf(2))().formatted() is_ equal to_ "Failure: 1 == [2]")

    @Test
    fun `string representation of first EqualWithin item is formatted`() =
        assertThat(
            (arrayOf(1) is_ equalWithin(3) to_ 2)().formatted()
                is_ equal to_ "Failure: [1] == 2 [+/- 3]"
        )

    @Test
    fun `string representation of second EqualWithin item is formatted`() =
        assertThat(
            (1 is_ equalWithin(3) to_ arrayOf(2))().formatted()
                is_ equal to_ "Failure: 1 == [2] [+/- 3]"
        )

    @Test
    fun `string representation of range of EqualWithin item is formatted`() =
        assertThat(
            (1 is_ equalWithin(arrayOf(2)) to_ 3)().formatted()
                is_ equal to_ "Failure: 1 == 3 [+/- [2]]"
        )

    @Test
    fun `ranges are compated by borders`() =
        assertTimeoutPreemptively(Duration.ofMillis(1000)) {
            assertThat(Long.MIN_VALUE..Long.MAX_VALUE is_ equal to_ Long.MIN_VALUE..Long.MAX_VALUE)
        }

    @Test
    fun `equal numbers of different types are equal`() =
        assertThat(1 is_ equal to_ 1.toByte())

    @Test
    fun `equal for large arrays`() = assertTimeoutPreemptively(Duration.ofMillis(10000)) {
        assertThat(
            ByteArray(0x100000) { it.toByte() } is_ equal
                to_ ByteArray(0x100000) { it.toByte() }
        )
    }

    @Test
    fun `compare arrays of different size`() =
        assertThat(arrayOf(1, 2, 3) is_ not - equal to_ arrayOf(1, 2, 3, 4))

    @Test
    fun `compare arrays of same size with one different item`() =
        assertThat(arrayOf(1, 2, 3) is_ not - equal to_ arrayOf(1, 3, 4))

    @Test
    fun `construction does not throw`() =
        assertThat({ Equal() } does not - throw_.exception)

    @Test
    fun `compare string with regex succeeds`() =
        assertThat("abcd" is_ equal to_ Regex("abcd"))

    @Test
    fun `compare string with regex fails`() =
        assertThat("abcde" is_ not - equal to_ Regex("abcd"))

    @Test
    fun `assertThat can be used with to without underscore`() =
        assertThat(1L is_ equal to 1)

    @Test
    fun `comparison of Path objects`() =
        assertThat(
            Path.of("a", "b", "c") is_ equal
                to_ Path.of("a", "b", "c")
        )

    @Test
    fun `comparison of two maps`() =
        assertThat(
            mapOf("a" to "A", "b" to 3, "c" to true) is_ equal
                to_ mapOf("a" to "A", "b" to 3, "c" to true)
        )
}
