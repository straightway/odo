/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.testing.flow

import org.junit.jupiter.api.Test
import straightway.expr.Expr
import straightway.expr.minus
import straightway.testing.assertDoesNotThrow
import straightway.testing.assertFails

class RelationTestIsSameAs {

    @Test
    fun accept() {
        var visited: Expr? = null
        same.accept { visited = it }
        assertThat(visited is_ same as_ same)
    }

    @Test
    fun passes() = assertDoesNotThrow { assertThat(a is_ same as_ a) }

    @Test
    fun negation_passes() = assertDoesNotThrow { assertThat(a is_ not - same as_ b) }

    @Test
    fun isSameAs_fails() = assertFails { assertThat(a is_ same as_ b) }

    @Test
    fun isNotSameAs_fails() = assertFails { assertThat(a is_ not - same as_ a) }

    @Test
    fun `explanation items are formatted`() =
        assertThat(
            ((arrayOf(1) is_ same as_ arrayOf(1))() as AssertionResult).explanation
                is_ equal to_ "[1] === [1]"
        )

    @Test
    fun `construction does not throw`() =
        assertThat({ Same() } does not - throw_.exception)

    private data class EqualButNotSame(val value: Int)
    private companion object {
        val a = EqualButNotSame(1)
        val b = EqualButNotSame(1)
    }
}
