/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.testing.flow

import org.junit.jupiter.api.Test
import straightway.expr.minus
import straightway.testing.assertDoesNotThrow
import straightway.testing.assertFails

class RelationTestGreaterThan {

    @Test
    fun succeeds_int() = assertDoesNotThrow { assertThat(2 is_ greater than 1) }

    @Test
    fun succeeds_mixed() = assertDoesNotThrow { assertThat(OtherComparable is_ greater than 1) }

    @Test
    fun fails() = assertFails { assertThat(1 is_ greater than 1) }

    @Test
    fun negated_succeeds() = assertDoesNotThrow { assertThat(1 is_ not - greater than 1) }

    @Test
    fun negated_fails() = assertFails { assertThat(2 is_ not - greater than 1) }

    @Test
    fun `comparison with uncomparable first item is false`() =
        assertFails { assertThat(arrayOf(2) is_ greater than 3) }

    @Test
    fun `comparison with uncomparable second item is false`() =
        assertFails { assertThat(2 is_ greater than arrayOf(3)) }

    @Test
    fun `items are formatted`() =
        assertThat(
            ((arrayOf(1) is_ greater than arrayOf(2))() as AssertionResult).explanation
                is_ equal to_ "[1] > [2]"
        )

    private object OtherComparable : Comparable<Int> {
        override fun compareTo(other: Int) = other
    }
}
