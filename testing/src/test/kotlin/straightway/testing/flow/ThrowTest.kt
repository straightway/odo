/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.testing.flow

import org.junit.jupiter.api.Test
import straightway.error.Panic
import straightway.expr.Expr
import straightway.expr.minus
import straightway.testing.assertDoesNotThrow

class ThrowTest {

    @Test
    fun construction() = assertDoesNotThrow { Throw() }

    @Test
    fun accept() {
        var visited: Expr? = null
        throw_.accept { visited = it }
        assertThat(visited is_ same as_ throw_)
    }

    @Test
    fun `expected exception`() =
        assertThat({ throw Panic("Aaaah!") } does throw_.exception)

    @Test
    fun `unexpected exception`() =
        assertThat({} does not - throw_.exception)

    @Test
    fun `exception of onexpected type`() {
        assertThat({ throw NullPointerException() } does not - throw_.type<Panic>())
    }
}
