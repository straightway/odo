/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.testing.flow

import org.junit.jupiter.api.Test
import straightway.expr.BoundExpr
import straightway.expr.Expr
import straightway.expr.FunExpr
import straightway.expr.Value

class ExpressionVisualizerTest {
    @Test
    fun value_visualizedDirectly() {
        val testedExpr = Value("Hello")
        assertThat(ExpressionVisualizer(testedExpr).string is_ equal to_ "Hello")
    }

    @Test
    fun monadicOp_visualizedFunctionCallStyle() {
        val testedExpr = func(1) - "arg"
        assertThat(ExpressionVisualizer(testedExpr).string is_ equal to_ "func1(arg)")
    }

    @Test
    fun dyadicOp_visualizedOperatorStyle() {
        val testedExpr = func(2) - "arg1" - "arg2"
        assertThat(ExpressionVisualizer(testedExpr).string is_ equal to_ "arg1 func2 arg2")
    }

    @Test
    fun arity3Op_visualizedFunctionCallStyle() {
        val testedExpr = func(3) - "arg1" - "arg2" - "arg3"
        assertThat(ExpressionVisualizer(testedExpr).string is_ equal to_ "func3(arg1, arg2, arg3)")
    }

    @Test
    fun combinedMonadicDyadicOp() {
        val testedExpr = func(1) - func(2) - "arg1" - "arg2"
        assertThat(ExpressionVisualizer(testedExpr).string is_ equal to_ "arg1 func1-func2 arg2")
    }

    @Test
    fun combinedDyadicMonadicOp() {
        val testedExpr = func(2) - func(1) - "arg1" - "arg2"
        assertThat(ExpressionVisualizer(testedExpr).string is_ equal to_ "func1(arg1) func2 arg2")
    }

    @Test
    fun deeplyNested() {
        val testedExpr = func(3, "top") -
            func(2, "sub1") -
            "arg1" -
            func(1, "op1") -
            func(1, "op2") -
            "arg2" -
            "arg3" -
            func(2, "sub2") -
            "arg4" -
            "arg5"
        assertThat(
            ExpressionVisualizer(testedExpr).string is_ equal to_
                "top(arg1 sub1 op1-op2(arg2), arg3, arg4 sub2 arg5)"
        )
    }

    @Test
    fun missingArgumentsAreFilled() {
        val testedExpr = func(2) - "arg1"
        assertThat(ExpressionVisualizer(testedExpr).string is_ equal to_ "arg1 func2 ?")
    }

    private companion object {
        operator fun Expr.minus(e: Expr) = BoundExpr(this, e)
        operator fun Expr.minus(v: Any) = BoundExpr(this, Value(v))
        fun func(arity: Int) = func(arity, "func$arity")
        fun func(arity: Int, name: String) = FunExpr(arity, name) { "result of $name" }
    }
}
