/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.testing.flow

import org.junit.jupiter.api.Test
import straightway.testing.bdd.Given
import java.time.Duration
import java.time.LocalDateTime

class EqualWithinTest {

    private companion object {
        val DATE_TIME_TEST_VALUE = LocalDateTime.of(2000, 1, 1, 0, 0, 0)
    }

    @Test
    fun `equal int numbers are equal`() =
        Given {
            equalWithin(3)
        } when_ {
            this(1, 1)
        } then {
            assert((it as AssertionResult).isSuccessful)
        }

    @Test
    fun `equal double numbers are equal`() =
        Given {
            equalWithin(3.0)
        } when_ {
            this(1.0, 1.0)
        } then {
            assert((it as AssertionResult).isSuccessful)
        }

    @Test
    fun `equal float numbers are equal`() =
        Given {
            equalWithin(3.0F)
        } when_ {
            this(1.0F, 1.0F)
        } then {
            assert((it as AssertionResult).isSuccessful)
        }

    @Test
    fun `equal strings are equal`() =
        Given {
            equalWithin(3.0F)
        } when_ {
            this("A", "A")
        } then {
            assert((it as AssertionResult).isSuccessful)
        }

    @Test
    fun `unequal objects are not equal`() =
        Given {
            equalWithin(3.0F)
        } when_ {
            this("A", "B")
        } then {
            assertThat((it as AssertionResult).isSuccessful is_ false)
        }

    @Test
    fun `numbers are not equal to non-numbers`() =
        Given {
            equalWithin(3.0F)
        } when_ {
            this(1, "A")
        } then {
            assertThat((it as AssertionResult).isSuccessful is_ false)
        }

    @Test
    fun `non-numbers are not equal to numbers`() =
        Given {
            equalWithin(3.0F)
        } when_ {
            this("A", 1)
        } then {
            assertThat((it as AssertionResult).isSuccessful is_ false)
        }

    @Test
    fun `equal numbers are equal even when range is not a number`() =
        Given {
            equalWithin("A")
        } when_ {
            this(1, 1)
        } then {
            assert((it as AssertionResult).isSuccessful)
        }

    @Test
    fun `int number are considered equal when within range, left number less`() =
        Given {
            equalWithin(3)
        } when_ {
            this(1L, 2)
        } then {
            assert((it as AssertionResult).isSuccessful)
        }

    @Test
    fun `int number are considered equal when within range, right number less`() =
        Given {
            equalWithin(3)
        } when_ {
            this(1L, 0)
        } then {
            assert((it as AssertionResult).isSuccessful)
        }

    @Test
    fun `int number are considered not equal when on range`() =
        Given {
            equalWithin(3)
        } when_ {
            this(1L, 4)
        } then {
            assertThat((it as AssertionResult).isSuccessful is_ false)
        }

    @Test
    fun `floar number are considered equal when within range, left number less`() =
        Given {
            equalWithin(3.0)
        } when_ {
            this(1.0, 2.0)
        } then {
            assert((it as AssertionResult).isSuccessful)
        }

    @Test
    fun `float number are considered equal when within range, right number less`() =
        Given {
            equalWithin(3.0)
        } when_ {
            this(1.0, 0.0)
        } then {
            assert((it as AssertionResult).isSuccessful)
        }

    @Test
    fun `float number are considered not equal when on range`() =
        Given {
            equalWithin(3.0)
        } when_ {
            this(1.0, 4.0)
        } then {
            assertThat((it as AssertionResult).isSuccessful is_ false)
        }

    @Test
    fun `range check works with float range and int values`() =
        Given {
            equalWithin(3.0)
        } when_ {
            this(1, 2)
        } then {
            assert((it as AssertionResult).isSuccessful)
        }

    @Test
    fun `range check works with int range and int and float values`() =
        Given {
            equalWithin(3)
        } when_ {
            this(1, 2.0)
        } then {
            assert((it as AssertionResult).isSuccessful)
        }

    @Test
    fun `range check works with int range and float and int values`() =
        Given {
            equalWithin(3)
        } when_ {
            this(1.0, 2)
        } then {
            assert((it as AssertionResult).isSuccessful)
        }

    @Test
    fun `equal datetimes are equal`() =
        Given {
            equalWithin(Duration.ofMinutes(3))
        } when_ {
            this(DATE_TIME_TEST_VALUE, DATE_TIME_TEST_VALUE)
        } then {
            assert((it as AssertionResult).isSuccessful)
        }

    @Test
    fun `datetimes are not equal to non-datetimes`() =
        Given {
            equalWithin(Duration.ofMinutes(3))
        } when_ {
            this(DATE_TIME_TEST_VALUE, "A")
        } then {
            assertThat((it as AssertionResult).isSuccessful is_ false)
        }

    @Test
    fun `non-datetimes are not equal to datetimes`() =
        Given {
            equalWithin(Duration.ofMinutes(3))
        } when_ {
            this("A", DATE_TIME_TEST_VALUE)
        } then {
            assertThat((it as AssertionResult).isSuccessful is_ false)
        }

    @Test
    fun `equal datetimes are equal even when range is not a duration`() =
        Given {
            equalWithin("A")
        } when_ {
            this(DATE_TIME_TEST_VALUE, DATE_TIME_TEST_VALUE)
        } then {
            assert((it as AssertionResult).isSuccessful)
        }

    @Test
    fun `datetimes are not equal if the exceed range with right greater`() =
        Given {
            equalWithin(Duration.ofMinutes(3))
        } when_ {
            this(DATE_TIME_TEST_VALUE, DATE_TIME_TEST_VALUE.plusMinutes(4))
        } then {
            assertThat((it as AssertionResult).isSuccessful is_ false)
        }

    @Test
    fun `datetimes are not equal if they exceed range with left greater`() =
        Given {
            equalWithin(Duration.ofMinutes(3))
        } when_ {
            this(DATE_TIME_TEST_VALUE, DATE_TIME_TEST_VALUE.minusMinutes(4))
        } then {
            assertThat((it as AssertionResult).isSuccessful is_ false)
        }

    @Test
    fun `datetimes are considered equal if within the range`() =
        Given {
            equalWithin(Duration.ofMinutes(3))
        } when_ {
            this(DATE_TIME_TEST_VALUE, DATE_TIME_TEST_VALUE.minusMinutes(2))
        } then {
            assert((it as AssertionResult).isSuccessful)
        }

    @Test
    fun `datetimes are considered equal if they are exactly on the range`() =
        Given {
            equalWithin(Duration.ofMinutes(3))
        } when_ {
            this(DATE_TIME_TEST_VALUE, DATE_TIME_TEST_VALUE.minusMinutes(3))
        } then {
            assertThat((it as AssertionResult).isSuccessful is_ false)
        }
}
