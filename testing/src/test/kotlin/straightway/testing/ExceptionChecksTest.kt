/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.testing

import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import straightway.error.Panic

class ExceptionChecksTest {

    @Test
    fun `checkActionForException success `() {
        var called = false
        checkActionForException(
            onSuccess = { called = true }
        ) {}
        assertTrue(called, "onSuccess callback was not called")
    }

    @Test
    fun `checkActionForException failure if no onFailue parameter given`() =
        assertThrows<AssertionError> {
            checkActionForException { throw Panic("Aaaah!") }
        }

    @Test
    fun `checkActionForExceptionAsync success `() {
        var called = false
        checkActionForExceptionAsync(
            onSuccess = { called = true }
        ) {}
        assertTrue(called, "onSuccess callback was not called")
    }

    @Test
    fun `checkActionForExceptionAsync failure if no onFailue parameter given`() =
        assertThrows<AssertionError> {
            checkActionForExceptionAsync { throw Panic("Aaaah!") }
        }
}
