/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.kernel_it

import org.junit.jupiter.api.Test
import straightway.odo.boot.BootLoader
import straightway.odo.kernel.Kernel
import straightway.testing.bdd.Given
import straightway.testing.flow.*
import java.util.*

class KernelTest {

    private val test get() = Given {
        APIMock.clearInstances()
        ApplicationMock.clearInstances()
        ServiceLoader.load(ModuleLayer.boot(), BootLoader::class.java).single()
    }

    @Test
    fun `kernel is boot loader`() =
        test when_ {
            this::class.qualifiedName
        } then {
            assertThat(it is_ equal to_ "straightway.odo.kernel.Kernel")
        }

    @Test
    fun `kernel starts application with API`() =
        test while_ {
            initialize(mapOf(Kernel.APP_MODULE_LAYER_INIT_ARG to ModuleLayer.boot()))
        } whenBlocking {
            boot()
        } then {
            val api = APIMock.instances.single()
            val application = ApplicationMock.instances.single()
            assertThat(ApplicationRunnerMock.instances has size of 1)
            assertThat(application.apisPassedToRun is_ equal to_ listOf(api))
        }
}
