/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.kernel_it

import straightway.odo.api.API
import straightway.odo.api.Application
import straightway.utils.WeakCollection

/**
 * Mocked implementation of the Application.Runner interface.
 */
class ApplicationRunnerMock : Application.Runner {

    override suspend fun run(app: Application, api: API) {
        with(app) {
            api.run()
        }
    }
    companion object {
        val instances: Collection<ApplicationRunnerMock> get() = instances_
        fun clearInstances() {
            instances_ = WeakCollection.of()
        }

        private var instances_ = WeakCollection.of<ApplicationRunnerMock>()
    }

    init {
        instances_ += this
    }
}
