/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.kernel_it

import com.nhaarman.mockitokotlin2.mock
import straightway.odo.api.API
import straightway.utils.WeakCollection

/**
 * Mock class for the straightway.odo API, which is exported from this java module.
 */
class APIMock(val args: Map<String, Any>) : API by mock() {

    companion object {
        val instances: Collection<APIMock> get() = instances_
        fun clearInstances() {
            instances_ = WeakCollection.of()
        }
        private var instances_ = WeakCollection.of<APIMock>()
    }

    class Factory : API.Factory {
        override fun createAPI(args: Map<String, Any>) = APIMock(args)
    }

    init {
        instances_ += this
    }
}
