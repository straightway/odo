/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
import straightway.odo.api.*;
import straightway.odo.boot.BootLoader;
import straightway.odo.kernel_it.*;

module straightway.odo.kernel_it {
    requires kotlin.stdlib;
    requires kotlinx.coroutines.core.jvm;
    requires mockito.kotlin;
    requires straightway.expr;
    requires straightway.odo.api;
    requires straightway.odo.boot;
    requires straightway.odo.kernel;
    requires straightway.testing;
    requires straightway.utils;
    uses BootLoader;
    provides API.Factory with APIMock.Factory;
    provides Application.Runner with ApplicationRunnerMock;
    provides Application with ApplicationMock;
}