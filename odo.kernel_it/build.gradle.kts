/*
 * Copyright 2016 straightway
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
dependencies {
    implementation(project(":expr"))
    implementation(project(":odo.api"))
    implementation(project(":odo.boot"))
    implementation(project(":odo.kernel"))
    implementation(project(":testing"))
    implementation(project(":utils"))
    implementation("org.junit.jupiter:junit-jupiter-api:5.3.1")
    implementation("org.junit.jupiter:junit-jupiter-params:5.3.1")
    implementation("com.nhaarman.mockitokotlin2:mockito-kotlin:2.2.0")
    implementation("org.mockito:mockito-core:2.+")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.3.9")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-jdk8:1.3.9")
}

tasks.named<Test>("test") {
    extensions.configure(org.javamodularity.moduleplugin.extensions.TestModuleOptions::class) {
        runOnClasspath = false
    }
}
