/*
 * Copyright 2016 straightway
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
val runtimeLib by configurations.creating

plugins {
    id("org.openjfx.javafxplugin") version "0.0.9"
}

javafx {
    version = "13"
    modules = listOf(
        "javafx.controls",
        "javafx.graphics",
        "javafx.media",
        "javafx.fxml",
        "javafx.swing",
        "javafx.web")
}

dependencies {
    implementation(project(":odo.api"))
    implementation("no.tornado:tornadofx2-odo:2.0.0-SNAPSHOT")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.3.9")

    val javafxVersion = "13"
    runtimeLib(project(":odo.api"))
    runtimeLib(project(":odo.api.desktop"))
    runtimeLib(project(":odo.boot"))
    runtimeLib(project(":odo.kernel"))
    runtimeLib("org.openjfx:javafx-media:$javafxVersion")
    runtimeLib("org.openjfx:javafx-fxml:$javafxVersion")
    runtimeLib("org.openjfx:javafx-swing:$javafxVersion")
    runtimeLib("org.openjfx:javafx-web:$javafxVersion")
    runtimeLib("no.tornado:tornadofx2-odo:2.0.0-SNAPSHOT")
    runtimeLib("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.3.9")
    runtimeLib("org.jetbrains.kotlinx:kotlinx-coroutines-jdk8:1.3.9")
}

val copyRuntimeLibs = tasks.register<Copy>("copyRuntimeLibs") {
    from(runtimeLib)
    into("$buildDir/runtimeLibs")
}

tasks.named<Task>("build") {
    dependsOn(copyRuntimeLibs)
}
