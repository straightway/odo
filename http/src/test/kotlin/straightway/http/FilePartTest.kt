/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.http

import org.junit.jupiter.api.Test
import straightway.testing.bdd.Given
import straightway.testing.flow.*
import java.io.File

class FilePartTest {

    private val test get() = Given {
        object : AutoCloseable {
            val fileContent = "Hello World!"
            val file = File.createTempFile("FilePartTest", ".txt")
            val sut = FilePart("key", file.toPath())

            override fun close() {
                file.delete()
            }

            init {
                file.writeText(fileContent)
            }
        }
    }

    @Test
    fun `key is accessible`() =
        test when_ {
            sut.key
        } then {
            assertThat(it is_ equal to_ "key")
        }

    @Test
    fun `data is read from file`() =
        test when_ {
            sut.data
        } then {
            assertThat(it is_ equal to_ fileContent.toByteArray())
        }

    @Test
    fun `header is composed properly`() =
        test when_ {
            sut.header
        } then {
            assertThat(
                it is_ equal to_
                    "name=\"key\"; filename=\"${file.toPath()}\"\r\n" +
                    "Content-Type: text/plain"
            )
        }

    @Test
    fun `content type is determined from file, if not specified`() =
        Given {
            object : AutoCloseable {
                val file = File.createTempFile("FilePartTest", ".json")
                val sut = FilePart("key", file.toPath())

                override fun close() {
                    file.delete()
                }
            }
        } when_ {
            sut.header
        } then {
            assertThat(
                it is_ equal to_
                    "name=\"key\"; filename=\"${file.toPath()}\"\r\n" +
                    "Content-Type: application/json"
            )
        }

    @Test
    fun `content type is as specified`() =
        Given {
            object : AutoCloseable {
                val file = File.createTempFile("FilePartTest", ".json")
                val sut = FilePart("key", file.toPath(), "contentType")

                override fun close() {
                    file.delete()
                }
            }
        } when_ {
            sut.header
        } then {
            assertThat(
                it is_ equal to_
                    "name=\"key\"; filename=\"${file.toPath()}\"\r\n" +
                    "Content-Type: contentType"
            )
        }
}
