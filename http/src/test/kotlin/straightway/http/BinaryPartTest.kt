/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.http

import org.junit.jupiter.api.Test
import straightway.testing.bdd.Given
import straightway.testing.flow.*

class BinaryPartTest {

    private val test get() = Given {
        BinaryPart("key", byteArrayOf(2, 3, 5), "contentType")
    }

    @Test
    fun `key is accessible`() =
        test when_ {
            key
        } then {
            assertThat(it is_ equal to_ "key")
        }

    @Test
    fun `data is accessible`() =
        test when_ {
            data
        } then {
            assertThat(it is_ equal to_ byteArrayOf(2, 3, 5))
        }

    @Test
    fun `header is as expected`() =
        test when_ {
            header
        } then {
            assertThat(it is_ equal to_ "name=\"key\";\r\nContent-Type: contentType")
        }

    @Test
    fun `default content type is octet-stream`() =
        Given {
            BinaryPart("key", byteArrayOf(2, 3, 5))
        } when_ {
            header
        } then {
            assertThat(
                it is_ equal to_
                    "name=\"key\";\r\nContent-Type: application/octet-stream"
            )
        }
}
