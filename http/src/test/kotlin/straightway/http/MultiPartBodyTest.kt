/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.http

import com.nhaarman.mockitokotlin2.*
import org.junit.jupiter.api.Test
import straightway.expr.minus
import straightway.testing.bdd.Given
import straightway.testing.flow.*
import java.net.http.HttpRequest

class MultiPartBodyTest {

    private val test get() = Given {
        val sut = mock<HttpRequest.Builder>()
        sut.apply {
            stub {
                on { header(any(), any()) }.thenAnswer { sut }
                on { POST(any()) }.thenAnswer { sut }
            }
        }
    }

    @Test
    fun `sets header with content type form-data`() =
        test when_ {
            postParts(mock())
        } then {
            verify(this).header(
                eq("Content-Type"),
                argThat {
                    startsWith("multipart/form-data; boundary=")
                }
            )
        }

    @Test
    fun `creates a new boundary for each request`() =
        test when_ {
            postParts(mock())
            postParts(mock())
        } then {
            httpHeadersFromCalls.also {
                assertThat(it has size of 2)
                assertThat(it[0] is_ not - equal to_ it[1])
            }
        }

    @Test
    fun `posts the part`() {
        val sentParts: Array<MultiPart> = arrayOf(mock(), mock())
        test when_ {
            postParts(*sentParts)
        } then {
            verify(this).POST(
                argThat {
                    this is MultiPartBody && parts.contentEquals(sentParts)
                }
            )
        }
    }

    // region Private

    private val HttpRequest.Builder.httpHeadersFromCalls: List<String> get() {
        val result = mutableListOf<String>()

        // Verify with a failing predicate, which collects the argument values
        // as a side effect. The failing predicate has the effect, that ALL calls
        // for the header function are scanned.
        verify(this, never()).header(
            any(),
            argThat { result.add(this); false }
        )

        return result
    }

    // endregion
}
