/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.http

import java.nio.file.Files
import java.nio.file.Path

/**
 * A binary data chunk read from a file as part of a mutli part http body.
 */
class FilePart(
    override val key: String,
    private val path: Path,
    private val specifiedContentType: String? = null
) : MultiPart {
    override val header =
        "name=\"$key\"; filename=\"$path\"\r\nContent-Type: $contentType"
    override val data: ByteArray
        get() = Files.readAllBytes(path)
    private val contentType get() = specifiedContentType ?: Files.probeContentType(path)
}
