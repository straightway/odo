/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.http

import straightway.compiler.Generated
import java.math.BigInteger
import java.net.http.HttpRequest
import java.util.*

private const val BOUNDARY_BITS = 256
private const val HEX = 16

fun HttpRequest.Builder.postParts(
    vararg parts: MultiPart
): HttpRequest.Builder {
    val boundary = BigInteger(BOUNDARY_BITS, Random()).toString(HEX)
    return header("Content-Type", "multipart/form-data; boundary=$boundary")
        .POST(MultiPartBody.ofParts(boundary, *parts))
}

/**
 * Contains parts of a multi part http body.
 */
interface MultiPartBody {

    val parts: Array<out MultiPart>

    companion object {

        fun ofParts(
            boundary: String,
            vararg parts: MultiPart
        ): HttpRequest.BodyPublisher = with(mutableListOf<ByteArray>()) {
            addParts(parts, boundary)
            addEndDelimiter(boundary)
            createMultiPartBody(parts)
        }

        // region Private

        private class MultiPartBodyImpl(
            publisher: HttpRequest.BodyPublisher,
            override val parts: Array<out MultiPart>
        ) : HttpRequest.BodyPublisher by publisher, MultiPartBody

        private const val CRLF = "\r\n"
        private const val CRLF2 = CRLF + CRLF

        private fun MutableList<ByteArray>.addParts(
            parts: Array<out MultiPart>,
            boundary: String
        ) {
            for (part in parts)
                addPart(boundary, part)
        }

        private fun MutableList<ByteArray>.addEndDelimiter(boundary: String) {
            add(bin("--$boundary--"))
        }

        private fun MutableList<ByteArray>.createMultiPartBody(
            parts: Array<out MultiPart>
        ) =
            MultiPartBodyImpl(HttpRequest.BodyPublishers.ofByteArrays(this), parts)

        private fun MutableList<ByteArray>.addPart(
            boundary: String,
            part: MultiPart
        ) {
            add(bin("${boundary.separator}${part.header}$CRLF2"))
            add(part.data)
            add(CRLF.toByteArray())
        }

        private val String.separator get(): String =
            "--$this${CRLF}Content-Disposition: form-data; "

        @Generated("jacoco sees two branches, while I only see one")
        private fun bin(s: String) = s.toByteArray()

        // endregion
    }
}
