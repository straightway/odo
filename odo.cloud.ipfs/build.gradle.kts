/*
 * Copyright 2016 straightway
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
import java.net.URI

apply(plugin = "de.undercouch.download")

repositories {
    maven { url = URI("https://jitpack.io") }
}

dependencies {
    implementation(project(":compiler"))
    implementation(project(":error"))
    implementation(project(":http"))
    implementation(project(":odo.cloud"))
    implementation(project(":units"))
    implementation(project(":utils"))
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.3.9")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-jdk8:1.3.9")
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.0.1")
    testImplementation("org.mock-server:mockserver-netty:5.11.1")
    //testRuntimeOnly("org.slf4j:slf4j-simple:1.7.30")
    testRuntimeOnly("org.slf4j:slf4j-nop:1.7.30")
}

val ipfsArchive get() = "go-ipfs_v0.8.0_linux-amd64.tar.gz"

tasks.register<de.undercouch.gradle.tasks.download.Download>("downloadIPFS") {
    File(rootProject.buildDir, "downloads").mkdirs()
    val baseFileName = "https://dist.ipfs.io/go-ipfs/v0.8.0/$ipfsArchive"
    src(arrayOf(baseFileName))
    dest("${rootProject.buildDir}/downloads")
    overwrite(false)
}

tasks.register<Copy>("installIPFS") {
    dependsOn(tasks.named<Task>("downloadIPFS"))
    from(tarTree("${rootProject.buildDir}/downloads/$ipfsArchive"))
    into("${rootProject.buildDir}/install")
}

tasks.named<Task>("test") {
    dependsOn(tasks.named<Task>("installIPFS"))
}
