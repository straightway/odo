#!/usr/bin/bash
#LOGGING='systemd-cat -t odo.ipfs'
LOGGING=""
IPFS_EXE=`dirname $0`/../../../../build/install/go-ipfs/ipfs
export IPFS_PATH=`dirname $0`/../../../build/tmp/testIPFS
rm -rf $IPFS_PATH
mkdir -p $IPFS_PATH
$LOGGING $IPFS_EXE init
$LOGGING $IPFS_EXE config --json Addresses.Swarm '["/ip4/0.0.0.0/tcp/0", "/ip6/::/tcp/0", "/ip4/0.0.0.0/udp/0/quic", "/ip6/::/udp/0/quic"]'
$LOGGING $IPFS_EXE config Addresses.API "/ip4/127.0.0.1/tcp/0"
$LOGGING $IPFS_EXE config Addresses.Gateway "/ip4/127.0.0.1/tcp/0"
$LOGGING $IPFS_EXE daemon --offline &
__IPFS_PID=$!

function shutDown() {
  kill $__IPFS_PID
}

trap shutDown INT TERM

wait $__IPFS_PID