/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.cloud.ipfs

import com.nhaarman.mockitokotlin2.mock
import org.junit.jupiter.api.*
import org.mockserver.client.ForwardChainExpectation
import org.mockserver.integration.ClientAndServer.startClientAndServer
import org.mockserver.model.*
import org.mockserver.model.HttpRequest.request
import org.mockserver.model.HttpResponse.response
import org.mockserver.model.StringBody.subString
import straightway.http.BinaryPart
import straightway.http.MultiPart
import straightway.testing.bdd.*
import straightway.testing.flow.*
import java.net.http.HttpClient

class IpfsApiImplTest {

    companion object {
        const val INVALID_HTTP_CHARACTERS = "\n\r\"\'\t\\/&%?"
        val server = startClientAndServer()
        private val port = server.localPort
        val ipfsSettings = mock<IpfsSettings> {
            on { apiBaseUri }.thenReturn("http://127.0.0.1:$port")
        }

        @AfterAll @JvmStatic
        fun close() {
            server.stop()
        }

        val requests get() = server.retrieveRecordedRequestsAndResponses(null)
    }

    private val test
        get() = Given {
            server.reset()
            IpfsApiImpl(ipfsSettings, HttpClient.newHttpClient())
        }

    @Test
    fun `settings are accessible`() =
        test when_ {
            settings
        } then {
            assertThat(settings is_ same as_ ipfsSettings)
        }

    @Test
    fun `cat issues http POST request`() {
        val fileContent = byteArrayOf(2, 3, 5, 7)
        val contentId = "contentID"
        test while_ {
            onHttpRequest("cat", "arg" to contentId)
                .respond(200) { withBody(fileContent) }
        } whenBlocking {
            cat(contentId)
        } then {
            assertThat(it is_ equal to_ fileContent)
        }
    }

    @Test
    fun `cat does error handling`() =
        assertErrorHandlingFor {
            cat("contentID")
        }

    @Test
    fun `cat panics when content ID contains invalid HTTP characters`() =
        INVALID_HTTP_CHARACTERS.forEach {
            test whenBlocking { cat("$it") } then panics
        }

    @Test
    fun `add issues POST request`() {
        val fileContent = byteArrayOf(2, 3, 5, 7)
        test while_ {
            onHttpRequest("add") {
                this.withBody(subString(String(fileContent)))
            }.respond(200) {
                withBody("{ \"Hash\": \"hashValue\", \"other\": 83 }")
            }
        } whenBlocking {
            add(BinaryPart("data/Key", fileContent))
        } then {
            assertThat(it is_ equal to_ "hashValue")
        }
    }

    @Test
    fun `add does error handling`() =
        assertErrorHandlingWithJsonResultFor {
            add(BinaryPart("data/Key", byteArrayOf(2, 3, 5, 7)))
        }

    @Test
    fun `add panics when data key contains invalid HTTP characters`() =
        INVALID_HTTP_CHARACTERS.forEach { invalidChar ->
            val data = mock<MultiPart> {
                on { key }.thenReturn("$invalidChar")
                on { header }.thenReturn("header")
                on { data }.thenReturn(byteArrayOf())
            }
            test whenBlocking { add(data) } then panics
        }

    @Test
    fun `ls issues http POST request`() =
        test while_ {
            onHttpRequest("ls", "arg" to "dirObj")
                .respond(200) {
                    withBody(
                        "{ \"Objects\": [" +
                            "{" +
                            "\"Hash\": \"1: What hash is this?\"," +
                            "\"Links\": [{" +
                            "\"Hash\": \"Hash1\"," +
                            "\"Name\": \"Name1\"," +
                            "\"Size\": 1," +
                            "\"Target\": \"Target1\"," +
                            "\"Type\": 11" +
                            "}, {" +
                            "\"Hash\": \"Hash2\"," +
                            "\"Name\": \"Name2\"," +
                            "\"Size\": 2," +
                            "\"Target\": \"Target2\"," +
                            "\"Type\": 22" +
                            "}]" +
                            "}]" +
                            "}"
                    )
                }
        } whenBlocking {
            ls("dirObj")
        } then {
            assertThat(
                it is_ equal to_ listOf(
                    IpfsApi.DirEntry("Name1", "Hash1", 11, 1),
                    IpfsApi.DirEntry("Name2", "Hash2", 22, 2)
                )
            )
        }

    @Test
    fun `ls does error handling`() =
        assertErrorHandlingWithJsonResultFor {
            ls("name")
        }

    @Test
    fun `ls panics when content ID contains invalid HTTP characters`() =
        INVALID_HTTP_CHARACTERS.replace("/", "").forEach {
            test whenBlocking { ls("$it") } then panics
        }

    @Test
    fun `ls panics on invalid result`() =
        test while_ {
            onHttpRequest("ls", "arg" to "dirObj")
                .respond(200) {
                    withBody(
                        "{ \"Objects\": [" +
                            "{" +
                            "\"Hash\": \"1: What hash is this?\"," +
                            "\"Links\": [{" +
                            "\"Hash\": [ \"Hash1\" ]," +
                            "\"Name\": \"Name1\"," +
                            "\"Size\": 1," +
                            "\"Target\": \"Target1\"," +
                            "\"Type\": 11" +
                            "}]" +
                            "}]" +
                            "}"
                    )
                }
        } whenBlocking {
            ls("dirObj")
        } then panics

    @Test
    fun `name_publish issues http POST request`() {
        val key = "key"
        val hash = "theHashValue"
        val result = "result"
        test while_ {
            onHttpRequest(
                "name/publish",
                "arg" to hash,
                "key" to key,
                "allow-offline" to "true"
            )
                .respond(200) {
                    withBody("{ \"Value\": \"$result\", \"other\": 83 }")
                }
        } whenBlocking {
            name.publish(key, hash)
        } then {
            assertThat(requests has size of 1)
        }
    }

    @Test
    fun `name_publish does error handling`() =
        assertErrorHandlingWithJsonResultFor {
            name.publish("key", "hash")
        }

    @Test
    fun `name_publish panics when content ID contains invalid HTTP characters`() =
        INVALID_HTTP_CHARACTERS.forEach {
            test whenBlocking { name.publish("key", "$it") } then panics
        }

    @Test
    fun `name_publish panics when key contains invalid HTTP characters`() =
        INVALID_HTTP_CHARACTERS.forEach {
            test whenBlocking { name.publish("$it", "contentID") } then panics
        }

    @Test
    fun `files_cp issues http post request`() {
        test while_ {
            onHttpRequest("files/cp", "arg" to "hash", "arg" to "/dst/path")
                .respond(200) {
                    withBody("Success")
                }
        } whenBlocking {
            files.cp("hash", "/dst/path")
        } then {
            assertThat(requests has size of 1)
        }
    }

    @Test
    fun `files_cp does error handling`() =
        assertErrorHandlingFor {
            files.cp("hash", "/dst/path")
        }

    @Test
    fun `files_cp panics when hash contains invalid HTTP characters`() =
        INVALID_HTTP_CHARACTERS.forEach {
            test whenBlocking {
                files.cp("$it", "/dst/path")
            } then panics
        }

    @Test
    fun `files_cp panics when path contains invalid HTTP characters`() =
        INVALID_HTTP_CHARACTERS.replace("/", "").forEach {
            test whenBlocking {
                files.cp("hash", "/dst/path${it}rest")
            } then panics
        }

    @Test
    fun `files_mkdir issues http post request`() {
        test while_ {
            onHttpRequest("files/mkdir", "arg" to "/dst/path", "parents" to "true")
                .respond(200) {
                    withBody("Success")
                }
        } whenBlocking {
            files.mkdir("/dst/path")
        } then {
            assertThat(requests has size of 1)
        }
    }

    @Test
    fun `files_mkdir does error handling`() =
        assertErrorHandlingFor {
            files.mkdir("/dst/path")
        }

    @Test
    fun `files_mkdir panics when path contains invalid HTTP characters`() =
        INVALID_HTTP_CHARACTERS.replace("/", "").forEach {
            test whenBlocking {
                files.mkdir("/dst/path${it}rest")
            } then panics
        }

    @Test
    fun `files_rm issues http post request`() {
        test while_ {
            onHttpRequest("files/rm", "arg" to "/dst/path", "force" to "true")
                .respond(200) {
                    withBody("Success")
                }
        } whenBlocking {
            files.rm("/dst/path")
        } then {
            assertThat(requests has size of 1)
        }
    }

    @Test
    fun `files_rm does error handling`() =
        assertErrorHandlingFor {
            files.rm("/dst/path")
        }

    @Test
    fun `files_rm panics when hash contains invalid HTTP characters`() =
        INVALID_HTTP_CHARACTERS.replace("/", "").forEach {
            test whenBlocking {
                files.rm("/dst/path${it}rest")
            } then panics
        }

    @Test
    fun `files_stat issues http POST request`() {
        val hash = "theHashValue"
        test while_ {
            onHttpRequest("files/stat", "arg" to "/dst/path")
                .respond(200) {
                    withBody("{ \"Hash\": \"$hash\", \"other\": 83 }")
                }
        } whenBlocking {
            files.stat("/dst/path")
        } then {
            assertThat(it is_ equal to_ hash)
        }
    }

    @Test
    fun `files_stat does error handling`() =
        assertErrorHandlingWithJsonResultFor {
            files.stat("/dst/file")
        }

    @Test
    fun `files_stat panics when content ID contains invalid HTTP characters`() =
        INVALID_HTTP_CHARACTERS.forEach {
            test whenBlocking { files.stat("/pre${it}post") } then panics
        }

    @Test
    fun `key_gen issues http POST request`() =
        test while_ {
            onHttpRequest("key/gen", "arg" to "keyName")
                .respond(200) {
                    withBody("{ \"Id\": \"generatedId\", \"name\": \"keyName\" }")
                }
        } whenBlocking {
            key.gen("keyName")
        } then {
            assertThat(it is_ equal to_ IpfsApi.Key.Instance("keyName", "generatedId"))
        }

    @Test
    fun `key_gen does error handling`() =
        assertErrorHandlingWithJsonResultFor {
            key.gen("name")
        }

    @Test
    fun `key_gen panics when content ID contains invalid HTTP characters`() =
        INVALID_HTTP_CHARACTERS.forEach {
            test whenBlocking { key.gen("$it") } then panics
        }

    @Test
    fun `key_rm issues http POST request`() =
        test while_ {
            onHttpRequest("key/rm", "arg" to "Name1")
                .respond(200) {
                    withBody(
                        "{ \"Keys\": [ " +
                            "{ \"Id\": \"Id1\", \"Name\": \"Name1\" }" +
                            " ] }"
                    )
                }
        } whenBlocking {
            key.rm("Name1")
        } then {
            assertThat(
                it is_ equal to_ listOf(
                    IpfsApi.Key.Instance("Name1", "Id1")
                )
            )
        }

    @Test
    fun `key_rm does error handling`() =
        assertErrorHandlingWithJsonResultFor {
            key.rm("Name")
        }

    @Test
    fun `key_rm panics when content ID contains invalid HTTP characters`() =
        INVALID_HTTP_CHARACTERS.forEach {
            test whenBlocking { key.rm("$it") } then panics
        }

    @Test
    fun `key_list issues http POST request`() =
        test while_ {
            onHttpRequest("key/list")
                .respond(200) {
                    withBody(
                        "{ \"Keys\": [ " +
                            "{ \"Id\": \"Id1\", \"Name\": \"Name1\" }," +
                            "{ \"Id\": \"Id2\", \"Name\": \"Name2\" }" +
                            " ] }"
                    )
                }
        } whenBlocking {
            key.list()
        } then {
            assertThat(
                it is_ equal to_ listOf(
                    IpfsApi.Key.Instance("Name1", "Id1"),
                    IpfsApi.Key.Instance("Name2", "Id2")
                )
            )
        }

    @Test
    fun `key_list does error handling`() =
        assertErrorHandlingWithJsonResultFor {
            key.gen("name")
        }

    // region private

    private fun assertErrorHandlingWithJsonResultFor(block: suspend IpfsApi.() -> Unit) {
        assertErrorHandlingFor(block)
        assertMissingJsonResultValueThrows(block)
        assertResultNotBeingJsonFormatThrows(block)
        assertResultOfNonPrimitiveJsonType(block)
        assertResultOfPrimitiveJsonTypeOtherThanString(block)
    }

    private fun assertMissingJsonResultValueThrows(block: suspend IpfsApi.() -> Unit) {
        test while_ {
            server.`when`(request()).respond(200) { withBody("{}") }
        } whenBlocking {
            block()
        } then panics {
            assertThat(
                it.state.toString() is_ equal to_
                    Regex(
                        "Invalid response from API: \\{} \\(.+\\)",
                        RegexOption.DOT_MATCHES_ALL
                    )
            )
        }
    }

    private fun assertResultNotBeingJsonFormatThrows(block: suspend IpfsApi.() -> Unit) {
        test while_ {
            server.`when`(request()).respond(200) { withBody("lalala") }
        } whenBlocking {
            block()
        } then panics {
            assertThat(
                it.state.toString() is_ equal to_
                    Regex(
                        "Invalid response from API: lalala \\(.+\\)",
                        RegexOption.DOT_MATCHES_ALL
                    )
            )
        }
    }

    private fun assertResultOfNonPrimitiveJsonType(block: suspend IpfsApi.() -> Unit) {
        test while_ {
            server.`when`(request()).respond(200) {
                withBody("{ \"Hash\": [], \"Value\": [] }")
            }
        } whenBlocking {
            block()
        } then panics {
            assertThat(
                it.state.toString() is_ equal to_
                    Regex(
                        "Invalid response from API: \\{ \"Hash\": \\[], \"Value\": \\[] } \\(.+\\)",
                        RegexOption.DOT_MATCHES_ALL
                    )
            )
        }
    }

    private fun assertResultOfPrimitiveJsonTypeOtherThanString(block: suspend IpfsApi.() -> Unit) {
        test while_ {
            server.`when`(request()).respond(200) {
                withBody("{ \"Hash\": 1, \"Value\": 2 }")
            }
        } whenBlocking {
            block()
        } then panics {
            assertThat(
                it.state.toString() is_ equal to_
                    Regex(
                        "Invalid response from API: \\{ \"Hash\": 1, \"Value\": 2 } \\(.+\\)",
                        RegexOption.DOT_MATCHES_ALL
                    )
            )
        }
    }

    private fun assertErrorHandlingFor(block: suspend IpfsApi.() -> Unit) {
        assertErrorHandlingFor(
            406,
            "Unknown error",
            block
        )
        assertErrorHandlingFor(
            400,
            "Malformed RPC, argument type error, etc",
            block
        )
        assertErrorHandlingFor(
            403,
            "RPC call forbidden",
            block
        )
        assertErrorHandlingFor(
            404,
            "RPC endpoint doesn't exist",
            block
        )
        assertErrorHandlingFor(
            405,
            "HTTP Method Not Allowed",
            block
        )
        assertErrorHandlingFor(
            500,
            "RPC endpoint returned an error",
            block
        )
    }

    private fun assertErrorHandlingFor(
        statusCode: Int,
        generalErrorExplanation: String,
        block: suspend IpfsApi.() -> Unit
    ) {
        val errorText = "the error text"
        test while_ {
            onAnyRequest.respond(statusCode) { withBody(errorText) }
        } whenBlocking {
            block()
        } then panics {
            assertThat(
                it.state is_ equal to_
                    "$generalErrorExplanation ($statusCode): $errorText"
            )
        }
    }

    private val onAnyRequest = server.`when`(request().withMethod("POST"))

    private fun httpRequest(
        command: String,
        vararg args: Pair<String, String>,
        requestBuilder: HttpRequest.() -> HttpRequest = { this }
    ): HttpRequest {

        var request = request()
            .withMethod("POST")
            .withPath("/$command")
            .requestBuilder()

        args.forEach {
            request = request.withQueryStringParameter(it.first, it.second)
        }

        return request
    }

    private fun onHttpRequest(
        command: String,
        vararg args: Pair<String, String>,
        requestBuilder: HttpRequest.() -> HttpRequest = { this }
    ): ForwardChainExpectation {
        return server.`when`(
            httpRequest(
                command = command,
                requestBuilder = requestBuilder,
                args = args
            )
        )
    }

    private fun ForwardChainExpectation.respond(
        statusCode: Int,
        response: HttpResponse.() -> HttpResponse
    ) =
        respond(
            response()
                .withStatusCode(statusCode)
                .response()
        )

    // endregion
}
