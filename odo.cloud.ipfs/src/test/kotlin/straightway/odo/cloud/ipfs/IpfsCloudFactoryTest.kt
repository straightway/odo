/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.cloud.ipfs

import com.nhaarman.mockitokotlin2.mock
import org.junit.jupiter.api.Test
import straightway.testing.bdd.Given
import straightway.testing.bdd.throws
import straightway.testing.flow.*
import java.net.URI

class IpfsCloudFactoryTest {

    private val test get() = Given {
        object {
            val api = mock<IpfsApi>()
            var apiSettings: Map<String, Any>? = null
            val publishers = mutableListOf<String>()
            val sut by lazy {
                IPFSCloudFactory(
                    {
                        apiSettings = it
                        api
                    },
                    { a, rootKey ->
                        assertThat(a is_ same as_ api)
                        mock<Publisher>().also { publishers.add(rootKey) }
                    }
                )
            }
        }
    }

    @Test
    fun `supports ipns uri scheme`() =
        test when_ {
            sut.supportedUriSchemes
        } then {
            assertThat(it is_ equal to_ setOf("ipns"))
        }

    @Test
    fun `creates cloud instance`() =
        test when_ {
            sut.createCloud(mapOf())
        } then {
            assertThat((it is IpfsCloud) is_ true)
        }

    @Test
    fun `passes settings to cloud instance constructor`() =
        test when_ {
            sut.createCloud(mapOf("a" to 3))
        } then {
            assertThat(apiSettings is_ equal to_ mapOf("a" to 3))
        }

    @Test
    fun `publish on returned cloud invokes root factory`() =
        test whenBlocking {
            sut.createCloud(mapOf())
                .publish(URI("ipns://root"), URI("ipfs://data"))
        } then {
            assertThat(publishers is_ equal to listOf("root"))
        }

    @Test
    fun `publish with cloud from default constructed factory does not throw`() =
        Given {
            IPFSCloudFactory()
        } whenBlocking {
            createCloud(mapOf())
                .publish(URI("ipns://root"), URI("ipfs://data"))
        } then throws.nothing
}
