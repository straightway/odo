/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.cloud.ipfs

import com.nhaarman.mockitokotlin2.*
import kotlinx.coroutines.*
import org.junit.jupiter.api.Test
import straightway.expr.minus
import straightway.testing.bdd.Given
import straightway.testing.bdd.panics
import straightway.testing.flow.*
import straightway.units.*

class PublisherImplTest {

    private val test get() =
        Given {
            object : AutoCloseable {
                val keyName = "keyName"
                val hash = "hash"
                var publishCalls = 0
                var stopPublishing = false
                var publishAction: suspend () -> Unit = {}
                val ipfsApiFiles: IpfsApi.Files = mock {
                    onBlocking { stat("/$keyName") }.thenAnswer { hash }
                }
                val ipfsApiName: IpfsApi.Name = mock {
                    onBlocking { publish(any(), any()) }.thenAnswer {
                        if (!stopPublishing) {
                            publishCalls++
                            runBlocking { publishAction() }
                        }
                    }
                }
                val ipfsApi: IpfsApi = mock {
                    on { files }.thenReturn(ipfsApiFiles)
                    on { name }.thenReturn(ipfsApiName)
                }
                val delay = 25[milli(second)]
                val sut = PublisherImpl(keyName, delay, ipfsApi)

                override fun close() = sut.close()
            }
        }

    @Test
    fun `publish awaits timeout until ipfs namePublish is issued`() =
        test whenBlocking {
            sut.publish(keyName)
            stopPublishing = true
        } then {
            assertThat(publishCalls is_ equal to_ 0)
        }

    @Test
    fun `publish issues ipfs namePublish after delay`() =
        test whenBlocking {
            sut.publish(hash)
            delay(delay * 2)
            stopPublishing = true
        } then {
            assertThat(publishCalls is_ equal to_ 1)
            verifyBlocking(ipfsApiName, times(1)) {
                publish(keyName, hash)
            }
        }

    @Test
    fun `two quick publish calls issue one ipfs namePublish after delay`() =
        test whenBlocking {
            sut.publish(keyName)
            sut.publish(keyName)
            delay(delay * 4)
            stopPublishing = true
        } then {
            assertThat(publishCalls is_ equal to_ 1)
        }

    @Test
    fun `another publish coming in while publishing, result in a second publish`() {
        var isFirstPublishExecuted = false
        test while_ {
            publishAction = {
                publishAction = {}
                sut.publish(keyName)
                isFirstPublishExecuted = true
            }
        } whenBlocking {
            sut.publish(keyName)
            while (!isFirstPublishExecuted) delay(5[milli(second)])
            delay(delay * 2)
            stopPublishing = true
        } then {
            assertThat(publishCalls is_ equal to_ 2)
        }
    }

    @Test
    fun `another publish coming in while publishing a 2nd time, then 3rd  publish`() {
        var isFirstPublishExecuted = false
        test while_ {
            publishAction = {
                publishAction = {}
                sut.publish(keyName)
                sut.publish(keyName)
                isFirstPublishExecuted = true
            }
        } whenBlocking {
            sut.publish(keyName)
            while (!isFirstPublishExecuted) delay(5[milli(second)])
            delay(delay * 2)
            stopPublishing = true
        } then {
            assertThat(publishCalls is_ equal to_ 2)
        }
    }

    @Test
    fun `publishing starts job`() =
        test whenBlocking {
            sut.publish(keyName)
        } then {
            assertThat(sut.currJob is_ not - same as_ null)
        }

    @Test
    fun `publishing while another job is running, throws`() =
        test while_ {
            sut.currJob = mock()
        } whenBlocking {
            sut.publish(keyName)
        } then panics
}

private suspend fun delay(time: UnitValue<Time>) = delay(time.toDuration().toMillis())
