/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.cloud.ipfs

import com.nhaarman.mockitokotlin2.*
import org.junit.jupiter.api.Test
import straightway.expr.minus
import straightway.testing.bdd.Given
import straightway.testing.bdd.panics
import straightway.testing.flow.*
import java.net.URI

class IpfsCloudTest {

    private companion object {
        private const val ROOT_KEY_NAME = "rootKeyName"
        private const val ROOT_KEY = "rootKey"
        private val content = byteArrayOf(2, 3, 5)
    }

    private val test get() = Given {
        object : AutoCloseable {
            val stored = mutableMapOf<String, ByteArray>()
            val rootDirContent = mutableListOf(
                IpfsApi.DirEntry(ROOT_KEY_NAME, "#RootHash", 1, -1)
            )
            val ipfsApiKey = mock<IpfsApi.Key> {
                onBlocking { list() }
                    .thenReturn(listOf(IpfsApi.Key.Instance(ROOT_KEY_NAME, ROOT_KEY)))
                onBlocking { gen(ROOT_KEY_NAME) }
                    .thenReturn(IpfsApi.Key.Instance(ROOT_KEY_NAME, ROOT_KEY))
            }
            val ipfsApiFiles = mock<IpfsApi.Files> {
                onBlocking { stat("/") }.thenReturn("#RootHash")
            }
            val ipfsApi = mock<IpfsApi> {
                on { files }.thenReturn(ipfsApiFiles)
                on { key }.thenReturn(ipfsApiKey)
                onBlocking { ls("#RootHash") }.thenAnswer { rootDirContent }
            }
            val publishers = mutableMapOf<String, Publisher>()
            var publisher: Publisher? = mock()
            var sut = IpfsCloud(ipfsApi) {
                (publisher ?: mock()).also { p -> publishers[it] = p }
            }

            override fun close() {
                sut.close()
            }
        }
    }

    private val testRoot get() = test while_ {
        stubbing(ipfsApiKey) {
            onBlocking { rm("name") }.thenReturn(
                listOf(IpfsApi.Key.Instance("name", "deletedKey"))
            )
            onBlocking { list() }.thenReturn(
                listOf(IpfsApi.Key.Instance("name", "deletedKey"))
            )
            onBlocking { gen(any()) }.thenReturn(
                IpfsApi.Key.Instance("name", "generatedKey")
            )
        }
    }

    @Test
    fun `retrieve executes ipfs cat`() =
        listOf("ipfs", "ipns").forEach { uriScheme ->
            test whileBlocking {
                stubbing(ipfsApi) {
                    onBlocking { cat("/$uriScheme/file") }
                        .thenReturn(byteArrayOf(2, 3, 5))
                }
            } whenBlocking {
                sut.retrieve(URI("$uriScheme://file"))
            } then {
                assertThat(it is_ equal to_ byteArrayOf(2, 3, 5))
                verifyBlocking(ipfsApi, times(1)) {
                    cat("/$uriScheme/file")
                }
            }
        }

    @Test
    fun `retrieve panics with invalid URI scheme`() =
        listOf(
            "http://someRoot/dir/file",
            "ipfs://hash/pathNotAllowed",
            "ipfs://hash/pathNotAllowed"
        ).forEach { invalidUri ->
            test whenBlocking {
                sut.retrieve(URI(invalidUri))
            } then panics
        }

    @Test
    fun `store calls ipfs add`() =
        test while_ {
            stubbing(ipfsApi) {
                onBlocking { add(any()) }.thenReturn("addedKey")
            }
        } whenBlocking {
            sut.store(content)
        } then {
            assertThat(it is_ equal to URI("ipfs://addedKey"))
            verifyBlocking(ipfsApi, times(1)) {
                add(argThat { data.contentEquals(content) })
            }
        }

    @Test
    fun `createRoot calls ipfs key gen`() =
        testRoot whenBlocking {
            sut.createRoot()
        } then {
            assertThat(it is_ equal to URI("ipns://generatedKey"))
            verifyBlocking(ipfsApiKey, times(1)) { gen(any()) }
        }

    @Test
    fun `removeRoot calls ipfs key rm`() =
        testRoot whenBlocking {
            sut.removeRoot(URI("ipns://deletedKey"))
        } then {
            verifyBlocking(ipfsApiKey, times(1)) { rm("name") }
        }

    @Test
    fun `removeRoot panics if ipfs key does not exist`() =
        testRoot whenBlocking {
            sut.removeRoot(URI("ipns://notExistingKey"))
        } then panics

    @Test
    fun `removeRoot panics with invalid uri scheme`() =
        listOf("http", "ipfs").forEach { invalidScheme ->
            testRoot whenBlocking {
                sut.removeRoot(URI("$invalidScheme://deletedKey"))
            } then panics
        }

    @Test
    fun `removeRoot panics if uri has a path`() =
        testRoot whenBlocking {
            sut.removeRoot(URI("ipns://deletedKey/path"))
        } then panics

    @Test
    fun `publish forwards request to publisher`() =
        test whenBlocking {
            sut.publish(URI("ipns://root"), URI("ipfs://data"))
        } then {
            verifyBlocking(publisher!!, times(1)) {
                publish("/ipfs/data")
            }
        }

    @Test
    fun `no publishers are initially created`() =
        test when_ {
            publishers
        } then {
            assertThat(publishers is_ empty)
        }

    @Test
    fun `publish creates new publisher for root`() =
        test whenBlocking {
            sut.publish(URI("ipns://root"), URI("ipfs://data"))
        } then {
            assertThat(publishers.keys is_ equal to listOf("root"))
        }

    @Test
    fun `publish creates new publisher for another root`() =
        test while_ {
            publisher = null
        } whenBlocking {
            sut.publish(URI("ipns://root1"), URI("ipfs://data"))
            sut.publish(URI("ipns://root2"), URI("ipfs://data"))
        } then {
            assertThat(publishers.keys is_ equal to listOf("root1", "root2"))
            assertThat(
                publishers.values.first() is_ not - equal
                    to publishers.values.last()
            )
        }

    @Test
    fun `publish re-uses publisher for the same root`() =
        test whenBlocking {
            sut.publish(URI("ipns://root"), URI("ipfs://data1"))
            sut.publish(URI("ipns://root"), URI("ipfs://data2"))
        } then {
            assertThat(publishers.keys is_ equal to listOf("root"))
        }

    @Test
    fun `close closes publisher, if it is closeable`() =
        test whileBlocking {
            sut.close()
            publisher = mock(extraInterfaces = arrayOf(AutoCloseable::class))
            sut = IpfsCloud(ipfsApi) { publisher!! }
        } whenBlocking {
            sut.publish(URI("ipns://publishKey"), URI("ipfs://dataKey"))
            sut.close()
        } then {
            verify(publisher as AutoCloseable, times(1)).close()
        }
}
