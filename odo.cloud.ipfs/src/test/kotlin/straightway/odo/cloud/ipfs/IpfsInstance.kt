/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.cloud.ipfs

import java.nio.file.Path
import java.util.concurrent.TimeUnit

/**
 * Control the life cycle of a local IPFS test instance.
 */
class IpfsInstance private constructor() : AutoCloseable {

    companion object {
        private val testIpfsAPI = Path.of("build", "tmp", "testIPFS", "api")
        private val testIpfsExe = Path.of("src", "test", "bash", "startTestIPFSInstance.sh")
        private var ipfsInstance: IpfsInstance? = null

        fun makeSureIsUpAndRunning() {
            ipfsInstance = ipfsInstance ?: IpfsInstance()
        }

        fun shutDown() {
            ipfsInstance?.close()
            ipfsInstance = null
        }
    }

    override fun close() {
        println("Waiting for IPFS to shut down")

        ipfsProcess.destroy()
        if (!ipfsProcess.waitFor(10, TimeUnit.SECONDS))
            ipfsProcess.destroyForcibly()

        println("Waiting for IPFS is down")
    }

    private val ipfsProcess: Process

    init {
        println("Starting IPFS")

        ipfsProcess = ProcessBuilder()
            .command("/usr/bin/bash", testIpfsExe.toString())
            .inheritIO()
            .start()

        do {
            print(".")
            Thread.sleep(500, 0)
        } while (!testIpfsAPI.toFile().exists())

        println("\nIPFS up and running")
    }
}
