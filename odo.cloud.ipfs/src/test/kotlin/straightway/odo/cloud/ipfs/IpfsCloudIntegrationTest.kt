/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.cloud.ipfs

import com.nhaarman.mockitokotlin2.mock
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.*
import straightway.expr.minus
import straightway.http.BinaryPart
import straightway.testing.assertPanics
import straightway.testing.bdd.Given
import straightway.testing.flow.*
import java.net.http.HttpClient
import java.nio.file.Path

class IpfsCloudIntegrationTest {

    companion object {
        val content = ByteArray(300000) { (it % 256).toByte() }
        val TEST_IPFS_PATH = Path.of("build", "tmp", "testIPFS")

        @AfterAll @JvmStatic
        fun overallTearDown() = IpfsInstance.shutDown()
    }

    private val test get() =
        Given {
            IpfsInstance.makeSureIsUpAndRunning()
            object : AutoCloseable {

                val ipfs: IpfsApi = IpfsApiImpl(
                    DefaultIpfsSettings(
                        mapOf("ipfs.repoPath" to TEST_IPFS_PATH.toAbsolutePath())
                    ),
                    HttpClient.newBuilder().build()
                )

                val publisher: Publisher = mock()

                val cloud: IpfsCloud by lazy {
                    IpfsCloud(ipfs) { publisher }
                }

                override fun close() = runBlocking {
                    ipfs.ls(ipfs.files.stat("/")).forEach {
                        ipfs.files.rm("/${it.name}")
                    }
                }
            }
        }

    @Test
    fun `stored file can be re-retrieved`() =
        test whenBlocking {
            val dataUri = cloud.store(content)
            cloud.retrieve(dataUri)
        } then {
            assertThat(it is_ equal to_ content)
        }

    @Test
    fun `add and cat`() =
        test whenBlocking {
            val addedHash = ipfs.add(BinaryPart("anyName", content))
            ipfs.cat(addedHash)
        } then {
            assertThat(it is_ equal to_ content)
        }

    @Test
    fun `keyGen and keyList`() =
        test whenBlocking {
            val newKey = ipfs.key.gen("newName")
            val allKeys = ipfs.key.list()
            object {
                val newKey = newKey
                val allKeys = allKeys
            }
        } then {
            assertThat(it.allKeys has values(it.newKey))
            assertThat(it.allKeys has size of 2)
            assertThat(it.allKeys.any { it.name == "self" } is_ true)
        }

    @Test
    fun `keyGen and keyRm`() =
        test whenBlocking {
            val newKey = ipfs.key.gen("newName")
            val removedKey = ipfs.key.rm("newName").single()
            val remainingKeys = ipfs.key.list()
            object {
                val newKey = newKey
                val removedKey = removedKey
                val remainingKeys = remainingKeys
            }
        } then {
            assertThat(it.newKey is_ equal to it.removedKey)
            assertThat(it.remainingKeys has not - values(it.newKey))
        }

    @Test
    fun `copy and stat`() =
        test whenBlocking {
            val addedHash = ipfs.add(BinaryPart("anyName", content))
            ipfs.files.cp("/ipfs/$addedHash", "/file")
            val copiedHash = ipfs.files.stat("/file")
            ipfs.cat(copiedHash)
        } then {
            assertThat(it is_ equal to_ content)
        }

    @Test
    fun `copy and rm`() =
        test whenBlocking {
            val addedHash = ipfs.add(BinaryPart("anyName", content))
            ipfs.files.cp("/ipfs/$addedHash", "/file")
            ipfs.files.rm("/file")
        } then {
            assertPanics { runBlocking { ipfs.files.stat("/file") } }
        }

    @Test
    fun `copy and ls`() {
        lateinit var addedHash: String
        test whenBlocking {
            addedHash = ipfs.add(BinaryPart("anyName", content))
            ipfs.files.cp("/ipfs/$addedHash", "/file1")
            ipfs.files.cp("/ipfs/$addedHash", "/file2")
            val rootDirHash = ipfs.files.stat("/")
            ipfs.ls(rootDirHash)
        } then {
            assertThat(
                it.sortedBy { it.name } is_ equal to_ listOf(
                    IpfsApi.DirEntry("file1", addedHash, 2, content.size.toLong()),
                    IpfsApi.DirEntry("file2", addedHash, 2, content.size.toLong())
                )
            )
        }
    }

    @Test
    fun `publish and ls the published name`() {
        lateinit var addedHash: String
        test whenBlocking {
            addedHash = ipfs.add(BinaryPart("anyName", content))
            ipfs.files.cp("/ipfs/$addedHash", "/file1")
            val rootDirHash = ipfs.files.stat("/")
            ipfs.name.publish("self", rootDirHash)
            val key = ipfs.key.list().single { it.name == "self" }.id
            ipfs.ls("/ipns/$key")
        } then {
            assertThat(
                it.sortedBy { it.name } is_ equal to_ listOf(
                    IpfsApi.DirEntry("file1", addedHash, 2, content.size.toLong())
                )
            )
        }
    }

    @Test
    fun `filesMkDir and ls`() {
        test whenBlocking {
            ipfs.files.mkdir("/sub/sub2/sub3")
            val rootDirHash = ipfs.files.stat("/")
            val hash = ipfs.files.stat("/sub/sub2/sub3")
            val result = ipfs.ls("/ipfs/$rootDirHash/sub/sub2")
            object {
                val hash = hash
                val result = result
            }
        } then {
            assertThat(
                it.result.sortedBy { it.name } is_ equal to_ listOf(
                    IpfsApi.DirEntry("sub3", it.hash, 1, 0)
                )
            )
        }
    }
}
