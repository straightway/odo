/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.cloud.ipfs

import org.junit.jupiter.api.Test
import straightway.testing.bdd.Given
import straightway.testing.flow.*
import java.io.File
import java.nio.file.Files

class DefaultIpfsSettingsTest {

    private val test get() = Given {
        object : AutoCloseable {
            val testDir = Files.createTempDirectory("DefaultIpfsSettingsTest").toFile()
            val apiFile = File(testDir, "api")
            val host = "127.0.0.1"
            val port = 5001
            val expectedApiAccessString = "/ip4/$host/tcp/$port"
            val sut = DefaultIpfsSettings(mapOf("ipfs.repoPath" to testDir.toPath()))

            override fun close() {
                apiFile.delete()
                testDir.delete()
            }

            init {
                apiFile.createNewFile()
                apiFile.writeText(expectedApiAccessString)
            }
        }
    }

    @Test
    fun `default path`() {
        assertThat(
            DefaultIpfsSettings.DEFAULT_IPFS_REPO_PATH
                .startsWith(System.getProperty("user.home")) is_ true
        )
        assertThat(
            DefaultIpfsSettings.DEFAULT_IPFS_REPO_PATH.toString() is_ equal to_
                Regex(".*\\.odo.ipfs.repo$")
        )
    }

    @Test
    fun `default IPFS repo path with empty settings`() =
        Given {
            DefaultIpfsSettings(mapOf())
        } when_ {
            ipfsRepoPath
        } then {
            assertThat(
                it.toString() is_ equal to_
                    DefaultIpfsSettings.DEFAULT_IPFS_REPO_PATH.toString()
            )
        }

    @Test
    fun `IPFS repo path can be specified by ipfs_repoPath property`() =
        test when_ {
            sut.ipfsRepoPath
        } then {
            assertThat(it.toString() is_ equal to_ testDir.absolutePath)
        }

    @Test
    fun `api access string is retrieved from api file in repo path`() =
        test when_ {
            sut.apiAccessString
        } then {
            assertThat(it is_ equal to_ expectedApiAccessString)
        }

    @Test
    fun `api base uri is determined from api access string`() =
        test when_ {
            sut.apiBaseUri
        } then {
            assertThat(it is_ equal to_ "http://$host:$port/api/v0")
        }
}
