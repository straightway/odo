/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.cloud.ipfs

import org.junit.jupiter.api.Test
import straightway.testing.flow.*

class IpfsApiTest {

    @Test
    fun `Key_Instance name is accessible`() =
        assertThat(IpfsApi.Key.Instance("name", "id").name is_ equal to_ "name")

    @Test
    fun `Key_Instance id is accessible`() =
        assertThat(IpfsApi.Key.Instance("name", "id").id is_ equal to_ "id")

    @Test
    fun `DirEntry name is accessible`() =
        assertThat(
            IpfsApi.DirEntry("name", "hash", 1, 2L).name
                is_ equal to_ "name"
        )

    @Test
    fun `DirEntry hash is accessible`() =
        assertThat(
            IpfsApi.DirEntry("name", "hash", 1, 2L).hash
                is_ equal to_ "hash"
        )

    @Test
    fun `DirEntry type is accessible`() =
        assertThat(
            IpfsApi.DirEntry("name", "hash", 1, 2L).type
                is_ equal to_ 1
        )

    @Test
    fun `DirEntry size is accessible`() =
        assertThat(
            IpfsApi.DirEntry("name", "hash", 1, 2L).size
                is_ equal to_ 2L
        )
}
