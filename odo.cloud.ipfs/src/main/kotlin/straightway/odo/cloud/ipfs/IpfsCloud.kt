/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.cloud.ipfs

import straightway.error.Panic
import straightway.http.BinaryPart
import straightway.odo.cloud.*
import java.net.URI
import java.util.*

/**
 * Cloud implementation using the interplanetary file system (IPFS) as storage.
 */
class IpfsCloud(
    private val ipfs: IpfsApi,
    private val publisherFactory: (String) -> Publisher
) : Cloud, AutoCloseable {

    override suspend fun retrieve(
        uri: URI,
        progressCallback: (TransferProgress) -> Unit
    ) = ipfs.cat(uri.ipfsPath)

    override suspend fun createRoot() =
        with(ipfs.key.gen(UUID.randomUUID().toString())) {
            URI("ipns://$id")
        }

    override suspend fun removeRoot(root: URI) {
        root.assertRootUriValid()
        val rootKey = ipfs.key.list().singleOrNull { it.id == root.host }
            ?: throw Panic("Root URI not registered: $root")
        ipfs.key.rm(rootKey.name)
    }

    override suspend fun store(
        content: ByteArray,
        progressCallback: (TransferProgress) -> Unit
    ): URI {
        val added = ipfs.add(BinaryPart(UUID.randomUUID().toString(), content))
        return URI("ipfs://$added")
    }

    override suspend fun publish(root: URI, data: URI) {
        root.assertRootUriValid()
        data.assertDataUriValid()
        val publisher = getPublisher(root)
        publisher.publish(data.ipfsPath)
    }

    override fun close() {
        publishers.values.forEach { publisher ->
            if (publisher is AutoCloseable)
                publisher.close()
        }
        publishers.clear()
    }

    // region Private

    private fun getPublisher(root: URI) =
        publishers[root.host]
            ?: publisherFactory(root.host).also { publishers.put(root.host, it) }

    private val publishers = mutableMapOf<String, Publisher>()

    companion object {

        private val supportedUriSchemes =
            setOf("ipfs", "ipns")

        private val URI.ipfsPath get() =
            "/$scheme/$host".also { assertDataUriValid() }

        private fun URI.assertRootUriValid() {
            if (scheme != "ipns" || path != "")
                throw Panic("Invalid root URI: $this")
        }

        private fun URI.assertDataUriValid() {
            if (scheme !in supportedUriSchemes || path != "")
                throw Panic("Unsupported URI: $this")
        }
    }

    // endregion
}
