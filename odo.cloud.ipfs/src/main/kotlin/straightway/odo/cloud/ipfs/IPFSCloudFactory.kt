/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.cloud.ipfs

import straightway.odo.cloud.Cloud
import straightway.units.get
import straightway.units.second
import java.net.http.HttpClient

/**
 * Cloud factory implementation providing a cloud instance using the
 * interplanetary file system (IPFS) as storage.
 */
class IPFSCloudFactory(
    private val apiFactory: (Map<String, Any>) -> IpfsApi,
    private val publisherFactory: (IpfsApi, String) -> Publisher
) : Cloud.Factory {

    constructor() : this(
        { settings ->
            IpfsApiImpl(
                DefaultIpfsSettings(settings),
                HttpClient.newHttpClient()
            )
        },
        { api, rootKey ->
            PublisherImpl(rootKey, 1[second], api)
        }
    )

    override val supportedUriSchemes = setOf("ipns")

    override fun createCloud(settings: Map<String, Any>): Cloud =
        apiFactory(settings).let {
            IpfsCloud(it) { rootKey -> publisherFactory(it, rootKey) }
        }
}
