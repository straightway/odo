/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.cloud.ipfs

import java.nio.file.Path

/**
 * Extract IPFS settings from the given map of settings.
 */
class DefaultIpfsSettings(settings: Map<String, Any>) : IpfsSettings {
    companion object {
        val DEFAULT_IPFS_REPO_PATH: Path =
            Path.of(System.getProperty("user.home"), ".odo", "ipfs", "repo")
    }

    val ipfsRepoPath: Path = settings["ipfs.repoPath"]?.toPath() ?: DEFAULT_IPFS_REPO_PATH

    val apiAccessString by lazy { ipfsRepoPath.toFile().resolve("api").readText() }

    override val apiBaseUri by lazy {
        val components = apiAccessString.split('/')
        val (_, _, host, _, tcpPort) = components
        "http://$host:$tcpPort/api/v0"
    }

    private fun Any.toPath(): Path = Path.of(toString())
}
