/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.cloud.ipfs

import kotlinx.coroutines.*
import kotlinx.coroutines.sync.Mutex
import straightway.error.Panic
import straightway.units.*
import straightway.utils.StateMachine

/**
 * Default implementation of the Publisher interface.
 */
class PublisherImpl(
    private val rootKey: String,
    private val delay: UnitValue<Time>,
    private val ipfs: IpfsApi
) : Publisher, AutoCloseable {

    override suspend fun publish(path: String) {
        withLock {
            rootHash = path
        }

        stateMachine.handleEvent(Event.Publish)
    }

    override fun close() {
        currJob?.cancel("Publisher closed")
    }

    // region Private

    private enum class State { Idle, Delayed, Publishing, PreliminaryPublishing; }
    private enum class Event { Publish, Timeout, PublishFinished; }

    internal var currJob: Deferred<*>? = null

    private val stateMachine = StateMachine<State, Event>(State.Idle) {
        inState(State.Idle) {
            on(Event.Publish) then transitionTo(State.Delayed)
        }

        inState(State.Delayed) {
            job(Event.Timeout) { delay(delay.toDuration().toMillis()) }
            on(Event.Publish) then stayInThisState
            on(Event.Timeout) then transitionTo(State.Publishing)
        }

        inState(State.Publishing) {
            job(Event.PublishFinished) {
                val hash = withLock { rootHash }
                ipfs.name.publish(rootKey, hash)
            }
            on(Event.PublishFinished) then transitionTo(State.Idle)
            on(Event.Publish) then transitionTo(State.PreliminaryPublishing)
        }

        inState(State.PreliminaryPublishing) {
            on(Event.Publish) then stayInThisState
            on(Event.PublishFinished) then transitionTo(State.Delayed)
        }
    }

    private fun StateMachine.StateConfigurator<State, Event>.job(
        finishedEvent: Event,
        block: suspend () -> Unit
    ) = onEntry { scheduleJob(finishedEvent, block) }

    private fun scheduleJob(finishedEvent: Event, block: suspend () -> Unit) {
        if (currJob !== null)
            throw Panic("Job already running")

        currJob = GlobalScope.async {
            block()
            currJob = null
            stateMachine.handleEvent(finishedEvent)
        }
    }

    private suspend fun <T> withLock(block: suspend () -> T): T {
        val isLocked = mutex.tryLock()
        return try {
            block()
        } finally {
            if (isLocked) mutex.unlock()
        }
    }

    private val mutex = Mutex()
    private lateinit var rootHash: String

    // endregion
}
