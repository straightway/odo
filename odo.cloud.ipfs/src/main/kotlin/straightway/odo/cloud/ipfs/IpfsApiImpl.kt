/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.cloud.ipfs

import kotlinx.coroutines.future.await
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.*
import straightway.error.Panic
import straightway.http.*
import java.net.URI
import java.net.http.*
import java.net.http.HttpRequest.BodyPublishers

/**
 * Implementation of the IPFS API.
 */
class IpfsApiImpl(
    val settings: IpfsSettings,
    private val httpClient: HttpClient
) : IpfsApi, IpfsSettings by settings {

    private companion object {
        private const val HTTP_SUCCESS_STATUS_CODE = 200
        private const val ALLOWED_HTTP_CHARS =
            "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-"
        private val STATUS_MESSAGES = mapOf(
            400 to "Malformed RPC, argument type error, etc",
            403 to "RPC call forbidden",
            404 to "RPC endpoint doesn't exist",
            405 to "HTTP Method Not Allowed",
            500 to "RPC endpoint returned an error"
        )
    }

    override suspend fun cat(id: String) =
        call(
            "cat",
            listOf("arg" to id),
            HttpResponse.BodyHandlers.ofByteArray()
        ) {
            POST(BodyPublishers.noBody())
        }

    override suspend fun add(data: MultiPart) =
        callWithJsonResult(
            "add",
            listOf(),
            "Hash"
        ) {
            postParts(httpClean(data))
        }

    override suspend fun ls(id: String): List<IpfsApi.DirEntry> =
        call<String>(
            "ls",
            listOf("arg" to id),
            HttpResponse.BodyHandlers.ofString()
        ) {
            POST(BodyPublishers.noBody())
        }.getMultipleJsonValues("Objects", "Links") {
            IpfsApi.DirEntry(
                jsonObject["Name"].asString,
                jsonObject["Hash"].asString,
                jsonObject["Type"].asInt,
                jsonObject["Size"].asLong
            )
        }

    override val name = object : IpfsApi.Name {
        override suspend fun publish(key: String, hash: String) {
            callWithJsonResult(
                "name/publish",
                listOf(
                    "arg" to hash,
                    "key" to key,
                    "allow-offline" to "true"
                ),
                "Value"
            ) {
                POST(BodyPublishers.noBody())
            }
        }
    }

    override val files = object : IpfsApi.Files {
        override suspend fun cp(srcHash: String, dst: String) {
            call<String>(
                "files/cp",
                listOf(
                    "arg" to srcHash,
                    "arg" to dst
                ),
                HttpResponse.BodyHandlers.ofString()
            ) {
                POST(BodyPublishers.noBody())
            }
        }

        override suspend fun mkdir(path: String) {
            call<String>(
                "files/mkdir",
                listOf(
                    "arg" to path,
                    "parents" to "true"
                ),
                HttpResponse.BodyHandlers.ofString()
            ) {
                POST(BodyPublishers.noBody())
            }
        }

        override suspend fun rm(file: String) {
            call<String>(
                "files/rm",
                listOf(
                    "arg" to file,
                    "force" to "true"
                ),
                HttpResponse.BodyHandlers.ofString()
            ) {
                POST(BodyPublishers.noBody())
            }
        }

        override suspend fun stat(file: String) =
            callWithJsonResult(
                "files/stat",
                listOf("arg" to file),
                "Hash"
            ) {
                POST(BodyPublishers.noBody())
            }
    }

    override val key = object : IpfsApi.Key {
        override suspend fun gen(name: String) = IpfsApi.Key.Instance(
            name,
            callWithJsonResult(
                "key/gen",
                listOf("arg" to name),
                "Id"
            ) {
                POST(BodyPublishers.noBody())
            }
        )

        override suspend fun rm(name: String) =
            call<String>(
                "key/rm",
                listOf("arg" to name),
                HttpResponse.BodyHandlers.ofString()
            ) {
                POST(BodyPublishers.noBody())
            }.getMultipleJsonValues("Keys") {
                IpfsApi.Key.Instance(
                    jsonObject["Name"].asString,
                    jsonObject["Id"].asString
                )
            }

        override suspend fun list() =
            call<String>(
                "key/list",
                listOf(),
                HttpResponse.BodyHandlers.ofString()
            ) {
                POST(BodyPublishers.noBody())
            }.getMultipleJsonValues("Keys") {
                IpfsApi.Key.Instance(
                    jsonObject["Name"].asString,
                    jsonObject["Id"].asString
                )
            }
    }

    // region Private

    private suspend fun callWithJsonResult(
        command: String,
        args: Iterable<Pair<String, String>>,
        jsonResultField: String,
        requestFactory: HttpRequest.Builder.() -> HttpRequest.Builder
    ) = with(call(command, args, HttpResponse.BodyHandlers.ofString(), requestFactory)) {
        panicOnException { getApiCallResultFromJson(jsonResultField) }
    }

    private fun <T> String.panicOnException(block: () -> T) =
        try {
            block()
        } catch (x: Throwable) {
            throw Panic("Invalid response from API: $this ($x)")
        }

    private fun <T> String.getMultipleJsonValues(
        topLevelKey: String,
        subLevelKey: String,
        elementGetter: JsonElement.() -> T
    ): List<T> = panicOnException {
        val resultMap = Json.decodeFromString<JsonObject>(this)
        resultMap[topLevelKey]!!.jsonArray.flatMap {
            it.jsonObject[subLevelKey]!!.jsonArray.map(elementGetter)
        }
    }

    private fun <T> String.getMultipleJsonValues(
        topLevelKey: String,
        elementGetter: JsonElement.() -> T
    ): List<T> = panicOnException {
        val resultMap = Json.decodeFromString<JsonObject>(this)
        val resultList = resultMap[topLevelKey]!!.jsonArray
        resultList.map(elementGetter)
    }

    private fun String.getApiCallResultFromJson(jsonResultField: String): String {
        val resultMap = Json.decodeFromString<JsonObject>(this)
        val result = resultMap[jsonResultField]
        return if (result is JsonPrimitive && result.isString) result.content
        else error("Invalid result type: $result")
    }

    private suspend fun <T> call(
        command: String,
        args: Iterable<Pair<String, String>>,
        resultHandler: HttpResponse.BodyHandler<T>,
        requestFactory: HttpRequest.Builder.() -> HttpRequest.Builder
    ) = with(httpRequest(command, httpClean(args), requestFactory).send(resultHandler)) {
        body().also {
            checkHttpError(it)
        }
    }

    private fun HttpResponse<*>.checkHttpError(it: Any?) {
        val statusCode = statusCode()
        if (statusCode != HTTP_SUCCESS_STATUS_CODE)
            throw Panic(
                "${STATUS_MESSAGES[statusCode] ?: "Unknown error"} " +
                    "($statusCode): ${it.stringMessage}"
            )
    }

    private suspend fun <T> HttpRequest.send(resultHandler: HttpResponse.BodyHandler<T>) =
        httpClient.sendAsync(this, resultHandler).await()

    private fun httpRequest(
        command: String,
        args: Iterable<Pair<String, String>>,
        requestFactory: HttpRequest.Builder.() -> HttpRequest.Builder
    ) =
        HttpRequest
            .newBuilder()
            .uri(URI("$apiBaseUri/$command").withArguments(args))
            .requestFactory()
            .build()

    private val Any?.stringMessage get() =
        when (this) {
            is ByteArray -> String(this)
            else -> if (this === null) "<null>" else toString()
        }

    private fun httpClean(p: MultiPart) =
        p.also {
            it.key.checkForNotAllowedHttpCharacters(alsoAllowed = "/")
        }

    private fun httpClean(m: Iterable<Pair<String, String>>) =
        m.also { s ->
            s.forEach {
                it.first.checkForNotAllowedHttpCharacters()
                it.second.checkForNotAllowedHttpCharacters(alsoAllowed = "/")
            }
        }

    private fun String.checkForNotAllowedHttpCharacters(alsoAllowed: String = "") {
        val allowed = "$ALLOWED_HTTP_CHARS$alsoAllowed"
        if (!all { it in allowed })
            throw Panic("\"$this\" contains invalid characters for HTTP")
    }

    // endregion
}

// region Private

private fun URI.withArguments(args: Iterable<Pair<String, String>>): URI =
    URI(scheme, userInfo, host, port, path, args.argString, fragment)

private val Iterable<Pair<String, String>>.argString get() =
    joinToString("&") { arg -> "${arg.first}=${arg.second}" }

private val JsonElement?.asString get() = asPrimitive { content }

private val JsonElement?.asInt get() = asPrimitive { int }

private val JsonElement?.asLong get() = asPrimitive { long }

private fun <T> JsonElement?.asPrimitive(block: JsonPrimitive.() -> T): T =
    if (this !is JsonPrimitive)
        throw Panic("Invalid JSON object: $this")
    else block()

// endregion
