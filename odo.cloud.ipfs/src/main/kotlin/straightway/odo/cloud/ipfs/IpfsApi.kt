/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.cloud.ipfs

import straightway.http.MultiPart

/**
 * API for accessing the interplanetary file system (IPFS).
 */
interface IpfsApi {
    suspend fun cat(id: String): ByteArray
    suspend fun add(data: MultiPart): String
    suspend fun ls(id: String): List<DirEntry>

    interface Name {
        suspend fun publish(key: String, hash: String)
    }
    val name: Name

    interface Files {
        suspend fun cp(srcHash: String, dst: String)
        suspend fun mkdir(path: String)
        suspend fun rm(file: String)
        suspend fun stat(file: String): String
    }
    val files: Files

    interface Key {
        suspend fun gen(name: String): Instance
        suspend fun list(): List<Instance>
        suspend fun rm(name: String): List<Instance>
        data class Instance(val name: String, val id: String)
    }
    val key: Key

    data class DirEntry(val name: String, val hash: String, val type: Int, val size: Long)
}
