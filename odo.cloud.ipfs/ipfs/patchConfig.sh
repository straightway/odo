#!/usr/bin/env bash

# Copyright 2016 straightway
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

INSTANCE_NO=$1

SWARM_PORT=$((4001 + "$INSTANCE_NO"))
API_PORT=$((5001 + "$INSTANCE_NO"))
GATEWAY_PORT=$((8080 + "$INSTANCE_NO"))

sed -e "s/\/tcp\/4001/\/tcp\/$SWARM_PORT/g" | \
sed -e "s/\/udp\/4001/\/udp\/$SWARM_PORT/g" | \
sed -e "s/\/tcp\/5001/\/tcp\/$API_PORT/g" | \
sed -e "s/\/tcp\/8080/\/tcp\/$GATEWAY_PORT/g"
