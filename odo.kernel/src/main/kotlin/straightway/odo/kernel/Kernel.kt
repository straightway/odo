/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.kernel

import straightway.error.Panic
import straightway.odo.api.API
import straightway.odo.api.Application
import straightway.odo.boot.BootLoader
import straightway.utils.*

/**
 * The kernel retrieves an API and an Environment instance and starts an application.
 * It is added as boot loader to an applications's module layer.
 */
class Kernel : BootLoader {

    companion object {

        const val SERVICE_GETTER_INIT_ARG = "__serviceGetter"
        const val APP_MODULE_LAYER_INIT_ARG = "__appModuleLayer"

        private val ModuleLayer.serviceGetter get() = ModuleServiceGetter(
            mapOf(Application::class to this)
        ) { load(it) }
    }

    override fun initialize(args: Map<String, Any>) =
        with(
            args.serviceGetter
                ?: throw Panic("Missing args for service getter")
        ) {
            if (isInitialized)
                throw Panic("Kernel alread initialized")

            api = getService<API.Factory>().createAPI(args)
            appRunner = getService()
            application = getService()

            isInitialized = true
        }

    override suspend fun boot() {
        if (!isInitialized)
            throw Panic("Kernel booted before initialization")
        appRunner.run(application, api)
    }

    // region private

    private var isInitialized = false
    private lateinit var application: Application
    private lateinit var api: API
    private lateinit var appRunner: Application.Runner

    private val Map<String, Any>.serviceGetter: ServiceGetter? get() =
        (this[SERVICE_GETTER_INIT_ARG] as ServiceGetter?) ?: appModuleLayerServiceGetter

    private val Map<String, Any>.appModuleLayerServiceGetter: ServiceGetter? get() =
        (this[APP_MODULE_LAYER_INIT_ARG] as ModuleLayer?)?.serviceGetter

    // endregion
}
