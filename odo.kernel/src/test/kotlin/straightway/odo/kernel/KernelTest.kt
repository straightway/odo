/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.kernel

import com.nhaarman.mockitokotlin2.*
import org.junit.jupiter.api.Test
import straightway.odo.api.API
import straightway.odo.api.Application
import straightway.testing.bdd.Given
import straightway.testing.bdd.panics
import straightway.utils.ServiceGetter

class KernelTest {

    private val testUnitialized get() = Given {
        object {
            val api = mock<API>()
            val apiFactory = mock<API.Factory> {
                on { createAPI(any()) }.thenReturn(api)
            }
            val application = mock<Application>()
            val runner = mock<Application.Runner>()
            val serviceGetter = mock<ServiceGetter> {
                on { getServicesUntyped(Application::class) }.thenReturn(listOf(application))
                on { getServicesUntyped(Application.Runner::class) }.thenReturn(listOf(runner))
                on { getServicesUntyped(API.Factory::class) }.thenReturn(listOf(apiFactory))
            }
            val initArgs = mapOf(Kernel.SERVICE_GETTER_INIT_ARG to serviceGetter)
            val sut = Kernel()
        }
    }

    private val test get() = testUnitialized while_ {
        sut.initialize(initArgs)
    }

    @Test
    fun `double initialization panics`() =
        test when_ {
            sut.initialize(initArgs)
        } then panics

    @Test
    fun `initialize creates API using the passed args`() =
        testUnitialized when_ {
            sut.initialize(initArgs)
        } then {
            verify(apiFactory, times(1)).createAPI(initArgs)
        }

    @Test
    fun `initialize gets app runner`() =
        testUnitialized when_ {
            sut.initialize(initArgs)
        } then {
            verify(serviceGetter, times(1))
                .getServicesUntyped(Application.Runner::class)
        }

    @Test
    fun `initialize gets application`() =
        testUnitialized when_ {
            sut.initialize(initArgs)
        } then {
            verify(serviceGetter, times(1))
                .getServicesUntyped(Application::class)
        }

    @Test
    fun `initialize without service getter nor app module layer panics`() =
        testUnitialized when_ {
            sut.initialize(mapOf())
        } then panics

    @Test
    fun `initialize with empty app module layer panics`() =
        testUnitialized when_ {
            sut.initialize(
                mapOf(Kernel.APP_MODULE_LAYER_INIT_ARG to ModuleLayer.empty())
            )
        } then panics

    @Test
    fun `boot before initialization panics`() =
        testUnitialized whenBlocking {
            sut.boot()
        } then panics

    @Test
    fun `boot starts application via runner`() {
        test whenBlocking {
            sut.boot()
        } then {
            verifyBlocking(runner, times(1)) {
                run(eq(application), any())
            }
        }
    }

    @Test
    fun `kernel passes API to runner`() {
        test whenBlocking {
            sut.boot()
        } then {
            verifyBlocking(runner, times(1)) {
                run(any(), eq(api))
            }
        }
    }
}
