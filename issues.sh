#!/usr/bin/env bash

COMMAND=$1
ISSUE_ID=$2

function hasState() {
  ISSUE_LIST=`ls issues`
  echo $ISSUE_LIST | grep "$FORMATTED_ISSUE_ID $1" > /dev/null
}

function fileName() {
  FORMATTED_ISSUE_ID=`printf "%04d" $ISSUE_ID`
  ls issues/$FORMATTED_ISSUE_ID\ $1\ *.txt
}

if [ "$COMMAND" == start ]
then
  FORMATTED_ISSUE_ID=`printf "%04d" $ISSUE_ID`
  ISSUE_TEXT=`curl --no-progress-meter https://codeberg.org/straightway/odo/issues/$ISSUE_ID | grep "<meta property=\"og:title\"" | sed -e "s/^.*content=\"//" | sed -e "s/\" \/>$//"`
  if [ "$ISSUE_TEXT" == "odo" ]
  then
    echo "Issue $ISSUE_ID not found"
  else
    ISSUE_FILE_NAME="$FORMATTED_ISSUE_ID PROG $ISSUE_TEXT.txt"
    ISSUE_FILE_TEXT="PROG #$ISSUE_ID: $ISSUE_TEXT"
    rm -f issues/"FORMATTED_ISSUE_ID*.txt"
    echo "$ISSUE_FILE_TEXT" > "issues/$ISSUE_FILE_NAME"
    LS_ISSUE=`ls issues/*"$FORMATTED_ISSUE_ID"*.txt`
    echo "Added $LS_ISSUE"
  fi
elif [ "$COMMAND" == cancel ]
then
  if hasState PROG
  then
    FILENAME=`fileName PROG`
    rm -f "$FILENAME"
    echo "Cancelled $FILENAME"
  else
    echo "Issue $ISSUE_ID does not exist or is not in progress"
  fi
elif [ "$COMMAND" == finish ]
then
  if hasState PROG
  then
    ISSUE_FILE_NAME=`fileName PROG`
    TARGET_FILE_NAME=`echo $ISSUE_FILE_NAME | sed -e "s/ PROG / DONE /"`
    cat "$ISSUE_FILE_NAME" | sed -e "s/^PROG/DONE/" > "$TARGET_FILE_NAME"
    rm -f "$ISSUE_FILE_NAME"
    FORMATTED_ISSUE_ID=`printf "%04d" $ISSUE_ID`
    LS_ISSUE=`ls issues/*"$FORMATTED_ISSUE_ID"*.txt`
    echo "Finished $LS_ISSUE"
  else
    echo "Issue $ISSUE_ID does not  exist or is not in progress"
  fi
elif [ "$COMMAND" = list ]
then
  OPTION=$2
  PENDING=true
  if [ "$OPTION" == --progress ] || [ "$OPTION" == --progress-first ]
  then
    cat issues/*.txt | grep "PROG" | sort -s -n -k 1 -k2.2
    PENDING=
  fi
  if [ "$OPTION" == --done ] || [ "$OPTION" == --progress-first ]
  then
    cat issues/*.txt | grep -v "PROG" | sort -s -n -k 1 -k2.2
    PENDING=
  fi
  if [ "$PENDING" == "true" ]
  then
    cat issues/*.txt | sort -s -n -k 1 -k2.2
  fi
else
  echo "Usage: $0 [start|finish|cancel] issue-number"
  echo "or $0 list [--progress|--progress-first|--done]"
fi