/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.hello

import straightway.odo.api.API
import straightway.odo.api.Application

/**
 * Hello world application, as simple example and for testing.
 */
class Hello : Application {
    override suspend fun API.run() {
        println("Running Hello")
        try {
            ui.messageToUser("Hello World!")
            println("Hello Finished")
        } catch (e: Throwable) {
            println("Aborted with exception: ${e.cause}")
        }
    }

    init {
        println("Hello initialized!")
    }
}
