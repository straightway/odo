/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.ui.desktop.model

import straightway.compiler.Generated
import straightway.odo.content.PropertySet
import straightway.odo.loader.FileSystemLoader
import straightway.odo.ui.desktop.model.SystemPaths.appsRepoPath
import straightway.odo.ui.desktop.model.SystemPaths.baseDir
import straightway.odo.ui.model.*
import tornadofx.Component
import tornadofx.EventBus
import tornadofx.ScopedInstance
import java.nio.file.Path
import java.nio.file.Paths

/**
 * The ui model containing all other models for all views of the ui.
 */
class RootModel(
    passedServices: Services?
) : Component(), ScopedInstance {

    constructor() : this(null)

    val apps: AppsModel
    val identities: IdentitiesModel
    val settings: PropertySet

    // region Private

    private val ipfsRepoDir = Path.of(baseDir.toString(), "ipfs", "repo").toString()

    init {
        baseDir.toFile().mkdirs()

        val settingsFile = Paths.get(baseDir.toString(), "config.json").toFile()
        settings =
            if (settingsFile.exists())
                PropertySet.fromJson(settingsFile.readText(Charsets.UTF_8))
            else PropertySet(
                mapOf(
                    "ipfs.repoPath" to
                        ipfsRepoDir
                )
            )

        val services = passedServices ?: ServicesImpl(settings.properties) {
            fire(ActionEvent(EventBus.RunOn.ApplicationThread, it))
        }

        apps = AppsModelImpl(
            appsRepoPath,
            services
        ) @Generated("Not mockable, as default constructor required") {
            FileSystemLoader()
        }

        identities = IdentitiesModelImpl(
            services,
            Paths.get(baseDir.toString(), "identities")
        )

        subscribe<ActionEvent> { it.action() }
    }

    // endregion
}
