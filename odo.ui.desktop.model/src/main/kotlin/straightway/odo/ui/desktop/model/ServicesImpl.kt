/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.ui.desktop.model

import straightway.compiler.Generated
import straightway.crypto.CryptoFactory
import straightway.odo.cloud.*
import straightway.odo.ui.desktop.model.SystemPaths.appsRepoPath
import straightway.odo.ui.model.AppUploaderImpl
import straightway.odo.ui.model.Dispatcher
import straightway.odo.ui.model.Services
import java.io.File
import java.util.*

/**
 * Productive implementation of the Services interface.
 */
@Generated(
    "This is a collection of concrete service entities, which is not" +
        "tested"
)
class ServicesImpl(
    settings: Map<String, Any>,
    override val uiThreadDispatcher: Dispatcher
) : Services {
    override val cryptoFactory = CryptoFactory()
    override val hasher = cryptoFactory.hasher.createHasher()
    override val cloud = cloudFactory.createCryptoCloud(settings)
    override val appUploader = AppUploaderImpl(appsRepoPath, this)
    override fun createContentUploader(
        cloud: CloudWrite,
        baseDir: File,
        block: ContentUploader.DirBuilder.() -> Unit
    ) = ContentUploaderImpl(cloud, baseDir, block)

    override fun createContentDownloader(cloud: CloudRead) = ContentDownloaderImpl(cloud)

    private val cloudFactory get() =
        CryptoCloud.Factory(SuperCloud.Factory(cloudFactories))
    private val cloudFactories get() =
        ServiceLoader.load(Cloud.Factory::class.java).toList()
}
