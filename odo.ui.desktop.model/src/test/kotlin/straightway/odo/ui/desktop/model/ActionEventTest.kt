/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.ui.desktop.model

import org.junit.jupiter.api.Test
import straightway.testing.bdd.Given
import straightway.testing.flow.*
import tornadofx.EventBus

class ActionEventTest {

    @Test
    fun `runOn is accessible`() =
        Given {
            ActionEvent(EventBus.RunOn.BackgroundThread) {}
        } when_ {
            runOn
        } then {
            assertThat(it is_ equal to EventBus.RunOn.BackgroundThread)
        }

    @Test
    fun `action is accessible`() {
        val a = {}
        Given {
            ActionEvent(EventBus.RunOn.BackgroundThread, a)
        } when_ {
            action
        } then {
            assertThat(it is_ same as_ a)
        }
    }
}
