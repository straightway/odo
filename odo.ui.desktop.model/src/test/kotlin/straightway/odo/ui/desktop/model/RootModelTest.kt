/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.ui.desktop.model

import com.nhaarman.mockitokotlin2.mock
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.fail
import straightway.expr.minus
import straightway.odo.ui.model.AppsModelImpl
import straightway.odo.ui.model.IdentitiesModelImpl
import straightway.odo.ui.model.Services
import straightway.testing.bdd.Given
import straightway.testing.flow.*
import java.nio.file.*

class RootModelTest {

    private fun test(block: Path.() -> Unit) =
        Given {
            object : AutoCloseable {
                val testDir = Files.createTempDirectory("RootModelTest").apply(block)
                val services: Services = mock()
                val sut by lazy { RootModel(services) }

                override fun close() {
                    println("Finalizing $testDir")
                    testDir.toFile().deleteRecursively()
                }

                init {
                    System.setProperty("user.home", testDir.toString())
                    println("Created $testDir")
                }
            }
        }

    @Test
    fun `default constructor does not throw`() =
        assertThat({ RootModel() } does not - throw_.exception)

    @Test
    fun `not existing root dir is created`() =
        test {
            // Do nothing
        } when_ {
            sut
        } then {
            assertThat(Paths.get(testDir.toString(), ".odo").toFile().exists() is_ true)
        }

    @Test
    fun `creates AppModelImpl with services`() =
        test {
            // Do nothing
        } when_ {
            sut.apps
        } then {
            if (it !is AppsModelImpl)
                fail("$it has wrong type")
            assertThat(it.services is_ same as_ services)
        }

    @Test
    fun `creates AppModelImpl with apps path`() =
        test {
            // Do nothing
        } when_ {
            sut.apps
        } then {
            if (it !is AppsModelImpl)
                fail("$it has wrong type")

            val expectedPath = Paths.get(testDir.toString(), ".odo", "apps")
            assertThat(it.repoPath is_ equal to expectedPath)
        }

    @Test
    fun `creates IdentitiesModelImpl with identities path`() =
        test {
            // Do nothing
        } when_ {
            sut.identities
        } then {
            if (it !is IdentitiesModelImpl)
                fail("$it has wrong type")

            val expectedPath = Paths.get(testDir.toString(), ".odo", "identities")
            assertThat(it.repoPath is_ equal to expectedPath)
        }

    @Test
    fun `missing config_json creates default SettingsModel`() =
        test {
            // Do nothing
        } when_ {
            sut.settings
        } then {
            assertThat(
                it.properties is_ equal to mapOf(
                    "ipfs.repoPath" to
                        Path.of(
                            SystemPaths.baseDir.toString(),
                            "ipfs",
                            "repo"
                        ).toString()
                )
            )
        }

    @Test
    fun `reads settings from existing config_json`() =
        test {
            Paths.get(toString(), ".odo").toFile().mkdirs()
            Paths.get(toString(), ".odo", "config.json").toFile().writeText(
                "{\"a\": \"A\", \"b\": 3, \"c\": true}"
            )
        } when_ {
            sut.settings
        } then {
            assertThat(it.properties is_ equal to mapOf("a" to "A", "b" to "3", "c" to "true"))
        }
}
