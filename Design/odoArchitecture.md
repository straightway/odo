# ODO Architectural Overview

ODO is a system running on mobile or desktop devices hosting ODO-apps. These apps can be uploaded to and downloaded from serverless distributed storage clouds. All data stored within these clouds is strongly encrypted. Currently, the only cloud implementation uses [IPFS](https://ipfs.io/).

![architecture overview](TopLevelArchitecture.png)

The following text outlines the target to reach with ODO from a top level perspective. Parts that are not yet implemented are marked _italic_.

## ODO-Apps

An ODO-app consists basically of an app manifest containing meta info and a jar archive with the app binaries. It may contain more optional files, e.g. an icon image.

_The app code has restricted dependencies, i.e. it may only depend on the API interfaces provided by ODO. Especially direct access to I/O,  UI and other system resources is blocked and only possible in a controlled and trustworthy way through the ODO API. This way, the user may grant privileges to ODO-apps, e.g. read or write access to local storage or cloud storage, using certain user identities (see below) for communication or presenting a UI to the user._

## ODO Identities

In ODO, a user may create any number of identities. An identity is basically an asymmetric crypto key pair, plus additional information such as name and image. So an identity may be used to digitally decrypt or sign content.

_The user may grant ODO-apps access to identities for contact sharing and secure communication._

## ODO contacts

_Users may strip identities from their private key and share them with other users as contacts. Users may grant access to all or only specifc contacts to ODO-apps. They may also grant the privilege to add or remove contacts._

## Cloud storage

The cloud storage in ODO is closely attached to the mechanisms in IPFS, which is the basis for the currently only cloud implementation.

Users may grant cloud access for ODO-apps. Because the cloud is the medium ODO-apps use to etablish communication, it is crucial to design cloud access in a way that makes it easy to write privacy-friendly ODO-apps.

On the other hand, leaking private information should be made as hard as possible. _So direct communication must be explicitly permitted by the user._ 

Read and write access to cloud storage is realized by means of cryptography. Content may be encrypted using a contact's public key to grant access only to this contact. Or it may be encrypted using a symmetric key to grant access to groups of contacts.

Write access to cloud storage is only possible through owned *cloud roots* (in IPFS published keys). Content may be stored to the cloud and then be published under a cloud root.

Cloud content is identified by Uniform Resouce Identifiers (URI). An URI may contain all information required to read the specified data, e.g. the cloud root and the decryption key. Or it may e.g. omit the decryption key, so that the user must provide it (e.g. by providing a private key of a contact). 

## Direct communication

Cloud storage is good for a follower-based communication model with polling, high latency and relatively static content. But it does not fit well for push notifications or streaming. _Here we have to other means, that may be granted to ODO-apps as privileges:_

* _pubsub: A simple publisher/subscriber pattern. An app may subscribe to a channel and others may send on this channel. Channel events are only received by ODO-apps if they are running and online. This allows direct communication between ODO-app instances._
* _Direct connection between peers using [Tor](https://www.torproject.org/) ._

## API

The ODO API is the main interface used by ODO-apps. It is the only way for them to interact with the user or the real world. The API checks privilages and makes sure that apps only do what they are allowed to do. It offers functionality in the following areas:

- User interaction
- _Networking_
- _Storage_
- _Identity management_
- _Contact management_

It may be extended later to provide means for:
- _Payment_
- _Cross app communication_
- ...

_The API is designed with downwards compatibility in mind, so that existing apps don't have to be adapted when new API version occur._

Currently, ODO is almost entirely implemented in [kotlin](https://kotlinlang.org), and also the API is a kotlin API. I offers suspendable functions wherever reasonable, so that ODO-apps are normally asynchronous and multi-threaded.

_However, one of the targets of ODO is making it as easy as possible to write apps. For this reason, it might be useful to support a development environment, that ist either interpreted or compiled just in time, to relieve developers from having to set up a build system. Candidates may be [python](https://python.org) or kotlin script (like in gradle)._

## Whistle blowing

_As ODO allows creating apps with strong technical privacy guarantees, it might be appealing for criminals to use such ODO-apps for distribution of illegal content or planning of crimes. As providing a master key for authorities would be a threat to the privacy of the very vast majority of respectable users, ODO provides general means for whistle blowers to report criminal content. The mechanism for blowing the whistle must be anonymous, easy to use, and as difficult as possible to detect so as to protect potential whistleblowers._

_One way to realize this would be to send encrypted content on a regular basis via a pubsub mechanism. If the user does not report any criminal content, it simply contains random content. If criminal content is reported, it may contain an encrypted URI._

_Please notice, that this must be part of the ODO launcher, not of the ODO-apps, so that all ODO-apps always support whistle blowing._