/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.crypto

import com.nhaarman.mockitokotlin2.*
import org.junit.jupiter.api.Test
import straightway.testing.bdd.*
import straightway.testing.flow.assertThat
import straightway.testing.flow.equal
import straightway.testing.flow.is_
import straightway.testing.flow.to_
import straightway.utils.serializeToByteArray
import straightway.utils.toByteArray
import java.io.Serializable

class HasherTest {

    @Test
    fun `hash value of serializable object is computed from raw binary representation`() =
        Given {
            mock<Hasher>()
        } when_ {
            getHash(TestSerializable(83))
        } then hasEffect {
            verify(this).getHash(eq(TestSerializable(83).serializeToByteArray()))
        }

    @Test
    fun `hash value of string is computed from raw string bytes in UTF8`() =
        Given {
            mock<Hasher>()
        } when_ {
            getHash("string")
        } then hasEffect {
            verify(this).getHash(eq("string".toByteArray()))
        }

    @Test
    fun `hash value of string as Serializable is computed from raw string bytes in UTF8`() =
        Given {
            mock<Hasher>()
        } when_ {
            getHash("string" as Serializable)
        } then hasEffect {
            verify(this).getHash(eq("string".toByteArray()))
        }

    @Test
    fun `totalHashOutputBytes for hasher with 0 hashBits panics`() =
        Given {
            Hasher.Properties("Algo", 0)
        } when_ {
            totalHashOutputBytes
        } then panics

    @Test
    fun `totalHashOutputBytes for hasher with 1 hashBits is 1 + 1 = 2`() =
        Given {
            Hasher.Properties("Algo", 1)
        } when_ {
            totalHashOutputBytes
        } then {
            assertThat(it is_ equal to_ 2)
        }

    @Test
    fun `totalHashOutputBytes for hasher with 9 hashBits is 2 + 1 = 3`() =
        Given {
            Hasher.Properties("Algo", 9)
        } when_ {
            totalHashOutputBytes
        } then {
            assertThat(it is_ equal to_ 3)
        }

    @Test
    fun `totalHashOutputBytes for hasher with 8 hashBits is 1 + 1 = 2`() =
        Given {
            Hasher.Properties("Algo", 8)
        } when_ {
            totalHashOutputBytes
        } then {
            assertThat(it is_ equal to_ 2)
        }

    @Test
    fun `totalHashOutputBytes for hasher with 16 hashBits is 2 + 1 = 3`() =
        Given {
            Hasher.Properties("Algo", 16)
        } when_ {
            totalHashOutputBytes
        } then {
            assertThat(it is_ equal to_ 3)
        }

    @Test
    fun `totalHashOutputBytes for hasher with 17 hashBits is 3 + 1 = 4`() =
        Given {
            Hasher.Properties("Algo", 17)
        } when_ {
            totalHashOutputBytes
        } then {
            assertThat(it is_ equal to_ 4)
        }

    @Test
    fun `HEADER_BYTES is 1`() =
        assertThat(Hasher.HEADER_BYTES is_ equal to 1)

    private data class TestSerializable(val i: Int) : Serializable {
        companion object { const val serialVersionUID = 1L }
    }
}
