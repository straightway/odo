/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.crypto

import com.nhaarman.mockitokotlin2.*
import org.junit.jupiter.api.Test
import straightway.testing.bdd.Given
import straightway.testing.flow.*
import straightway.utils.*

class CombinedSignatureCheckerTest : CombinedCryptorTestBase<CombinedSignatureChecker>() {

    @Test
    fun `encrypt takes symmetric encryption to encrypt content`() =
        test when_ {
            sut.encrypt(decryptedContent)
        } then {
            verify(contentCryptorFactory, times(1)).createCryptor()
            verify(contentCryptor, times(1)).encrypt(decryptedContent)
            verify(baseSignatureChecker, times(1)).encrypt(decryptedSymmetricKey)
            assertThat(it is_ equal to_ cipherText)
        }

    @Test
    fun `properties have proper values`() =
        test when_ {
            sut.properties
        } then {
            assertThat(
                it is_ equal to_ Encryptor.Properties(
                    maxClearTextBytes = contentCryptor.properties.maxClearTextBytes,
                    blockBytes = contentCryptor.properties.blockBytes,
                    KeyProperties(
                        "${baseSignatureChecker.properties.algorithm}/" +
                            contentCryptor.properties.algorithm,
                        baseSignatureChecker.properties.keyBits
                    )
                ) { it * 2 }
            )
        }

    @Test
    fun `encryptionKey is used from base signiature checker`() =
        test when_ {
            sut.encryptionKey
        } then {
            assertThat(it is_ equal to_ baseSignatureChecker.encryptionKey)
        }

    @Test
    fun `hashAlgorithm is used from base signiature checker`() =
        test when_ {
            sut.hashAlgorithm
        } then {
            assertThat(it is_ equal to_ baseSignatureChecker.hashAlgorithm)
        }

    @Test
    fun `getOutputBytes for decrypted content yields cipher text size`() =
        test when_ {
            sut.properties.getOutputBytes(decryptedContent.size)
        } then {
            assertThat(it is_ equal to_ cipherText.size)
        }

    @Test
    fun `getOutputBytes for key size not divisible by 8`() {
        test while_ {
            stubbing(contentCryptor) {
                on { properties }
                    .thenReturn(
                        Cryptor.Properties(
                            maxClearTextBytes = 1234,
                            blockBytes = 64,
                            fixedCipherTextBytes = 2345,
                            KeyProperties(
                                algorithm = "AES",
                                keyBits = 7
                            )
                        ) { encryptedContent.size }
                    )
            }
        } when_ {
            sut.properties.getOutputBytes(decryptedContent.size)
        } then {
            val keyLenEncodedSize = 2
            assertThat(
                it is_ equal to_
                    keyLenEncodedSize + encryptedSymmetricKey.size + encryptedContent.size
            )
        }
    }

    @Test
    fun `content smaller than base max content size doesnt use content encryptor`() {
        lateinit var content: ByteArray
        test while_ {
            content = ByteArray(baseSignatureChecker.properties.maxClearTextBytes) {
                it.toByte()
            }
        } when_ {
            sut.encrypt(content)
        } then {
            assertThat(
                it is_ equal to_
                    encryptedOtherContent.size.toShort().toByteArray() +
                    encryptedOtherContent
            )
        }
    }

    @Test
    fun `is serializable`() =
        test when_ {
            sut.serializeToByteArray()
        } then {
            assert(it.deserializeTo<SignatureChecker>() is CombinedSignatureChecker)
        }

    @Test
    fun `factory is serializable`() =
        Given {
            CombinedSignatureChecker.Factory(
                mock(serializable = true),
                mock(serializable = true)
            )
        } when_ {
            serializeToByteArray()
        } then {
            assert(
                it.deserializeTo<SignatureChecker.Factory>() is
                CombinedSignatureChecker.Factory
            )
        }

    @Test
    fun `factory created entities`() =
        Given {
            object {
                val baseSignatureChecker: SignatureChecker = mock()
                val baseSignatureCheckerFactory: SignatureChecker.Factory = mock {
                    on { createSignatureChecker(any()) }.thenReturn(baseSignatureChecker)
                }
                val contentEncryptorFactory: Cryptor.Factory = mock()
                val sut = CombinedSignatureChecker.Factory(
                    baseSignatureCheckerFactory,
                    contentEncryptorFactory
                )
            }
        } when_ {
            sut.createSignatureChecker(byteArrayOf())
        } then {
            assertThat(it.baseSignatureChecker is_ same as_ baseSignatureChecker)
            assertThat(it.contentCryptorFactory is_ same as_ contentEncryptorFactory)
        }

    @Test
    fun `facory has proper properties`() =
        Given {
            object {
                val expectedProperties = Encryptor.Properties(
                    maxClearTextBytes = 17,
                    blockBytes = 13,
                    KeyProperties(
                        algorithm = "BaseAlgo/ContAlgo",
                        keyBits = 7
                    )
                ) { it * 2 }
                val baseSignatureCheckerFactory: SignatureChecker.Factory = mock {
                    on { properties }.thenReturn(
                        Encryptor.Properties(
                            maxClearTextBytes = 2,
                            blockBytes = 5,
                            KeyProperties(
                                algorithm = "BaseAlgo",
                                keyBits = 7
                            )
                        ) { it * 3 }
                    )
                }
                val contentEncryptorFactory: Cryptor.Factory = mock {
                    on { properties }.thenReturn(
                        Cryptor.Properties(
                            Encryptor.Properties(
                                maxClearTextBytes = 17,
                                blockBytes = 13,
                                KeyProperties(
                                    algorithm = "ContAlgo",
                                    keyBits = 11
                                )
                            ) { it * 2 },
                            Decryptor.Properties(
                                fixedCipherTextBytes = 0,
                                KeyProperties(
                                    algorithm = "ContAlgo",
                                    keyBits = 11
                                )
                            )
                        )
                    )
                }
                val sut = CombinedSignatureChecker.Factory(
                    baseSignatureCheckerFactory,
                    contentEncryptorFactory
                )
            }
        } when_ {
            sut.properties
        } then {
            assertThat(it is_ equal to_ expectedProperties)
        }

    @Test
    fun `factory getOutputBytes for decrypted content yields cipher text size`() =
        Given {
            object {
                val keyFactor = 2
                val keySize = 7
                val contentFactor = 3
                val baseSignatureCheckerProperties: Encryptor.Properties = mock {
                    on { getOutputBytes(any()) }.thenAnswer { keyFactor * it.getArgument<Int>(0) }
                }
                val baseSignatureCheckerFactory: SignatureChecker.Factory = mock {
                    on { properties }.thenReturn(baseSignatureCheckerProperties)
                }
                val contentEncryptorProperties: Cryptor.Properties = mock {
                    on { getOutputBytes(any()) }.thenAnswer { contentFactor * it.getArgument<Int>(0) }
                    on { keyBits }.thenReturn(keySize * Byte.SIZE_BITS)
                }
                val contentEncryptorFactory: Cryptor.Factory = mock {
                    on { properties }.thenReturn(contentEncryptorProperties)
                }
                val sut = CombinedSignatureChecker.Factory(
                    baseSignatureCheckerFactory,
                    contentEncryptorFactory
                )
            }
        } when_ {
            sut.properties.getOutputBytes(5)
        } then {
            assertThat(
                it is_ equal to_
                    keyFactor * keySize +
                    contentFactor * 5 +
                    CombinedSignatureChecker.KEY_LENGHT_ENCRYPTION_SIZE
            )
        }

    // region base class overrides

    override fun TestEnvironment<CombinedSignatureChecker>.createSut() =
        CombinedSignatureChecker(baseSignatureChecker, contentCryptorFactory)

    // endregion
}
