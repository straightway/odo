/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.crypto

import org.junit.jupiter.api.Test
import straightway.testing.bdd.Given
import straightway.testing.flow.*
import straightway.utils.deserializeTo
import straightway.utils.serializeToByteArray

class KeyPropertiesTest {

    private val test get() = Given {
        KeyProperties("algo", 12)
    }

    @Test
    fun `algorithm is accessible`() =
        test when_ {
            algorithm
        } then {
            assertThat(it is_ equal to_ "algo")
        }

    @Test
    fun `keyBits is accessible`() =
        test when_ {
            keyBits
        } then {
            assertThat(it is_ equal to_ 12)
        }

    @Test
    fun `is serializable`() =
        test when_ {
            serializeToByteArray()
        } then {
            assertThat(it.deserializeTo<KeyProperties>() is_ equal to_ this)
        }
}
