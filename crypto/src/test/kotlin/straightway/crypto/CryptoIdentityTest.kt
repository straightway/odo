/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.crypto

import com.nhaarman.mockitokotlin2.mock
import org.junit.jupiter.api.Test
import straightway.testing.bdd.Given
import straightway.testing.flow.*

class CryptoIdentityTest {

    private companion object {
        val RAW_KEY_PAIR = byteArrayOf(2, 3, 5, 7)
    }

    private val test get() =
        Given {
            object {
                val hashAlgorithm = "HashAlgo"
                val signer: Signer = mock(serializable = true) {
                    on { properties }.thenReturn(
                        Decryptor.Properties(
                            fixedCipherTextBytes = 123,
                            KeyProperties(
                                algorithm = "keyAlgo",
                                keyBits = 17
                            )
                        )
                    )
                }
                val signatureChecker: SignatureChecker = mock(serializable = true) {
                    on { properties }.thenReturn(
                        Encryptor.Properties(
                            maxClearTextBytes = 19,
                            blockBytes = 23,
                            KeyProperties(
                                algorithm = "keyAlgo",
                                keyBits = 17
                            )
                        ) { it * 2 }
                    )
                }
                val sut = CryptoIdentity.Base(
                    hashAlgorithm,
                    signer,
                    signatureChecker,
                    RAW_KEY_PAIR
                )
            }
        }

    @Test
    fun `Base hash algorithm is accessible`() =
        test when_ {
            sut.hashAlgorithm
        } then {
            assertThat(it is_ equal to_ hashAlgorithm)
        }

    @Test
    fun `signer is accessible`() =
        test when_ {
            sut.signer
        } then {
            assertThat(it is_ same as_ signer)
        }

    @Test
    fun `signatureChecker is accessible`() =
        test when_ {
            sut.signatureChecker
        } then {
            assertThat(it is_ same as_ signatureChecker)
        }

    @Test
    fun `properties are combined from signer and signature checker`() =
        test when_ {
            sut.properties
        } then {
            assertThat(
                it.blockBytes is_ equal
                    to_ signatureChecker.properties.blockBytes
            )
            assertThat(
                it.maxClearTextBytes is_ equal
                    to_ signatureChecker.properties.maxClearTextBytes
            )
            assertThat(
                it.fixedCipherTextBytes is_ equal
                    to_ signer.properties.fixedCipherTextBytes
            )
            assertThat(
                it.algorithm is_ equal
                    to_ signer.properties.algorithm
            )
            assertThat(
                it.keyBits is_ equal
                    to_ signer.properties.keyBits
            )
        }
}
