/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.crypto

import org.junit.jupiter.api.Test
import straightway.expr.minus
import straightway.testing.bdd.Given
import straightway.testing.flow.*
import straightway.utils.deserializeTo
import straightway.utils.serializeToByteArray

class EncryptorTest {

    private val test get() =
        Given {
            Encryptor.Properties(
                maxClearTextBytes = 2,
                blockBytes = 3,
                keyProperties = KeyProperties("Algo", 5)
            ) { it * 2 }
        }

    @Test
    fun `properties are serializable`() =
        test when_ {
            serializeToByteArray()
        } then {
            val deserialized = it.deserializeTo<Encryptor.Properties>()
            assertThat(deserialized is_ equal to_ this)
            assertThat(deserialized.getOutputBytes(5) is_ equal to_ 10)
        }

    @Test
    fun `properties differ from other types' instances`() =
        test when_ {
            equals(83)
        } then {
            assertThat(it is_ false)
        }

    @Test
    fun `properties differ if the maxClearTextBytes values differ`() =
        test when_ {
            equals(copy(maxClearTextBytes = 83))
        } then {
            assertThat(it is_ false)
        }

    @Test
    fun `properties differ if the blockBytes values differ`() =
        test when_ {
            equals(copy(blockBytes = 83))
        } then {
            assertThat(it is_ false)
        }

    @Test
    fun `properties differ if the agorithm values differ`() =
        test when_ {
            equals(copy(keyProperties = KeyProperties("OtherAlgo", keyBits)))
        } then {
            assertThat(it is_ false)
        }

    @Test
    fun `properties differ if the keyBits values differ`() =
        test when_ {
            equals(copy(keyProperties = KeyProperties(algorithm, 83)))
        } then {
            assertThat(it is_ false)
        }

    @Test
    fun `properties are equal even if the outputSizeGetters differ`() =
        test when_ {
            equals(copy(outputSizeGetter = { it * 5 }))
        } then {
            assertThat(it is_ true)
        }

    @Test
    fun `hash codes differ if the maxClearTextBytes values differ`() =
        test when_ {
            hashCode() to copy(maxClearTextBytes = 83).hashCode()
        } then {
            assertThat(it.first is_ not - equal to_ it.second)
        }

    @Test
    fun `hash codes differ if the blockBytes values differ`() =
        test when_ {
            hashCode() to copy(blockBytes = 83).hashCode()
        } then {
            assertThat(it.first is_ not - equal to_ it.second)
        }

    @Test
    fun `hash codes differ if the agorithm values differ`() =
        test when_ {
            hashCode() to copy(keyProperties = KeyProperties("OtherAlgo", keyBits)).hashCode()
        } then {
            assertThat(it.first is_ not - equal to_ it.second)
        }

    @Test
    fun `hash codes differ if the keyBits values differ`() =
        test when_ {
            hashCode() to copy(keyProperties = KeyProperties(algorithm, 83)).hashCode()
        } then {
            assertThat(it.first is_ not - equal to_ it.second)
        }

    @Test
    fun `hash codes are equal even if the outputSizeGetters differ`() =
        test when_ {
            hashCode() to copy(outputSizeGetter = { it * 5 }).hashCode()
        } then {
            assertThat(it.first is_ equal to_ it.second)
        }
}
