/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.crypto

import com.nhaarman.mockitokotlin2.*
import org.junit.jupiter.api.Test
import straightway.testing.assertType
import straightway.testing.bdd.Given
import straightway.testing.flow.*
import straightway.utils.deserializeTo
import straightway.utils.serializeToByteArray

class CombinedCryptoIdentityFactoryTest {

    private val test
        get() =
            Given {
                object {
                    val hasherProperties = Hasher.Properties(
                        "HashAlgo",
                        13
                    )
                    val hasher: Hasher = mock(serializable = true) {
                        on { properties }.thenReturn(hasherProperties)
                    }
                    val hasherFactory: Hasher.Factory = mock(serializable = true) {
                        on { properties }.thenReturn(hasherProperties)
                        on { createHasher() }.thenReturn(hasher)
                    }
                    val symmetricCryptor: Cryptor = mock(serializable = true)
                    val contentCryptorFactory: Cryptor.Factory =
                        mock(serializable = true) {
                            on { properties }.thenReturn(
                                Cryptor.Properties(
                                    maxClearTextBytes = 31,
                                    blockBytes = 37,
                                    fixedCipherTextBytes = 0,
                                    KeyProperties(
                                        algorithm = "ContAlgo",
                                        keyBits = 41
                                    )
                                ) { it * 2 }
                            )
                            on { createCryptor() }.thenReturn(symmetricCryptor)
                        }
                    val encryptorProperties = Encryptor.Properties(
                        maxClearTextBytes = 2,
                        blockBytes = 3,
                        keyProperties = KeyProperties("BaseAlgo", 13)
                    ) { it * 3 }
                    val decryptorProperties = Decryptor.Properties(
                        fixedCipherTextBytes = 5,
                        keyProperties = KeyProperties("BaseAlgo", 13)
                    )
                    val baseIdentity: CryptoIdentity = mock(serializable = true) {
                        on { rawKeyPair }.thenReturn(byteArrayOf(2, 3, 5, 7))
                    }
                    val baseIdentityFactory: CryptoIdentity.Factory =
                        mock(serializable = true) {
                            on { createCryptoIdentity() }.thenReturn(baseIdentity)
                            on { getCryptoIdentity(any()) }.thenReturn(baseIdentity)
                            on { properties }.thenReturn(
                                Cryptor.Properties(
                                    encryptorProperties,
                                    decryptorProperties
                                )
                            )
                        }
                    val sut = CombinedCryptoIdentityFactory(
                        hasherFactory,
                        contentCryptorFactory,
                        baseIdentityFactory
                    )
                }
            }

    @Test
    fun `createCryptoIdentity uses CombinedSigner`() =
        test when_ {
            sut.createCryptoIdentity()
        } then {
            it.signer.assertType<CombinedSigner> {
                assertThat(baseSigner is_ same as_ baseIdentity)
            }
        }

    @Test
    fun `createCryptoIdentity uses CombinedSignatureChecker`() =
        test when_ {
            sut.createCryptoIdentity()
        } then {
            it.signatureChecker.assertType<CombinedSignatureChecker> {
                assertThat(baseSignatureChecker is_ same as_ baseIdentity)
            }
        }

    @Test
    fun `createCryptoIdentity uses rawKeyPair from base implementation`() =
        test when_ {
            sut.createCryptoIdentity()
        } then {
            assertThat(it.rawKeyPair is_ equal to byteArrayOf(2, 3, 5, 7))
        }

    @Test
    fun `createCryptoIdentity uses hash algorithm of hasher factory`() =
        test when_ {
            sut.createCryptoIdentity()
        } then {
            assertThat(it.hashAlgorithm is_ equal to_ "HashAlgo")
        }

    @Test
    fun `getCryptoIdentity calls base implementation`() =
        test when_ {
            sut.getCryptoIdentity(byteArrayOf(83, 89))
        } then {
            verify(baseIdentityFactory).getCryptoIdentity(byteArrayOf(83, 89))
        }

    @Test
    fun `getCryptoIdentity uses passed rawKeyPair`() =
        test when_ {
            sut.getCryptoIdentity(byteArrayOf(83, 89))
        } then {
            assertThat(it.rawKeyPair is_ equal to byteArrayOf(83, 89))
        }

    @Test
    fun `getCryptoIdentity uses CombinedSigner`() =
        test when_ {
            sut.getCryptoIdentity(byteArrayOf(83, 89))
        } then {
            it.signer.assertType<CombinedSigner> {
                assertThat(baseSigner is_ same as_ baseIdentity)
            }
        }

    @Test
    fun `getCryptoIdentity uses CombinedSignatureChecker`() =
        test when_ {
            sut.getCryptoIdentity(byteArrayOf(83, 89))
        } then {
            it.signatureChecker.assertType<CombinedSignatureChecker> {
                assertThat(baseSignatureChecker is_ same as_ baseIdentity)
            }
        }

    @Test
    fun `getCryptoIdentity uses hash algorithm of hasher factory`() =
        test when_ {
            sut.getCryptoIdentity(byteArrayOf(83, 89))
        } then {
            assertThat(it.hashAlgorithm is_ equal to_ "HashAlgo")
        }

    @Test
    fun `properties has proper values`() =
        test when_ {
            sut.properties
        } then {
            assertThat(
                it.algorithm is_ equal
                    to_ "BaseAlgo/ContAlgo"
            )
            assertThat(
                it.keyBits is_ equal
                    to_ encryptorProperties.keyBits
            )
            assertThat(
                it.blockBytes is_ equal
                    to_ contentCryptorFactory.properties.blockBytes
            )
            assertThat(
                it.maxClearTextBytes is_ equal
                    to_ contentCryptorFactory.properties.maxClearTextBytes
            )
            assertThat(
                it.fixedCipherTextBytes is_ equal
                    to_ contentCryptorFactory.properties.fixedCipherTextBytes
            )
        }

    @Test
    fun `is serializable`() =
        test when_ {
            sut.serializeToByteArray()
        } then {
            assert(
                it.deserializeTo<CryptoIdentity.Factory>()
                is CombinedCryptoIdentityFactory
            )
        }
}
