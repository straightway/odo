/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.crypto.impl

import org.junit.jupiter.api.Test
import straightway.crypto.HashAlgorithm
import straightway.testing.bdd.Given
import straightway.testing.bdd.throws
import straightway.testing.flow.assertThat
import straightway.testing.flow.equal
import straightway.testing.flow.is_
import straightway.testing.flow.to_
import java.security.MessageDigest

class SHA512HasherTest {

    private companion object {
        val valueToHash = byteArrayOf(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11)
        val expectedHash = byteArrayOf(HashAlgorithm.SHA512.encoded) +
            MessageDigest.getInstance("SHA-512").digest(valueToHash)
    }

    @Test
    fun `hasher uses SHA-512`() =
        Given {
            SHA512Hasher()
        } when_ {
            getHash(valueToHash)
        } then {
            assertThat(it is_ equal to_ expectedHash)
        }

    @Test
    fun `hashBits is according to bits of hashed value`() =
        Given {
            SHA512Hasher()
        } when_ {
            getHash(valueToHash)
        } then {
            assertThat((it.size - 1) * Byte.SIZE_BITS is_ equal to_ properties.hashBits)
        }

    @Test
    fun `algorithmId is HSA512`() =
        Given {
            SHA512Hasher.Factory()
        } when_ {
            properties.hashAlgorithm
        } then {
            assertThat(it is_ equal to_ "SHA512")
        }

    @Test
    fun `factory creates hasher`() =
        Given {
            SHA512Hasher.Factory()
        } when_ {
            createHasher()
        } then throws.nothing
}
