/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.crypto.impl

import org.junit.jupiter.api.Test
import straightway.crypto.*
import straightway.expr.minus
import straightway.testing.bdd.*
import straightway.testing.flow.*
import straightway.utils.deserializeTo
import straightway.utils.serializeToByteArray

class RSA2048RawSignatureCheckerTest {

    private companion object {
        val sut by lazy {
            RSA2048RawSignatureChecker(
                RSA2048RawTestEnvironment.cryptor.keyPair.public,
                RSA2048RawTestEnvironment.hasherFactory
            )
        }
        val matchingCryptor get() = RSA2048RawTestEnvironment.cryptor
        val notMatchingCryptor get() = RSA2048RawTestEnvironment.otherCryptor
    }

    @Test
    fun `can be created using raw key bytes`() =
        Given {
            RSA2048RawSignatureChecker(
                byteArrayOf(CipherAlgorithm.RSA2048.encoded) +
                    matchingCryptor.keyPair.public.encoded,
                CryptoFactoryTestEnvironment.factory.hasher
            )
        } when_ {
            matchingCryptor.sign(byteArrayOf(1, 2, 3))
        } then {
            assertThat(isSignatureValid(byteArrayOf(1, 2, 3), it) is_ true)
        }

    @Test
    fun `creation with invalid type in raw key bytes panics`() {
        Given {
            byteArrayOf(Byte.MIN_VALUE) + matchingCryptor.keyPair.public.encoded
        } when_ {
            RSA2048RawSignatureChecker(this, CryptoFactoryTestEnvironment.factory.hasher)
        } then panics
    }

    @Test
    fun `encrypted can be decrypted by matching cryptor`() =
        Given {
            sut
        } when_ {
            encrypt(byteArrayOf(1, 2, 3))
        } then {
            assertThat(matchingCryptor.decrypt(it) is_ equal to_ byteArrayOf(1, 2, 3))
        }

    @Test
    fun `encrypted can not be decrypted by not matching cryptor`() =
        Given {
            sut
        } when_ {
            encrypt(byteArrayOf(1, 2, 3))
        } then {
            assertThat({ notMatchingCryptor.decrypt(it) } does throw_.exception)
        }

    @Test
    fun `signature of matching cryptor can be verified`() =
        Given {
            sut
        } when_ {
            matchingCryptor.sign(byteArrayOf(1, 2, 3))
        } then {
            assertThat(isSignatureValid(byteArrayOf(1, 2, 3), it) is_ true)
        }

    @Test
    fun `signature from not matching cryptor can not be verified`() =
        Given {
            sut
        } when_ {
            notMatchingCryptor.sign(byteArrayOf(1, 2, 3))
        } then {
            assertThat(isSignatureValid(byteArrayOf(1, 2, 3), it) is_ false)
        }

    @Test
    fun `encryptionKey is public key`() =
        Given {
            sut
        } when_ {
            encryptionKey
        } then {
            assertThat(
                it is_ equal to_
                    byteArrayOf(CipherAlgorithm.RSA2048.encoded) +
                    matchingCryptor.keyPair.public.encoded
            )
        }

    @Test
    fun `signatureCheckKey is public key`() =
        Given {
            sut
        } when_ {
            signatureCheckKey
        } then {
            assertThat(
                it is_ equal to_
                    byteArrayOf(CipherAlgorithm.RSA2048.encoded) +
                    matchingCryptor.keyPair.public.encoded
            )
        }

    @Test
    fun `keyBits is 2048`() =
        Given {
            sut
        } when_ {
            properties.keyBits
        } then {
            assertThat(it is_ equal to_ 2048)
        }

    @Test
    fun `cryptor is serializable`() =
        Given {
            sut
        } when_ {
            serializeToByteArray()
        } then {
            val deserialzed = it.deserializeTo<SignatureChecker>()
            val signature = matchingCryptor.sign(byteArrayOf(1, 2, 3))
            assertThat(deserialzed.isSignatureValid(byteArrayOf(1, 2, 3), signature) is_ true)
        }

    @Test
    fun `hashAlgorithm is SHA512`() =
        Given {
            sut
        } when_ {
            hashAlgorithm
        } then {
            assertThat(it is_ equal to_ "SHA512")
        }

    @Test
    fun `block size is 0`() =
        Given {
            sut
        } when_ {
            properties.blockBytes
        } then {
            assertThat(it is_ equal to_ 0)
        }

    @Test
    fun `output size is always key size`() =
        (0..sut.properties.maxClearTextBytes).forEach {
            assertThat(
                sut.properties.getOutputBytes(it) is_
                    equal to_ ((sut.properties.keyBits - 1) / 8) + 1
            )
        }

    @Test
    fun `encryption with max clear text bytes does not throw`() =
        assertThat(
            { sut.encrypt(ByteArray(sut.properties.maxClearTextBytes)) } does
                not - throw_.exception
        )

    @Test
    fun `encryption with one byte more than max clear text bytes throws`() =
        assertThat(
            { sut.encrypt(ByteArray(sut.properties.maxClearTextBytes + 1)) } does
                throw_.exception
        )

    @Test
    fun `algorithm is RSA`() =
        Given {
            sut
        } when_ {
            properties.algorithm
        } then {
            assertThat(it is_ equal to_ "RSA")
        }

    @Test
    fun `key is accessible`() =
        Given {
            sut
        } when_ {
            sut.key
        } then {
            assertThat(
                it is_ same as_ matchingCryptor.keyPair.public
            )
        }

    @Test
    fun `factory yields RSA2048SignatureChecker`() =
        Given {
            RSA2048RawSignatureChecker.Factory(RSA2048RawTestEnvironment.hasherFactory)
        } when_ {
            createSignatureChecker(RSA2048RawTestEnvironment.cryptor.signatureCheckKey)
        } then throws.nothing

    @Test
    fun `factory properties are equal to instance properties`() =
        Given {
            RSA2048RawSignatureChecker.Factory(RSA2048RawTestEnvironment.hasherFactory)
        } when_ {
            properties
        } then {
            assertThat(
                it is_ equal to_
                    createSignatureChecker(
                        RSA2048RawTestEnvironment.cryptor.signatureCheckKey
                    ).properties
            )
        }

    @Test
    fun `factory is serializable`() =
        Given {
            RSA2048RawSignatureChecker.Factory(RSA2048RawTestEnvironment.hasherFactory)
        } when_ {
            serializeToByteArray()
        } then {
            assertThat(it.deserializeTo<SignatureChecker.Factory>().properties is_ equal to_ properties)
        }

    @Test
    fun `has serialVersionUID`() =
        assertThat(RSA2048RawCryptor.serialVersionUID is_ equal to_ 1L)
}
