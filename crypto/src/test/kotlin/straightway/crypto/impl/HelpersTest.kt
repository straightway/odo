/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.crypto.impl

import org.junit.jupiter.api.Test
import straightway.crypto.CipherAlgorithm
import straightway.testing.bdd.*
import straightway.testing.flow.*
import java.lang.IndexOutOfBoundsException
import java.security.KeyPairGenerator

class HelpersTest {

    private companion object {
        val keyPair by lazy {
            KeyPairGenerator.getInstance(RSA2048.properties.algorithm)
                .apply { initialize(RSA2048.properties.keyBits) }.genKeyPair()
        }
    }

    @Test
    fun `stripAndCheckAlgorithmType throws if byte array is empty`() =
        Given {
            byteArrayOf()
        } when_ {
            stripAndCheckAlgorithmType(CipherAlgorithm.AES256)
        } then throws<IndexOutOfBoundsException>()

    @Test
    fun `stripAndCheckAlgorithmType throws if type does not match`() =
        Given {
            byteArrayOf(CipherAlgorithm.RSA2048.encoded)
        } when_ {
            stripAndCheckAlgorithmType(CipherAlgorithm.AES256)
        } then panics

    @Test
    fun `stripAndCheckAlgorithmType returns rest bytes if type does match`() =
        Given {
            byteArrayOf(CipherAlgorithm.RSA2048.encoded, 1, 2, 3)
        } when_ {
            stripAndCheckAlgorithmType(CipherAlgorithm.RSA2048)
        } then {
            assertThat(it is_ equal to_ byteArrayOf(1, 2, 3))
        }

    @Test
    fun `with prepends encoded cipher algoritm to byte array`() =
        Given {
            byteArrayOf(1, 2, 3)
        } when_ {
            with(CipherAlgorithm.RSA2048)
        } then {
            assertThat(
                it is_ equal to_
                    byteArrayOf(CipherAlgorithm.RSA2048.encoded, 1, 2, 3)
            )
        }

    @Test
    fun `publicKeyFrom gets public key from raw bytes`() =
        Given {
            keyPair.public.encoded
        } when_ {
            publicKeyFrom(this, RSA2048.properties.algorithm)
        } then {
            assertThat(it is_ equal to_ keyPair.public)
        }

    @Test
    fun `privateKeyFrom gets private key from raw bytes`() =
        Given {
            keyPair.private.encoded
        } when_ {
            privateKeyFrom(this, RSA2048.properties.algorithm)
        } then {
            assertThat(it is_ equal to_ keyPair.private)
        }
}
