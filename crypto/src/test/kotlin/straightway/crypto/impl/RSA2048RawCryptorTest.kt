/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.crypto.impl

import com.nhaarman.mockitokotlin2.mock
import org.junit.jupiter.api.Test
import straightway.crypto.*
import straightway.expr.minus
import straightway.testing.bdd.Given
import straightway.testing.bdd.throws
import straightway.testing.flow.*
import straightway.utils.deserializeTo
import straightway.utils.serializeToByteArray

class RSA2048RawCryptorTest {

    private companion object {
        val hasherFactory get() = RSA2048RawTestEnvironment.hasherFactory
        val sut get() = RSA2048RawTestEnvironment.cryptor
        val otherInstance get() = RSA2048RawTestEnvironment.otherCryptor
    }

    @Test
    fun `new instance generates a new key`() =
        Given {
            sut
        } when_ {
            keyPair
        } then {
            assertThat(it.public.encoded is_ not - empty)
            assertThat(it.private.encoded is_ not - empty)
        }

    @Test
    fun `two instance generate different keys`() {
        assertThat(
            sut.keyPair.public.encoded is_ not - equal to_
                otherInstance.keyPair.public.encoded
        )
        assertThat(
            sut.keyPair.private.encoded is_ not - equal to_
                otherInstance.keyPair.private.encoded
        )
    }

    @Test
    fun `encrypted can be decrypted by same cryptor`() =
        Given {
            sut
        } when_ {
            encrypt(byteArrayOf(1, 2, 3))
        } then {
            assertThat(decrypt(it) is_ equal to_ byteArrayOf(1, 2, 3))
        }

    @Test
    fun `encrypted can not be decrypted by other cryptor`() =
        Given {
            sut
        } when_ {
            encrypt(byteArrayOf(1, 2, 3))
        } then {
            assertThat(
                { otherInstance.decrypt(it) } does throw_.exception
            )
        }

    @Test
    fun `signature can be verified with the same cryptor`() =
        Given {
            sut
        } when_ {
            sign(byteArrayOf(1, 2, 3))
        } then {
            assertThat(isSignatureValid(byteArrayOf(1, 2, 3), it) is_ true)
        }

    @Test
    fun `signature can not be verified with the other cryptor`() =
        Given {
            sut
        } when_ {
            sign(byteArrayOf(1, 2, 3))
        } then {
            assertThat(otherInstance.isSignatureValid(byteArrayOf(1, 2, 3), it) is_ false)
        }

    @Test
    fun `signKey is private key`() =
        Given {
            sut
        } when_ {
            signKey
        } then {
            assertThat(
                it is_ equal to_
                    byteArrayOf(CipherAlgorithm.RSA2048.encoded) + keyPair.private.encoded
            )
        }

    @Test
    fun `decryptionKey is private key`() =
        Given {
            sut
        } when_ {
            decryptionKey
        } then {
            assertThat(
                it is_ equal to_
                    byteArrayOf(CipherAlgorithm.RSA2048.encoded) + keyPair.private.encoded
            )
        }

    @Test
    fun `encryptionKey is public key`() =
        Given {
            sut
        } when_ {
            encryptionKey
        } then {
            assertThat(
                it is_ equal to_
                    byteArrayOf(CipherAlgorithm.RSA2048.encoded) + keyPair.public.encoded
            )
        }

    @Test
    fun `signatureCheckKey is public key`() =
        Given {
            sut
        } when_ {
            signatureCheckKey
        } then {
            assertThat(
                it is_ equal to_
                    byteArrayOf(CipherAlgorithm.RSA2048.encoded) + keyPair.public.encoded
            )
        }

    @Test
    fun `keyBits is 2048`() =
        Given {
            sut
        } when_ {
            properties.keyBits
        } then {
            assertThat(it is_ equal to_ 2048)
        }

    @Test
    fun `hashAlgorithm is SHA512`() =
        Given {
            sut
        } when_ {
            hashAlgorithm
        } then {
            assertThat(it is_ equal to_ "SHA512")
        }

    @Test
    fun `fixedCipherTextBytes is equal to the size of the cipher text`() =
        (1..sut.properties.maxClearTextBytes).forEach {
            assertThat(
                sut.encrypt(ByteArray(it)) has
                    size of sut.properties.fixedCipherTextBytes
            )
        }

    @Test
    fun `cryptor is serializable`() =
        Given {
            sut
        } when_ {
            serializeToByteArray()
        } then {
            val deserialzed = it.deserializeTo<CryptoIdentity>()
            val signature = sign(byteArrayOf(1, 2, 3))
            assertThat(deserialzed.isSignatureValid(byteArrayOf(1, 2, 3), signature) is_ true)
        }

    @Test
    fun `algorithm is RSA`() =
        Given {
            sut
        } when_ {
            properties.algorithm
        } then {
            assertThat(it is_ equal to_ "RSA")
        }

    @Test
    fun `rawKeyPair is serialized RSA key pair`() =
        Given {
            sut
        } when_ {
            rawKeyPair
        } then {
            assertThat(it is_ equal to sut.keyPair.serializeToByteArray())
        }

    @Test
    fun `has serialVersionUID`() =
        assertThat(RSA2048RawCryptor.serialVersionUID is_ equal to_ 1L)

    @Test
    fun `hashAlgorithm is forwarded from factory`() =
        Given {
            val factory = mock<Hasher.Factory> {
                on { properties }.thenReturn(
                    Hasher.Properties(
                        hashAlgorithm = "FactoryHashAlgorithm",
                        hashBits = 123
                    )
                )
            }
            RSA2048RawCryptor(factory)
        } when_ {
            hashAlgorithm
        } then {
            assertThat(it is_ equal to_ "FactoryHashAlgorithm")
        }

    @Test
    fun `factory returns crypto identity properties`() =
        Given {
            RSA2048RawCryptor.Factory(mock())
        } when_ {
            properties
        } then {
            assertThat(it.blockBytes is_ equal to_ 0)
            assertThat(it.maxClearTextBytes is_ equal to_ 245)
            assertThat(it.fixedCipherTextBytes is_ equal to_ 256)
        }

    @Test
    fun `factory creates fresh cryptor`() =
        Given {
            RSA2048RawCryptor.Factory(RSA2048RawTestEnvironment.hasherFactory)
        } when_ {
            createCryptoIdentity()
        } then throws.nothing

    @Test
    fun `factoy getCryptoIdentity reconstruct identity from rawKeyPair`() =
        Given {
            RSA2048RawCryptor.Factory(RSA2048RawTestEnvironment.hasherFactory)
        } when_ {
            getCryptoIdentity(sut.rawKeyPair)
        } then {
            val signed = byteArrayOf(5, 7, 11, 13)
            val signature = sut.sign(signed)
            assertThat(it.isSignatureValid(signed, signature) is_ true)
        }

    @Test
    fun `factory is serializable`() =
        Given {
            RSA2048RawCryptor.Factory(RSA2048RawTestEnvironment.hasherFactory)
        } when_ {
            serializeToByteArray()
        } then {
            assertThat(
                it.deserializeTo<CryptoIdentity.Factory>().properties
                    is_ equal to_ properties
            )
        }
}
