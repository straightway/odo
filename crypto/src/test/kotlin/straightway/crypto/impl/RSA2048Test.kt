/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.crypto.impl

import org.junit.jupiter.api.Test
import straightway.testing.bdd.Given
import straightway.testing.flow.*

class RSA2048Test {

    @Test
    fun `key properties is RSA2048`() =
        Given {
            RSA2048
        } when_ {
            properties
        } then {
            assertThat(it.algorithm is_ equal to_ "RSA")
            assertThat(it.keyBits is_ equal to_ 2048)
        }

    @Test
    fun `properties has proper values`() =
        Given {
            RSA2048
        } when_ {
            properties
        } then {
            assertThat(it.maxClearTextBytes is_ equal to_ 245)
            assertThat(it.blockBytes is_ equal to_ 0)
            assertThat(it.fixedCipherTextBytes is_ equal to_ 256)
        }
}
