/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.crypto.impl

import org.junit.jupiter.api.Test
import straightway.crypto.*
import straightway.testing.bdd.Given
import straightway.testing.bdd.throws
import straightway.testing.flow.*
import straightway.utils.deserializeTo
import straightway.utils.serializeToByteArray

class RSA2048RawSignerTest {

    private companion object {
        val sut by lazy {
            RSA2048RawSigner(
                RSA2048RawTestEnvironment.cryptor.keyPair.private,
                RSA2048RawTestEnvironment.hasherFactory
            )
        }
        val matchingCryptor get() = RSA2048RawTestEnvironment.cryptor
        val notMatchingCryptor get() = RSA2048RawTestEnvironment.otherCryptor
    }

    @Test
    fun `can be created using raw key bytes`() =
        Given {
            RSA2048RawSigner(
                byteArrayOf(CipherAlgorithm.RSA2048.encoded) +
                    matchingCryptor.keyPair.private.encoded,
                CryptoFactoryTestEnvironment.factory.hasher
            )
        } when_ {
            sign(byteArrayOf(1, 2, 3))
        } then {
            assertThat(matchingCryptor.isSignatureValid(byteArrayOf(1, 2, 3), it) is_ true)
        }

    @Test
    fun `encrypted can be decrypted by matching cryptor`() =
        Given {
            matchingCryptor
        } when_ {
            encrypt(byteArrayOf(1, 2, 3))
        } then {
            assertThat(sut.decrypt(it) is_ equal to_ byteArrayOf(1, 2, 3))
        }

    @Test
    fun `encrypted can not be decrypted by not matching cryptor`() =
        Given {
            notMatchingCryptor
        } when_ {
            encrypt(byteArrayOf(1, 2, 3))
        } then {
            assertThat({ sut.decrypt(it) } does throw_.exception)
        }

    @Test
    fun `signature can be verified by matching cryptor`() =
        Given {
            sut
        } when_ {
            sign(byteArrayOf(1, 2, 3))
        } then {
            assertThat(matchingCryptor.isSignatureValid(byteArrayOf(1, 2, 3), it) is_ true)
        }

    @Test
    fun `signature can not be verified by not matching cryptor`() =
        Given {
            sut
        } when_ {
            sign(byteArrayOf(1, 2, 3))
        } then {
            assertThat(
                notMatchingCryptor.isSignatureValid(byteArrayOf(1, 2, 3), it)
                    is_ false
            )
        }

    @Test
    fun `signKey is private key`() =
        Given {
            sut
        } when_ {
            signKey
        } then {
            assertThat(
                it is_ equal to_
                    byteArrayOf(CipherAlgorithm.RSA2048.encoded) +
                    matchingCryptor.keyPair.private.encoded
            )
        }

    @Test
    fun `decryptionKey is private key`() =
        Given {
            sut
        } when_ {
            decryptionKey
        } then {
            assertThat(
                it is_ equal to_
                    byteArrayOf(CipherAlgorithm.RSA2048.encoded) +
                    matchingCryptor.keyPair.private.encoded
            )
        }

    @Test
    fun `keyBits is 2048`() =
        Given {
            sut
        } when_ {
            properties.keyBits
        } then {
            assertThat(it is_ equal to_ 2048)
        }

    @Test
    fun `cryptor is serializable`() =
        Given {
            sut
        } when_ {
            serializeToByteArray()
        } then {
            val deserialzed = it.deserializeTo<Signer>()
            val signature = deserialzed.sign(byteArrayOf(1, 2, 3))
            assertThat(matchingCryptor.isSignatureValid(byteArrayOf(1, 2, 3), signature) is_ true)
        }

    @Test
    fun `hashAlgorithm is SHA512`() =
        Given {
            sut
        } when_ {
            hashAlgorithm
        } then {
            assertThat(it is_ equal to_ "SHA512")
        }

    @Test
    fun `fixedCipherTextBytes is equal to key bytes`() =
        Given {
            sut
        } when_ {
            properties.fixedCipherTextBytes
        } then {
            assertThat(it is_ equal to_ (properties.keyBits - 1) / 8 + 1)
        }

    @Test
    fun `algorithm is RSA`() =
        Given {
            sut
        } when_ {
            properties.algorithm
        } then {
            assertThat(it is_ equal to_ "RSA")
        }

    @Test
    fun `key is accessible`() =
        Given {
            sut
        } when_ {
            sut.key
        } then {
            assertThat(
                it is_ same as_ matchingCryptor.keyPair.private
            )
        }

    @Test
    fun `factory yields RSA2048Signer`() =
        Given {
            RSA2048RawSigner.Factory(RSA2048RawTestEnvironment.hasherFactory)
        } when_ {
            createSigner(RSA2048RawTestEnvironment.cryptor.signKey)
        } then throws.nothing

    @Test
    fun `factory properties are equal to instance properties`() =
        Given {
            RSA2048RawSigner.Factory(RSA2048RawTestEnvironment.hasherFactory)
        } when_ {
            properties
        } then {
            assertThat(
                it is_ equal to_
                    createSigner(RSA2048RawTestEnvironment.cryptor.signKey).properties
            )
        }

    @Test
    fun `factory is serializable`() =
        Given {
            RSA2048RawSigner.Factory(RSA2048RawTestEnvironment.hasherFactory)
        } when_ {
            serializeToByteArray()
        } then {
            assertThat(it.deserializeTo<Signer.Factory>().properties is_ equal to_ properties)
        }

    @Test
    fun `has serialVersionUID`() =
        assertThat(RSA2048RawCryptor.serialVersionUID is_ equal to_ 1L)
}
