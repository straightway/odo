/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.crypto.impl

import org.junit.jupiter.api.Test
import straightway.crypto.CipherAlgorithm
import straightway.crypto.Cryptor
import straightway.expr.minus
import straightway.testing.bdd.*
import straightway.testing.flow.assertThat
import straightway.testing.flow.does
import straightway.testing.flow.empty
import straightway.testing.flow.equal
import straightway.testing.flow.is_
import straightway.testing.flow.not
import straightway.testing.flow.throw_
import straightway.testing.flow.to_
import straightway.utils.deserializeTo
import straightway.utils.serializeToByteArray

class AES256CryptorTest {

    @Test
    fun `new instance generates a new key`() =
        Given {
            AES256Cryptor()
        } when_ {
            key
        } then {
            assertThat(it.encoded is_ not - empty)
        }

    @Test
    fun `two instance generates have different keys`() =
        Given {
            AES256Cryptor()
        } when_ {
            key
        } then {
            assertThat(it.encoded is_ not - equal to_ AES256Cryptor().key)
        }

    @Test
    fun `encrypted can be decrypted by same cryptor`() =
        Given {
            AES256Cryptor()
        } when_ {
            encrypt(byteArrayOf(1, 2, 3))
        } then {
            assertThat(decrypt(it) is_ equal to_ byteArrayOf(1, 2, 3))
        }

    @Test
    fun `encrypted can not be decrypted by other cryptor`() =
        Given {
            AES256Cryptor()
        } when_ {
            encrypt(byteArrayOf(1, 2, 3))
        } then {
            assertThat({ AES256Cryptor().decrypt(it) } does throw_.exception)
        }

    @Test
    fun `two cryptor instances with the same key passed can decrypt encrypted text`() {
        val keyBytes = byteArrayOf(CipherAlgorithm.AES256.encoded) +
            ByteArray(AES256Cryptor.properties.keyBits / Byte.SIZE_BITS) {
                it.toByte()
            }
        Given {
            AES256Cryptor(keyBytes)
        } when_ {
            encrypt(byteArrayOf(1, 2, 3))
        } then {
            assertThat(
                AES256Cryptor(keyBytes).decrypt(it)
                    is_ equal to_ byteArrayOf(1, 2, 3)
            )
        }
    }

    @Test
    fun `encryption key is the key bytes`() {
        val keyBytes = byteArrayOf(CipherAlgorithm.AES256.encoded) +
            ByteArray(AES256Cryptor.properties.keyBits / Byte.SIZE_BITS) {
                it.toByte()
            }
        Given {
            AES256Cryptor(keyBytes)
        } when_ {
            encryptionKey
        } then {
            assertThat(it is_ equal to_ keyBytes)
        }
    }

    @Test
    fun `encryption key is the algorithm type plus key bytes`() {
        val keyBytes = byteArrayOf(CipherAlgorithm.AES256.encoded) +
            ByteArray(AES256Cryptor.properties.keyBits / Byte.SIZE_BITS) {
                it.toByte()
            }
        Given {
            AES256Cryptor(keyBytes)
        } when_ {
            encryptionKey
        } then {
            assertThat(it is_ equal to_ keyBytes)
        }
    }

    @Test
    fun `encryption key and decryption key are the same`() =
        Given {
            AES256Cryptor()
        } when_ {
            decryptionKey
        } then {
            assertThat(it is_ equal to_ decryptionKey)
        }

    @Test
    fun `initialization with invalid algorithm type panics`() {
        Given {
            byteArrayOf(Byte.MIN_VALUE) +
                ByteArray(AES256Cryptor.properties.keyBits / Byte.SIZE_BITS) {
                    it.toByte()
                }
        } when_ {
            AES256Cryptor(this)
        } then panics
    }

    @Test
    fun `cryptor is serializable`() {
        lateinit var encrypted: ByteArray
        Given {
            AES256Cryptor()
        } while_ {
            encrypted = encrypt(byteArrayOf(1, 2, 3))
        } when_ {
            serializeToByteArray()
        } then {
            val deserializedCryptor = it.deserializeTo<Cryptor>()
            assertThat(deserializedCryptor.decrypt(encrypted) is_ equal to_ byteArrayOf(1, 2, 3))
        }
    }

    @Test
    fun `keyBits is 256`() =
        Given {
            AES256Cryptor()
        } when_ {
            properties.keyBits
        } then {
            assertThat(it is_ equal to_ 256)
        }

    @Test
    fun `block size is 16`() =
        Given {
            AES256Cryptor()
        } when_ {
            properties.blockBytes
        } then {
            assertThat(it is_ equal to_ 16)
        }

    @Test
    fun `max clear text size is unconstrained`() =
        Given {
            AES256Cryptor()
        } when_ {
            properties.maxClearTextBytes
        } then {
            assertThat(it is_ equal to_ Int.MAX_VALUE)
        }

    @Test
    fun `output size for input size`() =
        Given {
            AES256Cryptor()
        } when_ {
            properties.getOutputBytes(2 * properties.blockBytes)
        } then {
            assertThat(it is_ equal to_ 3 * properties.blockBytes)
        }

    @Test
    fun `fixedCipherTextSize is zero, because there is no fixed size`() =
        Given {
            AES256Cryptor()
        } when_ {
            properties.fixedCipherTextBytes
        } then {
            assertThat(it is_ equal to_ 0)
        }

    @Test
    fun `algorithm is AES256`() =
        Given {
            AES256Cryptor()
        } when_ {
            properties.algorithm
        } then {
            assertThat(it is_ equal to_ "AES")
        }

    @Test
    fun `factory returns crypto identity properties`() =
        Given {
            AES256Cryptor.Factory()
        } when_ {
            properties
        } then {
            assertThat(
                it.blockBytes is_ equal to_ 16
            )
            assertThat(
                it.maxClearTextBytes is_ equal to_ Int.MAX_VALUE
            )
            assertThat(
                it.fixedCipherTextBytes is_ equal to_ 0
            )
        }

    @Test
    fun `factory getCryptor returns AES256Cryptor from given key`() =
        Given {
            object {
                val key = ByteArray(16) { it.toByte() }
                val sut = AES256Cryptor.Factory()
            }
        } when_ {
            sut.getCryptor(byteArrayOf(CipherAlgorithm.AES256.encoded) + key)
        } then {
            assertThat(it.key.encoded is_ equal to_ key)
        }

    @Test
    fun `factory createCryptor returns fresh AES256Cryptor`() =
        Given {
            AES256Cryptor.Factory()
        } when_ {
            createCryptor()
        } then throws.nothing

    @Test
    fun `factory is serializable`() =
        Given {
            AES256Cryptor.Factory()
        } when_ {
            serializeToByteArray()
        } then {
            assertThat(
                it.deserializeTo<Cryptor.Factory>().properties
                    is_ equal to_ properties
            )
        }

    @Test
    fun `has serialVersionUID`() =
        assertThat(AES256Cryptor.serialVersionUID is_ equal to_ 1L)
}
