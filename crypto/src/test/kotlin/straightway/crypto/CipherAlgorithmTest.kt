/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.crypto

import org.junit.jupiter.api.Test
import straightway.testing.bdd.Given
import straightway.testing.bdd.panics
import straightway.testing.flow.*

class CipherAlgorithmTest {

    @Test
    fun `AES256 is symmetric`() =
        assertThat(CipherAlgorithm.AES256.isSymmetric is_ true)

    @Test
    fun `AES256 is encoded with 0`() =
        assertThat(CipherAlgorithm.AES256.encoded is_ equal to 0)

    @Test
    fun `RSA2048 is not symmetric`() =
        assertThat(CipherAlgorithm.RSA2048.isSymmetric is_ false)

    @Test
    fun `RSA2048 is encoded with 1`() =
        assertThat(CipherAlgorithm.RSA2048.encoded is_ equal to 1)

    @Test
    fun `cipherAlgorithm for ByteArray starting with 0 returns AES256`() =
        Given {
            byteArrayOf(0, 2, 3)
        } when_ {
            cipherAlgorithm
        } then {
            assertThat(it is_ equal to CipherAlgorithm.AES256)
        }

    @Test
    fun `cipherAlgorithm for ByteArray starting with 1 returns RSA2048`() =
        Given {
            byteArrayOf(1, 2, 3)
        } when_ {
            cipherAlgorithm
        } then {
            assertThat(it is_ equal to CipherAlgorithm.RSA2048)
        }

    @Test
    fun `cipherAlgorithm for ByteArray starting with invalid type encoding panics`() =
        Given {
            byteArrayOf(-1, 2, 3)
        } when_ {
            cipherAlgorithm
        } then panics

    @Test
    fun `cipherAlgorithm for empty ByteArray panics`() =
        Given {
            byteArrayOf()
        } when_ {
            cipherAlgorithm
        } then panics
}
