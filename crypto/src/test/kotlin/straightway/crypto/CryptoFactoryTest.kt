/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.crypto

import com.nhaarman.mockitokotlin2.mock
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.fail
import straightway.crypto.impl.*
import straightway.expr.minus
import straightway.testing.bdd.Given
import straightway.testing.flow.*
import straightway.utils.deserializeTo
import straightway.utils.serializeToByteArray

class CryptoFactoryTest {

    @Test
    fun `default factory yields AES256Cryptor as symmetric cryptor`() =
        assert(CryptoFactory().symmetricCryptor.createCryptor() is AES256Cryptor)

    @Test
    fun `default factory yields RSA2048 with AES256 as cryptoIdentity`() {
        val sut = CryptoFactory()
        sut.cryptoIdentity.createCryptoIdentity().also {

            if (it !is CryptoIdentity.Base)
                fail("Invalid crypto identity type")

            val signer = it.signer
            if (signer !is CombinedSigner)
                fail("Invalid signer type")

            val signatureChecker = it.signatureChecker
            if (signatureChecker !is CombinedSignatureChecker)
                fail("Invalid signatureChecker type")

            assert(signer.baseSigner is RSA2048RawCryptor)
            assertThat(
                signer.contentCryptorFactory
                    is_ same as_ sut.symmetricCryptor
            )

            assert(signatureChecker.baseSignatureChecker is RSA2048RawCryptor)
            assertThat(
                signatureChecker.contentCryptorFactory
                    is_ same as_ sut.symmetricCryptor
            )

            assertThat(
                signer.baseSigner
                    is_ same as_ signatureChecker.baseSignatureChecker
            )
            assertThat(
                signer.contentCryptorFactory
                    is_ same as_ signatureChecker.contentCryptorFactory
            )
        }
    }

    @Test
    fun `default factory yields SHA512Hasher as hasher`() =
        assert(CryptoFactory().hasher.createHasher() is SHA512Hasher)

    @Test
    fun `default factory is serializable`() =
        Given {
            CryptoFactory()
        } when_ {
            serializeToByteArray()
        } then {
            val deserialized = it.deserializeTo<CryptoFactory>()
            assertThat(
                deserialized.hasher.properties.hashAlgorithm is_ equal to_ "SHA512"
            )
        }

    @Test
    fun `symmetrically encrypt and decrypt`() =
        Given {
            object {
                val clearText = "Hello World".toByteArray()
                val cryptor = CryptoFactory().symmetricCryptor.createCryptor()
            }
        } when_ {
            cryptor.encrypt(clearText)
        } then {
            assertThat(it is_ not - equal to_ clearText)
            assertThat(cryptor.decrypt(it) is_ equal to_ clearText)
        }

    @Test
    fun `asymmetrically encrypt and decrypt small data`() =
        Given {
            val cryptoIdentityFactory = CryptoFactory().cryptoIdentity
            object {
                val clearText = "Hello World".toByteArray()
                val cryptor = cryptoIdentityFactory.createCryptoIdentity()
            }
        } when_ {
            cryptor.encrypt(clearText)
        } then {
            assertThat(clearText.size is_ less than RSA2048.properties.maxClearTextBytes)
            assertThat(it is_ not - equal to_ clearText)
            assertThat(it.size is_ equal to_ RSA2048.properties.fixedCipherTextBytes + 2)
            assertThat(cryptor.decrypt(it) is_ equal to_ clearText)
        }

    @Test
    fun `asymmetrically encrypt and decrypt big data`() =
        Given {
            val cryptoIdentityFactory = CryptoFactory().cryptoIdentity
            object {
                val clearText = ByteArray(
                    RSA2048.properties.encryptorProperties.maxClearTextBytes + 1
                ) {
                    it.toByte()
                }
                val cryptor = cryptoIdentityFactory.createCryptoIdentity()
            }
        } when_ {
            cryptor.encrypt(clearText)
        } then {
            assertThat(
                clearText.size is_ greater than
                    RSA2048.properties.maxClearTextBytes
            )
            assertThat(
                clearText.size is_ greater than
                    RSA2048.properties.maxClearTextBytes
            )
            assertThat(it is_ not - equal to_ clearText)
            assertThat(cryptor.decrypt(it) is_ equal to_ clearText)
        }

    @Test
    fun `signing and checking`() =
        Given {
            val cryptoIdentityFactory = CryptoFactory().cryptoIdentity
            object {
                val toSign = "Hello World".toByteArray()
                val cryptor = cryptoIdentityFactory.createCryptoIdentity()
            }
        } when_ {
            cryptor.sign(toSign)
        } then {
            assertThat(cryptor.isSignatureValid(toSign, it) is_ true)
            assertThat(cryptor.isSignatureValid("Lalala".toByteArray(), it) is_ false)
        }

    @Test
    fun `getEncryptorFor symmetric key calls according factory method`() =
        Given {
            object {
                val key = byteArrayOf(CipherAlgorithm.AES256.encoded)
                val result: Cryptor = mock()
                val symmetricFactory: Cryptor.Factory = mock {
                    on { getCryptor(key) }.thenReturn(result)
                }
                val sut: CryptoFactory = mock {
                    on { symmetricCryptor }.thenReturn(symmetricFactory)
                }
            }
        } when_ {
            sut.getEncryptorFor(key)
        } then {
            assertThat(it is_ same as_ result)
        }

    @Test
    fun `getEncryptorFor asymmetric key calls according factory method`() =
        Given {
            object {
                val key = byteArrayOf(CipherAlgorithm.RSA2048.encoded)
                val result: SignatureChecker = mock()
                val cryptoIdentity: CryptoIdentity.Factory = mock {
                    on { createSignatureChecker(key) }.thenReturn(result)
                }
                val sut: CryptoFactory = mock {
                    on { cryptoIdentity }.thenReturn(cryptoIdentity)
                }
            }
        } when_ {
            sut.getEncryptorFor(key)
        } then {
            assertThat(it is_ same as_ result)
        }

    @Test
    fun `getDecryptorFor symmetric key calls according factory method`() =
        Given {
            object {
                val key = byteArrayOf(CipherAlgorithm.AES256.encoded)
                val result: Cryptor = mock()
                val symmetricFactory: Cryptor.Factory = mock {
                    on { getCryptor(key) }.thenReturn(result)
                }
                val sut: CryptoFactory = mock {
                    on { symmetricCryptor }.thenReturn(symmetricFactory)
                }
            }
        } when_ {
            sut.getDecryptorFor(key)
        } then {
            assertThat(it is_ same as_ result)
        }

    @Test
    fun `getDecryptorFor asymmetric key calls according factory method`() =
        Given {
            object {
                val key = byteArrayOf(CipherAlgorithm.RSA2048.encoded)
                val result: Signer = mock()
                val cryptoIdentity: CryptoIdentity.Factory = mock {
                    on { createSigner(key) }.thenReturn(result)
                }
                val sut: CryptoFactory = mock {
                    on { cryptoIdentity }.thenReturn(cryptoIdentity)
                }
            }
        } when_ {
            sut.getDecryptorFor(key)
        } then {
            assertThat(it is_ same as_ result)
        }
}
