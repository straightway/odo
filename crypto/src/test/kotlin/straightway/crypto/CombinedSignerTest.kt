/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.crypto

import com.nhaarman.mockitokotlin2.*
import org.junit.jupiter.api.Test
import straightway.testing.bdd.Given
import straightway.testing.bdd.throws
import straightway.testing.flow.*
import straightway.utils.*
import kotlin.IndexOutOfBoundsException

class CombinedSignerTest : CombinedCryptorTestBase<CombinedSigner>() {

    @Test
    fun `decrypt uses symmetric decryption to decrypt content`() =
        test when_ {
            sut.decrypt(cipherText)
        } then {
            verify(baseSigner, times(1)).decrypt(encryptedSymmetricKey)
            verify(contentCryptorFactory, times(1)).getCryptor(decryptedSymmetricKey)
            verify(contentCryptor, times(1)).decrypt(encryptedContent)
        }

    @Test
    fun `cipher text lenght is taken from content encryptor`() =
        test when_ {
            sut.properties
        } then {
            assertThat(
                it.fixedCipherTextBytes is_ equal
                    to_ contentCryptorProperties.fixedCipherTextBytes
            )
            assertThat(
                it.algorithm is_ equal
                    to_ "${baseSigner.properties.algorithm}/${contentCryptorFactory.properties.algorithm}"
            )
            assertThat(
                it.keyBits is_ equal
                    to_ baseSigner.properties.keyBits
            )
        }

    @Test
    fun `decrypting empty array throws`() =
        test when_ {
            sut.decrypt(byteArrayOf())
        } then throws.exception {
            assert(it is IndexOutOfBoundsException)
        }

    @Test
    fun `decrypting array where the byte number exceeds the length throws`() =
        test when_ {
            sut.decrypt(byteArrayOf(0, 1))
        } then throws.exception {
            assert(it is IndexOutOfBoundsException)
        }

    @Test
    fun `decrypting empty array does not throw`() =
        test when_ {
            sut.decrypt(byteArrayOf(0, 1, 0))
        } then throws.nothing

    @Test
    fun `signKey is used from baseSigner`() =
        test when_ {
            sut.signKey
        } then {
            assertThat(it is_ equal to_ baseSigner.signKey)
        }

    @Test
    fun `sign is forwarded to baseSigner`() =
        test when_ {
            sut.sign(decryptedContent)
        } then {
            verify(baseSigner, times(1)).sign(decryptedContent)
        }

    @Test
    fun `decryptionKey is used from baseSigner`() =
        test when_ {
            sut.decryptionKey
        } then {
            assertThat(it is_ equal to_ baseSigner.decryptionKey)
        }

    @Test
    fun `hashAlgorithm is used from baseSigner`() =
        test when_ {
            sut.hashAlgorithm
        } then {
            assertThat(it is_ equal to_ baseSigner.hashAlgorithm)
        }

    @Test
    fun `decrypting content only encrypted by base encryptor`() =
        test when_ {
            sut.decrypt(
                encryptedSymmetricKey.size.toShort().toByteArray() +
                    encryptedSymmetricKey
            )
        } then {
            assertThat(it is_ equal to_ decryptedSymmetricKey)
        }

    @Test
    fun `is serializable`() =
        test when_ {
            sut.serializeToByteArray()
        } then {
            assert(it.deserializeTo<Signer>() is CombinedSigner)
        }

    @Test
    fun `factory is serializable`() =
        Given {
            CombinedSigner.Factory(mock(serializable = true), mock(serializable = true))
        } when_ {
            serializeToByteArray()
        } then {
            assert(it.deserializeTo<Signer.Factory>() is CombinedSigner.Factory)
        }

    @Test
    fun `factory creates entities`() =
        Given {
            object {
                val baseSigner: Signer = mock()
                val baseSignerFactory: Signer.Factory = mock {
                    on { createSigner(any()) }.thenReturn(baseSigner)
                }
                val contentDecryptorFactory: Cryptor.Factory = mock()
                val sut = CombinedSigner.Factory(
                    baseSignerFactory,
                    contentDecryptorFactory
                )
            }
        } when_ {
            sut.createSigner(byteArrayOf())
        } then {
            assertThat(it.baseSigner is_ same as_ baseSigner)
            assertThat(it.contentCryptorFactory is_ same as_ contentDecryptorFactory)
        }

    @Test
    fun `factory properties have proper values`() =
        Given {
            object {
                val properties = Decryptor.Properties(
                    7,
                    KeyProperties("Algo", 13)
                )
                val baseSignerFactory: Signer.Factory = mock {
                    on { properties }.thenReturn(properties)
                }
                val contentDecryptorFactory: Cryptor.Factory = mock {
                    on { properties }.thenReturn(
                        Cryptor.Properties(
                            maxClearTextBytes = 5,
                            blockBytes = 7,
                            fixedCipherTextBytes = 11,
                            KeyProperties(
                                algorithm = "ContentAlgo",
                                keyBits = 17
                            )
                        ) { it * 2 }
                    )
                }
                val sut = CombinedSigner.Factory(
                    baseSignerFactory,
                    contentDecryptorFactory
                )
            }
        } when_ {
            sut.properties
        } then {
            assertThat(
                it.fixedCipherTextBytes is_ equal
                    to_ 7 + 11 + CombinedSignatureChecker.KEY_LENGHT_ENCRYPTION_SIZE
            )
            assertThat(it.algorithm is_ equal to_ "Algo/ContentAlgo")
            assertThat(it.keyBits is_ equal to_ 13)
        }

    // region base class overrides

    override fun TestEnvironment<CombinedSigner>.createSut() =
        CombinedSigner(baseSigner, contentCryptorFactory)

    // endregion
}
