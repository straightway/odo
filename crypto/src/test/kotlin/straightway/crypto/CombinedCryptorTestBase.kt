/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.crypto

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import straightway.testing.bdd.Given
import straightway.utils.toByteArray

abstract class CombinedCryptorTestBase<T> {

    protected class TestEnvironment<T>(sutFactory: TestEnvironment<T>.() -> T) {
        val decryptedSymmetricKey = byteArrayOf(9, 10, 11, 12)
        val encryptedSymmetricKey = byteArrayOf(1, 2, 3, 4)
        val encryptedOtherContent = byteArrayOf(2, 3, 4, 5, 6)
        val encryptedContent = byteArrayOf(5, 6, 7, 8)
        val cipherText =
            encryptedSymmetricKey.size.toShort().toByteArray() +
                encryptedSymmetricKey +
                encryptedContent
        val decryptedContent = byteArrayOf(13, 14, 15, 16, 17, 18, 19)
        private val basHashAlgorithm = "baseSignerHashAlgorithm"
        val baseSigner: Signer = mock(serializable = true) {
            on { properties }.thenReturn(
                Decryptor.Properties(
                    fixedCipherTextBytes = 3,
                    KeyProperties(
                        algorithm = "keyAlgo",
                        keyBits = 83
                    )
                )
            )
            on { signKey }.thenReturn(byteArrayOf(17, 18, 19))
            on { decryptionKey }.thenReturn(byteArrayOf(23, 24, 25))
            on { hashAlgorithm }.thenReturn(basHashAlgorithm)
            on { decrypt(any()) }.thenReturn(decryptedSymmetricKey)
            on { sign(any()) }.thenReturn(byteArrayOf(20, 21, 22))
        }
        val baseSignatureChecker: SignatureChecker = mock(serializable = true) {
            on { properties }.thenReturn(
                Encryptor.Properties(
                    maxClearTextBytes = 5,
                    blockBytes = 7,
                    KeyProperties(
                        algorithm = "keyAlgo",
                        keyBits = 83
                    ),
                    outputSizeGetter = constantSizeGetter(encryptedSymmetricKey.size)
                )
            )
            on { encryptionKey }.thenReturn(byteArrayOf(26, 27, 28))
            on { hashAlgorithm }.thenReturn(basHashAlgorithm)
            on { encrypt(any()) }.thenReturn(encryptedOtherContent)
            on { encrypt(decryptedSymmetricKey) }.thenReturn(encryptedSymmetricKey)
        }
        val contentCryptorProperties = Cryptor.Properties(
            maxClearTextBytes = 1234,
            blockBytes = 64,
            fixedCipherTextBytes = 0,
            KeyProperties(
                algorithm = "AES",
                keyBits = decryptedSymmetricKey.size * 8
            ),
            constantSizeGetter(encryptedSymmetricKey.size)
        )
        val contentCryptor: Cryptor = mock(serializable = true) {
            on { properties }.thenReturn(contentCryptorProperties)
            on { decrypt(encryptedContent) }.thenReturn(decryptedContent)
            on { encrypt(decryptedContent) }.thenReturn(encryptedContent)
            on { encryptionKey }.thenReturn(decryptedSymmetricKey)
            on { decryptionKey }.thenReturn(decryptedSymmetricKey)
        }
        val contentCryptorFactory: Cryptor.Factory = mock(serializable = true) {
            on { properties }.thenReturn(contentCryptorProperties)
            on { getCryptor(any()) }.thenReturn(contentCryptor)
            on { createCryptor() }.thenReturn(contentCryptor)
        }

        val sut = sutFactory()

        private fun constantSizeGetter(size: Int): (Int) -> Int = { size }
    }

    protected val test get() = Given {
        TestEnvironment<T> { createSut() }
    }

    protected abstract fun TestEnvironment<T>.createSut(): T
}
