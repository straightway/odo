/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.crypto

import org.junit.jupiter.api.Test
import straightway.testing.bdd.Given
import straightway.testing.flow.*

class CombinedCryptorPropertiesTest {

    private fun given(
        baseFixedCipherTextBytes: Int,
        contentFixedCipherTextBytes: Int
    ) =
        Given {
            object {
                val base = Decryptor.Properties(
                    baseFixedCipherTextBytes,
                    KeyProperties("Base", 5)
                )
                val content = Cryptor.Properties(
                    maxClearTextBytes = 7,
                    blockBytes = 11,
                    fixedCipherTextBytes = contentFixedCipherTextBytes,
                    KeyProperties(
                        algorithm = "Content",
                        keyBits = 13
                    )
                ) { it * 2 }
            }
        }

    @Test
    fun `getCombinedDecryptorProperties combines proper encryptor properties`() =
        given(
            baseFixedCipherTextBytes = 7,
            contentFixedCipherTextBytes = 0
        ) when_ {
            getCombinedCryptorProperties(base, content) { it * 2 }
        } then {
            assertThat(
                it.encryptorProperties is_ equal to_
                    getCombinedEncryptorProperties(base, content) { it * 2 }
            )
        }

    @Test
    fun `getCombinedDecryptorProperties combines proper decryptor properties`() =
        given(
            baseFixedCipherTextBytes = 7,
            contentFixedCipherTextBytes = 0
        ) when_ {
            getCombinedCryptorProperties(base, content) { it * 2 }
        } then {
            assertThat(
                it.decryptorProperties is_ equal to_
                    getCombinedDecryptorProperties(base, content)
            )
        }

    @Test
    fun `getCombinedEncryptorProperties uses values from content cryptor`() =
        given(
            baseFixedCipherTextBytes = 7,
            contentFixedCipherTextBytes = 0
        ) when_ {
            getCombinedEncryptorProperties(base, content) { it * 2 }
        } then {
            assertThat(it.maxClearTextBytes is_ equal to_ content.maxClearTextBytes)
            assertThat(it.blockBytes is_ equal to_ content.blockBytes)
            assertThat(it.algorithm is_ equal to_ "Base/Content")
            assertThat(it.keyBits is_ equal to_ base.keyBits)
        }

    @Test
    fun `getCombinedDecryptorProperties has proper key properties`() =
        given(
            baseFixedCipherTextBytes = 7,
            contentFixedCipherTextBytes = 0
        ) when_ {
            getCombinedDecryptorProperties(base, content)
        } then {
            assertThat(it.algorithm is_ equal to_ "Base/Content")
            assertThat(it.keyBits is_ equal to_ base.keyBits)
        }

    @Test
    fun `getCombinedDecryptorProperties with unfixed content cipher text size`() =
        given(
            baseFixedCipherTextBytes = 7,
            contentFixedCipherTextBytes = 0
        ) when_ {
            getCombinedDecryptorProperties(base, content)
        } then {
            assertThat(it.fixedCipherTextBytes is_ equal to_ 0)
        }

    @Test
    fun `getCombinedDecryptorProperties with unfixed base cipher text size`() =
        given(
            baseFixedCipherTextBytes = 0,
            contentFixedCipherTextBytes = 7
        ) when_ {
            getCombinedDecryptorProperties(base, content)
        } then {
            assertThat(it.fixedCipherTextBytes is_ equal to_ 0)
        }

    @Test
    fun `getCombinedDecryptorProperties with fixed cipher text size`() =
        given(
            baseFixedCipherTextBytes = 19,
            contentFixedCipherTextBytes = 7
        ) when_ {
            getCombinedDecryptorProperties(base, content)
        } then {
            assertThat(
                it.fixedCipherTextBytes is_ equal
                    to_ 19 + 7 + CombinedSignatureChecker.KEY_LENGHT_ENCRYPTION_SIZE
            )
        }
}
