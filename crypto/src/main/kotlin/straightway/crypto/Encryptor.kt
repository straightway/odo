/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.crypto

import java.io.Serializable

/**
 * Encrypt data using a fixed internal key.
 */
interface Encryptor {
    val properties: Properties
    val encryptionKey: ByteArray
    fun encrypt(toEncrypt: ByteArray): ByteArray

    interface Properties : KeyProperties {
        val maxClearTextBytes: Int
        val blockBytes: Int
        fun getOutputBytes(inputSize: Int): Int

        companion object {

            operator fun invoke(
                maxClearTextBytes: Int,
                blockBytes: Int,
                keyProperties: KeyProperties,
                outputSizeGetter: (Int) -> Int
            ) =
                PropertiesImpl(
                    maxClearTextBytes,
                    blockBytes,
                    keyProperties,
                    outputSizeGetter
                )

            data class PropertiesImpl(
                override val maxClearTextBytes: Int,
                override val blockBytes: Int,
                private val keyProperties: KeyProperties,
                private val outputSizeGetter: (Int) -> Int
            ) : Properties, KeyProperties by keyProperties, Serializable {

                override fun getOutputBytes(inputSize: Int) =
                    outputSizeGetter(inputSize)

                override fun equals(other: Any?) =
                    other is Properties &&
                        other.maxClearTextBytes == maxClearTextBytes &&
                        other.blockBytes == blockBytes &&
                        KeyProperties(other.algorithm, other.keyBits) ==
                        KeyProperties(algorithm, keyBits)

                override fun hashCode() =
                    maxClearTextBytes.hashCode() xor
                        blockBytes.hashCode() xor
                        KeyProperties(algorithm, keyBits).hashCode()

                companion object {
                    const val serialVersionUID = 1L
                }
            }
        }
    }
}
