/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.crypto

import java.io.Serializable

/**
 * A crypto identity allows signing and verifying of digital signatures, as well as
 * encrypting and decrypting of data.
 */
interface CryptoIdentity : Signer, SignatureChecker, Cryptor {

    val rawKeyPair: ByteArray

    interface Factory : Signer.Factory, SignatureChecker.Factory {
        fun createCryptoIdentity(): CryptoIdentity
        fun getCryptoIdentity(rawKeyPair: ByteArray): CryptoIdentity
        override val properties: Cryptor.Properties
    }

    open class Base constructor(
        override val hashAlgorithm: String,
        val signer: Signer,
        val signatureChecker: SignatureChecker,
        override val rawKeyPair: ByteArray
    ) : CryptoIdentity,
        Signer by signer,
        SignatureChecker by signatureChecker,
        Serializable {

        override val properties: Cryptor.Properties
            get() = Cryptor.Properties(signatureChecker.properties, signer.properties)

        companion object {
            const val serialVersionUID = 1L
        }
    }
}
