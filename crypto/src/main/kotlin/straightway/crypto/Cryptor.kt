/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.crypto

import straightway.error.Panic
import java.io.Serializable

/**
 * A cryptographic key together with an algorithm allowing encrypting and decrypting
 * byte arrays.
 */
interface Cryptor : Encryptor, Decryptor {

    interface Factory {
        fun createCryptor(): Cryptor
        fun getCryptor(rawKey: ByteArray): Cryptor
        val properties: Properties
    }

    override val properties: Properties

    interface Properties : Encryptor.Properties, Decryptor.Properties {
        companion object {
            operator fun <E : Encryptor.Properties, D : Decryptor.Properties> invoke(
                encryptorProperties: E,
                decryptorProperties: D
            ) = Impl(encryptorProperties, decryptorProperties)

            operator fun invoke(
                maxClearTextBytes: Int,
                blockBytes: Int,
                fixedCipherTextBytes: Int,
                keyProperties: KeyProperties,
                outputSizeGetter: (Int) -> Int
            ) = Properties(
                Encryptor.Properties(
                    maxClearTextBytes,
                    blockBytes,
                    keyProperties,
                    outputSizeGetter
                ),
                Decryptor.Properties(fixedCipherTextBytes, keyProperties)
            )
        }

        data class Impl<E : Encryptor.Properties, D : Decryptor.Properties>(
            val encryptorProperties: E,
            val decryptorProperties: D
        ) : Properties,
            Encryptor.Properties by encryptorProperties,
            Decryptor.Properties by decryptorProperties,
            Serializable {

            override val algorithm get() = encryptorProperties.algorithm
            override val keyBits get() = encryptorProperties.keyBits

            init {
                if (encryptorProperties.algorithm != decryptorProperties.algorithm ||
                    encryptorProperties.keyBits != decryptorProperties.keyBits
                )
                    throw Panic(
                        "Contradicting key properties: $encryptorProperties " +
                            "vs. $decryptorProperties"
                    )
            }

            companion object {
                const val serialVersionUID = 1L
            }
        }
    }
}
