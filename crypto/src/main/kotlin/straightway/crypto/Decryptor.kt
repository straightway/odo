/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.crypto

import java.io.Serializable

/**
 * Decrypt data using a fixed internal key.
 */
interface Decryptor {
    val properties: Properties
    val decryptionKey: ByteArray
    fun decrypt(toDecrypt: ByteArray): ByteArray

    interface Properties : KeyProperties {
        val fixedCipherTextBytes: Int

        companion object {

            operator fun invoke(
                fixedCipherTextBytes: Int,
                keyProperties: KeyProperties
            ) = PropertiesImpl(fixedCipherTextBytes, keyProperties)

            data class PropertiesImpl(
                override val fixedCipherTextBytes: Int,
                private val keyProperties: KeyProperties
            ) : Properties, KeyProperties by keyProperties, Serializable {
                companion object {
                    const val serialVersionUID = 1L
                }
            }
        }
    }
}
