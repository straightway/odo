/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.crypto

import straightway.crypto.impl.*
import java.io.Serializable

/**
 * Combined crypto factory for symmetric cryptors, asymmetric cryptors and hashers.
 */
interface CryptoFactory : Serializable {

    val hasher: Hasher.Factory
    val symmetricCryptor: Cryptor.Factory
    val cryptoIdentity: CryptoIdentity.Factory

    companion object {
        operator fun invoke(): CryptoFactory = CryptoFactoryImpl()

        private class CryptoFactoryImpl : CryptoFactory, Serializable {
            override val hasher: Hasher.Factory =
                SHA512Hasher.Factory()
            override val symmetricCryptor: Cryptor.Factory =
                AES256Cryptor.Factory()
            override val cryptoIdentity: CryptoIdentity.Factory =
                CombinedCryptoIdentityFactory(
                    hasher,
                    symmetricCryptor,
                    RSA2048RawCryptor.Factory(hasher)
                )
            companion object {
                const val serialVersionUID = 1L
            }
        }
    }
}

fun CryptoFactory.getEncryptorFor(key: ByteArray): Encryptor =
    if (key.cipherAlgorithm.isSymmetric)
        symmetricCryptor.getCryptor(key)
    else cryptoIdentity.createSignatureChecker(key)

fun CryptoFactory.getDecryptorFor(key: ByteArray): Decryptor =
    if (key.cipherAlgorithm.isSymmetric)
        symmetricCryptor.getCryptor(key)
    else cryptoIdentity.createSigner(key)
