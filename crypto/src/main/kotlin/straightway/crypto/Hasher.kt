/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.crypto

import straightway.compiler.Generated
import straightway.error.Panic
import straightway.utils.toByteArray
import java.io.Serializable

/**
 * Compute hash codes for data arrays.
 */
interface Hasher {
    val properties: Properties
    fun getHash(data: ByteArray): ByteArray

    interface Factory {
        fun createHasher(): Hasher
        val properties: Properties
    }

    data class Properties(
        val hashAlgorithm: String,
        val hashBits: Int
    ) : Serializable {
        companion object {
            const val serialVersionUID = 1L
        }
    }

    @Generated("jacoco somehow marks this as uncovered, alsthough it is covered")
    companion object {
        const val HEADER_BYTES = 1
    }
}

fun Hasher.getHash(obj: Serializable) = getHash(obj.toByteArray())
val Hasher.Properties.totalHashOutputBytes get() =
    (
        if (hashBits <= 0)
            throw Panic("hashBits must be positive (got: $hashBits)")
        else (hashBits - 1) / Byte.SIZE_BITS + 1
        ) + Hasher.HEADER_BYTES
