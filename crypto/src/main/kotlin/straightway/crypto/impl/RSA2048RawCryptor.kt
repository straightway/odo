/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.crypto.impl

import straightway.crypto.*
import straightway.utils.*
import java.io.Serializable
import java.security.KeyPair
import java.security.KeyPairGenerator

/**
 * Cryptor implementation encrypting payload keys using RSA2048, while the payload
 * itself is encrypted symmetrically.
 */
class RSA2048RawCryptor(val keyPair: KeyPair, hasherFactory: Hasher.Factory) :
    CryptoIdentity.Base(
        hasherFactory.properties.hashAlgorithm,
        RSA2048RawSigner(keyPair.private, hasherFactory),
        RSA2048RawSignatureChecker(keyPair.public, hasherFactory),
        keyPair.serializeToByteArray()
    ),
    Serializable {

    constructor(hasherFactory: Hasher.Factory) : this(
        KeyPairGenerator.getInstance(RSA2048.properties.algorithm)
            .apply { initialize(RSA2048.properties.keyBits) }.genKeyPair(),
        hasherFactory
    )

    class Factory(
        private val hasherFactory: Hasher.Factory
    ) : CryptoIdentity.Factory,
        Signer.Factory by RSA2048RawSigner.Factory(hasherFactory),
        SignatureChecker.Factory by RSA2048RawSignatureChecker.Factory(hasherFactory),
        Serializable {

        override val properties get() = RSA2048.properties

        override fun createCryptoIdentity() =
            RSA2048RawCryptor(hasherFactory)

        override fun getCryptoIdentity(rawKeyPair: ByteArray) =
            RSA2048RawCryptor(rawKeyPair.deserializeTo(), hasherFactory)

        companion object {
            const val serialVersionUID = 1L
        }
    }

    companion object {
        const val serialVersionUID = 1L
    }
}
