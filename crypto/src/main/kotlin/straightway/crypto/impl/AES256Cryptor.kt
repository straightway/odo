/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.crypto.impl

import straightway.crypto.*
import java.io.Serializable
import javax.crypto.Cipher
import javax.crypto.KeyGenerator
import javax.crypto.SecretKey
import javax.crypto.spec.SecretKeySpec

/**
 * Cryptor implementation for AES256.
 */
class AES256Cryptor private constructor (
    val key: SecretKey
) : Cryptor, Serializable {

    constructor() : this(
        with(KeyGenerator.getInstance(ALGORITHM)) {
            init(KEY_BITS)
            generateKey()
        }
    )

    constructor(keyBytes: ByteArray) :
        this(SecretKeySpec(keyBytes.stripAndCheckAlgorithmType(CipherAlgorithm.AES256), "AES"))

    override val properties get() = Companion.properties

    override val encryptionKey get() = key.encoded.with(CipherAlgorithm.AES256)
    override val decryptionKey get() = encryptionKey

    override fun encrypt(toEncrypt: ByteArray) = encryptCipher.doFinal(toEncrypt)
    override fun decrypt(toDecrypt: ByteArray) = decryptCipher.doFinal(toDecrypt)

    companion object {
        const val serialVersionUID = 1L
        private const val KEY_BITS = 256
        private const val ALGORITHM = "AES"
        private val dummyKey =
            SecretKeySpec(ByteArray(KEY_BITS / Byte.SIZE_BITS), "AES")
        private val dummyCipher = createCipher(Cipher.ENCRYPT_MODE, dummyKey)
        val properties =
            Cryptor.Properties(
                maxClearTextBytes = Int.MAX_VALUE,
                blockBytes = dummyCipher.blockSize,
                fixedCipherTextBytes = 0,
                KeyProperties(ALGORITHM, KEY_BITS),
                dummyCipher::getOutputSize
            )

        private fun createCipher(mode: Int, key: SecretKey) =
            Cipher.getInstance("AES/ECB/PKCS5Padding").apply { init(mode, key) }
    }

    class Factory : Cryptor.Factory, Serializable {
        override fun createCryptor() = AES256Cryptor()
        override fun getCryptor(rawKey: ByteArray) = AES256Cryptor(rawKey)
        override val properties get() = AES256Cryptor.properties
        companion object {
            const val serialVersionUID = 1L
        }
    }

    private val decryptCipher: Cipher get() = createCipher(Cipher.DECRYPT_MODE, key)
    private val encryptCipher: Cipher get() = createCipher(Cipher.ENCRYPT_MODE, key)
}
