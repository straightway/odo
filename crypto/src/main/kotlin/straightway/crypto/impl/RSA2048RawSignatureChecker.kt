/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.crypto.impl

import straightway.crypto.*
import java.io.Serializable
import java.security.PublicKey
import java.security.Signature
import javax.crypto.Cipher

/**
 * Signature checker implementation checking signatures and decrypting payload keys
 * using RSA2048, while the payload itself is symmetrically decrypted.
 */
class RSA2048RawSignatureChecker(
    val key: PublicKey,
    hasherFactory: Hasher.Factory
) : SignatureChecker, Serializable {

    constructor(rawKey: ByteArray, hasherFactory: Hasher.Factory) :
        this(
            publicKeyFrom(
                rawKey.stripAndCheckAlgorithmType(CipherAlgorithm.RSA2048),
                RSA2048.properties.algorithm
            ),
            hasherFactory
        )

    override val properties get() = RSA2048.properties

    override val signatureCheckKey get() = key.encoded!!.with(CipherAlgorithm.RSA2048)
    override val encryptionKey get() = key.encoded!!.with(CipherAlgorithm.RSA2048)

    override val hashAlgorithm = hasherFactory.properties.hashAlgorithm

    override fun isSignatureValid(signed: ByteArray, signature: ByteArray) =
        with(signer) { update(signed); verify(signature) }

    override fun encrypt(toEncrypt: ByteArray) =
        encryptCipher.doFinal(toEncrypt)!!

    class Factory(
        private val hasherFactory: Hasher.Factory
    ) : SignatureChecker.Factory, Serializable {

        override val properties get() = RSA2048.properties

        override fun createSignatureChecker(rawKey: ByteArray) =
            RSA2048RawSignatureChecker(rawKey, hasherFactory)

        companion object {
            const val serialVersionUID = 1L
        }
    }

    companion object {
        const val serialVersionUID = 1L
    }

    private val signer get() =
        Signature.getInstance(signAlgorithm)!!.apply { initVerify(key) }

    private val signAlgorithm =
        "${hashAlgorithm}with${properties.algorithm}"

    private val encryptCipher: Cipher get() =
        Cipher.getInstance(properties.algorithm).apply {
            init(Cipher.ENCRYPT_MODE, key)
        }
}
