/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.crypto.impl

import straightway.crypto.*
import java.io.Serializable
import java.security.KeyFactory
import java.security.spec.X509EncodedKeySpec
import javax.crypto.Cipher

/**
 * Holder of general properties of the RSA 2048 algorith,
 */
object RSA2048 : Serializable {

    private const val ALGORITHM = "RSA"
    private const val KEY_BITS = 2048

    private val dummyPublicKey = X509EncodedKeySpec(
        byteArrayOf(
            48, -126, 1, 34, 48, 13, 6, 9, 42, -122, 72, -122, -9, 13, 1, 1, 1, 5, 0, 3,
            -126, 1, 15, 0, 48, -126, 1, 10, 2, -126, 1, 1, 0, -125, -81, 123, 32, -45,
            89, 114, -115, -83, 105, -11, 68, 13, 47, 70, -73, -23, 117, -100, 99, -127,
            -86, 42, 78, 99, 82, 18, -127, -47, 52, 48, -26, -39, -109, -128, -57, 106,
            123, 96, 33, 88, 29, 82, 118, 36, -18, -113, -127, -121, -16, -57, -84, -103,
            -93, 65, 19, -109, -53, -85, -91, 84, -6, -44, -41, -17, 60, -83, -84, -61,
            -64, -36, 22, 19, 70, 34, -57, -68, -5, -68, 70, 35, -101, 93, -88, -125,
            -54, 89, 45, -89, -12, -64, 95, -2, -33, -17, -14, -50, 22, -8, -122, 92, 82,
            -50, -31, 70, 96, 6, 79, 61, -56, 1, -6, 123, -32, -70, -26, 97, 31, -28, 21,
            58, -107, 48, 122, -95, 14, 84, -42, -63, 84, -66, 119, 67, 45, 103, -21, 2,
            28, -56, 94, -68, -92, -24, 109, -11, -10, 106, -37, 43, 100, 66, 101, 16,
            63, 31, -122, 45, 8, -45, 53, 74, -8, -122, -117, -42, -44, 1, -95, 65, -121,
            -42, -36, -121, 58, 75, 14, -69, -92, 9, -29, 49, -100, -35, -79, -31, -52,
            39, 6, 69, 11, 63, 86, 4, -22, 30, 108, 0, -27, -64, -97, -128, 29, 23, 57,
            -93, 94, -120, -16, -120, -67, 103, 116, -33, 111, -116, 19, 115, -103, -11,
            -46, -21, 26, -48, -81, -75, 47, 45, 21, 102, 102, 74, 63, 16, 14, 35, -121,
            90, -112, 101, -54, 52, 86, -62, 11, 1, 120, 119, -90, -91, 113, -114, 0,
            -59, 73, -93, 3, 2, 3, 1, 0, 1
        )
    )
    private val dummyKey =
        KeyFactory.getInstance(ALGORITHM).generatePublic(dummyPublicKey)
    private val cipher get() = Cipher.getInstance(ALGORITHM).apply {
        init(Cipher.ENCRYPT_MODE, dummyKey)
    }

    val properties =
        Cryptor.Properties(
            maxClearTextBytes = 245,
            blockBytes = 0,
            fixedCipherTextBytes = (KEY_BITS - 1) / Byte.SIZE_BITS + 1,
            KeyProperties(ALGORITHM, KEY_BITS),
            cipher::getOutputSize
        )

    const val serialVersionUID = 1L
}
