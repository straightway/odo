/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.crypto.impl

import straightway.crypto.*
import java.io.Serializable
import java.security.MessageDigest

/**
 * Hasher implementaion using the SHA-512 hashing algorithm.
 */
class SHA512Hasher : Hasher {
    companion object {
        private const val hashAlgorithm = "SHA-512"
        val properties = Hasher.Properties(
            hashAlgorithm = hashAlgorithm.replace("-", ""),
            hashBits = 512
        )
    }

    override val properties get() = Companion.properties

    override fun getHash(data: ByteArray) =
        byteArrayOf(HashAlgorithm.SHA512.encoded) +
            MessageDigest.getInstance(hashAlgorithm).digest(data)

    class Factory : Hasher.Factory, Serializable {
        override fun createHasher() = SHA512Hasher()
        override val properties get() = SHA512Hasher.properties
        companion object {
            const val serialVersionUID = 1L
        }
    }
}
