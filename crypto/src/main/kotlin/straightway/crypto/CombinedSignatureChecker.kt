/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.crypto

import straightway.utils.toByteArray
import java.io.Serializable

/**
 * Signature checker using a base signature checker with an asymmetric crypto key,
 * and a content cryptor to encrypt the content.
 */
class CombinedSignatureChecker(
    val baseSignatureChecker: SignatureChecker,
    val contentCryptorFactory: Cryptor.Factory
) : SignatureChecker by baseSignatureChecker, Serializable {

    override val properties get() =
        getCombinedEncryptorProperties(
            baseSignatureChecker.properties,
            contentCryptorFactory.properties
        ) {
            getOutputBytes(
                baseSignatureChecker.properties,
                contentCryptorFactory.properties,
                it
            )
        }

    override fun encrypt(toEncrypt: ByteArray) =
        if (toEncrypt.size <= baseSignatureChecker.properties.maxClearTextBytes)
            baseEncrypt(toEncrypt)
        else encryptLargeContent(toEncrypt)

    class Factory(
        private val baseSignatureCheckerFactory: SignatureChecker.Factory,
        private val contentCryptorFactory: Cryptor.Factory
    ) : SignatureChecker.Factory, Serializable {

        override val properties get() =
            getCombinedEncryptorProperties(
                baseSignatureCheckerFactory.properties,
                contentCryptorFactory.properties
            ) {
                getOutputBytes(
                    baseSignatureCheckerFactory.properties,
                    contentCryptorFactory.properties,
                    it
                )
            }

        override fun createSignatureChecker(rawKey: ByteArray) =
            CombinedSignatureChecker(
                baseSignatureCheckerFactory.createSignatureChecker(rawKey),
                contentCryptorFactory
            )

        companion object {
            const val serialVersionUID = 1L
        }
    }

    companion object {
        const val serialVersionUID = 1L
        val KEY_LENGHT_ENCRYPTION_SIZE = Short.SIZE_BYTES
        private fun getOutputBytes(
            baseProperties: Encryptor.Properties,
            contentCryptorProperties: Cryptor.Properties,
            inputSize: Int
        ): Int {
            val contentKeySize =
                (contentCryptorProperties.keyBits - 1) / Byte.SIZE_BITS + 1
            val encryptedContentKeySize = baseProperties.getOutputBytes(contentKeySize)
            val encryptedContentSize = contentCryptorProperties.getOutputBytes(inputSize)
            return KEY_LENGHT_ENCRYPTION_SIZE + encryptedContentKeySize + encryptedContentSize
        }
    }

    // region Private

    private fun baseEncrypt(toEncrypt: ByteArray): ByteArray {
        val encryptedContent = baseSignatureChecker.encrypt(toEncrypt)
        val encryptedContentLength = encryptedContent.size.toShort().toByteArray()
        return encryptedContentLength + encryptedContent
    }

    private fun encryptLargeContent(toEncrypt: ByteArray): ByteArray {
        val contentCryptor = contentCryptorFactory.createCryptor()
        val encryptedContent = contentCryptor.encrypt(toEncrypt)
        val contentKey = contentCryptor.encryptionKey
        return baseEncrypt(contentKey) + encryptedContent
    }

    // endregion
}
