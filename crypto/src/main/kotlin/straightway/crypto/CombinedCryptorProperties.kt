/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.crypto

fun getCombinedCryptorProperties(
    baseSignerProperties: Decryptor.Properties,
    contentCryptorProperties: Cryptor.Properties,
    outputBytesGetter: (Int) -> Int
) =
    Cryptor.Properties(
        getCombinedEncryptorProperties(
            baseSignerProperties,
            contentCryptorProperties,
            outputBytesGetter
        ),
        getCombinedDecryptorProperties(
            baseSignerProperties,
            contentCryptorProperties
        )
    )

fun getCombinedDecryptorProperties(
    baseSignerProperties: Decryptor.Properties,
    contentCryptorProperties: Decryptor.Properties
) = Decryptor.Properties(
    fixedCipherTextBytes = getCombinedFixedCipherTextBytes(
        baseSignerProperties.fixedCipherTextBytes,
        contentCryptorProperties.fixedCipherTextBytes
    ),
    KeyProperties(
        getCombinedAlgorithm(baseSignerProperties, contentCryptorProperties),
        baseSignerProperties.keyBits
    )
)

fun getCombinedEncryptorProperties(
    baseSignerProperties: KeyProperties,
    contentCryptorProperties: Encryptor.Properties,
    outputBytesGetter: (Int) -> Int
) = Encryptor.Properties(
    contentCryptorProperties.maxClearTextBytes,
    contentCryptorProperties.blockBytes,
    KeyProperties(
        getCombinedAlgorithm(baseSignerProperties, contentCryptorProperties),
        baseSignerProperties.keyBits
    ),
    outputBytesGetter
)

// region private

private fun getCombinedAlgorithm(
    baseSignerProperties: KeyProperties,
    contentCryptorProperties: KeyProperties
) = "${baseSignerProperties.algorithm}/${contentCryptorProperties.algorithm}"

private fun getCombinedFixedCipherTextBytes(baseBytes: Int, contentBytes: Int) =
    if (baseBytes == 0 || contentBytes == 0) 0
    else baseBytes + contentBytes + CombinedSignatureChecker.KEY_LENGHT_ENCRYPTION_SIZE

// endregion
