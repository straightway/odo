/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.crypto

import straightway.utils.getUnsignedShort
import java.io.Serializable

/**
 * Signer checker using a base signer with an asymmetric crypto key,
 * and a content cryptor to decrypt the content.
 */
class CombinedSigner(
    val baseSigner: Signer,
    internal val contentCryptorFactory: Cryptor.Factory
) : Signer by baseSigner, Serializable {

    override val properties: Decryptor.Properties get() =
        getCombinedDecryptorProperties(
            baseSigner.properties,
            contentCryptorFactory.properties
        )

    override fun decrypt(toDecrypt: ByteArray) = with(toDecrypt.baseDecrypt()) {
        if (rest.isEmpty()) decrypted else contentDecrypt()
    }

    class Factory(
        private val baseSignerFactory: Signer.Factory,
        private val contentDecryptorFactory: Cryptor.Factory
    ) : Signer.Factory, Serializable {

        override val properties get() =
            getCombinedDecryptorProperties(
                baseSignerFactory.properties,
                contentDecryptorFactory.properties
            )

        override fun createSigner(rawKey: ByteArray) = CombinedSigner(
            baseSignerFactory.createSigner(rawKey),
            contentDecryptorFactory
        )

        companion object {
            const val serialVersionUID = 1L
        }
    }

    companion object {
        const val serialVersionUID = 1L
    }

    // region Private

    private fun BaseDecryptionResult.contentDecrypt() =
        contentCryptorFactory.getCryptor(decrypted).decrypt(rest)

    private class BaseDecryptionResult(val decrypted: ByteArray, val rest: ByteArray)

    private fun ByteArray.baseDecrypt(): BaseDecryptionResult {
        val encryptedKey = sliceArray(2..(encodeLength + 1))
        val decryptedKey = baseSigner.decrypt(encryptedKey)
        val encryptedContent = sliceArray(encodeLength + 2..lastIndex)
        return BaseDecryptionResult(decryptedKey, encryptedContent)
    }

    private val ByteArray.encodeLength: Int
        get() = sliceArray(0..1).getUnsignedShort()

    // endregion
}
