/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.crypto

import java.io.Serializable

/**
 * A factory creating crypto identities with a base identity and a content cryptor.
 * When a chunk of data is encrypted, this is done using a new content cryptor with a
 * fresh crypto key. This crypto key is then encrypted by the base identity. Both
 * are put together as the cipher text. Decryption of course works the other way around.
 */
class CombinedCryptoIdentityFactory private constructor(
    private val hasherFactory: Hasher.Factory,
    private val contentCryptorFactory: Cryptor.Factory,
    private val baseIdentityFactory: CryptoIdentity.Factory,
    private val signerFactory: Signer.Factory,
    private val signatureCheckerFactory: SignatureChecker.Factory
) : CryptoIdentity.Factory,
    Signer.Factory by signerFactory,
    SignatureChecker.Factory by signatureCheckerFactory,
    Serializable {

    constructor(
        hasherFactory: Hasher.Factory,
        contentCryptorFactory: Cryptor.Factory,
        baseIdentityFactory: CryptoIdentity.Factory
    ) :
        this(
            hasherFactory,
            contentCryptorFactory,
            baseIdentityFactory,
            CombinedSigner.Factory(baseIdentityFactory, contentCryptorFactory),
            CombinedSignatureChecker.Factory(baseIdentityFactory, contentCryptorFactory)
        )

    override fun createCryptoIdentity() =
        baseIdentityFactory.createCryptoIdentity().let {
            CryptoIdentity.Base(
                hasherFactory.properties.hashAlgorithm,
                CombinedSigner(it, contentCryptorFactory),
                CombinedSignatureChecker(it, contentCryptorFactory),
                it.rawKeyPair
            )
        }

    override fun getCryptoIdentity(rawKeyPair: ByteArray) =
        baseIdentityFactory.getCryptoIdentity(rawKeyPair).let {
            CryptoIdentity.Base(
                hasherFactory.properties.hashAlgorithm,
                CombinedSigner(it, contentCryptorFactory),
                CombinedSignatureChecker(it, contentCryptorFactory),
                rawKeyPair
            )
        }

    override val properties =
        getCombinedCryptorProperties(
            baseIdentityFactory.properties,
            contentCryptorFactory.properties,
            signatureCheckerFactory.properties::getOutputBytes
        )

    companion object {
        const val serialVersionUID = 1L
    }
}
