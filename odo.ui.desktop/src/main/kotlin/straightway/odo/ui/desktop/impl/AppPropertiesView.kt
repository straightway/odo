/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.ui.desktop.impl

import javafx.geometry.Pos
import javafx.scene.input.Clipboard
import javafx.scene.input.ClipboardContent
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import straightway.odo.ui.desktop.model.RootModel
import straightway.odo.ui.model.App
import tornadofx.*

/**
 * View for displaying and editing app properties.
 */
class AppPropertiesView(private val subject: App) : View() {

    private val rootModel: RootModel by inject()

    override val root = form {
        fieldset {
            title = messages["title"]
            field(messages["appName"]) {
                label(subject.name)
            }
            field(messages["uri"]) {
                label(subject.uri?.toString() ?: messages["localApp"])
                button(Icon.COPY) {
                    tooltip(messages["copyUri"])
                    action(::copyAppUriToClipboard)
                    isDisable = subject.uri === null
                }
            }
        }
        hbox {
            alignment = Pos.CENTER_RIGHT
            button(Icon.UPLOAD) {
                tooltip(messages["upload_tooltip"])
                action(::onUpload)
            }
        }
    }

    private fun onUpload() {
        GlobalScope.launch {
            rootModel.apps.upload(subject)
        }
        close()
    }

    private fun copyAppUriToClipboard() {
        val clipboardContent = ClipboardContent()
        clipboardContent.putString(subject.uri.toString())
        Clipboard.getSystemClipboard().setContent(clipboardContent)
    }
}
