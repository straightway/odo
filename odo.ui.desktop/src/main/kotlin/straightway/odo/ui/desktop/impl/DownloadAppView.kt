/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.ui.desktop.impl

import javafx.beans.property.SimpleStringProperty
import javafx.geometry.Pos
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import straightway.odo.ui.desktop.model.RootModel
import tornadofx.*
import java.net.URI
import java.net.URISyntaxException

/**
 * View for specifying an app to download from the cloud.
 */
class DownloadAppView : View() {

    private val rootModel: RootModel by inject()
    private lateinit var uri: SimpleStringProperty

    override val root = form {
        val model = ViewModel()

        uri = model.bind { SimpleStringProperty() }
        title = messages["title"]

        fieldset {
            field(messages["downloadUri"]) {
                textfield(uri) {
                    validator {
                        if (it === null || it.isEmpty())
                            ValidationMessage(
                                messages["missingUri"],
                                ValidationSeverity.Error
                            )
                        else try {
                            URI(it)
                            null
                        } catch (_: URISyntaxException) {
                            ValidationMessage(
                                messages["invalidUri"],
                                ValidationSeverity.Error
                            )
                        }
                    }
                }
            }
        }
        hbox {
            alignment = Pos.CENTER_RIGHT
            button(General.messages["OK"]) {
                isDefaultButton = true
                enableWhen(model.valid)
                action {
                    close()
                    GlobalScope.launch {
                        rootModel.apps.download(URI(uri.get()))
                    }
                }
            }
            button(General.messages["cancel"]) {
                isCancelButton = true
                action {
                    close()
                }
            }
        }
    }
}
