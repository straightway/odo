/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.ui.desktop.impl

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon
import javafx.geometry.Pos
import javafx.scene.layout.HBox
import tornadofx.*

/**
 * A view displaying all contacts of a user.
 */
class ContactsView : View() {

    val model = listOf(
        Contact("Angela Merkel"),
        Contact("Joe Biden"),
        Contact("Emmanuel Macron")
    ).asObservable()

    override val root = editablecollectionpane(model, ::actionButtons) {
        borderpane {
            top {
                label(it.name) {
                    useMaxWidth = true
                    isWrapText = true
                    alignment = Pos.CENTER
                }
            }
        }
    }

    private fun actionButtons(bottom: HBox) {
        bottom.button(Icon.ADD) {
            tooltip(messages["add_tooltip"])
            action(::onAdded)
        }
    }

    init {
        title = FontAwesomeIcon.USERS.unicode()
    }

    private fun onAdded() {
        println("Adding contact")
    }
}
