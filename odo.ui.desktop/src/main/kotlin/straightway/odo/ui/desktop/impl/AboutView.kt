/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.ui.desktop.impl

import javafx.scene.Parent
import straightway.utils.reformattedLines
import tornadofx.*
import java.io.BufferedReader
import java.io.InputStream
import java.io.InputStreamReader

/**
 * Show copyright and version information.
 */
class AboutView : View() {

    override val root: Parent = form {
        fieldset {
            label(getResourceFileContent("about/notice.txt"))
            field(messages["version"]) {
                label(getResourceFileContent("about/version.txt"))
            }
            button(licenseLabelText) {
                action(::onLicense)
            }
            button(licenses3rdPartyLabelText) {
                action(::onShow3rdPartyLicenses)
            }
        }
    }

    // region Private

    private val licenseLabelText get() = messages["license"]
    private val licenses3rdPartyLabelText get() = messages["licenses3rdParty"]

    private fun onShow3rdPartyLicenses() {
        val licenses = getResourceFileContent("licenses_3rdParty/licenses.txt")
        openInternalWindow(
            TextView(licenses3rdPartyLabelText, licenses),
            owner = root,
            modal = true,
            movable = false,
        )
    }

    private fun onLicense() {
        val license = getResourceFileContent("about/license.txt")
        openInternalWindow(
            TextView(licenseLabelText, license),
            owner = root,
            modal = true,
            movable = false,
        )
    }

    private fun getResourceFileContent(path: String) =
        readFromInputStream(TextView::class.java.getResourceAsStream(path))

    private fun readFromInputStream(inputStream: InputStream) =
        BufferedReader(InputStreamReader(inputStream))
            .lineSequence()
            .joinToString("\n")
            .reformattedLines()

    init {
        title = messages["title"]
    }

    // endregion
}
