/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.ui.desktop.impl

import javafx.collections.ObservableList
import javafx.geometry.Pos
import javafx.scene.Node
import javafx.scene.layout.HBox
import tornadofx.*

fun <TItem> View.editablecollectionpane(
    model: ObservableList<TItem>,
    bottomItems: HBox.() -> Unit = {},
    cell: (TItem) -> Node
) = borderpane {
    center {
        datagrid(model) {
            cellCache(cell)
        }
    }
    bottom {
        hbox(alignment = Pos.BOTTOM_RIGHT) {
            bottomItems()
        }
    }
}
