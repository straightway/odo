/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.ui.desktop.impl

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon
import javafx.geometry.Pos
import javafx.scene.Parent
import javafx.scene.image.Image
import javafx.scene.layout.HBox
import kotlinx.coroutines.*
import straightway.odo.ui.desktop.model.RootModel
import straightway.odo.ui.model.InvalidAppException
import tornadofx.*
import java.io.File

/**
 * A view showing all installed apps.
 */
class InstalledAppsView : View() {

    private val rootModel: RootModel by inject()

    private val _root: Parent = editablecollectionpane(
        rootModel.apps.instances, ::actionButtons
    ) {
        borderpane {
            top {
                label(it.name) {
                    useMaxWidth = true
                    isWrapText = true
                    alignment = Pos.CENTER
                }
            }
            center {
                var image = Image((it.icon ?: defaultAppIcon).inputStream())
                if (image.isError)
                    image = Image(defaultAppIcon.inputStream())
                imageview(image) {
                    fitWidth = 100.0
                    fitHeight = 100.0
                }
            }
            bottom {
                hbox {
                    alignment = Pos.CENTER_RIGHT
                    button(Icon.PROPERTIES) {
                        tooltip(messages["properties_tooltip"])
                        action {
                            openInternalWindow(
                                AppPropertiesView(it),
                                owner = root,
                                modal = true,
                                movable = false,
                            )
                        }
                    }
                    button(Icon.LAUNCH) {
                        tooltip(messages["launch_tooltip"])
                        action {
                            GlobalScope.launch {
                                rootModel.apps.launch(it)
                            }
                        }
                    }
                    button(Icon.DELETE) {
                        tooltip(messages["delete_tooltip"])
                        action { rootModel.apps.delete(it) }
                    }
                }
            }
        }
    }

    override val root: Parent get() = _root

    private fun actionButtons(bottom: HBox) {
        bottom.button(Icon.DOWNLOAD) {
            tooltip(messages["download_tooltip"])
            action(::onDownload)
        }
        bottom.button(Icon.ADD) {
            tooltip(messages["add_tooltip"])
            action(::onAdd)
        }
    }

    init {
        title = FontAwesomeIcon.GEARS.unicode()
    }

    private fun onAdd() {
        val dir = chooseDirectory(messages["selectLocalAppDirectory"])
        try {
            rootModel.apps.addCopyOfApp(dir)
        } catch (e: InvalidAppException) {
            warning(dir, e.detail)
        } catch (e: Throwable) {
            warning(dir, e.message)
        }
    }

    private fun onDownload() {
        openInternalWindow(
            DownloadAppView(),
            owner = root,
            modal = true,
            movable = false,
        )
    }

    private fun warning(dir: File?, content: String?) {
        warning(
            title = messages["CouldNotAddLocalApp"],
            header = dir?.toString() ?: "",
            content = content
        )
    }

    private companion object {
        private val defaultAppIcon = InstalledAppsView::class.java
            .getResourceAsStream("icons/defaultAppIcon.png")
            .readAllBytes()
    }
}
