/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.ui.desktop.impl

import javafx.beans.property.SimpleStringProperty
import javafx.geometry.Pos
import javafx.scene.Node
import javafx.scene.image.Image
import javafx.scene.image.ImageView
import javafx.stage.FileChooser
import tornadofx.*
import java.io.ByteArrayInputStream

/**
 * A dialog allowing to specify a new identity to add.
 */
class AddIdentityView(
    private val parent: Node,
    private val onSuccess: (name: String, image: ByteArray?) -> Unit
) : View() {

    private lateinit var initialInputField: Node
    private lateinit var imageView: ImageView
    private lateinit var name: SimpleStringProperty
    private var image: ByteArray? = null

    @Suppress("SwallowedException")
    override val root = form {
        val model = ViewModel()

        name = model.bind { SimpleStringProperty() }
        title = messages["title"]
        fieldset {
            initialInputField = textfield(name) {
                required(message = messages["missingName"])
            }
        }
        fieldset {
            borderpane {
                center {
                    val image = Image(defaultIdentityIcon.inputStream())
                    imageView = imageview(image) {
                        fitWidth = 100.0
                        fitHeight = 100.0
                    }
                    button(graphic = imageView).action(::onChooseImage)
                }
            }
        }
        fieldset {
            hbox {
                alignment = Pos.BOTTOM_RIGHT
                button(General.messages["OK"]) {
                    enableWhen(model.valid)
                    isDefaultButton = true
                    action(::onOK)
                }
                button(General.messages["cancel"]) {
                    isCancelButton = true
                    action {
                        close()
                    }
                }
            }
        }
    }

    private fun onOK() {
        close()
        onSuccess(name.value, image)
    }

    override fun onUndock() {
        parent.isDisable = false
    }

    private fun onChooseImage() {
        val imageFile = chooseFile(
            messages["selectImage"],
            filters = arrayOf(
                FileChooser.ExtensionFilter(
                    "Image file",
                    listOf("*.png", "*.jpg", "*.jpeg")
                )
            )
        ).singleOrNull()

        if (imageFile == null)
            return

        image = imageFile.readBytes()
        imageView.image = Image(ByteArrayInputStream(image))
    }

    init {
        parent.isDisable = true
        runLater { initialInputField.requestFocus() }
    }

    private companion object {
        private val defaultIdentityIcon = AddIdentityView::class.java
            .getResourceAsStream("icons/defaultIdentityIcon.png")
            .readAllBytes()
    }
}
