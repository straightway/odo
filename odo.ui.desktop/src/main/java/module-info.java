/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
open module straightway.odo.ui.desktop {
    requires kotlin.stdlib;
    requires javafx.graphics;
    requires tornadofx;
    requires straightway.cryto;
    requires straightway.utils;
    requires straightway.odo.ui;
    requires java.desktop;
    requires java.net.http;
    requires straightway.compiler;
    requires straightway.odo.content;
    requires straightway.odo.api;
    requires straightway.odo.api.desktop;
    requires straightway.odo.loader;
    requires kotlinx.coroutines.core.jvm;
    requires straightway.odo.ui.desktop.model;
    requires de.jensd.fx.glyphs.fontawesome;
    requires de.jensd.fx.glyphs.commons;
    exports straightway.odo.ui.desktop;
    exports straightway.odo.ui.desktop.impl to javafx.graphics, tornadofx;
}
