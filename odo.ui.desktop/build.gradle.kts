/*
 * Copyright 2016 straightway
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
plugins {
    id("org.openjfx.javafxplugin") version "0.0.9"
}

javafx {
    version = "13"
    modules = listOf(
        "javafx.base",
        "javafx.controls",
        "javafx.fxml",
        "javafx.graphics",
        "javafx.media",
        "javafx.swing")
}

dependencies {
    implementation(project(":compiler"))
    implementation(project(":crypto"))
    implementation(project(":odo.api"))
    implementation(project(":odo.api.desktop"))
    implementation(project(":odo.boot"))
    implementation(project(":odo.content"))
    implementation(project(":odo.kernel"))
    implementation(project(":odo.loader"))
    implementation(project(":odo.ui"))
    implementation(project(":odo.ui.desktop.model"))
    implementation(project(":utils"))
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.3.9")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-jdk8:1.3.9")
    implementation("no.tornado:tornadofx2-odo:2.0.0-SNAPSHOT")
    implementation("de.jensd:fontawesomefx-commons:9.+")
}

tasks.register<Task>("generate3rdPartyLicenseInfo") {
    val thirdPartyLicensesPath = rootDir.listFiles()?.single { it.name == "licenses_3rdParty" }
    var result = ""
    thirdPartyLicensesPath
        ?.listFiles()
        ?.sorted()
        ?.forEach { thirdPartyLicenseDir ->
            result += "${thirdPartyLicenseDir.name.toUpperCase()}\n\n"
            thirdPartyLicenseDir.listFiles()?.forEach { licenseFile ->
                result += licenseFile.readText()
                result += "\n\n"
            }

            result += "\n"
        }

    val resourcesDir = sourceSets["main"]?.resources?.srcDirs?.single()
        ?.listFiles()?.single { it.name == "straightway" }
        ?.listFiles()?.single { it.name == "odo" }
        ?.listFiles()?.single { it.name == "ui" }
        ?.listFiles()?.single { it.name == "desktop" }
        ?.listFiles()?.single { it.name == "impl" }
    val thirdPartyResourcesDir = File(resourcesDir, "licenses_3rdParty")
    thirdPartyResourcesDir.mkdirs()
    val targetFile = File(thirdPartyResourcesDir, "licenses.txt")
    targetFile.writeText(result)
}

tasks.register<Task>("generateLicenseInfo") {
    val noticeFile = File(rootDir, "NOTICE")
    val licenseFile = File(rootDir, "LICENSE")

    val resourcesDir = sourceSets["main"]?.resources?.srcDirs?.single()
        ?.listFiles()?.single { it.name == "straightway" }
        ?.listFiles()?.single { it.name == "odo" }
        ?.listFiles()?.single { it.name == "ui" }
        ?.listFiles()?.single { it.name == "desktop" }
        ?.listFiles()?.single { it.name == "impl" }
    val licenseResourcesDir = File(resourcesDir, "about")
    licenseResourcesDir.mkdirs()
    File(licenseResourcesDir, "license.txt").writeText(licenseFile.readText())
    File(licenseResourcesDir, "notice.txt").writeText(noticeFile.readText())
    File(licenseResourcesDir, "version.txt").writeText("$version")
}

tasks.named<Task>("compileKotlin") {
    dependsOn(tasks.named<Task>("generate3rdPartyLicenseInfo"))
    dependsOn(tasks.named<Task>("generateLicenseInfo"))
}
