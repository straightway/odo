#!/usr/bin/env bash

# Copyright 2016 straightway
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

trap "exit" INT TERM ERR
trap "kill 0" EXIT

export ODO_ROOT=~/.odo
SCRIPT_DIR=`dirname $0`
SCRIPT_DIR=`realpath $SCRIPT_DIR`

INSTANCE_NO=""
if [ ! "$1" == "" ]
then
  INSTANCE_NO=$1
  ODO_ROOT="$SCRIPT_DIR/build/odoInstances/$INSTANCE_NO"
fi

./gradlew odo.cloud.ipfs:installIPFS

IPFS_CONFIG_PATH=$ODO_ROOT/ipfs/repo
mkdir -p $IPFS_CONFIG_PATH
if [ ! -e "$IPFS_CONFIG_PATH/config" ]
then
  ./ipfs.sh init &
  wait $!
fi

if [ "$INSTANCE_NO" != "" ]
then
  if [ ! -e "$IPFS_CONFIG_PATH/config.orig" ]
  then
    cp "$IPFS_CONFIG_PATH/config" "$IPFS_CONFIG_PATH/config.orig"
    odo.cloud.ipfs/ipfs/patchConfig.sh "$INSTANCE_NO" < "$IPFS_CONFIG_PATH/config.orig" > "$IPFS_CONFIG_PATH/config"
  fi
fi

./ipfs.sh daemon &
IPFS_PID=$!
echo "Started IPFS, PID: $IPFS_PID"

./gradlew odo.launcher:run

if ! [ -z "$IPFS_PID" ]
then
  kill $IPFS_PID
  echo "Killed IPFS, PID: $IPFS_PID"
fi