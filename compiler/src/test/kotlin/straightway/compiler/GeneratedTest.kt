/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.compiler

import org.junit.jupiter.api.Test
import straightway.testing.flow.*

class GeneratedTest {

    @Test
    fun `justification is as specified`() {
        assertThat(
            sut.justification is_ equal to_ "Justification"
        )
    }

    private companion object {
        val sut get() =
            TestDecorated::class.java.annotations.single { it is Generated } as Generated

        @Generated("Justification")
        private class TestDecorated
    }
}
