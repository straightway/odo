/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.numbers

import org.junit.jupiter.api.Test
import straightway.testing.flow.*
import java.math.BigDecimal
import java.math.RoundingMode

class NumberInfoTest {

    @Test
    fun `test Byte`() = 23.toByte().testAllMembersFor(
        expectedPrio = 0,
        valueMin = Byte.MIN_VALUE,
        valueMax = Byte.MAX_VALUE
    )

    @Test
    fun `test Short`() = 23.toShort().testAllMembersFor(
        expectedPrio = 100,
        valueMin = Short.MIN_VALUE,
        valueMax = Short.MAX_VALUE
    )

    @Test
    fun `test Int`() = 23.testAllMembersFor(
        expectedPrio = 200,
        valueMin = Int.MIN_VALUE,
        valueMax = Int.MAX_VALUE
    )

    @Test
    fun `test Long`() = 23L.testAllMembersFor(
        expectedPrio = 300,
        valueMin = Long.MIN_VALUE,
        valueMax = Long.MAX_VALUE
    )

    @Test
    fun `test BigInteger`() = "23".toBigInteger().testAllMembersFor(
        expectedPrio = 400,
        unifySource = 23
    )

    @Test
    fun `test Float`() = 23.0F.testAllMembersFor(
        expectedPrio = 500,
        unifySource = 23
    )

    @Test
    fun `test Double`() = 23.0.testAllMembersFor(
        expectedPrio = 600,
        unifySource = 23
    )

    @Test
    fun `test BigDecimal`() = "23.0".toBigDecimal().testAllMembersFor(
        expectedPrio = 700,
        unifySource = 23
    )

    @Test
    fun `test BigDecimal division`() {
        val numberInfo = NumberInfo[BigDecimal::class]
        assertThat(
            "1".toBigDecimal().(numberInfo.div)("3".toBigDecimal()) is_ equal to_
                "1".toBigDecimal()
                    .divide("3".toBigDecimal(), 32, RoundingMode.HALF_UP)
        )
    }

    @Test
    fun `unifyIfPossible without min, but with max`() {
        val sut = createSut(min = null)
        assertThat(sut.unifyIfPossible(5.1) is_ equal to_ 5.1)
    }

    @Test
    fun `unifyIfPossible without max, but with min`() {
        val sut = createSut(max = null)
        assertThat(sut.unifyIfPossible(5.1) is_ equal to_ 5.1)
    }

    @Test
    fun `unifyIfPossible with value below min`() {
        val sut = createSut(min = 0)
        assertThat(sut.unifyIfPossible(-5.1) is_ equal to_ -5.1)
    }

    @Test
    fun `unifyIfPossible with value above max`() {
        val sut = createSut(max = 20)
        assertThat(sut.unifyIfPossible(30.2) is_ equal to_ 30.2)
    }

    @Test
    fun `unifyIfPossible with within range`() {
        val sut = createSut(min = -20, max = 20)
        assertThat(sut.unifyIfPossible(2.4) is_ equal to_ 102)
    }

    @Test
    fun `unifyIfPossible without range`() {
        val sut = createSut(min = null, max = null)
        assertThat(sut.unifyIfPossible(2.4) is_ equal to_ 2.4)
    }

    @Test
    fun `unifyIfPossible with value below range minimum`() {
        val sut = createSut(min = 5, max = 10)
        assertThat(sut.unifyIfPossible(2.4) is_ equal to_ 2.4)
    }

    @Test
    fun `unifyIfPossible with value above range maximum`() {
        val sut = createSut(min = 0, max = 1)
        assertThat(sut.unifyIfPossible(2.4) is_ equal to_ 2.4)
    }

    // region Private

    private fun createSut(min: Int? = -10, max: Int? = 10) =
        NumberInfo(
            prio = 0,
            min = min,
            max = max,
            unify = { toInt() + 100 },
            round = { this },
            plus = { this + it },
            minus = { this - it },
            times = { this * it },
            div = { this / it },
            rem = { this % it },
            compare = { (this as Int).compareTo(it as Int) }
        )

    private fun <T : Number> T.testAllMembersFor(
        expectedPrio: Int,
        valueMin: T? = null,
        valueMax: T? = null,
        unifySource: Number? = null
    ) {
        val numberInfo = NumberInfo[this]
        val two = numberInfo.unify(2)
        val lowerTestValue = this.(numberInfo.minus)(two)
        assertThat(numberInfo.prio is_ equal to_ expectedPrio)
        if (valueMin != null)
            assertThat(numberInfo.min is_ equal to_ valueMin)
        else
            assertThat(numberInfo.min is_ null_)
        if (valueMax != null)
            assertThat(numberInfo.max is_ equal to_ valueMax)
        else
            assertThat(numberInfo.max is_ null_)
        assertThat(numberInfo.unify(unifySource ?: toDouble()) is_ equal to_ this)
        assertThat(numberInfo.round(this) is_ equal to_ this)
        assertThat(this.(numberInfo.plus)(this) is_ equal to_ 2 * this)
        assertThat(this.(numberInfo.minus)(this) is_ equal to_ 0)
        assertThat(two.(numberInfo.times)(this) is_ equal to_ 2 * this)
        assertThat(this.(numberInfo.div)(two) is_ equal to_ this / 2)
        assertThat(this.(numberInfo.rem)(two) is_ equal to_ this % 2)
        assertThat(this.(numberInfo.compare)(this) is_ equal to_ 0)
        assertThat(lowerTestValue.(numberInfo.compare)(this) is_ equal to_ -1)
        assertThat(this.(numberInfo.compare)(lowerTestValue) is_ equal to_ 1)
    }

    // endregion
}
