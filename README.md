[//]: # (Copyright 2016 straightway contributors)

[//]: # (Licensed under the Apache License, Version 2.0 [the "License"];)
[//]: # (you may not use this file except in compliance with the License.)
[//]: # (You may obtain a copy of the License at)

[//]: # (http://www.apache.org/licenses/LICENSE-2.0)

[//]: # (Unless required by applicable law or agreed to in writing, software)
[//]: # (distributed under the License is distributed on an "AS IS" BASIS,)
[//]: # (WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.)
[//]: # (See the License for the specific language governing permissions and)
[//]: # (limitations under the License.)

ODO Distributed Online
======================

Breaking News: Proof-of-Concept Prototype released!!
----------------------------------------------------

From now on, the ODO project is open for [contribution](Documents/Contributing.md)!! Please help!!

What is ODO?
------------

Currently: Work in progress, pre-alpha, available only as proof-of-concept prototype.

What is the Vision behind ODO?
------------------------------

ODO wants to become the "swiss army knife" for the development of _distributed communication apps_. It will allow developing such apps for various platforms: mobile, desktop, server, maybe even IoT devices. A _layered API_ will allow quick development of simple apps in a low-code-manner, or complex apps using all the features of modern software development environments.

### Get rid of Backend Servers

One of the key ideas is to _get rid of central servers_, meaning the app is really just the app on the device it is running on, no backends, no serers. All data is _encrypted_ and _distributed_ over all devices having ODO installed (using [IPFS](https://ipfs.io)). This means that an app with ODO can be run with minimal resources on the operator side, as you don't need AWS, Firebase, Azure or whatever.

### Make Privacy and Security easy

One of the main goals of the layered API is to make it easy to develop secure apps with good privacy by design. Of course it is not possible, to have an API which technically prevents all possible security vulnerabilities and privacy problems. But an API can _focus_ on privacy and security, and make these things easy. And while categories of privacy problems arising from a single entity controlling a central server, these kinds of problems simply does not exist with ODO.

Have a closer look at the [top level design document](Design/odoArchitecture.md) for more info about the technical ideas.

When can I have it?
-------------------

Please be patient ;-) We have the following milestones until the 1.0 release (checked=reached):

- [x] __Proof-of-concept Prototype__: We have a simple "Hello World" app with a first simple version of the ODO API. This app is distributed via IPFS and can be loaded and run by every ODO installation..
- [ ] __App: Project Chat__: The first useful ODO app should be a simple chat app used by the ODO developers to organize their work. This way, they get a feeling for the API and see, what is needed most badly.
- [ ] __Pilot Apps__: A few pilot apps, covering important basic use cases, should be developed. These pilot apps should cover most parts of the ODO API, so that it is oriented on the practial needs of app developers. These pilot apps could be developed for interested 3rd parties.
- [ ] __Stable API__: Problems in the ODO API are identified and fixed and the API is reviewed and stabilized.
- [ ] __Release 1.0__

I want to contribute!
---------------------

If you would like to contribute, start with [this document](Documents/Contributing.md), and the go on with [onboarding](Documents/Onboarding.md). Also have a look at your [code of conduct](Documents/CodeOfConduct/README.md).
