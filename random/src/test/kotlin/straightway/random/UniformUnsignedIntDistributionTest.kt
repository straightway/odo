/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.random

import org.junit.jupiter.api.Test
import straightway.testing.bdd.Given
import straightway.testing.bdd.throws
import straightway.testing.flow.assertThat
import straightway.testing.flow.equal
import straightway.testing.flow.is_
import straightway.testing.flow.to_
import straightway.testing.flow.values
import straightway.utils.toByteArray

class UniformUnsignedIntDistributionTest {

    private val test get() =
        Given {
            object {
                val source =
                    0x01020304.toByteArray() +
                        0x05060708.toByteArray()
                val sut = UniformUnsignedIntDistribution(source.iterator())
            }
        }

    @Test
    fun `first number comes from random source`() =
        test when_ { sut.next() } then { assertThat(it is_ equal to_ 0x01020304) }

    @Test
    fun `second number comes from random source`() =
        test while_ { sut.next() } when_ { sut.next() } then {
            assertThat(it is_ equal to_ 0x05060708)
        }

    @Test
    fun `exception is thrown if not enough bytes available`() {
        Given {
            UniformUnsignedIntDistribution(listOf<Byte>(1).iterator())
        } when_ {
            next()
        } then throws.exception
    }

    @Test
    fun `hasNext returns false if source has no next element`() =
        Given {
            UniformUnsignedIntDistribution(listOf<Byte>().iterator())
        } when_ {
            hasNext()
        } then {
            assertThat(it is_ false)
        }

    @Test
    fun `can be used with LINQ like expressions`() =
        test when_ { sut.take(2) } then {
            assertThat(it is_ equal to_ values(0x01020304, 0x05060708))
        }

    @Test
    fun `sign bit is ignored`() =
        Given {
            UniformUnsignedIntDistribution(
                listOf(
                    0x81.toByte(),
                    0x82.toByte(),
                    0x83.toByte(),
                    0x84.toByte()
                ).iterator()
            )
        } when_ {
            next()
        } then {
            assertThat(it is_ equal to_ 0x01828384)
        }
}
