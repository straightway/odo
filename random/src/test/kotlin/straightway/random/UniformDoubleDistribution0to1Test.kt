/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.random

import org.junit.jupiter.api.Test
import straightway.testing.bdd.Given
import straightway.testing.flow.assertThat
import straightway.testing.flow.equal
import straightway.testing.flow.is_
import straightway.testing.flow.to_
import straightway.utils.toByteArray

class UniformDoubleDistribution0to1Test {

    @Test
    fun `random stream of 0 yields 0`() =
        Given {
            UniformDoubleDistribution0to1(randomStream(0))
        } when_ {
            next()
        } then {
            assertThat(it is_ equal to_ 0.0)
        }

    @Test
    fun `random stream of 0xff yields 1`() =
        Given {
            UniformDoubleDistribution0to1(randomStream(-1L))
        } when_ {
            next()
        } then {
            assertThat(it is_ equal to_ 1.0)
        }

    @Test
    fun `random stream of last bit 0 and all others 0xff yields 0,5`() =
        Given {
            UniformDoubleDistribution0to1(randomStream(0x7f_ffff_ffff_ffffL))
        } when_ {
            next()
        } then {
            assertThat(it is_ equal to_ 0.5)
        }

    @Test
    fun `random stream of last twi bits 0 and all others 0xff yields 0,25`() =
        Given {
            UniformDoubleDistribution0to1(randomStream(0x3f_ffff_ffff_ffffL))
        } when_ {
            next()
        } then {
            assertThat(it is_ equal to_ 0.25)
        }

    @Test
    fun `toRandomStream yields stream with specified pseudo random numbers`() =
        Given {
            listOf(0.1, 0.0, 0.5, 1.0, 0.8)
        } when_ {
            toRandomStream()
        } then {
            val randomValues = UniformDoubleDistribution0to1(it.iterator()).toList()
            assertThat(randomValues is_ equal to_ this)
        }

    private fun randomStream(bits: Long) = bits.toByteArray().drop(1).iterator()
}
