/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.random

import org.junit.jupiter.api.Test
import straightway.testing.bdd.Given
import straightway.testing.bdd.throws
import straightway.testing.flow.assertThat
import straightway.testing.flow.empty
import straightway.testing.flow.equal
import straightway.testing.flow.is_
import straightway.testing.flow.to_
import straightway.testing.flow.values
import straightway.utils.toByteArray

class RandomChooserTest {

    private val test get() =
        Given {
            object {
                var randomStream
                    get() = sut.source
                    set(stream) { sut = RandomChooser(stream) }
                var sut = RandomChooser(byteStreamOf(0, 1, 2, 3, 4, 5, 6))
            }
        }

    @Test
    fun `choice from an empty collection is empty`() =
        test when_ { sut.chooseFrom(listOf<Int>(), 1) } then {
            assertThat(it is_ empty)
        }

    @Test
    fun `choice from single item is single item`() =
        test when_ { sut.chooseFrom(listOf(83), 1) } then {
            assertThat(it is_ equal to_ values(83))
        }

    @Test
    fun `choice from single item with overflowing random number`() =
        test while_ {
            this.randomStream = byteStreamOf(1000)
        } when_ {
            sut.chooseFrom(listOf(83), 1)
        } then {
            assertThat(it is_ equal to_ values(83))
        }

    @Test
    fun `choice of zero items is empty`() =
        test when_ { sut.chooseFrom(listOf(83), 0) } then {
            assertThat(it is_ empty)
        }

    @Test
    fun `choosing from a two element list with swapped items due to random number`() =
        test while_ { randomStream = byteStreamOf(1) } when_ {
            sut.chooseFrom(listOf(1, 2), 1)
        } then {
            assertThat(it is_ equal to_ values(2))
        }

    @Test
    fun `choosing from a two element list with not swapped items due to random number`() =
        test while_ { randomStream = byteStreamOf(0) } when_ {
            sut.chooseFrom(listOf(1, 2), 1)
        } then {
            assertThat(it is_ equal to_ values(1))
        }

    @Test
    fun `choosing from a two element list with not enough numbers in the random stream`() {
        test while_ { randomStream = byteStreamOf() } when_ {
            sut.chooseFrom(listOf(1, 2), 1)
        } then throws.exception
    }

    @Test
    fun `choosing two item from three element list, where first item is not swapped`() =
        test while_ { randomStream = byteStreamOf(0, 1) } when_ {
            sut.chooseFrom(listOf(1, 2, 3), 2)
        } then {
            assertThat(it is_ equal to_ values(1, 3))
        }

    @Test
    fun `choosing elements not directly adjacent to first one`() =
        test while_ {
            randomStream = byteStreamOf(2, 0)
        } when_ {
            sut.chooseFrom(listOf(0, 1, 2), 2)
        } then {
            assertThat(it is_ equal to_ listOf(2, 1))
        }

    private companion object {
        fun byteStreamOf(vararg ints: Int) = byteArrayOf(*ints).iterator()
        fun byteArrayOf(vararg ints: Int): ByteArray =
            if (ints.isEmpty())
                ByteArray(0)
            else ints.first().toByteArray() + byteArrayOf(*ints.sliceArray(1 until ints.size))
    }
}
