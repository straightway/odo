/*
 * Copyright 2016 straightway
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
plugins {
    kotlin("jvm") apply false
    kotlin("plugin.serialization")
    id("org.javamodularity.moduleplugin") version "1.7.0" apply false
    id("org.jmailen.kotlinter") version "3.0.2" apply false
    id("io.gitlab.arturbosch.detekt") version "1.15.0" apply false
    id("net.minecrell.licenser") version "0.4.1" apply false
    id("de.undercouch.download") version "4.1.1" apply false
    jacoco
}

allprojects {
    repositories {
        mavenCentral()
        jcenter()
        maven("https://repo1.maven.org/maven2")
        maven("file:pages/repo")
        maven("https://straightway.codeberg.page/repo")
        mavenLocal()
    }
}

subprojects {
    version = "0.5"

    apply(plugin = "org.jetbrains.kotlin.jvm")
    apply(plugin = "java")
    apply(plugin = "org.javamodularity.moduleplugin")
    apply(plugin = "org.jmailen.kotlinter")
    apply(plugin = "io.gitlab.arturbosch.detekt")
    apply(plugin = "net.minecrell.licenser")
    apply(plugin = "maven-publish")
    apply(plugin = "jacoco")
    apply(plugin = "kotlinx-serialization")

    extra.set("uploadToArchive", false)
    val repoUri = uri("file://" + rootProject.file("pages/repo"))
    val configFile = project.file("config.gradle.kts")
    if (configFile.exists())
        apply(from = configFile)

    dependencies {
        "implementation"(kotlin("stdlib"))
        "implementation"(kotlin("reflect"))
        "testImplementation"("org.junit.jupiter:junit-jupiter-api:5.3.1")
        "testImplementation"("org.junit.jupiter:junit-jupiter-params:5.3.1")
        "testImplementation"("com.nhaarman.mockitokotlin2:mockito-kotlin:2.2.0")
        "testImplementation"("org.mockito:mockito-core:2.+")
        "testRuntimeOnly"("org.junit.jupiter:junit-jupiter-engine:5.3.1")

        if (name != "testing") {
            "testImplementation"(project(":expr"))
            "testImplementation"(project(":testing"))
        }
    }

    configure<net.minecrell.gradle.licenser.LicenseExtension> {
        header = rootProject.file("config/LicenseHeader.txt")
        newLine = false
    }

    configure<io.gitlab.arturbosch.detekt.extensions.DetektExtension> {
        failFast = true
        buildUponDefaultConfig = true
        config = files("$rootDir/config/detekt/config.yml")

        reports {
            html.enabled = true
            xml.enabled = false
            txt.enabled = false
        }
    }

    configure<org.jmailen.gradle.kotlinter.KotlinterExtension> {
        disabledRules = arrayOf("no-wildcard-imports")
    }

    configure<JavaPluginExtension> {
        modularity.inferModulePath.set(true)
    }

    if (extra.get("uploadToArchive") == true) {
        val projectName = project.path.substring(1)
        configure<PublishingExtension> {
            publications {
                create<MavenPublication>(projectName) {
                    if (extra.has("mavenGroupId"))
                        groupId = extra.get("mavenGroupId").toString()
                    from(components["java"])
                }
            }

            repositories {
                maven {
                    name = "straightway"
                    url = repoUri
                }
            }
        }
    }

    tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
        kotlinOptions {
            jvmTarget = "11"
            languageVersion = "1.4"
        }
    }

    tasks.withType<JacocoCoverageVerification> {
        violationRules {
            rule {
                enabled = true
                element = "CLASS"
                excludes = (0..20).map { "*.$it" } + "*_it.*"
                limit {
                    counter = "INSTRUCTION"
                    value = "COVEREDRATIO"
                    minimum = "0.97".toBigDecimal()
                }
            }
        }
    }

    tasks.named<Test>("test") {
        finalizedBy(tasks.named<Task>("jacocoTestReport"))
        useJUnitPlatform()
        if (extensions.findByType(org.javamodularity.moduleplugin.extensions.TestModuleOptions::class) != null)
            extensions.configure(org.javamodularity.moduleplugin.extensions.TestModuleOptions::class) {
                runOnClasspath = true
            }
        systemProperty(
            "straightway.odo.runLongRunningTests",
            System.getProperty("straightway.odo.runLongRunningTests"))
    }

    tasks.named<Task>("check") {
        finalizedBy(tasks.named<Task>("jacocoTestCoverageVerification"))
    }

    tasks.register<Task>("format") {
        dependsOn(tasks.named<Task>("formatKotlin"))
        dependsOn(tasks.named<Task>("licenseFormat"))
    }

    tasks.register<Task>("fcheck") {
        dependsOn(tasks.named<Task>("format"))
        finalizedBy(tasks.named<Task>("check"))
    }
}

tasks.register<JacocoReport>("jacocoRootReport") {
    val testReportTasks = subprojects.getTasks<JacocoReport>("jacocoTestReport")
    testReportTasks.forEach { dependsOn(it) }

    val kotlinCompileTasks =
        subprojects.getTasks<org.jetbrains.kotlin.gradle.tasks.KotlinCompile>(
            "compileKotlin")

    val kotlinSourceDirs = kotlinCompileTasks.flatMap {
        it.source.files
    }.filter {
        it.name != "module-info.java"
    }.map {
        var cutPath = it
        while (cutPath != null && cutPath.name != "kotlin")
            cutPath = cutPath.parentFile
        cutPath ?: it
    }.toSet()

    additionalSourceDirs.setFrom(files(kotlinSourceDirs))
    sourceDirectories.setFrom(files(kotlinSourceDirs))

    val outputDirs = kotlinCompileTasks.flatMap { it.outputs.files }.toSet()
    classDirectories.setFrom(outputDirs)

    val jacocoReportsExecutionData = testReportTasks.map {
        it.executionData
    }.flatMap { it?.files ?: setOf<File>() }.filter { it.exists() }

    executionData.setFrom(files(jacocoReportsExecutionData))
    reports {
        html.setEnabled(true)
        xml.setEnabled(false)
        csv.setEnabled(false)
    }
    doFirst {
        executionData.setFrom(files(executionData.filter { it.exists() }))
    }
}

tasks.register<JacocoReport>("check") {
    val checkTasks = subprojects.getTasks<Task>("check")
    checkTasks.forEach { dependsOn(it) }
    finalizedBy(tasks.named<Task>("jacocoRootReport"))
}

tasks.register<Task>("format") {
    val formatTasks = subprojects.getTasks<Task>("formatKotlin")
    formatTasks.forEach { dependsOn(it) }
    val licenseTasks = subprojects.getTasks<Task>("licenseFormat")
    licenseTasks.forEach { dependsOn(it) }
    val testLicenseTasks = subprojects.getTasks<Task>("licenseFormatTest")
    testLicenseTasks.forEach { dependsOn(it) }
}

tasks.register<Task>("fcheck") {
    dependsOn(tasks.named<Task>("format"))
    finalizedBy(tasks.named<Task>("check"))
}

tasks.register<Task>("test") {
    val testTasks = subprojects.getTasks<Task>("test")
    testTasks.forEach { dependsOn(it) }
    finalizedBy(tasks.named<Task>("jacocoRootReport"))
}

inline fun <reified T: Task> Iterable<Project>.getTasks(name: String): List<T> =
    map {
        try {
            it.tasks.named<T>(name)
        } catch (x: UnknownTaskException) {
            null
        }
    }.filter {
        it != null
    }.map {
        it!!.get()
    }