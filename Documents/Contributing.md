[//]: # (Copyright 2016 straightway contributors)

[//]: # (Licensed under the Apache License, Version 2.0 [the "License"];)
[//]: # (you may not use this file except in compliance with the License.)
[//]: # (You may obtain a copy of the License at)

[//]: # (http://www.apache.org/licenses/LICENSE-2.0)

[//]: # (Unless required by applicable law or agreed to in writing, software)
[//]: # (distributed under the License is distributed on an "AS IS" BASIS,)
[//]: # (WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.)
[//]: # (See the License for the specific language governing permissions and)
[//]: # (limitations under the License.)

# Contribute to ODO

We appreciate any help we can get with ODO, so feel very welcome! Please check in wich area you would like to contribute, and then go on with [oboarding](Onboarding.md).

We need help in the areas listed below (don't worry if you don't have competences in every point). If you cannot find the proper thing for you, but would like to contribute anyway, don't hesitate! Together we'll find out where you can help us! 

We have a modest [code of conduct](CodeOfConduct/README.md) defining some aspects of the development process. Please have a look at this document to check if you find this appropriate for you. If not, don't hesitate do suggest improvements!

## Community Management

What we need the most badly is a community management, which organizes the ODO community to attract and integrate new contributors in all the areas listed below. If you want to contribute to this project and have a talent for communication and organization, we really need you!

## Coding

The general software design of ODO is outlined in the [architecture document](../Design/odoArchitecture.md). Open issues can be found [here](https://codeberg.org/straightway/ODO/issues).

To start coding, follow the steps described in the ['Getting Started' document](GettingStarted.md). When writing code, follow the rules described in the [coding guideline](CodeOfConduct/CodingAndDocumentation.md).

We need development expertise especially in the following areas:
* Kotlin
* Desktop UI with [TornadoFX](https://tornadofx.io/)
* Mobile apps for
    * Android
    * iOS
* [IPFS](https://ipfs.io)
* [Tor](https://www.torproject.org/)
* Network programming
* Cryptography
* Cyber security
* Software architecture
* API design

## User interface design

ODO should have a clear UI concept over all supported platforms. Simplicity is the first priority (of course without compromise in privacy and security).

## Testing

ODO must be tested on all relevant platforms. While the bread-and-butter-tests should run automatically (using [JUnit 5](https://junit.org/junit5/)), free testing is necessary to capture areas, that cannot be tested automatically, or that are hard to figure out by the developers beforhand.

We also need a concept for testing in order to be sure, that we don't miss anything and have stable and reliable software.

## Internationalization

Once the user interface is stable, it should be translated into various languages. Also other aspects of internationalization should be considered, that may initially not have been in the focus of the developers.

Also project documents and public relation stuff should be reviewed by native speakers and, where useful, translated to different languages. 

## Organization and culture

Straightway aims to be a loose group of volunteer software enthousiasts providing useful software tools for everyone. Straightway's first project is [ODO distributed online](../README.md).

Keeping such a project running efficiently requires a good organization of the team and a team culture, that makes every member feel comfortable, taking into accout, that the team members spend their limited spare time on the project, next to their regular jobs.

Especially (but not only) in the beginning, agility will be a important factor, in order to quickly react on problems or inefficiencies arising from the project organization. The development process is defined in our [code of conduct](CodeOfConduct/README.md), which needs frequent update and improvement. 

## Fund raising

Currently, money is no issue at all for ODO or straightway, and there is no concept for raising funds and what to do with the money, if we should be able to do this. Of course, money could be spent to pay professional full-time team members and infrastructure.

However, first of all a concept for all this is required, which then must be executed. And this is closely related to organizationonal issues.

On other aspect, which may be a bit cross-cutting, is [monetarization](Monetarization.md). Without denying our dedication to freedom and open source, we want to enable small firms to save or earn money with ODO.

## Public relations

For attracting more contriutors and as preparation for announcing releases in the future, we need a concept for public relations. 

## Legal expertise

ODO raises some legal questions, that requires expertise in this area:
* [License](../LICENSE): How to properly write code and documents to comply with the license? What are the pitfalls? 
* Third party licenses: What must be done to comply with these licenses?
* [GDPR](https://gdpr-info.eu/): ODO uses [IPFS](https://ipfs.io) as a basis, which is a distributed storage network. So how to make ODO and the ODO-apps GDPR-compliant, e.g. with respect to chapter 3?
* Compliance with legislation outside the european union.
* Organization: Straightway currently is no formal kind of legal entity, it only exists within a few heads and on Codeberg. But when it grows, it surely makes sense to think about what kind of legal entity it should be, and how to implement this. 
* Money: So far, straightway has no money. This makes thinks easy. But as soon as people are willing to donate for straightway, we must have a bank account and rules how to use the money and how to decide about it.
* Criminal content: ODO aims to avoid criminal content, provided by criminal users, by giving whistle blowers ways report such content. How such content can be rated and reported to the authorities is a legal topic.  

## Donations

**So far we cannot accept donations (see 'Money' point in section 'Legal expertise'). What we currently need are helping hands. If you still want to donate money, then to the friends of [Codeberg](https://liberapay.com/codeberg/donate), who are thankfully providing essential parts of the development infrastructure for this project.**
