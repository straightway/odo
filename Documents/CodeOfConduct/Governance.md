[//]: # (Copyright 2016 straightway contributors)

[//]: # (Licensed under the Apache License, Version 2.0 [the "License"];)
[//]: # (you may not use this file except in compliance with the License.)
[//]: # (You may obtain a copy of the License at)

[//]: # (http://www.apache.org/licenses/LICENSE-2.0)

[//]: # (Unless required by applicable law or agreed to in writing, software)
[//]: # (distributed under the License is distributed on an "AS IS" BASIS,)
[//]: # (WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.)
[//]: # (See the License for the specific language governing permissions and)
[//]: # (limitations under the License.)

# Governance

ODO is a very small project at the moment. Until further notice, the Head of Project (see [organigram](Organigram.md)) has the final decision and a veto right on all decisions. This includes (but is not limited to):
* Approval of issues

The basis for all decisions is always set by the [constitution](Constitution.md).

When the project gets more members, the governance structures will be pragmatically adapted according to the needs.