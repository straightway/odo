[//]: # (Copyright 2016 straightway contributors)

[//]: # (Licensed under the Apache License, Version 2.0 [the "License"];)
[//]: # (you may not use this file except in compliance with the License.)
[//]: # (You may obtain a copy of the License at)

[//]: # (http://www.apache.org/licenses/LICENSE-2.0)

[//]: # (Unless required by applicable law or agreed to in writing, software)
[//]: # (distributed under the License is distributed on an "AS IS" BASIS,)
[//]: # (WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.)
[//]: # (See the License for the specific language governing permissions and)
[//]: # (limitations under the License.)

# Code of Conduct

To avoid chaos and make the basics clear, the ODO projekt has a very modest code of conduct, which is described here. We take the freedom to change this code of conduct whenever we feel this would be necessary, because this system should help us and not stand in our way.

**Contents**
* [Constitution](Constitution.md): The spirit and basic values
* [Governance](Governance.md): How decisions are made
* [Release Life Cycle](ReleaseLifeCycle.md): Describes the states of an ODO software release
* [Issue Tracking](IssueTracking.md): Life cycle of bugs, features and other issues
* [Coding](CodingAndDocumentation.md): How to write code and documents
* [Testing](Testing.md): How to test

**Appendix**
* [Organigram](../Organigram.md)
