[//]: # (Copyright 2016 straightway contributors)

[//]: # (Licensed under the Apache License, Version 2.0 [the "License"];)
[//]: # (you may not use this file except in compliance with the License.)
[//]: # (You may obtain a copy of the License at)

[//]: # (http://www.apache.org/licenses/LICENSE-2.0)

[//]: # (Unless required by applicable law or agreed to in writing, software)
[//]: # (distributed under the License is distributed on an "AS IS" BASIS,)
[//]: # (WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.)
[//]: # (See the License for the specific language governing permissions and)
[//]: # (limitations under the License.)

# Issue Tracking

All changes to the ODO software must base on an [issue](https://codeberg.org/straightway/odo/issues) of the codeberg issue tracking system. Any bugs or change request must be entered there as issues. Before adding a new issue, check if it has already been reported.

## Labels and states

Issues may be flagged using one or more of the followig labels:

| Label          | Meaning
|----------------|-----------------------------------------------------------
| Bug            | Some kind of defect or unexpected behavior in the software
| CodeQuality    | A suggestion to enhance the code quality
| Documentation  | A suggestion to enhance or correct the documentation
| Feature        | A new feature to implement
| Legal          | A legal issue in the software or its documentation
| Performance    | A suggestion to enhance the performance
| UI             | A suggestion to enhance the user interface
| FixDescription | The description of the issue needs to be improved  
| Sporadic       | The issue is not well reproducible
| QM             | An issue relate to the quality management system itself
| Version:XXX    | The issue occurred in software version XXX
| Branch:XXX     | The issue is being fixed on branch XXX

An issue is approved by assigning a milestone to it. This milestone also defines the software version, where it will be resolved.Only approved issues (i.e. with a valid milestone) may be processed.

When an issue is started, one or more users are assigned responsible for it. This may be done by the users themselves. The implementation of the issue is then done according to the [coding guideline](CodingAndDocumentation.md).

After the issue is done, it is closed.

## Issue description

The issue description must be short, precise and complete and require as few follow-up questions as possible.

### Bugs

For bugs, the follwing aspects should be covered:

* Software version: The version number displayed in the about section of the software. If this ends with 'SNAPSHOT', also the branch and revision number it was built from are useful.
* Environment: Hardware (incl. important details, e.g. CPU, RAM), OS (incl. version), JVM version.
* Steps to reproduce
* If applicable
  * The ODO-app which triggered the issue
  * Exception stack trace
  * Log files
  * The data which was used to trigger the issue 

### Feature requests

Feature requests shall be written as [user stories](https://en.wikipedia.org/wiki/User_story) in the form ```As a <role> I want <action> to achieve <goal>``` (or similar). It shall also include a list of clear acceptance criteria.

### Other types of issues

Clearly describe what the point is.