[//]: # (Copyright 2016 straightway contributors)

[//]: # (Licensed under the Apache License, Version 2.0 [the "License"];)
[//]: # (you may not use this file except in compliance with the License.)
[//]: # (You may obtain a copy of the License at)

[//]: # (http://www.apache.org/licenses/LICENSE-2.0)

[//]: # (Unless required by applicable law or agreed to in writing, software)
[//]: # (distributed under the License is distributed on an "AS IS" BASIS,)
[//]: # (WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.)
[//]: # (See the License for the specific language governing permissions and)
[//]: # (limitations under the License.)

# Release Life Cycle

A release starts with a set of planned requirements and ends with a usable software.

## Release documents

The release documents are stored in git under ```Documents/Releases``` in a subfolder named with the release number and a short release description, e.g. ```0.5 Proof of Concept Prototype```.

This folder contains the following files:

| File            | Description
|-----------------|------------
| Requirements.md | Description of all requirements that must be fulfilled for the release.
| Environment.md  | Describe the development environment for the release.

## Creating a new release

To start the work for a new release, do the following things:

- Set the version number in the top level ```build.gradle.kts``` file, suffixed by ```-SNAPSHOT```. Example: ```version = "0.5-SNAPSHOT"``` 
- Create the release folder under ```Documents/Releases``` as described above.
- Add all required files to the release folder:
    - ```Requirements.md```: Describe an initial set of requirements.
  - ```Environemt.md```: Define the following properties
    - Integration branch name. For releases after 0.5, this must not be ```main```
    - Artifacts the software will consist of
- Create a [label](https://codeberg.org/straightway/odo/labels) named ```Version:``` + the version number, e.g. ```Version:0.5```
- Create a branch for 

## During development

- Create issues with the label ```Feature``` to implement the features of the release. These issues may either implement existing requirements or may add additional requirements (incl. updating ```Requirements.md```).
- Merge all fully implemented and tested features to the integration branch.
- Mark implemented requirements accordingly in the ```Requirements.md``` file.

## Finalizing the release

Create an issue for finalizing the release and use this issue for the final commits:

- Check if all requirements have been marked as implemented in ```Requirements.md``.
- Remove the ```-SNAPSHOT``` suffix from the version number in the top level ```build.gradle.kts``` file.
- Update any other documents where the release is mentioned.
- Commit and merge the changes to the integration branch.
- Merge the integration branch to the ```main``` branch.
- Build and upload all artifacts defined in ```Environment.md```.
- Have a bottle of champaign with the teammates.