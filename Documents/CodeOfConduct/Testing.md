[//]: # (Copyright 2016 straightway contributors)

[//]: # (Licensed under the Apache License, Version 2.0 [the "License"];)
[//]: # (you may not use this file except in compliance with the License.)
[//]: # (You may obtain a copy of the License at)

[//]: # (http://www.apache.org/licenses/LICENSE-2.0)

[//]: # (Unless required by applicable law or agreed to in writing, software)
[//]: # (distributed under the License is distributed on an "AS IS" BASIS,)
[//]: # (WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.)
[//]: # (See the License for the specific language governing permissions and)
[//]: # (limitations under the License.)

# Testing

In ODO, the following testing strategy applies:

1. Automatic tests
    1. Automatic test on unit level shall strive to cover 100%.
    2. Automatic test on different levels of integration shall cover all relevant use cases.
    3. Bug shall be reproduced with automatic tests (either on unit or integration level) before fixing them, if possible.
2. Manual tests: All manual tests are free tests, that explore the corner cases of the system, that are not (yet) tested automatically. Important new test cases shall be implemented as automatic tests.

All bugs shall be described in dedicated [issues](IssueTracking.md). 
