[//]: # (Copyright 2016 straightway contributors)

[//]: # (Licensed under the Apache License, Version 2.0 [the "License"];)
[//]: # (you may not use this file except in compliance with the License.)
[//]: # (You may obtain a copy of the License at)

[//]: # (http://www.apache.org/licenses/LICENSE-2.0)

[//]: # (Unless required by applicable law or agreed to in writing, software)
[//]: # (distributed under the License is distributed on an "AS IS" BASIS,)
[//]: # (WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.)
[//]: # (See the License for the specific language governing permissions and)
[//]: # (limitations under the License.)

# Constitution

This constitution of the ODO project is the basis for our collaboration. It states our fundamental spirit and values and defines our code of conduct. It is binding for all decisions made within the ODO project.

We are dedicated to the concepts and values expressed in the [#webIsOurs manifest](https://webisours.codeberg.page).

All tools used within the ODO project, no matter for what, shall be available under a free license according to the [license classification of the Free Software Foundation](https://www.gnu.org/licenses/license-list.en.html) (green, purple and yellow left border). No tool shall be used that track users and their interactions for commercial interests in any way.

In conversations we follow the spirit of the [netiquette](https://tools.ietf.org/html/rfc1855).

Our mindset and behavior are [agile](https://agilemanifesto.org/). When we encounter obstacles, we remove or work around them pragmatically, without compromising our values.

The governance of the ODO project is described [in a separate document](Governance.md).