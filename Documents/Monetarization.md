[//]: # (Copyright 2016 straightway contributors)

[//]: # (Licensed under the Apache License, Version 2.0 [the "License"];)
[//]: # (you may not use this file except in compliance with the License.)
[//]: # (You may obtain a copy of the License at)

[//]: # (http://www.apache.org/licenses/LICENSE-2.0)

[//]: # (Unless required by applicable law or agreed to in writing, software)
[//]: # (distributed under the License is distributed on an "AS IS" BASIS,)
[//]: # (WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.)
[//]: # (See the License for the specific language governing permissions and)
[//]: # (limitations under the License.)

# Monetarization

ODO as a generic platform shall always be free and open source!

But to make it a success, the ability for firms to earn or save money with ODO can be an important factor. This document outlines, how this could be achieved.

## Saving Money with ODO

In comparison to classical mobile apps or websites, the operating costs of ODO-apps are much lower due to the absence of servers: No server machines, no server administration, no central target for cyber attacks. In addition, deployment is just as easy as updating a website. This makes ODO attractive for low budget or no budget apps. Plus, simple ODO-apps can be developed with relativley low effort. For many use cases, a small global user base of ODO would not be a problem because a specific ODO-app for that use case is only used by a limited number of users (e.g., the employees of a company or the members of a project) who all are willing to install ODO on their devices to get the concrete benefit of that specific ODO-app.

Generally, also serverless web shops are thinkable. Such shops are especially attractive for small businesses due to reduced hosting costs. To make this kind of application even more attractive, it would be desirable to integrate a simple payment API into ODO. However, in order to effectively offer services to the public via ODO-apps, ODO needs a considerable user base, at least among the potential customers. This might be accelerated by a growing number of entities running small ODO-apps, becaus this increases to isntall base of the ODO platform. This might be a way to circiumvent the network effects of the big platforms, but it will take time. 

So in many cases, ODO can help saving money, especially for small businesses or niche applications with a clearly defined community of users.

## Earning Money with ODO

### Professional development of ODO-apps

The ODO API is layered, which allows developing ODO-apps on different scales of complexity. While the top layer allows non-professional developers (power users) writing simple ODO-apps, the API deeper layers provide more flexibility and power, at the cost of more complexity.

This is an opportunity for software companies to commercially provide professinally developed ODO-apps and maintenance.

### Providing background services

ODO uses [IPFS](https://ipfs.io) as distributed cloud storage. It lies in the nature of such distributed storage, that it may "forget" content. In some cases, this is desirable, in some cases not. However, for some use cases one may want to make sure that data stored in the cloud ist properly archived and can be accessed reliably. This could be achieved with a paid "pinning" service (i.e. permanently storing specific data blocks). Please notice that privacy is not tampered in any way by such a service, because it would only pin encrypted data (because all data stored in the cloud is always encrypted in ODO). Pricing for such a pinning service could be volume based.

Also thinkable would be interface bridges to other systems, that are not ODO-based. These interface bridges may be offered as paid services, e.g.:

* A payment service that allows simple and secure integration of payments into ODO-apps, as backend for the ODO payment API.
* E-government systems
* Public health infrastructure
* Industrial order management and production planning
* E-Mail

### Paid ODO-apps

ODO apps could be offered for money. For example the decryption key of an ODO-app file could be purchased to be able to start the app.

### Sales within ODO-apps

ODO-apps could offer in app sales using the payment API, e.g. in distributed online games, video-on-demand, digital newspapers, etc. 