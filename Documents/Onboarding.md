[//]: # (Copyright 2016 straightway contributors)

[//]: # (Licensed under the Apache License, Version 2.0 [the "License"];)
[//]: # (you may not use this file except in compliance with the License.)
[//]: # (You may obtain a copy of the License at)

[//]: # (http://www.apache.org/licenses/LICENSE-2.0)

[//]: # (Unless required by applicable law or agreed to in writing, software)
[//]: # (distributed under the License is distributed on an "AS IS" BASIS,)
[//]: # (WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.)
[//]: # (See the License for the specific language governing permissions and)
[//]: # (limitations under the License.)

# Onboarding ODO

Welcome to the ODO project!

If you think about contributing to ODO, please read through the [contrinuting document](Contributing.md) and check in which area(s) your help could be most effective. If you don't find anything for you in this document that really works for you: Never mind, we'll find something for you.

What you definitely need is an e-mail and a [Codeberg](https://codeberg.org) account. When you have both, just contact ```straightway (at) gmx.org``` and write some words about yourself, your skills, your motivation, with how much time, how regularly and in which area you would like to contribute.