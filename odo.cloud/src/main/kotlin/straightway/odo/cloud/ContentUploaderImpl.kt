/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.cloud

import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import straightway.compiler.Generated
import straightway.error.Panic
import straightway.odo.content.*
import java.net.URI
import java.nio.file.Path

/**
 * Upload files and directories to a CloudWrite instance.
 */
class ContentUploaderImpl(
    private val cloud: CloudWrite,
    baseDir: java.io.File,
    block: ContentUploader.DirBuilder.() -> Unit
) : ContentUploader {

    override suspend fun upload(): URI = dir.upload()

    // region private

    private inner class DirBuilder(
        override val dir: java.io.File
    ) : ContentUploader.DirBuilder {

        override val name: String = dir.name
        override suspend fun getContentItemDescriptor() =
            ContentItemDescriptor(upload().withFileName(name))

        override var description: String = ""
        override val items get() = _items

        override fun link(f: ContentItemDescriptor) =
            addOrUpdate(f.uri.fileName, { contentItemDescriptor = f }) {
                FileLinkBuilder(f)
            }
        override fun link(dirName: String, d: ContentBundleDescriptor) =
            addOrUpdate(dirName, { contentBundleDescriptor = d }) {
                DirLinkBuilder(dirName, d)
            }

        override fun file(
            name: String,
            block: ContentUploader.FileBuilder.() -> Unit
        ) = addOrUpdate(name, block) { FileBuilder(this, name) }

        override fun dir(
            d: String,
            block: ContentUploader.DirBuilder.() -> Unit
        ) = addOrUpdate(d, block) {
            DirBuilder(
                dir.listFiles()?.singleOrNull { it.name == d }
                    ?: throw Panic("Invalid directory: $dir")
            )
        }

        override fun tree(d: String): Unit = dir(d) {
            dir.listFiles()?.forEach {
                if (it.isDirectory)
                    tree(it.name)
                else file(it.name)
            } ?: panicInvalidDirectory()
        }

        override fun tree() {
            dir.listFiles()?.forEach {
                if (it.isDirectory)
                    tree(it.name)
                else file(it.name)
            } ?: panicInvalidDirectory()
        }

        private fun panicInvalidDirectory() {
            throw Panic("invalid directory: $dir")
        }

        private inline fun <reified T : ContentUploader.ContentItemBuilder> addOrUpdate(
            name: String,
            initializer: T.() -> Unit,
            creator: () -> T
        ) = when (val existingItem = _items.singleOrNull { it.name == name }) {
            null -> _items += creator().apply(initializer)
            is T -> existingItem.initializer()
            else -> throw Panic("Other item named $name already exists: $existingItem")
        }

        suspend fun upload(): URI {
            val contentBundle = ContentBundleDescriptor(getContentItemDescriptors())
                .with(DESCRIPTION, description)

            val contentBundleJson = Json.encodeToString(contentBundle)
            return cloud.store(contentBundleJson)
                .withContentBundleFlag
                .withFileName(name)
        }

        private suspend fun getContentItemDescriptors() =
            items.map { it.getContentItemDescriptor() }

        private var _items = listOf<ContentUploader.ContentItemBuilder>()

        // endregion
    }

    private inner class FileBuilder(
        baseDir: DirBuilder,
        override val name: String
    ) : ContentUploader.FileBuilder {
        override suspend fun getContentItemDescriptor() =
            ContentItemDescriptor(store().withFileName(name))
                .with(TAGS, tags)
                .with(MIME_TYPE, mimeType)
                .with(ENCODING, encoding)
        override var tags = setOf<String>()
        override var mimeType: String? = null
        override var encoding: String? = null

        // region private

        private val file = Path.of(baseDir.dir.absolutePath, name).toFile()
        private suspend fun store() = cloud.store(fileContent)
        private val fileContent get() = file.readBytes()

        init {
            if (!file.exists())
                throw Panic("File $file does not exist")
            if (file.isDirectory)
                throw Panic("File $file is a directory")
        }

        // endregion
    }

    @Generated("Jacoco sees code branches I don't see")
    private class FileLinkBuilder(
        var contentItemDescriptor: ContentItemDescriptor
    ) : ContentUploader.ContentItemBuilder {
        override val name get() = contentItemDescriptor.uri.fileName
        override suspend fun getContentItemDescriptor() = contentItemDescriptor
    }

    @Generated("Jacoco sees code branches I don't see")
    private inner class DirLinkBuilder(
        override val name: String,
        var contentBundleDescriptor: ContentBundleDescriptor
    ) : ContentUploader.ContentItemBuilder {
        override suspend fun getContentItemDescriptor() =
            ContentItemDescriptor(store().withFileName(name))

        // region private

        private suspend fun store() = cloud.store(jsonContent).withContentBundleFlag
        private val jsonContent get() = Json.encodeToString(contentBundleDescriptor)

        // endregion
    }

    private val dir = DirBuilder(baseDir).apply { block() }

    // endregion
}

@Generated("Jacoco sees branches I don't see")
private suspend fun CloudWrite.store(s: String) = store(s.toByteArray(Charsets.UTF_8))
