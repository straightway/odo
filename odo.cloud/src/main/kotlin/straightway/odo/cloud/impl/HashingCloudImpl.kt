/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.cloud.impl

import straightway.crypto.Hasher
import straightway.error.Panic
import straightway.odo.cloud.*
import straightway.odo.content.hash
import straightway.odo.content.withHash
import java.net.URI

/**
 * Cloud implementation which automatically generates content hash codes while sotring
 * or checks those hases while retrieving.
 */
class HashingCloudImpl(
    private val baseCloud: Cloud,
    private val hasher: Hasher
) : Cloud by baseCloud {

    override suspend fun retrieve(
        uri: URI,
        progressCallback: (TransferProgress) -> Unit
    ) = baseCloud.retrieve(uri, progressCallback).also {
        val expectedHash = uri.hash
        if (expectedHash != null && !expectedHash.contentEquals(hasher.getHash(it)))
            throw Panic("Invalid hash code for $uri")
    }

    override suspend fun store(
        content: ByteArray,
        progressCallback: (TransferProgress) -> Unit
    ) = baseCloud.store(content, progressCallback).withHash(hasher.getHash(content))
}
