/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.cloud.impl

import straightway.compiler.Generated
import straightway.crypto.Decryptor
import straightway.crypto.Encryptor
import straightway.odo.cloud.*
import java.net.URI

/**
 * Crypto cloud read and write implementation.
 */
class CryptoCloudImpl(
    settings: Map<String, Any>,
    superCloudFactory: SuperCloud.Factory
) : CryptoCloud, AutoCloseable {

    override fun decryptingWith(decryptor: Decryptor): SuperCloudRead =
        DecryptingSuperCloudRead(decryptor)

    override fun encryptingWith(encryptor: Encryptor): SuperCloudWrite =
        EncryptingSuperCloudWrite(encryptor)

    override fun close() {
        if (superCloud is AutoCloseable)
            superCloud.close()
    }

    class Factory(
        internal val superCloudFactory: SuperCloud.Factory
    ) : CryptoCloud.Factory {
        override val supportedUriSchemes = superCloudFactory.supportedUriSchemes
        override fun createCryptoCloud(settings: Map<String, Any>): CryptoCloud =
            CryptoCloudImpl(settings, superCloudFactory)
    }

    // region Private

    private val superCloud = superCloudFactory.createSuperCloud(settings)

    private inner class DecryptingSuperCloudRead(private val decryptor: Decryptor) :
        SuperCloudRead {
        override fun readVia(uriScheme: String) =
            DecryptingCloudRead(decryptor, superCloud.readVia(uriScheme))
    }

    private class DecryptingCloudRead(
        private val decryptor: Decryptor,
        private val base: CloudRead
    ) : CloudRead by base {
        override suspend fun retrieve(
            uri: URI,
            progressCallback: (TransferProgress) -> Unit
        ): ByteArray = decryptor.decrypt(base.retrieve(uri, progressCallback))
    }

    private inner class EncryptingSuperCloudWrite(private val encryptor: Encryptor) :
        SuperCloudWrite {
        override fun writeVia(uriScheme: String) =
            EncryptingCloudWrite(encryptor, superCloud.writeVia(uriScheme))
    }

    private class EncryptingCloudWrite(
        private val encryptor: Encryptor,
        private val base: CloudWrite
    ) : CloudWrite by base {

        @Generated("jaccoco sees branches I don't see")
        override suspend fun store(
            content: ByteArray,
            progressCallback: (TransferProgress) -> Unit
        ): URI = encryptor.encrypt(content).let {
            base.store(it, progressCallback)
        }
    }

    // endregion
}
