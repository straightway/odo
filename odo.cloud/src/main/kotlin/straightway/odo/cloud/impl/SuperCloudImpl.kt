/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.cloud.impl

import straightway.compiler.Generated
import straightway.error.Panic
import straightway.odo.cloud.*

/**
 * Implementation of the SuperCloud interface. Instantiates all clouds provided
 * by the specified factories.
 */
class SuperCloudImpl(
    settings: Map<String, Any>,
    subCloudFactories: List<Cloud.Factory>
) : SuperCloud, AutoCloseable {

    @Generated("jacoco does not handle the exception properly")
    override fun readVia(uriScheme: String) =
        uriSchemeToSubCloud[uriScheme] ?: throwInvalid(uriScheme)

    @Generated("jacoco does not handle the exception properly")
    override fun writeVia(uriScheme: String) =
        uriSchemeToSubCloud[uriScheme] ?: throwInvalid(uriScheme)

    override fun close() =
        subClouds.forEach { if (it is AutoCloseable) it.close() }

    class Factory(
        internal val subCloudFactories: List<Cloud.Factory>
    ) : SuperCloud.Factory {
        override val supportedUriSchemes = getCombinedUriSchemes(subCloudFactories)
        override fun createSuperCloud(settings: Map<String, Any>): SuperCloud =
            SuperCloudImpl(settings, subCloudFactories)
    }

    // region Private

    private val subClouds: List<Cloud>
    private val uriSchemeToSubCloud: Map<String, Cloud>

    private fun <T> throwInvalid(uriScheme: String): T =
        throw Panic("Unsupported scheme for URI $uriScheme")

    init {
        getCombinedUriSchemes(subCloudFactories)
        val mutableSubClouds = mutableListOf<Cloud>()
        uriSchemeToSubCloud = subCloudFactories.flatMap { factory ->
            val subCloud = factory.createCloud(settings)
            mutableSubClouds.add(subCloud)
            factory.supportedUriSchemes.map { it to subCloud }
        }.toMap()
        subClouds = mutableSubClouds
    }

    private companion object {
        fun getCombinedUriSchemes(subCloudFactories: List<Cloud.Factory>): Set<String> {
            val allUriSchemes = subCloudFactories.flatMap { it.supportedUriSchemes }
            if (allUriSchemes.distinct() != allUriSchemes)
                throw Panic(
                    "sub cloud factories have overlapping URI schemes: " +
                        allUriSchemes.joinToString(", ")
                )
            return allUriSchemes.toSet()
        }
    }

    // endregion
}
