/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.cloud

import java.net.URI

/**
 * Write access to the cloud. All functions get an abstract ID as specifier for the
 * written content, and return an URI for read access.
 */
interface CloudWrite {

    suspend fun createRoot(): URI
    suspend fun removeRoot(root: URI)

    suspend fun store(
        content: ByteArray,
        progressCallback: (TransferProgress) -> Unit = {}
    ): URI

    suspend fun publish(root: URI, data: URI)
}
