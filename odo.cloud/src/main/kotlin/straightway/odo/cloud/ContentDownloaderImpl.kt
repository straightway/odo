/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.cloud

import kotlinx.serialization.json.Json
import straightway.error.Panic
import straightway.odo.content.*
import java.io.File
import java.net.URI
import java.nio.file.Paths

/**
 * Download files ot directories from a CloudRead instance.
 */
class ContentDownloaderImpl(
    private val cloud: CloudRead
) : ContentDownloader {

    override suspend fun download(basePath: File, uri: URI) =
        basePath.download(uri, setOf())

    // region Private

    private suspend fun File.download(uri: URI, downloadedUris: Set<URI>) {
        downloadedUris.checkCircularDependencyWith(uri)

        with(Paths.get(path, uri.fileName).toFile()) {
            if (uri.isContentBundle)
                downloadDirectoryTree(uri, downloadedUris)
            else downloadSingleFile(uri)
        }
    }

    private fun Set<URI>.checkCircularDependencyWith(uri: URI) {
        if (contains(uri))
            throw Panic("Circular dependency with $uri")
    }

    private suspend fun File.downloadDirectoryTree(uri: URI, downloadedUris: Set<URI>) {
        val contentBundleDescriptor = getContentBundleDescriptorFrom(uri)
        mkdirs()
        contentBundleDescriptor.items.forEach {
            download(it.uri, downloadedUris + uri)
        }
    }

    private suspend fun getContentBundleDescriptorFrom(uri: URI): ContentBundleDescriptor {
        val fileContent = cloud.retrieve(uri)
        return fileContent.contentBundleDescriptor
    }

    private val ByteArray.contentBundleDescriptor get() =
        Json.decodeFromString(
            ContentBundleDescriptor.serializer(),
            toString(Charsets.UTF_8)
        )

    private suspend fun File.downloadSingleFile(uri: URI) {
        val fileContent = cloud.retrieve(uri)
        writeBytes(fileContent)
    }

    // endregion
}
