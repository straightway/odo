/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.cloud

import straightway.odo.content.ContentBundleDescriptor
import straightway.odo.content.ContentItemDescriptor
import java.net.URI

/**
 * Upload content to the cloud. Add files and folders to the content using
 * a builder pattern.
 */
interface ContentUploader {
    suspend fun upload(): URI

    interface ContentItemBuilder {
        val name: String
        suspend fun getContentItemDescriptor(): ContentItemDescriptor
    }

    interface DirBuilder : ContentItemBuilder {
        val dir: java.io.File
        var description: String
        val items: List<ContentItemBuilder>
        fun link(f: ContentItemDescriptor)
        fun link(dirName: String, d: ContentBundleDescriptor)
        fun file(name: String, block: FileBuilder.() -> Unit = {})
        fun dir(d: String, block: DirBuilder.() -> Unit = {})
        fun tree()
        fun tree(d: String)
    }

    interface FileBuilder : ContentItemBuilder {
        var tags: Set<String>
        var mimeType: String?
        var encoding: String?
    }
}
