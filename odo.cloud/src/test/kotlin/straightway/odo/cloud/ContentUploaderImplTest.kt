/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.cloud

import com.nhaarman.mockitokotlin2.*
import kotlinx.serialization.json.Json
import org.junit.jupiter.api.Test
import straightway.odo.content.*
import straightway.testing.assertPanics
import straightway.testing.bdd.Given
import straightway.testing.bdd.panics
import straightway.testing.flow.*
import java.io.File
import java.net.URI
import java.nio.file.Files
import java.nio.file.Path

class ContentUploaderImplTest {

    private companion object {
        val CONTENT = byteArrayOf(2, 3, 5, 7)
        val CONTENT2 = byteArrayOf(3, 5, 7, 11)
    }

    private fun test(block: ContentUploader.DirBuilder.() -> Unit) =
        Given {
            object : AutoCloseable {
                val testDir =
                    Files.createTempDirectory("ContentUploaderTest").toFile()
                var nextUriId = 1
                val uploaded = mutableMapOf<URI, ByteArray>()
                val cloud: CloudWrite = mock {
                    onBlocking { store(any(), any()) }.thenAnswer {
                        val uri = URI("ipns://$nextUriId")
                        ++nextUriId
                        uploaded[uri] = it.getArgument(0)
                        uri
                    }
                }
                val sut = ContentUploaderImpl(cloud, testDir, block)

                fun ContentBundleDescriptor.isReferencing(
                    fileName: String,
                    content: ByteArray
                ) = item(uploaded.getByContent(content).single().key)
                    .uri.fileName == fileName

                override fun close() {
                    testDir.deleteRecursively()
                }
            }
        }

    @Test
    fun `upload non-existing file panics`() =
        assertPanics {
            test {
                file("notExisting")
            }
        }

    @Test
    fun `upload non-existing dir panics`() =
        assertPanics {
            test {
                dir("notExisting")
            }
        }

    @Test
    fun `upload dir as file panics`() =
        assertPanics {
            test {
                createDir("dir") {}
                file("dir")
            }
        }

    @Test
    fun `upload file as dir panics`() =
        test {} while_ {
            File(testDir, "file").writeText("content")
        } when_ {
            ContentUploaderImpl(cloud, testDir.listFiles()!!.single()) { dir("file") }
        } then panics

    @Test
    fun `upload single file in content bundle`() =
        test {
            createFile("testFile", CONTENT)
            file("testFile")
        } whenBlocking {
            sut.upload()
        } then {
            assertThat(it.fileName is_ equal to testDir.name)
            with(uploaded.getContentBundle(it)) {
                assertThat(isReferencing("testFile", CONTENT) is_ true)
            }
        }

    @Test
    fun `setting description in content bundle`() =
        test {
            createFile("testFile", CONTENT)
            file("testFile")
            description = "the description"
            assertThat(description is_ equal to "the description")
        } whenBlocking {
            sut.upload()
        } then {
            with(uploaded.getContentBundle(it)) {
                assertThat(description is_ equal to "the description")
            }
        }

    @Test
    fun `upload multiple files selects the right ones`() =
        test {
            (0 until 10).forEach {
                createFile("$it", CONTENT + it.toByte())
                file("$it")
            }
        } whenBlocking {
            sut.upload()
        } then {
            with(uploaded.getContentBundle(it)) {
                (0 until 10).forEach { i ->
                    assertThat(
                        isReferencing("$i", CONTENT + i.toByte())
                            is_ true
                    )
                }
            }
        }

    @Test
    fun `update file upload`() =
        test {
            createFile("testFile", CONTENT)
            file("testFile")
            file("testFile") { tags += "updated" }
        } whenBlocking {
            sut.upload()
        } then {
            assertThat(it.fileName is_ equal to testDir.name)
            with(uploaded.getContentBundle(it)) {
                assertThat(items.single().tags is_ equal to setOf("updated"))
            }
        }

    @Test
    fun `set file tags`() =
        test {
            createFile("testFile", CONTENT)
            file("testFile") { tags += "tag" }
        } whenBlocking {
            sut.upload()
        } then {
            with(uploaded.getContentBundle(it)) {
                assertThat(items.single().tags is_ equal to setOf("tag"))
            }
        }

    @Test
    fun `set file mimeType`() =
        test {
            createFile("testFile", CONTENT)
            file("testFile") {
                mimeType = "MimeType"
                assertThat(mimeType is_ equal to "MimeType")
            }
        } whenBlocking {
            sut.upload()
        } then {
            with(uploaded.getContentBundle(it)) {
                assertThat(items.single().mimeType is_ equal to "MimeType")
            }
        }

    @Test
    fun `set file encoding`() =
        test {
            createFile("testFile", CONTENT)
            file("testFile") {
                encoding = "Encoding"
                assertThat(encoding is_ equal to "Encoding")
            }
        } whenBlocking {
            sut.upload()
        } then {
            with(uploaded.getContentBundle(it)) {
                assertThat(items.single().encoding is_ equal to "Encoding")
            }
        }

    @Test
    fun `upload single file link in content bundle`() {
        val item = ContentItemDescriptor(URI("uri").withFileName("name"))
        test {
            link(item)
        } whenBlocking {
            sut.upload()
        } then {
            assertThat(it.fileName is_ equal to testDir.name)
            with(uploaded.getContentBundle(it)) {
                assertThat(items.single() is_ equal to item)
            }
        }
    }

    @Test
    fun `update single file link`() {
        val item = ContentItemDescriptor(URI("uri").withFileName("name"))
        test {
            link(item.with("Tags", setOf("old")))
            link(item)
        } whenBlocking {
            sut.upload()
        } then {
            assertThat(it.fileName is_ equal to testDir.name)
            with(uploaded.getContentBundle(it)) {
                assertThat(items.single() is_ equal to item)
            }
        }
    }

    @Test
    fun `upload single dir link in content bundle`() {
        val dir = ContentBundleDescriptor(listOf())
        test {
            link("subDir", dir)
        } whenBlocking {
            sut.upload()
        } then {
            assertThat(it.fileName is_ equal to testDir.name)
            with(uploaded.getContentBundle(it)) {
                with(items.single()) {
                    assertThat(uri.fileName is_ equal to "subDir")
                    assertThat(uploaded.getContentBundle(uri) is_ equal to dir)
                    assertThat(uri.isContentBundle is_ true)
                }
            }
        }
    }

    @Test
    fun `update single dir link`() {
        val dir = ContentBundleDescriptor(listOf())
        test {
            link(
                "subDir",
                dir.copy(
                    items = listOf(
                        ContentItemDescriptor(URI("itemUri").withFileName("item"))
                    )
                )
            )
            link("subDir", dir)
        } whenBlocking {
            sut.upload()
        } then {
            assertThat(it.fileName is_ equal to testDir.name)
            with(uploaded.getContentBundle(it)) {
                with(items.single()) {
                    assertThat(uri.fileName is_ equal to "subDir")
                    assertThat(uploaded.getContentBundle(uri) is_ equal to dir)
                }
            }
        }
    }

    @Test
    fun `upload single file in sub content bundle`() =
        test {
            createDir("subDir") {
                dir("subDir") {
                    createFile("testFile", CONTENT)
                    file("testFile")
                }
            }
        } whenBlocking {
            sut.upload()
        } then {
            assertThat(it.isContentBundle is_ true)
            assertThat(it.fileName is_ equal to testDir.name)
            with(uploaded.getContentBundle(it)) {
                with(items.single()) {
                    assertThat(uri.fileName is_ equal to "subDir")
                    assertThat(uri.isContentBundle is_ true)
                    with(uploaded.getContentBundle(uri)) {
                        assertThat(uri.fileName is_ equal to "subDir")
                        assertThat(isReferencing("testFile", CONTENT) is_ true)
                    }
                }
            }
        }

    @Test
    fun `update dir upload`() =
        test {
            createDir("subDir") {
                dir("subDir")
                dir("subDir") {
                    createFile("testFile", CONTENT)
                    file("testFile")
                }
            }
        } whenBlocking {
            sut.upload()
        } then {
            assertThat(it.fileName is_ equal to testDir.name)
            with(uploaded.getContentBundle(it)) {
                with(items.single()) {
                    assertThat(uri.fileName is_ equal to "subDir")
                    with(uploaded.getContentBundle(uri)) {
                        assertThat(uri.fileName is_ equal to "subDir")
                        assertThat(isReferencing("testFile", CONTENT) is_ true)
                    }
                }
            }
        }

    @Test
    fun `update with different type panics`() =
        assertPanics {
            test {
                val newDir = createDir("name") {}
                dir("name")
                newDir.deleteRecursively()
                createFile("name", CONTENT)
                file("name")
            }
        }

    @Test
    fun `recursively upload directory without sub directories`() =
        test {
            createDir("subDir") {
                File(this, "testFile").writeBytes(CONTENT)
            }

            tree("subDir")
        } whenBlocking {
            sut.upload()
        } then {
            assertThat(it.fileName is_ equal to testDir.name)
            with(uploaded.getContentBundle(it)) {
                with(items.single()) {
                    assertThat(uri.fileName is_ equal to "subDir")
                    with(uploaded.getContentBundle(uri)) {
                        assertThat(uri.fileName is_ equal to "subDir")
                        assertThat(isReferencing("testFile", CONTENT) is_ true)
                    }
                }
            }
        }

    @Test
    fun `recursively upload directory which is in reality a file panics`() =
        assertPanics {
            test {
                createFile("subDir", CONTENT)
                tree("subDir")
            }
        }

    @Test
    fun `recursively upload directory with sub directories`() =
        test {
            createDir("subDir") {
                val subSubDir = File(this, "subSubDir")
                subSubDir.mkdirs()
                File(subSubDir, "testFile").writeBytes(CONTENT)
            }

            tree("subDir")
        } whenBlocking {
            sut.upload()
        } then {
            assertThat(it.fileName is_ equal to testDir.name)
            with(uploaded.getContentBundle(it)) {
                with(items.single()) {
                    assertThat(uri.fileName is_ equal to "subDir")
                    with(uploaded.getContentBundle(uri)) {
                        assertThat(uri.fileName is_ equal to "subDir")
                        with(items.single()) {
                            assertThat(uri.fileName is_ equal to "subSubDir")
                            with(uploaded.getContentBundle(uri)) {
                                assertThat(uri.fileName is_ equal to "subSubDir")
                                assertThat(isReferencing("testFile", CONTENT) is_ true)
                            }
                        }
                    }
                }
            }
        }

    @Test
    fun `recursively upload complete root directory`() =
        test {
            createDir("subDir") {
                val subSubDir = File(this, "subSubDir")
                subSubDir.mkdirs()
                File(subSubDir, "testFile").writeBytes(CONTENT)
            }
            createFile("file", CONTENT2)

            tree()
        } whenBlocking {
            sut.upload()
        } then {
            assertThat(it.fileName is_ equal to testDir.name)
            with(uploaded.getContentBundle(it)) {
                with(items.single { f -> f.uri.fileName == "subDir" }) {
                    with(uploaded.getContentBundle(uri)) {
                        assertThat(uri.fileName is_ equal to "subDir")
                        with(items.single()) {
                            assertThat(uri.fileName is_ equal to "subSubDir")
                            with(uploaded.getContentBundle(uri)) {
                                assertThat(uri.fileName is_ equal to "subSubDir")
                                assertThat(isReferencing("testFile", CONTENT) is_ true)
                            }
                        }
                    }
                }

                assertThat(items.count { f -> f.uri.fileName == "file" } is_ equal to 1)
                assertThat(isReferencing("file", CONTENT2) is_ true)

                assertThat(items has size of 2)
            }
        }

    // region Private

    private fun ContentUploader.DirBuilder.createDir(
        name: String,
        block: File.() -> Unit
    ) =
        Path.of(dir.path, name).toFile().also {
            it.mkdirs()
            it.block()
        }

    private fun ContentUploader.DirBuilder.createFile(name: String, content: ByteArray) =
        Path.of(dir.path, name).toFile().writeBytes(content)

    private fun Map<URI, ByteArray>.getContentBundle(
        uri: URI
    ) = Json.decodeFromString(
        ContentBundleDescriptor.serializer(),
        getContentAsString(uri)
    )

    private fun Map<URI, ByteArray>.getContentAsString(
        uri: URI
    ): String = this[uri.withoutQueryArgs]!!.toString(Charsets.UTF_8)

    private val URI.withoutQueryArgs get() =
        URI(scheme, userInfo, host, port, path, null, fragment)

    private fun Map<URI, ByteArray>.getByContent(content: ByteArray) =
        entries.filter { it.value.contentEquals(content) }

    private fun ContentBundleDescriptor.item(uri: URI) =
        items.single { it.uri.withoutFileName == uri.withoutFileName }

    private val URI.withoutFileName get() = withFileName("removed")

    // endregion
}
