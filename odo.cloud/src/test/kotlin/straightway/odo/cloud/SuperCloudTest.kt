/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.cloud

import com.nhaarman.mockitokotlin2.mock
import org.junit.jupiter.api.Test
import straightway.odo.cloud.impl.SuperCloudImpl
import straightway.testing.bdd.Given

class SuperCloudTest {

    @Test
    fun `Factory creates SuperloudImpl`() =
        assert(SuperCloud.Factory(listOf(mock())) is SuperCloudImpl.Factory)

    @Test
    fun `Factory passes sub cloudd factories to CryptoCloudImpl`() =
        Given {
            object {
                val subCloudFactory = mock<Cloud.Factory>()
            }
        } when_ {
            SuperCloud.Factory(listOf(subCloudFactory))
        } then {
            assert(
                it is SuperCloudImpl.Factory &&
                    it.subCloudFactories.single() === subCloudFactory
            )
        }
}
