/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.cloud

import com.nhaarman.mockitokotlin2.mock
import org.junit.jupiter.api.Test
import straightway.odo.cloud.impl.CryptoCloudImpl
import straightway.testing.bdd.Given

class CryptoCloudTest {

    @Test
    fun `Factory creates CryptoCloudImpl`() =
        assert(CryptoCloud.Factory(mock()) is CryptoCloudImpl.Factory)

    @Test
    fun `Factory passes super cloud factory to CryptoCloudImpl`() =
        Given {
            object {
                val superCloudFactory = mock<SuperCloud.Factory>()
            }
        } when_ {
            CryptoCloud.Factory(superCloudFactory)
        } then {
            assert(
                it is CryptoCloudImpl.Factory &&
                    it.superCloudFactory === superCloudFactory
            )
        }
}
