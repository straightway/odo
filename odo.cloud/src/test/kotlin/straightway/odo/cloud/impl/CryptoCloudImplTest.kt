/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.cloud.impl

import com.nhaarman.mockitokotlin2.*
import org.junit.jupiter.api.Test
import straightway.crypto.Cryptor
import straightway.odo.cloud.*
import straightway.testing.bdd.Given
import straightway.testing.flow.*
import java.net.URI

class CryptoCloudImplTest {

    private val test get() =
        Given {
            object {
                val uriScheme = "uriScheme"
                val encrypted = byteArrayOf(2, 3, 5)
                val decrypted = byteArrayOf(7, 11, 13)
                val parentDirUri = URI("http://parent/dir/ID")
                val contentUri = URI("a:b")
                val root = URI("rootId")
                val listDir = listOf(contentUri)
                val cloud: Cloud = mock {
                    onBlocking { retrieve(any(), any()) }.thenReturn(encrypted)
                    onBlocking { store(any(), any()) }.thenReturn(contentUri)
                    onBlocking { createRoot() }.thenReturn(root)
                }
                var superCloud: SuperCloud = mock {
                    on { readVia(any()) }.thenReturn(cloud)
                    on { writeVia(any()) }.thenReturn(cloud)
                }
                val cryptor: Cryptor = mock {
                    on { decrypt(encrypted) }.thenReturn(decrypted)
                    on { encrypt(decrypted) }.thenReturn(encrypted)
                }
                val progressCallback: (TransferProgress) -> Unit = {}
                var sut = CryptoCloudImpl(
                    mapOf(),
                    mock {
                        on { createSuperCloud(any()) }.thenReturn(superCloud)
                    }
                )

                fun verifyRead(block: suspend CloudRead.() -> Unit) {
                    verify(superCloud, times(1)).readVia(uriScheme)
                    verifyBlocking(cloud, times(1), block)
                }

                fun verifyWrite(block: suspend CloudWrite.() -> Unit) {
                    verify(superCloud, times(1)).writeVia(uriScheme)
                    verifyBlocking(cloud, times(1), block)
                }
            }
        }

    @Test
    fun `decryptingWith returns wrapped SuperCloudRead decrypting the retrieve results`() =
        test whenBlocking {
            sut
                .decryptingWith(cryptor)
                .readVia(uriScheme)
                .retrieve(contentUri, progressCallback)
        } then {
            assertThat(it is_ equal to_ decrypted)
            verify(superCloud, times(1)).readVia(uriScheme)
            verifyBlocking(cloud, times(1)) {
                retrieve(contentUri, progressCallback)
            }
        }

    @Test
    fun `encryptWith calls store with encrypted data`() =
        test whenBlocking {
            sut
                .encryptingWith(cryptor)
                .writeVia(uriScheme)
                .store(decrypted)
        } then {
            verifyWrite { store(encrypted) }
        }

    @Test
    fun `encryptWith calls store and returns result URI`() =
        test whenBlocking {
            sut
                .encryptingWith(cryptor)
                .writeVia(uriScheme)
                .store(decrypted)
        } then {
            assertThat(it is_ equal to contentUri)
        }

    @Test
    fun `encryptWith calls createRoot for base cloud`() =
        test whenBlocking {
            sut
                .encryptingWith(cryptor)
                .writeVia(uriScheme)
                .createRoot()
        } then {
            verifyWrite { createRoot() }
            assertThat(it is_ equal to root)
        }

    @Test
    fun `encryptWith calls removeRoot for base cloud`() =
        test whenBlocking {
            sut
                .encryptingWith(cryptor)
                .writeVia(uriScheme)
                .removeRoot(root)
        } then {
            verifyWrite { removeRoot(root) }
        }

    @Test
    fun `encryptWith calls publish for base cloud`() =
        test whenBlocking {
            sut
                .encryptingWith(cryptor)
                .writeVia(uriScheme)
                .publish(root, contentUri)
        } then {
            verifyWrite { publish(root, contentUri) }
        }

    @Test
    fun `close closes SuperCloud`() =
        test whileBlocking {
            superCloud = mock(extraInterfaces = arrayOf(AutoCloseable::class))
            sut = CryptoCloudImpl(
                mapOf(),
                mock {
                    on { createSuperCloud(any()) }.thenReturn(superCloud)
                }
            )
        } when_ {
            sut.close()
        } then {
            verify(superCloud as AutoCloseable, times(1)).close()
        }

    @Test
    fun `factory creates crypto cloud`() =
        Given {
            object {
                val superCloudFactory: SuperCloud.Factory = mock()
                val settings = mapOf<String, Any>("Key" to 83)
                val sut = CryptoCloudImpl.Factory(superCloudFactory)
            }
        } when_ {
            sut.createCryptoCloud(settings)
        } then {
            verify(superCloudFactory, times(1)).createSuperCloud(settings)
        }

    @Test
    fun `factory returns supported uri schemes from super cloud`() =
        Given {
            object {
                val superCloudFactory: SuperCloud.Factory = mock {
                    on { supportedUriSchemes }.thenReturn(setOf("a", "b", "c"))
                }
                val sut = CryptoCloudImpl.Factory(superCloudFactory)
            }
        } when_ {
            sut.supportedUriSchemes
        } then {
            assertThat(it is_ equal to_ setOf("a", "b", "c"))
        }
}
