/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.cloud.impl

import com.nhaarman.mockitokotlin2.*
import org.junit.jupiter.api.Test
import straightway.error.Panic
import straightway.odo.cloud.Cloud
import straightway.testing.bdd.Given
import straightway.testing.bdd.panics
import straightway.testing.flow.*

class SuperCloudImplTest {

    private val test get() =
        Given {
            object {
                val subClouds: List<Cloud> = listOf(
                    mock(),
                    mock(extraInterfaces = arrayOf(AutoCloseable::class))
                )
                val subCloudFactories: List<Cloud.Factory> = listOf(
                    mock {
                        on { supportedUriSchemes }.thenReturn(setOf("a", "b"))
                        on { createCloud(any()) }.thenReturn(subClouds[0])
                    },
                    mock {
                        on { supportedUriSchemes }.thenReturn(setOf("c", "d"))
                        on { createCloud(any()) }.thenReturn(subClouds[1])
                    }
                )
                val settings = mapOf("key" to "value")
                val sut = SuperCloudImpl(settings, subCloudFactories)
            }
        }

    @Test
    fun `construction with overlapping sub factories throws`() =
        assertThat(
            {
                SuperCloudImpl(
                    mapOf(),
                    listOf(
                        mock { on { supportedUriSchemes }.thenReturn(setOf("a", "b")) },
                        mock { on { supportedUriSchemes }.thenReturn(setOf("a", "c")) }
                    )
                )
            }
                does throw_.type<Panic>()
        )

    @Test
    fun `construction creates sub clouds and passes settings to their construction`() =
        test when_ {} then {
            subCloudFactories.forEach {
                verifyBlocking(it, times(1)) {
                    createCloud(settings)
                }
            }
        }

    @Test
    fun `readVia returns existing sub cloud by URI scheme`() =
        test when_ {
            sut.readVia("a")
        } then {
            assertThat(it is_ equal to_ subClouds[0])
        }

    @Test
    fun `readVia throws if there is no sub cloud for the given URI scheme`() =
        test when_ {
            sut.readVia("not existing URI scheme")
        } then panics

    @Test
    fun `writeVia returns existing sub cloud by URI scheme`() =
        test when_ {
            sut.writeVia("a")
        } then {
            assertThat(it is_ equal to_ subClouds[0])
        }

    @Test
    fun `writeVia throws if there is no sub cloud for the given URI scheme`() =
        test when_ {
            sut.writeVia("not existing URI scheme")
        } then panics

    @Test
    fun `close closes AutoCloseable clouds`() =
        test when_ {
            sut.close()
        } then {
            verify(subClouds[1] as AutoCloseable, times(1)).close()
        }

    @Test
    fun `factory creates SuperCloudImpl`() =
        test whenBlocking {
            val factory = SuperCloudImpl.Factory(subCloudFactories)
            factory.createSuperCloud(mapOf())
        } then {
            assert(it is SuperCloudImpl)
        }

    @Test
    fun `factory construction with overlapping sub factories throws`() =
        assertThat(
            {
                SuperCloudImpl.Factory(
                    listOf(
                        mock { on { supportedUriSchemes }.thenReturn(setOf("a", "b")) },
                        mock { on { supportedUriSchemes }.thenReturn(setOf("a", "c")) }
                    )
                )
            }
                does throw_.type<Panic>()
        )

    @Test
    fun `factory contains supported uri schemes from sub factories throws`() =
        Given {
            SuperCloudImpl.Factory(
                listOf(
                    mock { on { supportedUriSchemes }.thenReturn(setOf("a")) },
                    mock { on { supportedUriSchemes }.thenReturn(setOf("b")) }
                )
            )
        } when_ {
            supportedUriSchemes
        } then {
            assertThat(it is_ equal to_ setOf("a", "b"))
        }

    // region Private

    private fun createMockedCloudFactory(uriSchemes: List<String>, cloud: Cloud) =
        mock<Cloud.Factory> {
            on { uriSchemes }.thenReturn(uriSchemes)
            on { createCloud(any()) }.thenReturn(cloud)
        }

    // endregion
}
