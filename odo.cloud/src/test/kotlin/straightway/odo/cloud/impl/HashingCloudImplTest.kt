/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.cloud.impl

import com.nhaarman.mockitokotlin2.*
import org.junit.jupiter.api.Test
import straightway.crypto.Hasher
import straightway.odo.cloud.*
import straightway.odo.content.withHash
import straightway.testing.bdd.Given
import straightway.testing.bdd.panics
import straightway.testing.flow.*
import java.net.URI

class HashingCloudImplTest {

    private val test get() = Given {
        object {
            val uri = URI("http://a.b/path")
            val progressCallBack: (TransferProgress) -> Unit = {}
            val content = byteArrayOf(2, 3, 5, 7)
            val root = URI("rootId")
            val hash = byteArrayOf(0, 11, 13, 17, 19)
            val hasher: Hasher = mock {
                on { getHash(content) }.thenReturn(hash)
            }
            val baseCloud: Cloud = mock {
                onBlocking { retrieve(any(), any()) }.thenReturn(content)
                onBlocking { store(any(), any()) }.thenReturn(uri)
                onBlocking { createRoot() }.thenReturn(root)
            }
            val sut = HashingCloudImpl(baseCloud, hasher)
        }
    }

    @Test
    fun `retrieve forwards call to base cloud`() =
        test whenBlocking {
            sut.retrieve(uri, progressCallBack)
        } then {
            verifyBlocking(baseCloud, times(1)) {
                retrieve(uri, progressCallBack)
            }
        }

    @Test
    fun `retrieve with valid hash returns content`() =
        test whenBlocking {
            sut.retrieve(uri.withHash(hash), progressCallBack)
        } then {
            assertThat(it is_ equal to content)
        }

    @Test
    fun `retrieve with invalid hash panics`() =
        test whenBlocking {
            sut.retrieve(uri.withHash(byteArrayOf()), progressCallBack)
        } then panics

    @Test
    fun `store forwards call to base cloud`() =
        test whenBlocking {
            sut.store(content, progressCallBack)
        } then {
            verifyBlocking(baseCloud, times(1)) {
                store(content, progressCallBack)
            }
        }

    @Test
    fun `store returns uri with content hash`() =
        test whenBlocking {
            sut.store(content, progressCallBack)
        } then {
            assertThat(it is_ equal to uri.withHash(hash))
        }

    @Test
    fun `publish forwards call to base cloud`() =
        test whenBlocking {
            sut.publish(root, uri)
        } then {
            verifyBlocking(baseCloud, times(1)) {
                publish(root, uri)
            }
        }

    @Test
    fun `createRoot forwards call to base cloud`() =
        test whenBlocking {
            sut.createRoot()
        } then {
            verifyBlocking(baseCloud, times(1)) {
                createRoot()
            }

            assertThat(it is_ equal to root)
        }

    @Test
    fun `removeRoot forwards call to base cloud`() =
        test whenBlocking {
            sut.removeRoot(root)
        } then {
            verifyBlocking(baseCloud, times(1)) {
                removeRoot(root)
            }
        }
}
