/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.cloud

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import org.junit.jupiter.api.Test
import straightway.error.Panic
import straightway.testing.bdd.Given
import straightway.testing.flow.*
import java.net.URI
import java.nio.file.Files
import java.nio.file.Paths

class UploadDownloadIntegrationTest {

    private fun test(block: ContentUploader.DirBuilder.() -> Unit) =
        Given {
            object : AutoCloseable {
                val testDir =
                    Files.createTempDirectory("UploadDownloadIntegrationTest").toFile()
                val uploadDir = Paths.get(testDir.path, "upload").toFile()
                val downloadDir = Paths.get(testDir.path, "download").toFile()
                var nextUriId = 1
                val data = mutableMapOf<URI, ByteArray>()
                val cloud: Cloud = mock {
                    onBlocking { retrieve(any(), any()) }.thenAnswer {
                        val uri = it.getArgument<URI>(0).withoutQueryArgs
                        data[uri] ?: throw Panic("Data for $uri not found")
                    }
                    onBlocking { store(any(), any()) }.thenAnswer {
                        val uri = URI("ipns://$nextUriId")
                        ++nextUriId
                        data[uri] = it.getArgument(0)
                        uri
                    }
                }
                val uploader = ContentUploaderImpl(cloud, uploadDir) {
                    uploadDir.mkdirs()
                    downloadDir.mkdirs()
                    block()
                }
                val downloader = ContentDownloaderImpl(cloud)

                override fun close() {
                    testDir.deleteRecursively()
                }

                private val URI.withoutQueryArgs get() = URI(
                    scheme,
                    userInfo,
                    host,
                    port,
                    path,
                    null,
                    fragment
                )
            }
        }

    @Test
    fun `uploaded content can be downloaded`() =
        test {
            val file1 = Paths.get(dir.path, "file1").toFile()
            file1.writeBytes(byteArrayOf(2, 3, 5, 7))
            val subDir = Paths.get(dir.path, "subDir").toFile()
            subDir.mkdirs()
            val file2 = Paths.get(subDir.path, "file2").toFile()
            file2.writeBytes(byteArrayOf(11, 13, 17, 19))
            file("file1")
            tree("subDir")
        } whenBlocking {
            val uri = uploader.upload()
            downloader.download(downloadDir, uri)
        } then {
            val targetDir = downloadDir.listFiles()!!.single()
            assertThat(targetDir.name is_ equal to "upload")
            assertThat(
                targetDir.listFiles()!!.map { it.name } has
                    values("file1", "subDir")
            )
            assertThat(
                Paths.get(targetDir.path, "file1").toFile().readBytes()
                    is_ equal to byteArrayOf(2, 3, 5, 7)
            )
            assertThat(
                Paths.get(targetDir.path, "subDir", "file2").toFile().readBytes()
                    is_ equal to byteArrayOf(11, 13, 17, 19)
            )
        }
}
