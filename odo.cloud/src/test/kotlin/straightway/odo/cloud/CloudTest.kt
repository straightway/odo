/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.cloud

import com.nhaarman.mockitokotlin2.*
import org.junit.jupiter.api.Test
import straightway.crypto.Hasher
import straightway.odo.content.withHash
import straightway.testing.bdd.Given
import straightway.testing.flow.*
import java.net.URI

class CloudTest {

    private val test get() =
        Given {
            object {
                val uri = URI("http:/a.b/path")
                val content = byteArrayOf(2, 3, 5, 7)
                val hash = byteArrayOf(0, 11, 13, 17, 19)
                val root = URI("rootId")
                val transferProgressCb: (TransferProgress) -> Unit = {}
                val baseCloud: Cloud = mock {
                    onBlocking { store(any(), any()) }.thenReturn(uri)
                    onBlocking { createRoot() }.thenReturn(root)
                }
                val baseCloudFactory: Cloud.Factory = mock {
                    on { supportedUriSchemes }.thenReturn(setOf("a", "b", "c"))
                    on { createCloud(any()) }.thenReturn(baseCloud)
                }
                val hasher: Hasher = mock {
                    on { getHash(content) }.thenReturn(hash)
                }
            }
        }

    @Test
    fun `HashingFactory returns a factory supporting the same uri schemes as the base`() =
        test when_ {
            Cloud.HashingFactory(baseCloudFactory, hasher)
        } then {
            assertThat(
                it.supportedUriSchemes is_ equal
                    to baseCloudFactory.supportedUriSchemes
            )
        }

    @Test
    fun `HashingFactory returns a factory creating a hashing cloud wrapping store`() =
        test whenBlocking {
            val cloud = Cloud.HashingFactory(baseCloudFactory, hasher)
                .createCloud(mapOf("key" to "value"))
            cloud.store(content, transferProgressCb)
        } then {
            verifyBlocking(baseCloud, times(1)) {
                store(content, transferProgressCb)
            }

            assertThat(it is_ equal to uri.withHash(hash))
        }

    @Test
    fun `HashingFactory returns a factory creating a hashing cloud wrapping publish`() =
        test whenBlocking {
            val cloud = Cloud.HashingFactory(baseCloudFactory, hasher)
                .createCloud(mapOf("key" to "value"))
            cloud.publish(root, uri)
        } then {
            verifyBlocking(baseCloud, times(1)) {
                publish(root, uri)
            }
        }

    @Test
    fun `HashingFactory returns a factory creating a hashing cloud wrapping createRoot`() =
        test whenBlocking {
            val cloud = Cloud.HashingFactory(baseCloudFactory, hasher)
                .createCloud(mapOf("key" to "value"))
            cloud.createRoot()
        } then {
            verifyBlocking(baseCloud, times(1)) {
                createRoot()
            }
            assertThat(it is_ equal to root)
        }

    @Test
    fun `HashingFactory returns a factory creating a hashing cloud wrapping removeRoot`() =
        test whenBlocking {
            val cloud = Cloud.HashingFactory(baseCloudFactory, hasher)
                .createCloud(mapOf("key" to "value"))
            cloud.removeRoot(root)
        } then {
            verifyBlocking(baseCloud, times(1)) {
                removeRoot(root)
            }
        }
}
