/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.cloud

import com.nhaarman.mockitokotlin2.*
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import org.junit.jupiter.api.Test
import straightway.error.Panic
import straightway.odo.content.*
import straightway.testing.bdd.Given
import straightway.testing.bdd.panics
import straightway.testing.flow.*
import java.net.URI
import java.nio.file.Files
import java.nio.file.Paths

class ContentDownloaderImplTest {

    private companion object {
        val TARGET_URI = URI("targetUri")
        const val TARGET_FILE_NAME = "target"
    }

    private fun test(block: () -> Map<URI, ByteArray>) =
        Given {
            object : AutoCloseable {
                val testDir =
                    Files.createTempDirectory("ContentDownloaderTest").toFile()
                val targetPath = Paths.get(testDir.path, TARGET_FILE_NAME)
                val data = block()
                val cloud: CloudRead = mock {
                    onBlocking { retrieve(any(), any()) }.thenAnswer {
                        val uri = it.getArgument<URI>(0).withoutQueryArgs
                        data[uri] ?: throw Panic("Data for $uri not found")
                    }
                }
                val sut = ContentDownloaderImpl(cloud)

                override fun close() {
                    testDir.deleteRecursively()
                }

                private val targetUri get() =
                    if (data[TARGET_URI]?.isDirectory == true)
                        TARGET_URI.withContentBundleFlag
                    else TARGET_URI

                private val ByteArray.isDirectory get() =
                    try {
                        Json.decodeFromString(
                            ContentBundleDescriptor.serializer(),
                            toString(Charsets.UTF_8)
                        )
                        true
                    } catch (x: Exception) {
                        false
                    }

                private val URI.withoutQueryArgs get() = URI(
                    scheme,
                    userInfo,
                    host,
                    port,
                    path,
                    null,
                    fragment
                )
            }
        }

    @Test
    fun `file uri retrieves file`() =
        test {
            mapOf(
                TARGET_URI to byteArrayOf(2, 3, 5, 7)
            )
        } whenBlocking {
            sut.download(testDir, TARGET_URI.withFileName(TARGET_FILE_NAME))
        } then {
            assertThat(targetPath.toFile().exists() is_ true)
            assertThat(targetPath.toFile().isDirectory is_ false)
        }

    @Test
    fun `content bundle uri retrieves empty directory`() =
        test {
            mapOf(
                TARGET_URI to Json.encodeToString(
                    ContentBundleDescriptor(listOf())
                ).toByteArray(Charsets.UTF_8)
            )
        } whenBlocking {
            sut.download(
                testDir,
                TARGET_URI
                    .withFileName(TARGET_FILE_NAME)
                    .withContentBundleFlag
            )
        } then {
            assertThat(targetPath.toFile().exists() is_ true)
            assertThat(targetPath.toFile().isDirectory is_ true)
        }

    @Test
    fun `content bundle uri retrieves directory with content`() =
        test {
            val itemUri = URI("item")
            mapOf(
                itemUri to byteArrayOf(2, 3, 5, 7),
                TARGET_URI to Json.encodeToString(
                    ContentBundleDescriptor(
                        listOf(ContentItemDescriptor(itemUri.withFileName("item.bin")))
                    )
                ).toByteArray(Charsets.UTF_8)
            )
        } whenBlocking {
            sut.download(
                testDir,
                TARGET_URI
                    .withFileName(TARGET_FILE_NAME)
                    .withContentBundleFlag
            )
        } then {
            val dir = targetPath.toFile()
            assertThat(dir.exists() is_ true)
            assertThat(dir.isDirectory is_ true)
            val file = dir.listFiles()!!.single()
            assertThat(file.name is_ equal to "item.bin")
            assertThat(file.readBytes() is_ equal to byteArrayOf(2, 3, 5, 7))
        }

    @Test
    fun `content bundle uri retrieves directory with content in subfolder`() =
        test {
            val itemUri = URI("itemUri")
            val subFolderUri = URI("subFolderUri")
            val subFolder = ContentBundleDescriptor(
                listOf(ContentItemDescriptor(itemUri.withFileName("item.bin")))
            )
            val mainFolder = ContentBundleDescriptor(
                listOf(
                    ContentItemDescriptor(
                        subFolderUri
                            .withFileName("subFolder")
                            .withContentBundleFlag
                    )
                )
            )
            mapOf(
                itemUri to byteArrayOf(2, 3, 5, 7),
                subFolderUri to Json.encodeToString(subFolder).toByteArray(Charsets.UTF_8),
                TARGET_URI to Json.encodeToString(mainFolder).toByteArray(Charsets.UTF_8)
            )
        } whenBlocking {
            sut.download(
                testDir,
                TARGET_URI
                    .withFileName(TARGET_FILE_NAME)
                    .withContentBundleFlag
            )
        } then {
            val dir = targetPath.toFile()
            assertThat(dir.exists() is_ true)
            assertThat(dir.isDirectory is_ true)
            val subFolder = dir.listFiles()!!.single()
            assertThat(subFolder.name is_ equal to "subFolder")
            assertThat(subFolder.isDirectory is_ true)
            val file = subFolder.listFiles()!!.single()
            assertThat(file.name is_ equal to "item.bin")
            assertThat(file.readBytes() is_ equal to byteArrayOf(2, 3, 5, 7))
        }

    @Test
    fun `circular link panics`() {
        val targetUri = TARGET_URI
            .withFileName(TARGET_FILE_NAME)
            .withContentBundleFlag
        val subFolderUri = TARGET_URI
            .withFileName("subFolder")
            .withContentBundleFlag
        test {
            val mainFolder = ContentBundleDescriptor(
                listOf(ContentItemDescriptor(subFolderUri))
            )
            mapOf(
                TARGET_URI to Json.encodeToString(mainFolder).toByteArray(Charsets.UTF_8)
            )
        } whenBlocking {
            sut.download(testDir, targetUri)
        } then panics {
            assertThat(it.state is_ equal to "Circular dependency with $subFolderUri")
            val dir = targetPath.toFile()
            assertThat(dir.exists() is_ true)
            assertThat(dir.isDirectory is_ true)
            assertThat(dir.listFiles()!!.single().listFiles() is_ empty)
        }
    }
}
