/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.odo.cloud

import com.nhaarman.mockitokotlin2.*
import org.junit.jupiter.api.Test
import straightway.testing.bdd.Given
import straightway.units.byte
import straightway.units.get
import java.net.URI

class CloudReadTest {

    @Test
    fun `retrieve without progress callback transfers nothing`() =
        Given {
            object {
                val contentUri = URI("http://a.b")
                val sut = mock<CloudRead> {
                    onBlocking { retrieve(any(), any()) }.thenAnswer { args ->
                        val progressCallback =
                            args.getArgument<(TransferProgress) -> Unit>(1)
                        progressCallback(TransferProgress(0[byte], 100[byte]))
                        byteArrayOf()
                    }
                }
            }
        } whenBlocking {
            sut.retrieve(contentUri)
        } then {
            verifyBlocking(sut, times(1)) {
                retrieve(eq(contentUri), any())
            }
        }
}
