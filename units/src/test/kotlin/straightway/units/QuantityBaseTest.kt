/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.units

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import straightway.expr.minus
import straightway.testing.flow.*

class QuantityBaseTest {

    @Test
    fun scale() = assertThat(TestQuantity(kilo).scale is_ equal to_ kilo)

    @Test
    fun baseMagnitude() = assertThat(TestQuantity(kilo).baseMagnitude is_ equal to_ 1)

    @Test
    fun baseQuantity() = assertThat(TestQuantity(kilo).baseQuantity is_ equal to_ TestQuantity(uni))

    @Test
    fun withScale() = assertThat((TestQuantity(kilo) withScale deca).scale is_ equal to_ deca)

    @Test
    fun `equals for equal object`() =
        assertThat(TestQuantity(kilo).equals(TestQuantity(kilo)) is_ true)

    @Test
    fun `differs for null`() =
        assertThat(TestQuantity(kilo).equals(null) is_ false)

    @Test
    fun `differs for completely different classes`() =
        assertThat(TestQuantity(kilo).equals("Lalala") is_ false)

    @Test
    fun `differs for different quantity classes`() =
        assertThat(TestQuantity(kilo).equals(meter) is_ false)

    @Test
    fun `differs for objects differing in scale`() =
        assertThat(TestQuantity(kilo).equals(TestQuantity(deca)) is_ false)

    @Test
    fun `hashCode for equal objects is equal`() =
        assertThat(TestQuantity(kilo).hashCode() is_ equal to_ TestQuantity(kilo).hashCode())

    @Test
    fun `hashCode differs for objects differing by id`() =
        assertThat(TestQuantity(kilo).hashCode() is_ not - equal to_ OtherTestQuantity(kilo).hashCode())

    @Test
    fun `hashCode differs for objects differing by scale`() =
        assertThat(TestQuantity(kilo).hashCode() is_ not - equal to_ TestQuantity(deca).hashCode())

    @Test
    fun toStringResult() = assertEquals("kTU", (testUnit withScale kilo).toString())

    private class TestQuantity(scale: UnitScale) : QuantityBase("TU", scale, { TestQuantity(it) })
    private class OtherTestQuantity(scale: UnitScale) : QuantityBase("TU", scale, { OtherTestQuantity(it) })

    private val testUnit = TestQuantity(uni)
}
