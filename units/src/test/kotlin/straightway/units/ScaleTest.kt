/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
import org.junit.jupiter.api.Test
import straightway.testing.flow.*
import straightway.units.*

class ScaleTest {

    @Test
    fun `atto magnitude`() = assertThat(atto.magnitude is_ equal to_ 1e-18)

    @Test
    fun `femto magnitude`() = assertThat(femto.magnitude is_ equal to_ 1e-15)

    @Test
    fun `pico magnitude`() = assertThat(pico.magnitude is_ equal to_ 1e-12)

    @Test
    fun `nano magnitude`() = assertThat(nano.magnitude is_ equal to_ 1e-9)

    @Test
    fun `micro magnitude`() = assertThat(micro.magnitude is_ equal to_ 1e-6)

    @Test
    fun `micro prefix`() = assertThat(micro.prefix is_ equal to_ "µ")

    @Test
    fun `milli magnitude`() = assertThat(milli.magnitude is_ equal to_ 1e-3)

    @Test
    fun `centi magnitude`() = assertThat(centi.magnitude is_ equal to_ 1e-2)

    @Test
    fun `deci magnitude`() = assertThat(deci.magnitude is_ equal to_ 1e-1)

    @Test
    fun `uni magnitude`() = assertThat(uni.magnitude is_ equal to_ 1)

    @Test
    fun `uni prefix`() = assertThat(uni.prefix is_ equal to_ "")

    @Test
    fun `deca magnitude`() = assertThat(deca.magnitude is_ equal to_ 10)

    @Test
    fun `deca prefix`() = assertThat(deca.prefix is_ equal to_ "da")

    @Test
    fun `hecto magnitude`() = assertThat(hecto.magnitude is_ equal to_ 100)

    @Test
    fun `kilo magnitude`() = assertThat(kilo.magnitude is_ equal to_ 1_000)

    @Test
    fun `mega magnitude`() = assertThat(mega.magnitude is_ equal to_ 1_000_000)

    @Test
    fun `giga magnitude`() = assertThat(giga.magnitude is_ equal to_ 1_000_000_000)

    @Test
    fun `tera magnitude`() = assertThat(tera.magnitude is_ equal to_ 1_000_000_000_000)

    @Test
    fun `peta magnitude`() = assertThat(peta.magnitude is_ equal to_ 1_000_000_000_000_000)

    @Test
    fun `exa magnitude`() = assertThat(exa.magnitude is_ equal to_ 1_000_000_000_000_000_000)

    @Test
    fun `ki magnitude`() = assertThat(ki.magnitude is_ equal to_ 1_024)

    @Test
    fun `mi magnitude`() = assertThat(mi.magnitude is_ equal to_ 1_048_576)

    @Test
    fun `gi magnitude`() = assertThat(gi.magnitude is_ equal to_ 1_073_741_824)

    @Test
    fun `ti magnitude`() = assertThat(ti.magnitude is_ equal to_ 1_099_511_627_776)

    @Test
    fun `pi magnitude`() = assertThat(pi.magnitude is_ equal to_ 1_125_899_906_842_624)

    @Test
    fun `ei magnitude`() = assertThat(ei.magnitude is_ equal to_ 1_152_921_504_606_846_976)
}
