/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.units

import straightway.compiler.Generated
import straightway.error.Panic

/**
 * Combine a double value with a unit.
 */
data class UnitDouble<TQuantity : Quantity>(
    override val value: Double,
    override val unit: TQuantity
) : UnitValue<TQuantity> {

    override val baseValue: Double by lazy {
        value * unit.siScale.magnitude.toDouble() * unit.baseMagnitude.toDouble() +
            unit.valueShift.toDouble()
    }

    override operator fun get(newUnit: TQuantity) = UnitDouble(
        (baseValue - newUnit.valueShift.toDouble()) *
            newUnit.siScale.reciprocal.magnitude.toDouble() /
            newUnit.baseMagnitude.toDouble(),
        newUnit
    )

    operator fun rangeTo(endInclusive: UnitDouble<TQuantity>) =
        Range(this, endInclusive)

    operator fun unaryMinus() = UnitDouble(-value, unit)

    operator fun unaryPlus() = this

    operator fun times(x: Double) = UnitDouble(value * x, unit)

    operator fun div(x: Double) = UnitDouble(value / x, unit)

    inline fun <reified TQuantity2 : Quantity> convertQuantity() =
        UnitDouble(value, unit as TQuantity2)

    @Generated("Kotlin compiler generates unreachable code")
    override fun toString() =
        "$value $unit".trimEnd()

    override fun equals(other: Any?) =
        other is UnitValue<*> &&
            other.unit.id == unit.id &&
            other.baseValue.toDouble().compareTo(baseValue) == 0

    override fun hashCode() =
        value.hashCode() xor unit.hashCode()

    override fun compareTo(other: UnitValue<TQuantity>) =
        baseValue.compareTo(other.baseValue.toDouble())

    companion object {
        const val serialVersionUID = 1L
    }

    data class Range<TQuantity : Quantity>(
        override val start: UnitDouble<TQuantity>,
        override val endInclusive: UnitDouble<TQuantity>
    ) : ClosedRange<UnitDouble<TQuantity>> {
        override fun toString() = "$start..$endInclusive"
    }
}

inline operator fun <reified TQuantity : Quantity> UnitDouble<TQuantity>.plus(
    other: UnitDouble<TQuantity>
) =
    (baseValue + other.baseValue)[other.unit.baseQuantity as TQuantity]

inline operator fun <reified TQuantity : Quantity> UnitDouble<TQuantity>.minus(
    other: UnitDouble<TQuantity>
) =
    (baseValue - other.baseValue)[other.unit.baseQuantity as TQuantity]

inline operator fun <reified TQuantity : Quantity, reified TOtherQuantity : Quantity>
UnitDouble<TQuantity>.times(other: UnitDouble<TOtherQuantity>) =
    (baseValue * other.baseValue)[
        unit.baseQuantity as TQuantity * other.unit.baseQuantity as TOtherQuantity
    ]

inline operator fun <reified TQuantity : Quantity, reified TOtherQuantity : Quantity>
UnitDouble<TQuantity>.div(other: UnitDouble<TOtherQuantity>) =
    (baseValue / other.baseValue)[
        unit.baseQuantity as TQuantity /
            other.unit.baseQuantity as TOtherQuantity
    ]

/**
 * Create a UnitDouble by combining a double with a given unit in square brackets.
 */
operator fun <TQuantity : Quantity> Double.get(unit: TQuantity): UnitDouble<TQuantity> =
    UnitDouble(this, unit)

/**
 * Convert the given unit double to a value with another compatible unit.
 * This can be useful to assign a simplified unit when using arithmetic operators
 * with unit doubles.
 */
inline operator fun <TQuantity1 : Quantity, reified TQuantity2 : Quantity>
UnitDouble<TQuantity1>.get(unit: TQuantity2): UnitDouble<TQuantity2> =
    if (this.unit.id != unit.id) throw Panic("Incompatible units: $unit for $this ")
    else (baseValue - unit.valueShift.toDouble())[
        unit.withScale(unit.siScaleCorrection)
    ][unit].convertQuantity()

operator fun <TQuantity : Quantity> Double.times(other: UnitDouble<TQuantity>) =
    other * this

inline operator fun <reified TQuantity : Quantity> Double.div(other: UnitDouble<TQuantity>) =
    UnitDouble(this / other.value, Reciprocal(other.unit))
