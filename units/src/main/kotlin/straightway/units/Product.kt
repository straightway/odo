/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.units

import straightway.compiler.Generated
import straightway.error.Panic
import straightway.numbers.times
import java.lang.Integer.max

/**
 * The product of two quantities.
 * Pleas notice that due to limitations of the language, this product is not
 * commutative. However, equivalent quantities are guaranteed to have the same id.
 */
class Product<QLeft : Quantity, QRight : Quantity>
internal constructor(
    internal val left: QLeft,
    internal val right: QRight,
    override val scale: UnitScale,
    internal val isAutoScale: Boolean,
    override val baseMagnitude: Number,
    val explicitSymbol: String? = null,
    override val siScaleCorrection: UnitScale = uni
) : Quantity {

    constructor(left: QLeft, right: QRight) : this(
        left,
        right,
        left.siScale * right.siScale,
        true,
        left.baseMagnitude * right.baseMagnitude
    )

    override val id: QuantityId
        get() = (listOf(idFactors.numerators(one.id)) + idFactors.denominators).joinToString("/")

    override infix fun withScale(scale: UnitScale) =
        Product(left, right, scale, false, baseMagnitude, explicitSymbol)

    override val baseQuantity: Quantity
        get() = left.baseQuantity * right.baseQuantity

    infix fun withSymbol(newSymbol: String) =
        Product(left, right, uni, false, baseMagnitude, newSymbol, siScale.reciprocal)

    infix fun withBaseMagnitude(baseMagnitude: Number): Product<QLeft, QRight> =
        Product(left, right, scale, isAutoScale, baseMagnitude, explicitSymbol)

    inline infix fun <reified Q : Product<*, *>> normalizedToTypeOf(target: Q): Q = when {
        id != target.id -> throw Panic("Unit $this cannot be normalized as $target")
        explicitSymbol != null -> target withSymbol explicitSymbol
        else -> target
    }.withScale(scale).withBaseMagnitude(baseMagnitude) as Q

    override fun equals(other: Any?) =
        other is Product<*, *> &&
            id == other.id &&
            scale == other.scale

    override fun hashCode() =
        left.hashCode() xor
            right.hashCode() xor
            scale.hashCode() xor
            baseMagnitude.hashCode() xor
            (explicitSymbol?.hashCode() ?: 0)

    override fun toString() = stringRepresentation

    // region private

    private val idFactors by lazy { getFactorRepresentation { id } }

    private val toStringFactors by lazy { getFactorRepresentation { toString() } }

    private val toStringBaseFactors by lazy {
        getFactorRepresentation { withScale(uni * UnitScale(baseMagnitude)).toString() }
    }

    private val toStringBase: String
        get() =
            (listOf(toStringBaseFactors.numerators("1")) + toStringBaseFactors.denominators)
                .joinToString("/")

    private val hasUniformRepresentation get() = toStringFactors.size == idFactors.size

    private val fractionString
        get() =
            (listOf(toStringFactors.numerators("1")) + toStringFactors.denominators)
                .joinToString("/")

    private val scaledString
        get() =
            if (explicitSymbol == null) "$scale($toStringBase)" else "$scale$explicitSymbol"

    private val unscaledString get() = explicitSymbol ?: toStringBase

    private val stringRepresentation by lazy {
        when {
            isAutoScale && hasUniformRepresentation -> fractionString
            scale == uni -> unscaledString
            else -> scaledString
        }
    }

    private fun getFactorRepresentation(getter: Quantity.() -> String): List<String> {
        return sortedFactors(getter)
            .groupBy { it }
            .cancelled()
            .map { it.key pow it.value }
            .sorted()
    }

    // endregion
}

operator fun <QLeft : Quantity, QRight : Quantity> QLeft.times(right: QRight) =
    Product(this, right)

operator fun <QLeft : Quantity, QRight : Quantity> QLeft.div(right: QRight) =
    Product(this, Reciprocal(right))

typealias Square<T> = Product<T, T>

fun <T : Quantity> square(q: T) = Square(q, q)

typealias Cubic<T> = Product<T, Square<T>>

fun <T : Quantity> cubic(q: T) = Cubic(q, square(q))

// region Private

private const val LINEAR = 1
private const val SQUARE = 2
private const val CUBIC = 3

private infix fun String.pow(exponent: Int) =
    when (exponent) {
        LINEAR -> this
        SQUARE -> "$this²"
        CUBIC -> "$this³"
        else -> "$this^$exponent"
    }

private fun List<String>.numerators(default: String) =
    this.filter { !it.isReciproke }.combineWithDefault(default)

private val List<String>.denominators: List<String>
    get() {
        val result = this.filter { it.isReciproke }.map { it.substring(2) }
        return if (result.isEmpty()) listOf() else listOf(result.joinToString("*"))
    }

private fun List<String>.combineWithDefault(default: String) =
    if (this.isEmpty()) default else this.joinToString("*")

private fun Quantity.sortedFactors(getter: Quantity.() -> String): List<String> = when (this) {
    is Product<*, *> -> left.sortedFactors(getter) + right.sortedFactors(getter)
    is Reciprocal<*> -> this.wrapped.sortedFactors(getter).map { it.reciproke }
    else -> listOf(getter())
}

private fun Map<String, List<String>>.cancelled() =
    map(this::cancelFactor).filter(::hasNotNullExponent).toMap()

private fun Map<String, List<String>>.cancelFactor(entry: Map.Entry<String, List<String>>) =
    Pair(entry.key, max(0, entry.exponent - entry.reciprokeExponent(this)))

private fun hasNotNullExponent(entry: Pair<String, Int>) = entry.second != 0
private fun Map.Entry<String, List<String>>.reciprokeExponent(factors: Map<String, List<String>>) =
    factors[reciproke]?.size ?: 0

private val Map.Entry<String, List<String>>.exponent get() = value.size
private val Map.Entry<String, List<String>>.reciproke get() = key.reciproke
private val String.reciproke
    @Generated("kotlin compiler generates strange branches")
    get() = if (this.isReciproke) substring(2) else "1/$this"
private val String.isReciproke
    get() = startsWith("1/")

// endregion
